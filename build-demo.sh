#!/bin/sh

OUTPUT_DIR="prima-regional-model-demo"

if [ -e ${OUTPUT_DIR} ]; then
  echo 'The directory '${OUTPUT_DIR}' still exist!'
  read -p 'Remove ? [yN] ' yn;
  case $yn in
    [yY]*) rm -Rf ${OUTPUT_DIR};;
    * ) echo "OK, exiting"; exit;;
  esac
fi
mkdir ${OUTPUT_DIR}

# building the doc
txt2tags -t xhtml -o ${OUTPUT_DIR}/README.html doc/manual.t2t && \
cp -r doc/img ${OUTPUT_DIR}/ && \
mvn package && {
  cp target/prima-regional-model-1.0-SNAPSHOT-jar-with-dependencies.jar ${OUTPUT_DIR}/
  chmod +x ${OUTPUT_DIR}/prima-regional-model-1.0-SNAPSHOT-jar-with-dependencies.jar
  mkdir ${OUTPUT_DIR}/Cantal
  cp -r dataIn/Cantal/15* ${OUTPUT_DIR}/Cantal/
  cp -r dataIn/Cantal/outside0 ${OUTPUT_DIR}/Cantal/
  cp dataIn/Cantal/ficEnt_demo.xml ${OUTPUT_DIR}/Cantal/
  for f in $(grep '.csv'  dataIn/Cantal/ficEnt_demo.xml | sed -e 's/^[^>]*>\([^<]*\)<.*$/\1/'); do
    cp dataIn/Cantal/$f ${OUTPUT_DIR}/Cantal/ 2> /dev/null
  done
  cp dataIn/Cantal/immigrants.txt ${OUTPUT_DIR}/Cantal/
  cp -r ../Cantal-SHP/ ${OUTPUT_DIR}/
  cp src/main/resources/run.bat ${OUTPUT_DIR}/
  echo "Building ZIP archive …"
  zip -qr ${OUTPUT_DIR}.zip ${OUTPUT_DIR}
}