/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.cemagref.prima.regionalmodel;

import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import java.util.Arrays;
import fr.cemagref.prima.regionalmodel.Exploration.ParameterValues;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class ExplorationTest {

    @Test
    public void testCompletePlan() throws BadDataException {
        List<ParameterValues> parametersValues = new ArrayList<ParameterValues>();
        parametersValues.add(new ParameterValues("a", new Integer[]{1,2}));
        parametersValues.add(new ParameterValues("b", new Integer[]{1,2}));
        List<List<Exploration.ParameterValue>> result = Exploration.completePlan(parametersValues);
        assertEquals(4, result.size());
        parametersValues = new ArrayList<ParameterValues>();
        parametersValues.add(new ParameterValues("a", new Integer[]{1,2}));
        ParameterValues b = new ParameterValues("b", new Integer[]{1,2});
        ParameterValues c = new ParameterValues("c", new Integer[]{1,2});
        Exploration.ComposedParametersValues bc = new Exploration.ComposedParametersValues(Arrays.asList(b,c));
        parametersValues.add(bc);
        result = Exploration.completePlan(parametersValues);
        assertEquals(4, result.size());
    }

}