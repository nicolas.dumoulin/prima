/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.prima.regionalmodel.LabourOffice.JobProposition;
import java.io.IOException;
import java.util.List;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class LabourOfficeTest {

    private String notFoundMessage = "Municipality not found in the result";
    private Municipality municipalityA;
    private Municipality municipalityB;
    private Municipality municipalityC;
    private Municipality municipalityD;
    private MunicipalitySet region;
    private LabourOffice labourOffice;

    public LabourOfficeTest() {
    }

    @Before
    public void setUp() throws Exception {
        // init a region with
        //  - 3 activities
        //  - 2 professions
        //  - all patterns for one or two activities
        //  - 4 municipalities with some availabilities
        municipalityA = new Municipality("A", new int[]{5, 0, 0});
        municipalityA.setProximityJob(municipalityB, municipalityC, municipalityD);
        municipalityB = new Municipality("B", new int[]{0, 5, 0});
        municipalityC = new Municipality("C", new int[]{5, 0, 5});
        municipalityD = new Municipality("D", new int[]{0, 5, 5});
        municipalityA.setProximityJob(municipalityB, municipalityC, municipalityD);
        municipalityB.setProximityJob(municipalityA, municipalityC, municipalityD);
        municipalityC.setProximityJob(municipalityA, municipalityB, municipalityD);
        municipalityD.setProximityJob(municipalityA, municipalityB, municipalityC);
        region = RegionTest.buildARegion(municipalityA, municipalityB, municipalityC, municipalityD);
        labourOffice = region.getLabourOffice();
    }

    /**
     * Test of getMunicipalitiesForJob method, of class LabourOffice.
     */
    @Test
    public void testGetMunicipalitiesForJob() {
        JobProposition result = labourOffice.getMunicipalitiesForJob(0);
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityA));
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityC));
        result = labourOffice.getMunicipalitiesForJob(1);
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityB));
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityD));
        result = labourOffice.getMunicipalitiesForJob(2);
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityC));
        assertTrue(
                notFoundMessage, ArrayUtils.contains(result.getJobsLocations(), municipalityD));


    }

    /**
     * Test of getMunicipalitiesForActivityPattern method, of class LabourOffice.
     */
    @Test
    public void testGetMunicipalitiesForActivityPattern() {
        ActivityPattern pattern = new ActivityPattern(true, false, false);
        List<JobProposition> result = labourOffice.getMunicipalitiesForActivityPattern(pattern);
        assertEquals(
                "2 job propositions should have been found", 2, result.size());
        assertEquals(
                "1 municipality should be returned in each job propositions"
                + " (pattern contains 1 job)", 1, result.get(0).getJobsLocations().length);
        assertEquals(
                "1 municipality should be returned in each job propositions"
                + " (pattern contains 1 job)", 1, result.get(1).getJobsLocations().length);
        assertEquals(
                notFoundMessage, municipalityA, result.get(0).getJobsLocations()[0]);
        assertEquals(
                notFoundMessage, municipalityC, result.get(1).getJobsLocations()[0]);


    }

    /**
     * Test of getMunicipalitiesForProfession method, of class LabourOffice.
     */
    @Test
    public void testGetMunicipalitiesForProfession() {
        List<JobProposition> result = labourOffice.getMunicipalitiesForProfession(0);
        assertEquals(
                "4 job propositions should have been found", 4, result.size());
        assertEquals(
                notFoundMessage, municipalityA, result.get(0).getJobsLocations()[0]);
        // C is found two times, for the two different patterns that match the profession
        assertEquals(
                notFoundMessage, municipalityC, result.get(1).getJobsLocations()[0]);
        assertEquals(
                notFoundMessage, municipalityC, result.get(2).getJobsLocations()[0]);
        assertEquals(
                notFoundMessage, municipalityD, result.get(3).getJobsLocations()[0]);


    }

    /**
     * Test of getLocalMunicipalitiesForActivityPattern method, of class LabourOffice.
     */
    // TODO init a network
    //@Test
    /*public void testGetLocalMunicipalitiesForActivityPattern() {
        ActivityPattern pattern = new ActivityPattern(true, false, false);
        List<JobProposition> result = labourOffice.getLocalMunicipalitiesForActivityPattern(pattern, municipalityA);
        assertEquals(
                "2 job propositions should have been found", 2, result.size());
        assertArrayEquals(
                notFoundMessage,
                new Municipality[]{municipalityA},
                result.get(0).getJobsLocations());
        assertArrayEquals(
                notFoundMessage,
                new Municipality[]{municipalityC},
                result.get(1).getJobsLocations());
    }*/

    /**
     * Test of getLocalMunicipalitiesForProfession method, of class LabourOffice.
     */
    // TODO init a network
    //@Test
    public void testGetLocalMunicipalitiesForProfession() {
        List<JobProposition> result = labourOffice.getLocalMunicipalitiesForProfession(municipalityA, 0);
        assertEquals(
                "4 job propositions should have been found", 4, result.size());
        assertEquals(
                notFoundMessage, municipalityA, result.get(0).getJobsLocations()[0]);
        // C is found two times, for the two different patterns that match the profession
        assertEquals(
                notFoundMessage, municipalityC, result.get(1).getJobsLocations()[0]);
        assertEquals(
                notFoundMessage, municipalityC, result.get(2).getJobsLocations()[0]);
        assertEquals(
                notFoundMessage, municipalityD, result.get(3).getJobsLocations()[0]);

    }
}
