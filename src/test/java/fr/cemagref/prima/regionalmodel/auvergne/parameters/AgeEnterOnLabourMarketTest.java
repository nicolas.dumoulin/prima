/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.auvergne.parameters;

import fr.cemagref.prima.regionalmodel.Municipality;
import java.io.File;
import fr.cemagref.prima.regionalmodel.parameters.Value;
import java.util.ArrayList;
import java.util.List;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.Household;
import fr.cemagref.prima.regionalmodel.RegionTest;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class AgeEnterOnLabourMarketTest {

    /**
     * Test of getValue method, of class AgeEnterOnLabourMarket.
     */
    @Test
    public void testGetValue() throws Exception {
        Application application = new Application(new File("dataIn/CondatNew"), null);
        Value<Integer, Individual> parameter = (AgeEnterOnLabourMarket) Application.fromXML("<fr.cemagref.prima.regionalmodel.auvergne.parameters.AgeEnterOnLabourMarket>"
                + "<filename>ageEnterLabourMarketAuvergne.csv</filename>"
                + "</fr.cemagref.prima.regionalmodel.auvergne.parameters.AgeEnterOnLabourMarket>");
        application.getRandom().setSeed(new long[]{12345, 12345, 12345, 12345, 12345, 12345});
        parameter.init(application);
        Individual ind = new Individual(true, (byte) 50, Individual.Status.WORKER, 3);
        List<Individual> list = new ArrayList<Individual>();
        list.add(ind);
        Household hh = new Household(null, list, null);
        for (int i = 0; i < 5; i++) {
            assertEquals(51, (int) parameter.getValue(ind));
        }
        Individual ind2 = new Individual(true, (byte) 20, Individual.Status.WORKER, 2);
        ind2.setParent1(ind);
        hh.addMember(ind2);
        Municipality mun = new Municipality("A", new int[]{0, 0, 0});
        RegionTest.buildARegion(mun).setMyApplication(application);
        hh.setMyVillage(mun);
        assertEquals(21, (int) parameter.getValue(ind2));
        assertEquals(22, (int) parameter.getValue(ind2));
        assertEquals(22, (int) parameter.getValue(ind2));
        assertEquals(26, (int) parameter.getValue(ind2));
        assertEquals(22, (int) parameter.getValue(ind2));
        assertEquals(24, (int) parameter.getValue(ind2));
        assertEquals(23, (int) parameter.getValue(ind2));
        assertEquals(23, (int) parameter.getValue(ind2));

    }
}
