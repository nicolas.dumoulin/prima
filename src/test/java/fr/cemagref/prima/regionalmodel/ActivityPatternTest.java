/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.cemagref.prima.regionalmodel;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class ActivityPatternTest {


    private void testGetJobs(boolean[] pattern, int expResult) {
        ActivityPattern instance = new ActivityPattern(pattern, 0);
        int result = instance.getJob();
        assertEquals(expResult, result);
    }

    /**
     * Test of getJobs method, of class ActivityPattern.
     */
    @Test
    public void testGetJobs() {
        testGetJobs(new boolean[] {false, true, false}, 1);
        testGetJobs(new boolean[] {false, false, false}, -1);
        testGetJobs(new boolean[] {}, -1);
        testGetJobs(new boolean[] {true}, 0);
    }

    @Test
    public void testGetJob() {
        assertEquals(1, new ActivityPattern(new boolean[]{false, true, false}, 0).getJob());
        assertEquals(-1, new ActivityPattern(new boolean[]{false, false, false}, 0).getJob());
        assertEquals(-1, new ActivityPattern(new boolean[]{}, 0).getJob());
    }


}