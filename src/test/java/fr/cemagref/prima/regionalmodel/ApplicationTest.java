/*
 *  Copyright (C) 2010 dumoulin
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import java.util.logging.LogManager;
import java.util.ArrayList;
import fr.cemagref.prima.regionalmodel.Individual.Status;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dumoulin
 */
public class ApplicationTest {

    private int oldPopulationSize;

    @Test
    public void testInit() throws Exception {
        try {
            LogManager.getLogManager().readConfiguration(Application.class.getResourceAsStream("logging.properties"));
        } catch (IOException ex) {
            Logger.getLogger(ApplicationTest.class.getName()).log(Level.WARNING,
                    "Problems to load the logging configuration file", ex);
        }

        // load parameters and init the model
        Application application = new Application("dataIn/CondatNew/ficEnt.xml");
        application.loadFiles();
        MunicipalitySet region = new MunicipalitySet(application, application.getParameters(), application.getScenarios(), false);

        // Process simulation
        for (int i = 0; i < 10; i++) {
            oldPopulationSize = region.getCounters().getPopulationSize();
            checkMunicipalitySet(region);
            region.iteration(i);
        }
        checkMunicipalitySet(region);
        // compare data
        /*int[][] availableResidences = new int[][]{{1, 0, 0, 0}, {0, 22, 5, 77}, {0, 0, 1, 1}, {1, 2, 0, 2}, {0, 0, 0, 1}, {0, 0, 0, 0}, {0, 0, 0, 6}};
         int[] popSize = new int[]{206, 1402, 267, 947, 302, 61, 327};
         int[] nbActives = new int[]{86, 394, 89, 259, 119, 26, 56};
         for (int i = 0; i < region.getMyMunicipalities().size(); i++) {
         final Municipality mun = region.getMyMunicipalities().get(i);
         for (int j = 0; j < mun.getResidencesTypeCount(); j++) {
         assertEquals(availableResidences[i][j], mun.getFreeResidences(j).size());
         }
         assertEquals(popSize[i], mun.getPopulationSize());
         assertEquals(nbActives[i], mun.getTotalActives());
         }*/
    }

    private void checkMunicipalitySet(MunicipalitySet region) {
        checkOccupiedActivitiesCounters(region);
        int popsize = computePopSize(region);
        assertEquals(popsize, region.getCounters().getPopulationSize());
        int naturalBalance = computeNaturalBalance(region);
        assertEquals(naturalBalance, region.getCounters().getAnnualNaturalBalance());
        //assertEquals(popsize, oldPopulationSize + region.getMigratoryBalance() + naturalBalance - region.getNbIndividualsExpulsed());
        Map<Municipality, Integer> exceptedWorkers = new HashMap<Municipality, Integer>();
        for (Municipality mun : region.getMyMunicipalities()) {
            exceptedWorkers.put(mun, 0);
        }
        for (Municipality mun : region.getOutsides()) {
            exceptedWorkers.put(mun, 0);
        }
        for (Municipality mun : region.getMyMunicipalities()) {
            assertEquals(mun.getResidencesTypeCount(), region.getParameters().getNbSizeRes());
            for (int i = 0; i < mun.getResidencesTypeCount(); i++) {
                assertTrue("Residence availability should be positive", mun.getFreeResidences(i).size() >= 0);
                assertEquals("Error for residence count for type " + i + " in " + region.getCurrentYear() + " for mun " + mun.getName(),
                        mun.getResidenceOffer(i), mun.getFreeResidencesCount(i) + mun.getOccupiedResidencesCount(i));
            }
            for (Household hh : mun.getMyHouseholds()) {
                String errorOnHshType = "Error with household type ";
                if (hh.getHshType() == Household.Type.SINGLE) {
                    assertEquals(errorOnHshType + hh, 1, hh.getSize());
                } else if (hh.getHshType() == Household.Type.MONOPARENTAL) {
                    assertEquals(errorOnHshType + hh, 1, hh.getAdults().size());
                } else if (hh.getHshType() == Household.Type.COUPLE_WITHOUT_CHILDREN) {
                    assertEquals(errorOnHshType + hh, 2, hh.getSize());
                } else if (hh.getHshType() == Household.Type.COUPLE_WITH_CHILDREN) {
                    assertEquals(errorOnHshType + hh, 2, hh.getAdults().size());
                }
                assertFalse("Some households have not been suppressed", hh.isJustSuppressed());
                assertFalse("An household should have at least one adult " + hh, hh.getAdults().isEmpty());
                for (Individual ind : hh) {
                    if (region.getCurrentYear() > 1990 && !ind.getMyHousehold().getResidence().isTransit()) {
                        assertTrue("This individual (" + ind.getId() + ", hh " + ind.getMyHousehold().getId() + ") should not be alive! He's "
                                + ind.getAge() + " old and should die at " + ind.getAgeToDie(),
                                ind.getAgeToDie() >= ind.getAge());
                    }
                    if (ind.getStatus() == Status.WORKER) {
                        assertTrue("Anormal worker " + ind.getId() + " in " + ind.getMyHousehold(), (ind.getMyActivityPattern().getIndex() != 0) && (ind.getJobLocation() != null) && (ind.getJobLocation() != null));
                    } else if (ind.getStatus() == Status.UNEMPLOYED) {
                        assertTrue("Anormal unemployed " + ind.getId() + " in " + ind.getMyHousehold(), (ind.getMyActivityPattern().getIndex() == 0) && (ind.getJobLocation() == hh.getMyVillage()));
                    }
                }
                if (!hh.isJustSuppressed() && !hh.getResidence().isTransit()) {
                    for (Individual ind : hh) {
                        if (ind.getStatus() == Status.WORKER) {
                            assertTrue("ind #" + ind.getId() + " (in hh #" + hh.getId() + ") is working at " + mun.getName() + " but isn't in the workers list of this mun", ind.getJobLocation().getMyWorkers()[ind.getAgeToEnterOnTheLabourMarket()].contains(ind));
                            exceptedWorkers.put(ind.getJobLocation(), 1 + exceptedWorkers.get(ind.getJobLocation()));
                        }
                    }
                }
            }
        }
        for (Municipality mun : region.getMyMunicipalities()) {
            int workersCount = 0;
            for (List<Individual> myWorkers : mun.getMyWorkers()) {
                if (myWorkers != null) {
                    for (Individual individual : myWorkers) {
                        assertSame("This worker should be in " + individual.getJobLocation().getName() + " instead of " + mun.getName() + ": " + individual.getId(), individual.getJobLocation(), mun);
                    }
                    workersCount += myWorkers.size();
                }
            }
            assertEquals(workersCount + " counted instead of " + exceptedWorkers.get(mun) + " in " + mun.getName(), (Integer) exceptedWorkers.get(mun), (Integer) workersCount);
        }
    }

    public static int computePopSize(MunicipalitySet region) {
        int size = 0;
        for (Municipality mun : region.getMyMunicipalities()) {
            for (Household hs : mun.getMyHouseholds()) {
                if (!hs.getResidence().isTransit()) {
                    if (!hs.isJustSuppressed()) {
                        size = size + hs.listOfMembers.size();
                    }
                }
            }
        }
        return size;
    }

    public static int computeNaturalBalance(MunicipalitySet region) {
        int count = 0;
        for (Municipality mun : region.getMyMunicipalities()) {
            count -= mun.getCounters().getNbDeath();
            count += mun.getCounters().getNbBirth();
        }
        return count;
    }

    public void checkOccupiedActivitiesCounters(MunicipalitySet region) throws RuntimeException {
        for (Municipality mun : region.getMyMunicipalities()) {
            float[] occupiedActivitiesByRes = mun.getOccupiedActivitiesByRes();
            float[] occupiedActivitiesByExt = mun.getOccupiedActivitiesByExt();
            for (int i = 1; i < mun.getOfferedActivities().length; i++) {
                List<Individual> foundExtIndiv = new ArrayList<Individual>();
                int found = 0, foundRes = 0, foundExt = 0, foundOutside = 0;
                List<Municipality> munsToProcess = new ArrayList<Municipality>();
                munsToProcess.addAll(region.getMyMunicipalities());
                for (Municipality muni : munsToProcess) {
                    for (Household household : muni.getMyHouseholds()) {
                        if (!household.isJustSuppressed()) {
                            for (Individual individual : household) {
                                if ((individual.getMyActivityPattern() != null) && (individual.getJobLocation() == mun) && (individual.getStatus() != Status.UNEMPLOYED)) {
                                    if (i == individual.getMyActivityPattern().getJob()) {
                                        if (muni == mun) {
                                            foundRes++;
                                        } else if (muni.isOutside()) {
                                            foundOutside++;
                                        } else {
                                            foundExtIndiv.add(individual);
                                            foundExt++;
                                        }
                                        found++;
                                    }
                                }
                            }
                        }
                    }
                }
                int count = (int) (occupiedActivitiesByRes[i] + occupiedActivitiesByExt[i]);
                String message = "Counters differs from data for mun #" + mun.getName() + " and activity #" + i;
                assertEquals(message, foundExt, Math.round(occupiedActivitiesByExt[i]));
                assertEquals(message, foundRes, Math.round(occupiedActivitiesByRes[i]));
                assertEquals(message, found, count);
                //assertEquals(message, foundOutside, Math.round(mun.getOccupiedActivitiesByOutside()[i]));
            }
        }
    }
}
