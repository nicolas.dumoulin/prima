/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class MunicipalityTest {

    @Test
    public void testGetProximityJobOrigin() throws Exception {
        Municipality municipalityA = new Municipality("A", new int[]{0,0,0});
        Municipality municipalityB = new Municipality("B", new int[]{0,0,0});
        Municipality municipalityC = new Municipality("C", new int[]{0,0,0});
        municipalityA.setProximityJob(municipalityB, municipalityC);
        municipalityB.setProximityJob(municipalityA);
        municipalityC.setProximityJob(municipalityB);
        RegionTest.buildARegion(municipalityA, municipalityB, municipalityC);
        List<Municipality> proximityJobOrigin = municipalityA.getProximityJobOrigin();
        assertEquals(1, proximityJobOrigin.size());
        assertEquals(municipalityB, proximityJobOrigin.get(0));
    }
}
