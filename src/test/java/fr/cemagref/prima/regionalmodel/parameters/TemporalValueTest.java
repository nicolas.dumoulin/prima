/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class TemporalValueTest {

    /**
     * Test of getValue method, of class TemporalValue.
     */
    @Test
    public void testGetValue() {
        TemporalValue tv = (TemporalValue) Application.fromXML(this.getClass().getResourceAsStream("temporalValue.xml"));
        tv.update(2010);
        assertEquals(10, tv.getValue(null));
        tv.update(2011);
        assertEquals(10, tv.getValue(null));
        tv.update(2012);
        assertEquals(12, tv.getValue(null));
        tv.update(2032);
        assertEquals(12, tv.getValue(null));
    }

}