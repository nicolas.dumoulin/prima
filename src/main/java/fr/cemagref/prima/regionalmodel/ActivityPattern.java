/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

public class ActivityPattern {

    private boolean[] pattern;
    private int index;
    private int job;

    /*************************************************
     *Constructor of the class:                      *
     * It needs the vector describing the pattern    *
     * and the value for availability                *
     *************************************************/
    public ActivityPattern(boolean[] myPattern, int indice) {
        pattern = new boolean[myPattern.length];
        System.arraycopy(myPattern, 0, pattern, 0, myPattern.length);
        this.index = indice;
        this.job = -1;
        for (int i = 0; i < pattern.length; i++) {
            if (pattern[i]) {
                if (this.job == -1) {
                    this.job = i;
                } else {
                    throw new RuntimeException("Multi-activities patterns are no more supported!");
                }
            }
        }
    }

    public ActivityPattern(boolean... pattern) {
        this(pattern, 0);
    }

    /********************************************
     *            Methods for the class         *
     *******************************************/
    public boolean[] getPattern() {
        return pattern;
    }

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder(pattern.length);
        for (boolean b : pattern) {
            buff.append(b ? 1 : 0);
        }
        return buff.toString();
    }

    /**
     * The job matched by this pattern.
     * @return the job or -1 if no job present
     */
    public int getJob() {
        return job;
    }

    public int getIndex() {
        return index;
    }

}
