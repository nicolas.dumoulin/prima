/*
 * Copyright (C) 2013 Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

/**
 * Load the municipalities parameters and convert them to a binary file.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class MunicipalitiesParametersConverter {
    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("This program requires two arguments: <model input file> <villages binary output file>");
            System.exit(1);
        } else {
            Application app = new Application(args[0]);
            app.loadFiles();
            app.getParameters().saveVillagesParametersToBinary(args[1]);
        }
    }
  
}
