/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.prima.regionalmodel.Individual.Status;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class MunicipalityCounters {

    private Municipality myMunicipality;
    /**
     * Number of new households during a given iteration
     */
    private int newHouseholdNb;
    /**
     * Number of households moving out during an iteration (household which
     * leave the village)
     */
    private int nbHouseholdMoveOut;
    /**
     * Number of households moving inside during an iteration (household which
     * probabilisticMove without changing of village)
     */
    private int nbHouseholdMoveInside;
    /**
     * Number of households moving in during an iteration (household who enter
     * into the village)
     */
    private int nbHouseholdMoveIn;
    /**
     * Number of households suppressed because they have joined or because one
     * people died
     */
    private int nbHouseholdSuppressed;
    /**
     * Number of children suppressed due to the fact that their parents are
     * died, whatever their age.
     */
    private int nbKilledOrphans;
    /**
     * Number of households suppressed because of the housing scenario
     */
    private int nbHouseholdExpulsed;
    /**
     * Number of individuals suppressed because of the housing scenario
     */
    private int nbIndividualsExpulsed;
    /**
     * Number of households moving out during an iteration (household which
     * leave the village)
     */
    private int nbIndividualMoveOut;
    /**
     * Number of households moving in during an iteration (household who enter
     * into the village)
     */
    private int nbIndividualMoveIn;
    private int nbImmigrants0_14;
    private int nbImmigrants15_24;

    @Observable(description = "potential immigrants")
    public int getNbPotentialImmigrants() {
        int size = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (household.getResidence().isTransit()) {
                size += household.size();
            }
        }
        return size;
    }

    @Observable(description = "migratory balance avec Outside")
    public int getMigratoryBalanceOutside() {
        return nbImmigrants - nbOutMigrants;
    }

    @Observable(description = "natural balance")
    public int getNaturalBalance() {
        return getNbBirth() - getNbDeath();
    }

    public int getNbImmigrants0_14() {
        return nbImmigrants0_14;
    }

    public int getNbImmigrants15_24() {
        return nbImmigrants15_24;
    }

    public int getNbImmigrants25_29() {
        return nbImmigrants25_29;
    }

    public int getNbImmigrants30_39() {
        return nbImmigrants30_39;
    }

    public int getNbImmigrants40_59() {
        return nbImmigrants40_59;
    }

    public int getNbImmigrants60_74() {
        return nbImmigrants60_74;
    }

    public int getNbImmigrants75andMore() {
        return nbImmigrants75andMore;
    }
    private int nbImmigrants25_29;
    private int nbImmigrants30_39;
    private int nbImmigrants40_59;
    private int nbImmigrants60_74;
    private int nbImmigrants75andMore;
    /**
     * Number of individuals moving inside during an iteration (household who
     * enter into the village)
     */
    private int nbIndividualMoveInsideOtherMun;
    private int nbHouseholdMoveInsideSameMun;
    private int nbIndividualMoveInsideSameMun;
    private int numBirth;
    private int numDeath;
    private int[] hshMoversBySize = new int[4];
    /**
     * Distance Distribution of residential mobilities for individuals
     */
    private int[] dDistResMob;
    /**
     * Distance Distribution of residential mobilities for households
     */
    private int[] dDistResMobHsh;
    /**
     * Number of households without a house during the current iteration
     */
    private int nbOfDwellingLacking;
    /**
     * Number of immigrants leaving their transit residence and arriving in this
     * municipality during a given iteration
     */
    private int nbImmigrants;
    /**
     * Number of immigrants living in a single household, leaving their transit
     * residence and arriving in this municipality during a given iteration
     */
    private int nbImmigrantsSingle;
    /**
     * Number of immigrants being 60 and more
     */
    private int nbImmigrants60;
    private int activeImmigrants;
    private int inactiveImmigrants;
    /**
     * Number of out migrants quitting the set of municipalities
     */
    private int nbOutMigrants;

    public MunicipalityCounters(Municipality municipality) {
        this.myMunicipality = municipality;
        resetCounters();
    }

    @Observable(description = "")
    public String getMunicipalityName() {
        return myMunicipality.getName();
    }

    @Observable(description = "")
    public int getNewHouseholdNb() {
        return newHouseholdNb;
    }

    @Observable(description = "")
    public int getNbHouseholdMoveIn() {
        return nbHouseholdMoveIn;
    }

    @Observable(description = "")
    public int getNbHouseholdMoveInside() {
        return nbHouseholdMoveInside;
    }

    @Observable(description = "")
    public int getNbHouseholdMoveOut() {
        return nbHouseholdMoveOut;
    }

    @Observable(description = "")
    public int getNbHouseholdSuppressed() {
        return nbHouseholdSuppressed;
    }

    @Observable(description = "name of the municipality")
    public String getName() {
        return myMunicipality.getName();
    }

    @Observable(description = "")
    public int getNbKilledOrphans() {
        return nbKilledOrphans;
    }

    @Observable(description = "")
    public int getNbHouseholdExpulsed() {
        return nbHouseholdExpulsed;
    }

    @Observable(description = "")
    public int getNbIndividualsExpulsed() {
        return nbIndividualsExpulsed;
    }

    @Observable(description = "")
    public int getNbIndividualMoveIn() {
        return nbIndividualMoveIn;
    }

    @Observable(description = "")
    public int getNbIndividualMoveInsideOtherMun() {
        return nbIndividualMoveInsideOtherMun;
    }

    @Observable(description = "")
    public int getNbHouseholdMoveInsideSameMun() {
        return nbHouseholdMoveInsideSameMun;
    }

    @Observable(description = "")
    public int getNbIndividualMoveInsideSameMun() {
        return nbIndividualMoveInsideSameMun;
    }

    @Observable(description = "")
    public int getNbIndividualMoveOut() {
        return nbIndividualMoveOut;
    }

    @Observable(description = "")
    public int getNumBirth() {
        return numBirth;
    }

    @Observable(description = "")
    public int getNbBirth() {
        return numBirth;
    }

    @Observable(description = "")
    public int getNumDeath() {
        return numDeath;
    }

    @Observable(description = "")
    public int getNbDeath() {
        return numDeath;
    }

    @Observable(description = "")
    public int getNbOfDwellingLacking() {
        return nbOfDwellingLacking;
    }

    @Observable(description = "")
    public int getNbImmigrants() {
        return nbImmigrants;
    }

    @Observable(description = "")
    public int getNbActiveImmigrants() {
        return activeImmigrants;
    }

    @Observable(description = "")
    public int getNbInactiveImmigrants() {
        return inactiveImmigrants;
    }

    @Observable(description = "")
    public int getNbImmigrants60() {
        return nbImmigrants60;
    }

    @Observable(description = "migrants going out of the set of municipalities")
    public int getNbOutMigrants() {
        return nbOutMigrants;
    }

    @Observable(description = "")
    public int getNbImmigrantsSingle() {
        return nbImmigrantsSingle;
    }

    @Observable(description = "", size = {18})
    public int[] getDistDistribResMobility() {
        return dDistResMob;
    }

    @Observable(description = "", size = {4})
    public int[] getDisthshMoversBySize() {
        return hshMoversBySize;
    }

    @Observable(description = "", size = {18})
    public int[] getDistDistribResMobilityHsh() {
        return dDistResMobHsh;
    }

    public void incNewHouseholdNb(int newHouseholdNb) {
        this.newHouseholdNb += newHouseholdNb;
    }

    public void incNbHouseholdMoveIn(int nbHouseholdMoveIn) {
        this.nbHouseholdMoveIn += nbHouseholdMoveIn;
    }

    public void incNbHouseholdMoveInside(int nbHouseholdMoveInside) {
        this.nbHouseholdMoveInside += nbHouseholdMoveInside;
    }

    public void incNbHouseholdMoveOut(int nbHouseholdMoveOut) {
        this.nbHouseholdMoveOut += nbHouseholdMoveOut;
    }

    public void incNbHouseholdSuppressed(int nbHouseholdSuppressed) {
        this.nbHouseholdSuppressed += nbHouseholdSuppressed;
    }

    public void incNbKilledOrphans(int nbKilledOrphans) {
        this.nbKilledOrphans += nbKilledOrphans;
        incNumDeath(nbKilledOrphans);
    }

    public void incNbHouseholdExpulsed(int nb) {
        this.nbHouseholdExpulsed += nb;
    }

    public void incNbIndividualsExpulsed(int nb) {
        this.nbIndividualsExpulsed += nb;
    }

    public void incNbIndividualMoveIn(int nbIndividualMoveIn) {
        this.nbIndividualMoveIn += nbIndividualMoveIn;
    }

    public void incNbIndividualMoveInsideOtherMun(int nbIndivMoveInside, double dist) {
        this.nbIndividualMoveInsideOtherMun += nbIndivMoveInside;
        incDistribDistanceResidentMobility(nbIndivMoveInside, dist);
    }

    public void incDistribDistanceResidentMobility(int quant, double dist) {
        int d = (int) (dist / 1000.00f);
        if (d != 0) {
            d = (d - 1) / 3;
            if (d > 16) {
                d = 16;
            }
        }
        int type = quant ;
        if (quant > 4) {
            type = 4;
        }
        hshMoversBySize[type - 1]++;
        dDistResMob[d] = dDistResMob[d] + quant;
        //dDistResMob[d] = dDistResMob[d]++ ;
        //incDistribDistanceResidentMobilityHsh(d);
    }

    public void incDistribDistanceResidentMobilityHsh(int d) {
        dDistResMobHsh[d] = dDistResMobHsh[d]++;
    }

    public void incNbHouseholdMoveInsideSameMun(int nbHouseholdMoveInsideSameMun) {
        this.nbHouseholdMoveInsideSameMun += nbHouseholdMoveInsideSameMun;
    }

    public void incNbIndividualMoveInsideSameMun(int nbIndivMoveInsideSameMun) {
        this.nbIndividualMoveInsideSameMun += nbIndivMoveInsideSameMun;
        incDistribDistanceResidentMobility(nbIndivMoveInsideSameMun, 0.0);
    }

    public void incNbIndividualMoveOut(int nbIndividualMoveOut) {
        this.nbIndividualMoveOut += nbIndividualMoveOut;
    }

    public void incNbOfDwellingLacking(int nbOfDwellingLacking) {
        this.nbOfDwellingLacking += nbOfDwellingLacking;
    }

    public void incNbImmigrants(int nbImmigrants) {
        this.nbImmigrants += nbImmigrants;
    }

    public void incNbImmigrantsInAges(Household househ) {
        //if (househ.getAdults().size()==0) System.err.println(househ.getMyVillage().getName()+" id hsh "+househ.getId()+" size "+househ.getSize()+" "+househ+" need for residence "+househ.needOfResidence()) ;
        for (Individual indiv : househ.getCopyOfMembers()) {
            if (indiv.getStatus() == Status.UNEMPLOYED || indiv.getStatus() == Status.WORKER) {
                activeImmigrants++;
            }
            if (indiv.getStatus() == Status.INACTIVE) {
                inactiveImmigrants++;
            }
            if (indiv.getAge() < 15) {
                this.nbImmigrants0_14++;
            } else {
                if (indiv.getAge() < 25) {
                    this.nbImmigrants15_24++;
                } else {
                    if (indiv.getAge() < 30) {
                        this.nbImmigrants25_29++;
                    } else {
                        if (indiv.getAge() < 40) {
                            this.nbImmigrants30_39++;
                        } else {
                            if (indiv.getAge() < 60) {
                                this.nbImmigrants40_59++;
                            } else {
                                if (indiv.getAge() < 75) {
                                    this.nbImmigrants60_74++;
                                } else {
                                    this.nbImmigrants75andMore++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void incNbImmigrants60(int nbImmigrants) {
        this.nbImmigrants60 += nbImmigrants;
    }

    public void incNbOutMigrants(int nbOutMig) {
        this.nbOutMigrants += nbOutMig;
    }

    public void incNbImmigrantsSingle(int nbImmigrants) {
        this.nbImmigrantsSingle += nbImmigrants;
    }

    public void incNumBirth(int numBirth) {
        this.numBirth += numBirth;
    }

    public void incNumDeath(int numDeath) {
        this.numDeath += numDeath;
    }

    public void resetCounters() {
        newHouseholdNb = 0;
        nbHouseholdMoveOut = 0;
        nbHouseholdMoveInside = 0;
        nbHouseholdMoveIn = 0;
        nbHouseholdSuppressed = 0;
        nbKilledOrphans = 0;
        nbHouseholdExpulsed = 0;
        nbIndividualsExpulsed = 0;
        nbIndividualMoveOut = 0;
        nbIndividualMoveIn = 0;
        nbImmigrants0_14 = 0;
        nbImmigrants15_24 = 0;
        nbImmigrants25_29 = 0;
        nbImmigrants30_39 = 0;
        nbImmigrants40_59 = 0;
        nbImmigrants60_74 = 0;
        nbImmigrants75andMore = 0;
        activeImmigrants = 0;
        inactiveImmigrants = 0;
        nbIndividualMoveInsideOtherMun = 0;
        nbHouseholdMoveInsideSameMun = 0;
        nbIndividualMoveInsideSameMun = 0;
        numBirth = 0;
        numDeath = 0;
        nbOfDwellingLacking = 0;
        nbImmigrants = 0;
        nbImmigrants60 = 0;
        nbImmigrantsSingle = 0;
        nbOutMigrants = 0;
        dDistResMob = new int[17];
        Arrays.fill(dDistResMob, 0);
        dDistResMobHsh = new int[17];
        Arrays.fill(dDistResMobHsh, 0);
    }

    @Observable(description = "Parameter NbChild")
    public float getNbChild() {
        return (float) myMunicipality.getMyRegion().getParameters().getNbChild(null);
    }

    @Observable(description = "Population size")
    public int getPopulationSize() {
        return myMunicipality.getPopulationSize();
    }

    @Observable(description = "Nb Singles without potential immigrants")
    public int getNbSinglesWoPotImmig() {
        int size = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                if (household.singleAdult()) {
                    size++;
                }
            }
        }
        return size;
    }

    @Observable(description = "Nb Singles with potential immigrants")
    public int getNbSinglesWithPotImmig() {
        int size = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (household.singleAdult()) {
                size++;
            }
        }
        return size;
    }

    @Observable(description = "Distribution of sizes of housholds", size = {6})
    public int[] getDistribHshSize() {
        int maxSize = 6;
        int data[] = new int[maxSize];
        Arrays.fill(data, 0);
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                if (household.size() > maxSize) {
                    data[data.length - 1]++;
                } else {
                    data[household.size() - 1]++;
                }
            }
        }
        return data;
    }

    public String[] getDistribHshTypeLabels(Integer dimension) {
        List<String> labels = new ArrayList<String>(Household.Type.values().length);
        for (Household.Type label : Household.Type.values()) {
            labels.add(label.toString());
        }
        return labels.toArray(new String[]{});
    }

    @Observable(description = "Distribution of type of housholds", labelsMethod = "getDistribHshTypeLabels", size = {0})
    public int[] getDistribHshType() {
        int data[] = new int[5];
        Arrays.fill(data, 0);
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                data[household.getHshType().ordinal()]++;
            }
        }
        return data;
    }

    @Observable(description = "Distribution of the professions of unemployed and workers", size = {6})
    public int[] getDistribProfessionsUnemployedAndWorkers() {
        int[] data = new int[myMunicipality.getMyRegion().getNbProfessions()];
        Arrays.fill(data, 0);
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if ((ind.getStatus() == Status.UNEMPLOYED) || (ind.getStatus() == Status.WORKER)) {
                        data[ind.getProfession()]++;
                    }
                }
            }
        }
        return data;
    }

    @Observable(description = "Distribution of the professions of unemployed", size = {6})
    public int[] getDistribProfessionsUnemployed() {
        int[] data = new int[myMunicipality.getMyRegion().getNbProfessions()];
        Arrays.fill(data, 0);
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.UNEMPLOYED) {
                        data[ind.getProfession()]++;
                    }
                }
            }
        }
        return data;
    }

    @Observable(description = "Distribution of the professions of workers", size = {6})
    public int[] getDistribProfessionsWorkers() {
        int[] data = new int[myMunicipality.getMyRegion().getNbProfessions()];
        Arrays.fill(data, 0);
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.WORKER) {
                        data[ind.getProfession()]++;
                    }
                }
            }
        }
        return data;
    }

    @Observable(description = "Unemployed (w/o immigrants)")
    public int getUnemployedWithoutImmigrantsCount() {
        int size = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.UNEMPLOYED) {
                        size++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Free jobs for farmers (w/o outsides)")
    public int getFreeJobsForFarmers() {
        int size = 0;
        for (int i = 0; i
                < myMunicipality.getOccupiedActivities().length; i++) {
            /*
             * if (i == 0 || i == 6 || i == 12 || i == 18) {
             * System.err.println(myMunicipality.getName() + " offer " +
             * myMunicipality.getOfferedActivities(i) + " res " +
             * myMunicipality.getOccupiedActivitiesByRes(i) + " ext " +
             * myMunicipality.getOccupiedActivitiesByExt(i) + " out " +
             * myMunicipality.getOccupiedActivitiesByOutside(i)); }
             */
            if (i == 0) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 6) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 12) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 18) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
        }
        return size;
    }

    @Observable(description = "Offered jobs for farmers")
    public int getOfferedJobsForFarmers() {
        int size = 0;
        for (int i = 0; i
                < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 0) {
                size = size + (int) myMunicipality.getOfferedActivities(i);
            }
            if (i == 6) {
                size = size + (int) myMunicipality.getOfferedActivities(i);
            }
            if (i == 12) {
                size = size + (int) myMunicipality.getOfferedActivities(i);
            }
            if (i == 18) {
                size = size + (int) myMunicipality.getOfferedActivities(i);
            }
        }
        return size;
    }

    @Observable(description = "Offered jobs")
    public int getOfferedJobs() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            size = size + (int) myMunicipality.getOfferedActivities(i);
        }
        return size;
    }

    @Observable(description = "Occupied jobs by workers")
    public int getOccupiedJobsByWorkers() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 5) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 11) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 17) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 23) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by craftmen")
    public int getOccupiedJobsByCraftmen() {
        int size = 0;
        for (int i = 0; i
                < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 1) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 7) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 13) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 19) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by executives")
    public int getOccupiedJobsByExecutives() {
        int size = 0;
        for (int i = 0; i
                < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 2) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 8) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 14) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 20) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by employees")
    public int getOccupiedJobsByEmployees() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 4) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 10) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 16) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 22) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by prof. interm.")
    public int getOccupiedJobsByProfInterm() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 3) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 9) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 15) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 21) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by farmers")
    public int getOccupiedJobsByFarmers() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 0) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 6) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 12) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
            if (i == 18) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i) + (int) myMunicipality.getOccupiedActivitiesByExt(i) + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by res")
    public int getOccupiedJobsByRes() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            size = size + (int) myMunicipality.getOccupiedActivitiesByRes(i);
        }
        return size;
    }

    @Observable(description = "Occupied jobs by ext")
    public int getOccupiedJobsByExt() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            size = size + (int) myMunicipality.getOccupiedActivitiesByExt(i);
        }
        return size;
    }

    @Observable(description = "Occupied jobs by outside")
    public int getOccupiedJobsByOutside() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            size = size + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
        }
        return size;
    }

    @Observable(description = "Occupied jobs by outside farmers")
    public int getOccupiedJobsByOutsideFarmer() {
        int size = 0;
        for (int i = 0; i
                < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 0 || i == 6 || i == 12 || i == 18) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by outside craftmen")
    public int getOccupiedJobsByOutsideCraftmen() {
        int size = 0;
        for (int i = 0; i
                < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 1 || i == 7 || i == 13 || i == 19) {
                size = size + (int) myMunicipality.getOccupiedActivitiesByOutside(i);
            }
        }
        return size;
    }
    //*/

    @Observable(description = "Free jobs for workers (w/o outsides)")
    public int getFreeJobsForWorkers() {
        int size = 0;
        for (int i = 0; i
                < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 5) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 11) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 17) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 23) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
        }
        return size;
    }

    @Observable(description = "Free jobs - total")
    public int getFreeJobs() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            size = size + (int) myMunicipality.getFreeActivities(i);
        }
        return size;
    }

    @Observable(description = "Free jobs for craftmen (w/o outsides)")
    public int getFreeJobsForCraftmen() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 1) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 7) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 13) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 19) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
        }
        return size;
    }

    @Observable(description = "Free jobs for executives (w/o outsides)")
    public int getFreeJobsForExecutives() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 2) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 8) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 14) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 20) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
        }
        return size;
    }

    @Observable(description = "Free jobs for Interm Prof (w/o outsides)")
    public int getFreeJobsForIntermProf() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 3) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 9) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 15) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 21) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
        }
        return size;
    }

    @Observable(description = "Free jobs for employees (w/o outsides)")
    public int getFreeJobsForEmployees() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getOccupiedActivities().length; i++) {
            if (i == 4) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 10) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 16) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
            if (i == 22) {
                size = size + (int) myMunicipality.getFreeActivities(i);
            }
        }
        return size;
    }

    @Observable(description = "Distribution of the job offers by activity sectors", size = {4})
    public int[] getDistribActivitySectorJobOffers() {
        int temp = 0;
        int[] data = new int[myMunicipality.getMyRegion().getNbActivSectors()];
        Arrays.fill(data, 0);
        for (int i = 0; i < myMunicipality.getMyRegion().getNbActivSectors(); i++) {
            temp = 0;
            for (int j = 0; j < myMunicipality.getMyRegion().getNbProfessions(); j++) {
                temp = myMunicipality.getEndogeneousOfferedJob()[j + (myMunicipality.getMyRegion().getNbProfessions() * i)] + myMunicipality.getExogeneousOfferedJob()[j + (myMunicipality.getMyRegion().getNbProfessions() * i)];
                data[i] = data[i] + temp;
            }
        }
        return data;
    }

    @Observable(description = "Distribution of the job occupations by activity sectors", size = {4})
    public int[] getDistribActivitySectorJobOccupations() {
        int[] data = new int[myMunicipality.getMyRegion().getNbActivSectors()];
        Arrays.fill(data, 0);
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.WORKER) {
                        data[ind.getSectorOfActivity()]++;
                    }
                }
            }
        }
        return data;
    }

    @Observable(description = "Nb of inactives")
    public int getNbInactives() {
        int size = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            for (Individual ind : household) {
                if (ind.getStatus() == Status.INACTIVE) {
                    size++;
                }
            }
        }
        return size;
    }

    @Observable(description = "services jobs offer")
    public int getServicesJobOffer() {
        int count = 0;
        for (int i = myMunicipality.getMyRegion().getNbProfessions() * 3; i < myMunicipality.getMyRegion().getNbActivities(); i++) {
            count += myMunicipality.getOfferedActivities(i);
        }
        return count;
    }

    @Observable(description = "Actives")
    public int getActivesCount() {
        int size = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.UNEMPLOYED) {
                        size++;
                    }
                    if (ind.getStatus() == Status.WORKER) {
                        size++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Occupied Actives")
    public int getActivesOccupiedCount() {
        int size = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.WORKER) {
                        size++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Nb Retired")
    public int getNbRetireds() {
        int r = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.RETIRED) {
                        r++;
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Students > 18")
    public int getNbStudentsSup18() {
        int r = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.STUDENT && ind.getAge() > 18) {
                        r++;
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Students")
    public int getNbStudents() {
        int r = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.STUDENT) {
                        r++;
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Actives")
    public int getNbActives() {
        int r = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.UNEMPLOYED || ind.getStatus() == Status.WORKER) {
                        r++;
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Actives 15_64 years' old")
    public int getNbActives15_64() {
        int r = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.UNEMPLOYED || ind.getStatus() == Status.WORKER) {
                        if (ind.getAge() > 15 && ind.getAge() < 65) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Actives having an occupation 15_64 years' old")
    public int getNbActivesOccupied15_64() {
        int r = 0;
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Status.WORKER) {
                        if (ind.getAge() > 15 && ind.getAge() < 65) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Hsh size 1 in needForRes")
    public int hsh1NeedForRes() {
        int nbStudent = 0;
        int nbRetired = 0;
        int nbOthers = 0;
        int size = 0;
        for (Household hsh : myMunicipality.getMyHouseholds()) {
            if (hsh.needOfResidence() && hsh.getSize() == 1) {
                size++;


                if (hsh.getAdults().get(0).getStatus() == Individual.Status.RETIRED) {
                    nbRetired++;
                } else {
                    if (hsh.getAdults().get(0).getStatus() == Individual.Status.STUDENT) {
                        nbStudent++;
                    } else {
                        nbOthers++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Hsh size 2 in needForRes")
    public int hsh2NeedForRes() {
        int size = 0;
        for (Household hsh : myMunicipality.getMyHouseholds()) {
            if (hsh.needOfResidence() && hsh.getSize() == 2) {
                size++;
            }
        }
        return size;
    }

    @Observable(description = "Hsh size 3 in needForRes")
    public int hsh3NeedForRes() {
        int size = 0;
        for (Household hsh : myMunicipality.getMyHouseholds()) {
            if (hsh.needOfResidence() && hsh.getSize() == 3) {
                size++;
            }
        }
        return size;
    }

    @Observable(description = "Hsh size > 3 in needForRes")
    public int hsh4NeedForRes() {
        int size = 0;
        for (Household hsh : myMunicipality.getMyHouseholds()) {
            if (hsh.needOfResidence() && hsh.getSize() > 3) {
                size++;
            }
        }
        return size;
    }

    public String[] getDistribPlaceOfWorkLabels(Integer dimension) {
        List<String> labels = new ArrayList<String>(18 + myMunicipality.getMyRegion().getOutsides().size());
        labels.add("workAt[0,3]");
        for (int i = 1; i < 17; i++) {
            labels.add("workAt[" + (3 * i + 1) + "," + (3 * (i + 1)) + "]");
        }
        labels.add("outside");
        labels.add("meanInside");
        return labels.toArray(new String[]{});
    }

    @Observable(description = "Distance Distribution of the places of work", labelsMethod = "getDistribPlaceOfWorkLabels", size = {0})
    public double[] getDistribPlaceOfWork() {
        double[] data = new double[19];
        Arrays.fill(data, 0);
        double sum = 0;
        int count = 0;
        for (Household hh : myMunicipality.getMyHouseholds()) {
            for (Individual ind : hh) {
                if (ind.getStatus() == Status.WORKER) {
                    if (ind.getJobLocation().isOutside()) {
                        data[17]++;
                    } else {
                        double distFloat = myMunicipality.euclidianDistanceFrom(ind.getMyHousehold().getMyVillage()) / 1000.00f;
                        // local commuters are excludes from the average distance computation
                        if (ind.getMyHousehold().getMyVillage() != myMunicipality) {
                            sum += distFloat;
                            count++;
                        }
                        int d = (int) (distFloat);
                        if (d <= 51) {
                            if (d == 0) {
                                data[0]++;
                            } else {
                                data[(d - 1) / 3]++;
                            }
                        } else {
                            data[16]++;
                        }
                    }
                }
            }
        }
        data[18] = sum / count;
        return data;
    }

    @Observable(description = "Nb of individuals moving inside (same village or not)")
    public int getNbIndividualMoveInside() {
        return getNbIndividualMoveInsideOtherMun() + getNbIndividualMoveInsideSameMun();
    }

    public String[] getDistribResMobilityLabels(Integer dimension) {
        List<String> labels = new ArrayList<String>(17);
        labels.add("moveAt[0,3]");
        for (int i = 1; i < 17; i++) {
            labels.add("moveAt[" + (3 * i + 1) + "," + (3 * (i + 1)) + "]");
        }
        return labels.toArray(new String[]{});
    }

    @Observable(description = "Distance Distribution of the residential mobilities", labelsMethod = "getDistribResMobilityLabels", size = {0})
    public int[] getDistDistribResidMobility() {
        return getDistDistribResMobility();
    }

    @Observable(description = "Residence offer")
    public int getResidenceOffer() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getMyRegion().getNbSizesResidence(); i++) {
            size = size + myMunicipality.getResidenceOffer(i);
        }
        return size;
    }

    @Observable(description = "Residence offer size of parameterised type")
    public int getResidenceOffer(int type) {
        return myMunicipality.getResidenceOffer(type);
    }

    @Observable(description = "Residence offer size 1")
    public int getResidenceOfferS1() {
        return myMunicipality.getResidenceOffer(0);
    }

    @Observable(description = "Residence offer size 2")
    public int getResidenceOfferS2() {
        return myMunicipality.getResidenceOffer(1);
    }

    @Observable(description = "Residence offer size 3")
    public int getResidenceOfferS3() {
        return myMunicipality.getResidenceOffer(2);
    }

    @Observable(description = "Residence offer size 4")
    public int getResidenceOfferS4() {
        return myMunicipality.getResidenceOffer(3);
    }

    @Observable(description = "Residence offer")
    public int getFreeResidenceOffer() {
        int size = 0;
        for (int i = 0; i < myMunicipality.getMyRegion().getNbSizesResidence(); i++) {
            size = size + myMunicipality.getFreeResidencesCount(i);
        }
        return size;
    }

    @Observable(description = "Free Residence offer of parameterised type")
    public int getFreeResidenceOffer(int type) {
        return myMunicipality.getFreeResidencesCount(type);
    }

    @Observable(description = "Free Residence offer size 1")
    public int getFreeResidenceOfferS1() {
        return myMunicipality.getFreeResidencesCount(0);
    }

    @Observable(description = "Free Residence offer size 2")
    public int getFreeResidenceOfferS2() {
        return myMunicipality.getFreeResidencesCount(1);
    }

    @Observable(description = "Free Residence offer size 3")
    public int getFreeResidenceOfferS3() {
        return myMunicipality.getFreeResidencesCount(2);
    }

    @Observable(description = "Free Residence offer size 4")
    public int getFreeResidenceOfferS4() {
        return myMunicipality.getFreeResidencesCount(3);
    }

    @Observable(description = "Distribution of age by range of 5 years (first range: [0,5[)", size = {20})
    public int[] getDistribAgeBy5() {
        int width = 5;
        int max = 101;
        int nbRange = max / width;
        max = nbRange * width;
        int[] data = new int[nbRange];
        Arrays.fill(data, 0);
        for (Household household : myMunicipality.getMyHouseholds()) {
            if (!household.getResidence().isTransit()) {
                for (Individual ind : household) {
                    if (ind.getAge() > max - width) {
                        data[data.length - 1]++;
                    } else {
                        data[ind.getAge() / width]++;
                    }
                }
            }
        }
        return data;
    }
}
