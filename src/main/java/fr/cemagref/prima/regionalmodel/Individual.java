 /*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.prima.regionalmodel.MunicipalitiesNetwork.SubNetwork;
import fr.cemagref.prima.regionalmodel.parameters.IntValueFromExponentialLaw;
import fr.cemagref.prima.regionalmodel.parameters.IntValueFromNormalLaw;
import fr.cemagref.prima.regionalmodel.parameters.Parameters;
import fr.cemagref.prima.regionalmodel.parameters.Value;
import fr.cemagref.prima.regionalmodel.tools.Random;
import java.util.*;
import umontreal.iro.lecuyer.probdist.ExponentialDistFromMean;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class Individual {

    private static int cpt = 0;
    private int id;
    /**
     * farCommuter indicates the individual have changed job and the new job is
     * too far from its current residence
     */
    private boolean farCommuter = false;

    public boolean isFarCommuter() {
        return farCommuter;
    }

    public void setFarCommuter(boolean farCommuter) {
        this.farCommuter = farCommuter;
    }

    public static enum Status {

        STUDENT, WORKER, UNEMPLOYED, RETIRED, INACTIVE;
    }
    /**
     * age of the individual
     */
    private byte age;
    public boolean adult;
    /**
     * status of the individual
     */
    private Status status;
    /**
     * This individual status should be changed to inactive at the end of the
     * step
     */
    private transient boolean willBeInactive = false;
    /**
     * social position of the individual (In France, it corresponds to the
     * socioprofessional category but it can also been seing as level of
     * education, of revenue... - it defines the type of the activity in the
     * activity sector
     */
    protected int profession = -1;
    /**
     * Age at which the individual is going to enter on the labour market It is
     * picked out when it was born
     */
    private transient int ageToEnterOnTheLabourMarket;
    /**
     * Activity pattern of the individual if it has got one
     */
    private ActivityPattern myActivityPattern;
    /**
     * Next activity pattern that the individual is going to find a place to
     * adopt
     */
    private transient ActivityPattern nextActPat;
    /**
     * Searched profession (corresponds to a profession number or to -1 if the
     * individual does not search for a job)
     */
    private int searchedProf;

    public int getSearchedProf() {
        return searchedProf;
    }

    public void setSearchedProf(int searchedProf) {
        this.searchedProf = searchedProf;
    }
    /**
     * Previous activity pattern that the individual is going to find a place to
     * adopt
     */
    private transient ActivityPattern previousActPat;
    /**
     * Place of Work related to the previousActPat of the individual for each
     * pattern in the same order than the one with which activities are
     * presented in the ActivityPattern S. Huet 25.02.2010
     */
    protected transient Municipality previousJobLocation;
    /**
     * Place of Work of the individual for each pattern in the same order than
     * the one with which activities are presented in the ActivityPattern S.
     * Huet 7.10.2009
     */
    protected Municipality jobLocation;
    /**
     * Indicating at a given time if each member looks for a job
     */
    private transient boolean lookForJob;
    /**
     * Household from which the individual is a member
     */
    private transient Household myHousehold;
    private transient short migrationYear = -1;
    private transient short lastFiringYear = -1;
    private transient Municipality lastFiringMun = null;
    private transient short lastSplittingYear = -1;
    private transient short lastJoiningYear = -1;
    private transient short lastMovingYear = -1;
    private transient short becomeAdultYear = -1;
    private transient Individual parent1 = null;
    private transient Individual parent2 = null;

    public void setParent1(Individual parent1) {
        this.parent1 = parent1;
    }

    public long getId() {
        return id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status val) {
        this.status = val;
    }

    public boolean willBeInactive() {
        return willBeInactive;
    }

    public byte getAge() {
        return age;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAge(int val) {
        if (val >= 127) {
            //this.age = Byte.MAX_VALUE; // Attention Byte.MAX_VALUE = -128
            this.age = 127;
            //throw new IllegalArgumentException("age can't be greater than " + Byte.MAX_VALUE);
        } else {
            this.age = (byte) val;
        }
    }

    public Random getRandom() {
        return getMyHousehold().getMyVillage().getMyRegion().getMyApplication().getRandom();
    }

    /**
     * Returns the profession ID or -1 if undefined.
     *
     * @return
     */
    public int getProfession() {
        return profession;
    }

    public void setProfession(int profess) {
        // FIXME original code with multipattern was if (profess[0] != getProfession() && getProfession() != -1) {
        // it should test if a profession has been already fixed
        // if the profession change, the age to go on retirement can also changes
        if (profess != getProfession() && getProfession() != -1) {
            initAgeToGoOnRetirement();
        }
        this.profession = profess;
    }
    /**
     * Age at which the individual go on retirement
     */
    private transient int ageToGoOnRetirement;

    public int getAgeToGoOnRetirement() {
        return ageToGoOnRetirement;
    }

    public void setAgeToGoOnRetirement(int AgeToGoOnRetirement) {
        this.ageToGoOnRetirement = AgeToGoOnRetirement;
    }
    /**
     * Age of death
     */
    private transient int ageToDie;

    public int getAgeToDie() {
        return ageToDie;
    }

    public void setAgeToDie(int AgeDie) {
        this.ageToDie = AgeDie;
    }

    public int getAgeToEnterOnTheLabourMarket() {
        if (ageToEnterOnTheLabourMarket == 14) {
            // can occur due to the initial population (cf initPatternOfMyInd)
            return 15;
        }
        return ageToEnterOnTheLabourMarket;
    }

    public void setAgeToEnterOnTheLabourMarket(int ageToEnterOnTheLabourMarket) {
        this.ageToEnterOnTheLabourMarket = ageToEnterOnTheLabourMarket;
    }

    public ActivityPattern getMyActivityPattern() {
        return myActivityPattern;
    }

    public int getSectorOfActivity() {
        return (myActivityPattern.getIndex() - 1) / myHousehold.getMyVillage().getMyRegion().getNbProfessions();
    }

    public void setMyActivityPattern(ActivityPattern myActivityPattern) {
        this.myActivityPattern = myActivityPattern;
    }

    public ActivityPattern getNextActPat() {
        return nextActPat;
    }

    public void setNextActPat(ActivityPattern nextActPat) {
        this.nextActPat = nextActPat;
    }

    public ActivityPattern getPreviousActPat() {
        return previousActPat;
    }

    public void setPreviousActPat(ActivityPattern previousActPat) {
        this.previousActPat = previousActPat;
    }

    public Municipality getpreviousJobLocation() {
        return previousJobLocation;
    }

    public void setPreviousJobLocation(Municipality mun) {
        this.previousJobLocation = mun;
    }

    public Municipality getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(Municipality mun) {
        jobLocation = mun;
    }

    public Household getMyHousehold() {
        return myHousehold;
    }

    public void setMyHousehold(Household myHousehold) {
        this.myHousehold = myHousehold;
    }

    public short getMigrationYear() {
        return migrationYear;
    }

    public void setMigrationYear(short migrationYear) {
        this.migrationYear = migrationYear;
    }

    public Municipality getLastFiringMun() {
        return lastFiringMun;
    }

    public void setLastFiringMun(Municipality lastFiringMun) {
        this.lastFiringMun = lastFiringMun;
    }

    public short getLastFiringYear() {
        return lastFiringYear;
    }

    public void setLastFiringYear(short lastFiringYear) {
        this.lastFiringYear = lastFiringYear;
    }

    public short getLastJoiningYear() {
        return lastJoiningYear;
    }

    public void setLastJoiningYear(short lastJoiningYear) {
        this.lastJoiningYear = lastJoiningYear;
    }

    public short getLastSplittingYear() {
        return lastSplittingYear;
    }

    public void setLastSplittingYear(short lastSplittingYear) {
        this.lastSplittingYear = lastSplittingYear;
    }

    public short getLastMovingYear() {
        return lastMovingYear;
    }

    public void setLastMovingYear(short lastMovingYear) {
        this.lastMovingYear = lastMovingYear;
    }

    public short getBecomeAdultYear() {
        return becomeAdultYear;
    }

    public Individual getParent1() {
        return parent1;
    }

    public Individual getParent2() {
        return parent2;
    }

    public boolean isLookForJob() {
        return lookForJob;
    }

    public void setLookForJob(boolean lookForJob) {
        this.lookForJob = lookForJob;
    }

    /**
     * The constructor only takes an complete already defined individual as
     * parameter to create a new individual: - which remain the same (means the
     * individual change of household) - which is duplicate to represente a
     * migrant S. Huet 12.04.109
     */
    public Individual(Individual ind, boolean migrant) {
        if (!migrant) {
            this.id = ind.id;
            this.lookForJob = ind.lookForJob;
            this.searchedProf = ind.searchedProf;
            this.nextActPat = ind.getNextActPat();
            this.previousActPat = ind.getPreviousActPat();
            this.previousJobLocation = ind.getpreviousJobLocation();
            this.myHousehold = ind.getMyHousehold();
            this.migrationYear = ind.migrationYear;
            this.lastFiringMun = ind.lastFiringMun;
            this.lastFiringYear = ind.lastFiringYear;
            this.lastJoiningYear = ind.lastJoiningYear;
            this.lastSplittingYear = ind.lastSplittingYear;
            this.lastMovingYear = ind.lastMovingYear;
            this.becomeAdultYear = ind.becomeAdultYear;
        } else {
            this.id = ++cpt;
            this.lookForJob = false;
            this.searchedProf = ind.getProfession();
            this.nextActPat = null;
            this.previousActPat = null;
            this.previousJobLocation = null;
        }
        setAge(ind.getAge());
        this.parent1 = ind.parent1;
        this.parent2 = ind.parent2;
        this.adult = ind.adult;
        this.ageToEnterOnTheLabourMarket = ind.getAgeToEnterOnTheLabourMarket();
        this.ageToGoOnRetirement = ind.getAgeToGoOnRetirement();
        this.ageToDie = ind.ageToDie;
        this.status = ind.getStatus();
        this.myActivityPattern = ind.getMyActivityPattern();
        this.jobLocation = ind.jobLocation;
        this.profession = ind.profession;
    }

    /**
     * Duplicate an individual for non-migrant. This constructor is equivalent
     * to new Individual(ind, false).
     *
     * @param ind
     */
    public Individual(Individual ind) {
        this(ind, false);
    }

    /**
     * The constructor of this class needs the age of the individuals and its
     * status. It is to create a new individual at the birth time S. Huet
     * 6.04.09
     */
    public Individual(Household myHouseH, int age, Status status) {
        this.id = ++cpt;
        this.myHousehold = myHouseH;
        this.setAge(age);
        this.adult = false;
        this.status = status;
        this.searchedProf = -1;
        this.ageToEnterOnTheLabourMarket = -1;
        this.ageToGoOnRetirement = -1;
        List<Individual> adults = myHouseH.getAdults();
        this.parent1 = adults.get(0);
        this.parent2 = adults.get(1);
        initAgeToEnterOnLabourMarket();
        affectAFirstProfession();
        initAgeToGoOnRetirement();
        initAgeToDie();
        jobLocation = getMyHousehold().getMyVillage();
    }

    /**
     * The constructor of this class needs the age of the individuals and the
     * parameter managing the age to become a worker (from a student status)
     * [young] and the age to become a retirer [old] Used at the initialisation
     * time S. Huet 28.07.09
     */
    public Individual(Household myHouseH, boolean isAdult, int age) {
        this.id = ++cpt;
        this.myHousehold = myHouseH;
        this.setAge(age);
        this.adult = isAdult;
        if (!isAdult) {
            parent1 = getMyHousehold().getAdults().get(0);
            if (getMyHousehold().getHshTypeCode() == 3) {
                parent2 = getMyHousehold().getAdults().get(1);
            }
        }
        this.searchedProf = -1;
        setAgeToEnterOnTheLabourMarket(-1);
        setAgeToGoOnRetirement(-1);
        jobLocation = getMyHousehold().getMyVillage();
        initStatusDoToAge(); // then certain become children and retirer (worker will be initialized later)
    }

    /**
     * Constructor used for migrants creation.
     *
     * @param age
     * @param status
     * @param professions
     */
    public Individual(boolean isAdult, byte age, Status status, int professions) {
        this.id = ++cpt;
        this.adult = isAdult;
        this.setAge(age);
        this.adult = true;
        this.status = status;
        this.profession = professions;
        this.searchedProf = -1;
    }

    @Override
    public String toString() {
        return "#" + id;
    }

    /*
     * public void initIndividual() { if (getAgeToEnterOnTheLabourMarket() ==
     * -1) { initAgeToEnterOnLabourMarket(); } if (getAgeToGoOnRetirement() ==
     * -1) { initAgeToGoOnRetirement(); } initAgeToDie(); }
     */
    private Parameters getParameters() {
        return myHousehold.getMyVillage().getMyRegion().getParameters();
    }

    /**
     * update the individuals (age plus step year [step is a parameter as yound
     * and old defining respectively age to become a student and age to become a
     * retirer] and status) S. Huet 6.04.09
     */
    public void update(Municipality vil) {
        setAge(getAge() + getMyHousehold().getMyVillage().getMyRegion().getIterationValue());
        updateStatusDoToAge(vil);
    }

    /**
     * Promote to the adult status, create a new household in the same residence
     * that the parents' one
     */
    public void becomeAdult() {
        Household parent = getMyHousehold();
        this.adult = true;
        this.becomeAdultYear = (short) myHousehold.getMyVillage().getMyRegion().getCurrentYear();
        List<Individual> listofmembers = new ArrayList<Individual>();
        myHousehold.suppressMember(this);
        listofmembers.add(this);
        Household newHsh = new Household(myHousehold.getMyVillage(), listofmembers, null);
        myHousehold.getMyVillage().getCounters().incNewHouseholdNb(1);
        myHousehold.getMyVillage().getMyHouseholds().add(newHsh);
        if (parent.getResidence().isTransit()) {
            newHsh.setTransitResidence();
        } else {
            newHsh.setResidence(parent.getResidence());
        }
        this.myHousehold = newHsh;
        parent.updateHshFeaturesWhenChildLeaving();
    }

    /**
     * update the status of the individuals regarding its age ; it enters on the
     * labour market if it is ageToEnterOnTheLabourMarket; it quits the labour
     * market if it is ageToGoOnRetirement the age is assessed via the parameter
     * S. Huet 6.04.09 Modified SH 12.02.2010
     */
    private void updateStatusDoToAge(Municipality vil) {
        if (getAge() == getAgeToEnterOnTheLabourMarket()) { // enter on the labour market
            if (getStatus() == Status.STUDENT) { // doesn't change the individual who are at the beginning already worker before the age to enter on the labour market
                setStatus(Status.UNEMPLOYED);
                getMyHousehold().getMyVillage().getMyRegion().labourMarketEnter++;
                takeAnActivPattern(vil.getMyRegion().getAllPatterns()[0], vil);
                // If the individual is a farmer and one of its parents is a farmer, then it is susceptible to take a farmer employment directly
                // if an employment is available
                if (getProfession() == 0) {
                    setLookForJob(true);
                    setSearchedProf(getProfession());
                    seekJob();
                }
            }
        } else {
            if (getAge() >= getAgeToGoOnRetirement()) {
                if (getStatus() != Status.RETIRED) {
                    if (getMyActivityPattern() != null) {
                        leaveMyActivPattern();
                    } // at the initialisation time, the individual has no activity pattern
                    setStatus(Status.RETIRED);
                    getMyHousehold().getMyVillage().getMyRegion().retirementEnter++;
                    setNextActPat(null);
                }
            }
        }
    }

    /**
     * init the status of the individuals regarding its age S. Huet 31.07.09
     * Modified SH 12.02.2010
     */
    private void initStatusDoToAge() {
        if (getAge() < getAgeToEnterOnTheLabourMarket()) {
            setStatus(Status.STUDENT);
            setMyActivityPattern(null);
        } else {
            if (getAge() >= getAgeToGoOnRetirement()) {
                setStatus(Status.RETIRED);
                setMyActivityPattern(null);
            } else { // all adult are unemployed at the beginning
                setStatus(Status.UNEMPLOYED);
            }
        }
    }

    /**
     * Utility method for get an age from a Parameter after a coherence check
     * during the initialization, with an exception for the normal law.
     *
     * @param valueParameter
     * @return
     */
    private int getAgeAfterInitializationCoherenceChecking(Value<Integer, Individual> valueParameter) {
        int value = 0;
        int b = 50;
        int c = 0;
        if (valueParameter instanceof IntValueFromNormalLaw) {
            // specific case for normal distribution to be sure to get a value
            IntValueFromNormalLaw valueFromNormalLaw = (IntValueFromNormalLaw) valueParameter;
            value = valueFromNormalLaw.getValue(this);
            // After this upper bound, we have no chance to find a suitable age
            double max = valueFromNormalLaw.getMean() + (2 * valueFromNormalLaw.getStdDev());
            if (getAge() <= max) {
                while (value <= getAge()) {
                    value = valueFromNormalLaw.getValue(this);
                }
            } else {
                // this individual is too old for fitting in the distribution. Let's retire now!
                value = getAge() + 1;
            }
        } else {
            value = valueParameter.getValue(this);
            while (value <= getAge()) {
                value = valueParameter.getValue(this);
                c++;
                if (c == b) {
                    value = getAge() + 1;
                    break;
                }
            }
        }
        return value;
    }

    /**
     * init the age the individual is going to enter on the labour market
     * (looking for a job)
     */
    public void initAgeToEnterOnLabourMarket() {
        setAgeToEnterOnTheLabourMarket(getParameters().getAgeEnterLabourMarket(this));
    }

    /**
     * This method compute again the age of some events to take into account the
     * "real" status at the initialisation time of the individual
     */
    public void updateAgeLabourMarket() {
        setAgeToEnterOnTheLabourMarket(getAgeAfterInitializationCoherenceChecking(getParameters().getAgeEnterLabourMarket()));
    }

    /**
     * init the age to go on retirement
     */
    public void initAgeToGoOnRetirement() {
        setAgeToGoOnRetirement(getParameters().getAgeToRetire(this));
    }

    /**
     * This method compute again the age of some events to take into account the
     * "real" status at the initialisation time of the individual
     */
    public void updateAgeRetirement() {
        setAgeToGoOnRetirement(getAgeAfterInitializationCoherenceChecking(getParameters().getAgeToRetire()));
    }

    /**
     * init the age to die S. Huet 15.11.2010
     */
    public void initAgeToDie() {
        if (getParameters().getAgeToDie() instanceof IntValueFromExponentialLaw) {
            if (ExponentialDistFromMean.density(((IntValueFromExponentialLaw) getParameters().getAgeToDie()).getMean(), getAge()) < 0.003) {
                setAgeToDie(getAge() + 1);
                return;
            }
        }
        do {
            setAgeToDie(getParameters().getAgeToDie(this));
            //if (getAgeToDie()==getAge()) System.out.println("IL MEURT CETTE ANNEE "+getAge()+" age to die "+getAgeToDie()+" transit "+getMyHousehold().getResidence().isTransit()) ;
        } while (getAgeToDie() < getAge());
    }

    /**
     * Attribute a first profession to the individual just entering on the
     * labour market S. Huet 20.10.2010
     */
    public void affectAFirstProfession() {
        setProfession(getParameters().getFirstProfession(this));
    }

    /**
     * Method looking at the next job state of the individual depending on the
     * only transition matrix to decide about the individual labour situation
     * depending on the current one (see the method lookAtTheLabourSituation()
     * in Household to have more detail Changed by S. Huet 21.01.2010 Changed S.
     * Huet 25.02.2010
     */
    public boolean pickTheNextJobSituation(Municipality vil, float[] transitionMatrix, ActivityPattern[] allpattern) {
        boolean lookActPat = false;
        int pos = vil.getMyRegion().getNbOfUnactiveSituations() + vil.getMyRegion().getNbOfUnemployedSituations();
        int chosen = getMyHousehold().getRandom().nextIndexWithDistribution(transitionMatrix);
        int newPatt = chosen - pos + 1;
        this.willBeInactive = false;
        // If the next state does not immediately require a place to practice it (as a work), the individual is immediately updated with it
        switch (getStatus()) {
            case INACTIVE:
                if (chosen < vil.getMyRegion().getNbOfUnactiveSituations()) { // the individual becomes unactive
                    becomeUnactive(vil);
                } else { // the individual becomes unemployed and then it is directly going to look for an available job
                    becomeUnemployed(vil);
                    lookActPat = true;
                }
                break;
            case UNEMPLOYED:
                if (chosen < vil.getMyRegion().getNbOfUnactiveSituations()) { // the individual becomes unactive
                    leaveMyActivPattern();
                    becomeUnactive(vil);
                } else { // the individual remains unemployed and then it is directly going to look for an available job
                    lookActPat = true;
                }
                break;
            case WORKER:
                if (chosen < vil.getMyRegion().getNbOfUnactiveSituations()) { // the individual becomes unactive
                    leaveMyActivPattern();
                    becomeUnactive(vil);
                } else { // the individual remains unemployed and then it is directly going to look for an available job
                    if (chosen >= pos) { // the individual tries to change of active activity pattern
                        if (allpattern[newPatt] != getMyActivityPattern()) {
                            setNextActPat(allpattern[newPatt]);
                            lookActPat = true;
                        }
                    }
                }
                break;
        }
        return lookActPat;
    }

    /**
     * Method looking at the next job state of the individual depending on the
     * only transition matrix to decide about the individual labour situation
     * depending on the current one (see the method lookAtTheLabourSituation()
     * in Household to have more detail Changed by S. Huet 21.01.2010 Changed S.
     * Huet 25.02.2010
     */
    public void pickTheNextJobSituationNew(Municipality vil) {
        this.willBeInactive = false;
        setLookForJob(false);
        setSearchedProf(-1);
        // If the next state does not immediately require a place to practice it (as a work), the individual is immediately updated with it
        //try {
        switch (getStatus()) {
            case INACTIVE:
                if (getParameters().remainInactive(this)) {
                    // the individual becomes unactive
                    becomeUnactive(vil);
                } else { // the individual becomes unemployed and then it is directly going to look for an available job
                    // If the individual is a farmer, it can directly be a farmer again if an employment is available (without being an unemployed)
                    // That is done because a spouse who stopped farming for taking care of children can be a farmer again directly when she stops
                    // if the husband is still a farmer
                    if (getProfession() == 0) {
                        setLookForJob(true);
                        setSearchedProf(getProfession());
                        jobLocation = getMyHousehold().getMyVillage();
                        seekJob();
                        if (getStatus() == Status.INACTIVE) { // if he did'nt find a job
                            becomeUnemployed(vil);
                            setSearchedProf(getParameters().getProfIfUnactive(this));
                        }
                    } else {
                        becomeUnemployed(vil);
                        getMyHousehold().getMyVillage().getMyRegion().inactivityOut++;
                        setSearchedProf(getParameters().getProfIfUnactive(this));
                    }
                }
                break;
            case UNEMPLOYED:
                if (getParameters().isInactiveAfterUnemployment(this)) {
                    // the individual becomes unactive
                    leaveMyActivPattern();
                    becomeUnactive(vil);
                    getMyHousehold().getMyVillage().getMyRegion().inactivityEntry++;
                } else { // the individual remains unemployed and then it is directly going to look for an available job
                    if (getMyHousehold().getResidence().isTransit()) {
                        setSearchedProf(getProfession());
                    } else {
                        setSearchedProf(getParameters().getProfIfUnactive(this));
                    }
                }
                break;
            case WORKER:
                if (getParameters().isInactiveAfterEmployment(this)) {
                    // the individual becomes unactive
                    leaveMyActivPattern();
                    becomeUnactive(vil);
                    getMyHousehold().getMyVillage().getMyRegion().inactivityEntry++;
                } else {
                    setSearchedProf(getParameters().getProfIfWorking(this));
                }
                break;
        }
        //}
        /*
         catch(Exception e) {
         System.err.println("e "+e) ;
         System.err.println(" A indiv " + id + " vil " + vil.getName() + " ma ville " + getMyHousehold().getMyVillage().getName() + " status " + getStatus() + " place of work " + getJobLocation() + " activ pat " + getMyActivityPattern()+" hsh "+getMyHousehold());
         System.err.println(" age labour "+getAgeToEnterOnTheLabourMarket()+" age retired "+getAgeToGoOnRetirement()) ;
         //System.exit(0) ;
         }
         */
        if (getSearchedProf() > -1) {
            setLookForJob(true);
        }
    }

    /**
     * Method making the individual unactive S. Huet, 21.01.2010
     */
    public void becomeUnactive(Municipality vil) {
        // update the statut and the place of work (that is his own municipality in this case)
        setJobLocation(vil);
        setStatus(Status.INACTIVE);
        setNextActPat(null);
    }

    /**
     * Method making the individual unemployed S. Huet, 21.01.2010
     */
    public void becomeUnemployed(Municipality vil) {
        getMyHousehold().getMyVillage().getMyRegion().getCounters().incBecomeUnemploy(1);
        // update the statut and the place of work (that is his own municipality in this case)
        setJobLocation(vil);
        setStatus(Status.UNEMPLOYED);
        setNextActPat(null);
        takeAnActivPattern(getMyHousehold().getMyVillage().getMyRegion().getAllPatterns()[0], vil);
    }

    /**
     * An individual leaves a pattern and makes the corresponding activities
     * less occupied
     *
     * @param indicates the municipality where the activity pattern occupancy
     * will be decreased
     */
    public void leaveMyActivPattern() {
        setPreviousActPat(getMyActivityPattern());
        setPreviousJobLocation(getJobLocation());
        if (getStatus() == Status.WORKER) {
            getJobLocation().removeWorker(this);
            if (getJobLocation() == getMyHousehold().getMyVillage()) {
                getMyHousehold().getMyVillage().decreaseOccupiedActivityRes(getMyActivityPattern().getJob());
            } else {
                getJobLocation().decreaseOccupiedActivityExt(getMyActivityPattern().getJob());
            }
            getMyHousehold().getMyVillage().getMyRegion().getLabourOffice().updateAvailabilities(getMyActivityPattern().getJob(), getJobLocation());
        }
        setMyActivityPattern(null);
    }

    /**
     * An individual takes a pattern, makes the corresponding activities more
     * occupied and update the job location.
     */
    public void takeAnActivPattern(ActivityPattern myPat, Municipality place) {
        if (getPreviousActPat() != null) {
            getMyHousehold().getMyVillage().getMyRegion().getCounters().incStopBeingUnemployed(1);
        }
        setPreviousActPat(null);
        setPreviousJobLocation(null);
        int prof = 0;
        if (getStatus() == Status.WORKER || getStatus() == Status.UNEMPLOYED || getStatus() == Status.INACTIVE) {
            setMyActivityPattern(myPat);
            jobLocation = place;
            if (myPat == null && getStatus() == Status.UNEMPLOYED) {
                myPat = getMyHousehold().getMyVillage().getMyRegion().getAllPatterns()[0];
                setMyActivityPattern(getMyHousehold().getMyVillage().getMyRegion().getAllPatterns()[0]);
            }
            if (myPat.getJob() == -1) {
                setStatus(Status.UNEMPLOYED);
            } else {
                setStatus(Status.WORKER);
                place.addWorker(this);
                // Update() the profession attribute of the individual
                prof = getMyHousehold().getMyVillage().getMyRegion().getRegionalActivities()[getMyActivityPattern().getJob()].getProfession();
                // Update() the counters of occupied activities
                if (place == getMyHousehold().getMyVillage()) {
                    place.increaseOccupiedActivityRes(getMyActivityPattern().getJob());
                } else {
                    place.increaseOccupiedActivityExt(getMyActivityPattern().getJob());
                }
                getMyHousehold().getMyVillage().getMyRegion().getLabourOffice().updateAvailabilities(getMyActivityPattern().getJob(), place);
                // Change the profession only if the individual is now a worker
                setProfession(prof);
            }
        }
        if (!isAdult() && myPat.getIndex() != 0) {
            becomeAdult();
        }
    }

    /**
     * Method allowing to find a job to the individual who has been fired at the
     * initialisation time due to the lack of correspondancy between the offer
     * and the job affectation in the initial population
     */
    public boolean findJob() {
        boolean found = false;
        //int trials = 0;
        //while (!found && trials < 500) {
        found = seekJobInitTime();
        //trials++;
        //}
        //if (!found && getRandom().nextDouble() <= getMyHousehold().getMyVillage().getCommutOutsideProba()) {
        if (!found) {
            List<LabourOffice.JobProposition> myJob = getMyHousehold().getMyVillage().getMyRegion().getLabourOffice().getOfferInOutside(getSearchedProf());
            prepareTakeJobInit(myJob);
            found = true;
        }
        return found;
    }

    /**
     * Method allowing the individual to search for a job - the boolean init
     * time is to replace the initially fired commuters to a possible offered
     * job - they have to find a job in another municipalityn than their
     * residence place since they are commuters by definition (controlled
     * variables) at the initial time - based on the ancient method (see
     * comments for seekJobComplex)
     */
    public boolean seekJobInitTimeOld() {
        boolean found = false;
        int trial = 0;
        int trialMax = 1;
        List<SubNetwork> testedNet = new ArrayList<SubNetwork>();
        // TODO : make the max number of trials (ie trialMax) a "real" parameter of the model
        List<LabourOffice.JobProposition> myJob = new ArrayList<LabourOffice.JobProposition>();
        LabourOffice labourOffice = getMyHousehold().getMyVillage().getMyRegion().getLabourOffice();
        //int trialMax = labourOffice.getNetwork().getSubNetwork(getMyHousehold().getMyVillage()).size();
        while (trial < trialMax && !found) {
            trial++;
            labourOffice.getNetwork().getSubNetwork(getMyHousehold().getMyVillage()).size();
            SubNetwork nextSubNetwork = labourOffice.getNetwork().nextSubNetwork(getMyHousehold().getMyVillage());
            while (testedNet.contains(nextSubNetwork)) {
                nextSubNetwork = labourOffice.getNetwork().nextSubNetwork(getMyHousehold().getMyVillage());
            }
            testedNet.add(nextSubNetwork);
            // we have picked out the first distance class, and now we're looking only in the current mun
            // explique le décrochage
            myJob = labourOffice.getLocalMunicipalitiesForProfession(nextSubNetwork, getSearchedProf());
            if (myJob.size() > 0) {
                found = true;
            }
        }
        // local search
        if (found) {
            found = prepareTakeJobInit(myJob);
            if (!found) {
                setNextActPat(null);
            }
        }
        return found;
    }

    //based on the ancient method (see comments for seekJobComplex)
    public boolean prepareTakeJobInit(List<LabourOffice.JobProposition> myJob) {
        boolean found = true;
        int chosen = getMyHousehold().getRandom().nextInt(0, myJob.size() - 1);
        if (myJob.get(chosen).getJobsLocations()[0] == getMyHousehold().getMyVillage()) {
            found = false;
        } else {
            setNextActPat(myJob.get(chosen).getPattern());
            leaveMyActivPattern();
            takeAnActivPattern(getNextActPat(), myJob.get(chosen).getJobsLocations()[0]);
            // check if she has became a far commuter
            if (getStatus() == Status.WORKER) {
                workFar(getMyHousehold().getMyVillage(), getJobLocation());
            }
            if (isFarCommuter()) {
                getMyHousehold().setLeader(this);
                getMyHousehold().setNeedOfResidence(true);
            }
        }
        if (found) {
            setLookForJob(false);
        }
        return found;
    }

    /**
     * Method allowing the individual to search for a job based on an acceptable
     * distance picked out following a distribution law depending on the type of
     * the village of residence (Mamourou and Lenormand's study, 2011) - old
     * method
     */
    public boolean seekJobComplex() {
        boolean found = false;
        boolean foundInTheMunOfRes = false;
        int trial = 0;
        int trialMax = 2;
        List<SubNetwork> testedNet = new ArrayList<SubNetwork>();
        // TODO : make the max number of trials (ie trialMax) a "real" parameter of the model
        List<LabourOffice.JobProposition> myJob = new ArrayList<LabourOffice.JobProposition>();
        LabourOffice labourOffice = getMyHousehold().getMyVillage().getMyRegion().getLabourOffice();
        int chosen = -1;
        while (trial < trialMax && !found) {
            trial++;
            SubNetwork nextSubNetwork = labourOffice.getNetwork().nextSubNetwork(getMyHousehold().getMyVillage());
            while (testedNet.contains(nextSubNetwork)) {
                nextSubNetwork = labourOffice.getNetwork().nextSubNetwork(getMyHousehold().getMyVillage());
            }
            testedNet.add(nextSubNetwork);
            if (nextSubNetwork.getDistance() == labourOffice.getNetwork().getClassWidth()) {
                // we have picked out the first distance class, and now we're looking only in the current mun
                // explique le décrochage
                //myJob = labourOffice.getJobPropositionsInMunicipality(getMyHousehold().getMyVillage(), getSearchedProf());
                myJob = labourOffice.getLocalMunicipalitiesForProfession(nextSubNetwork, getSearchedProf());
                if (myJob.size() > 0) {
                    found = true;
                    chosen = getMyHousehold().getRandom().nextInt(0, myJob.size() - 1);
                    if (myJob.get(chosen).getJobsLocations()[0] == getMyHousehold().getMyVillage()) {
                        foundInTheMunOfRes = true;
                    }
                }
                //if (!found) {
                //myJob = labourOffice.getLocalMunicipalitiesForProfession(nextSubNetwork, getSearchedProf());
                //}
                //if (myJob.size() > 0) {
                //found = true;
                //}
            } else {
                //if (getRandom().nextDouble() <= getMyHousehold().getMyVillage().getCommutOutsideProba()) {
                //myJob = labourOffice.getOfferInOutside(getSearchedProf());
                //} else {
                myJob = labourOffice.getLocalMunicipalitiesForProfession(nextSubNetwork, getSearchedProf());
                if (myJob.size() > 0) {
                    found = true;
                }
                //}
            }
        }
        if (!foundInTheMunOfRes && getRandom().nextDouble() <= getMyHousehold().getMyVillage().getCommutOutsideProba()) {
            myJob = labourOffice.getOfferInOutside(getSearchedProf());
        }
        // local search
        if (found) {
            if (!foundInTheMunOfRes) {
                chosen = getMyHousehold().getRandom().nextInt(0, myJob.size() - 1);
            }
            setNextActPat(myJob.get(chosen).getPattern());
            leaveMyActivPattern();
            takeAnActivPattern(getNextActPat(), myJob.get(chosen).getJobsLocations()[0]);
            // check if she has became a far commuter
            if (getStatus() == Status.WORKER) {
                workFar(getMyHousehold().getMyVillage(), getJobLocation());
            }
            if (isFarCommuter()) {
                getMyHousehold().setLeader(this);
                getMyHousehold().setNeedOfResidence(true);
            }
        } else {
            setNextActPat(null);
        }
        setLookForJob(false);
        return found;
    }

    /**
     * Method allowing the individual to search for a job from closer to further
     * from the village of residence
     */
    public boolean seekJobInitTime() {
        double probaRefuse = 0.0;
        boolean found = false;
        boolean foundInTheMunOfRes = false;
        List<LabourOffice.JobProposition> myJob = new ArrayList<LabourOffice.JobProposition>();
        LabourOffice labourOffice = getMyHousehold().getMyVillage().getMyRegion().getLabourOffice();
        int chosen = -1;
        // the individual is going to look for a job starting from the closer villages to the further, it stops when it founds a job
        SortedMap<Double, SubNetwork> subNetworks = labourOffice.getNetwork().getSubNetwork(getMyHousehold().getMyVillage());
        // save the keys, ordered them in descending order (they are in ascending one at the beginning)
        int nbKeys = subNetworks.size();
        Double[] probas = new Double[nbKeys];
        for (Map.Entry<Double, SubNetwork> entry : subNetworks.entrySet()) {
            nbKeys--;
            probas[nbKeys] = (entry.getKey()).doubleValue();
        }
        nbKeys = subNetworks.size();
        for (int i = 0; i < nbKeys; i++) {
            SubNetwork nextSubNetwork = subNetworks.get(probas[i]);
            foundInTheMunOfRes = false;
            myJob = labourOffice.getLocalMunicipalitiesForProfession(nextSubNetwork, getSearchedProf());
            if (nextSubNetwork.getDistance() == labourOffice.getNetwork().getClassWidth()) {
                // we have picked out the first distance class, and now we're looking only in the current mun
                if (myJob.size() > 0) {
                    // it accepts the job following the distribution of acceptance of the distance
                    if (getRandom().nextDouble() <= probaRefuse) {
                        found = false;
                    } else {
                        found = true;
                        chosen = getMyHousehold().getRandom().nextInt(0, myJob.size() - 1);
                        if (myJob.get(chosen).getJobsLocations()[0] == getMyHousehold().getMyVillage()) {
                            foundInTheMunOfRes = true;
                            found = false;
                        } else { // if it has not found out in its own municipalities, it has to decide if it commutes outside the region
                            if (getRandom().nextDouble() <= getMyHousehold().getMyVillage().getCommutOutsideProba()) {
                                myJob = labourOffice.getOfferInOutside(getSearchedProf());
                            }
                        }
                    }
                }
            } else {
                if (myJob.size() > 0) {
                    // it accepts the job following the distribution of acceptance of the distance
                    if (getRandom().nextDouble() <= probaRefuse) {
                        found = false;
                    } else {
                        found = true;
                    }
                }
            }
            if (found) {
                break;
            }
            //probaRefuse = probaRefuse + (entry.getKey()).doubleValue();
            // proba Refuse remains at 0 since it is at the beginning and commuters (already decide they do not work in their residence place has to find a place for work)
        }
        // in case it does not work in its own village, it can decide to commute outside whatever it has found or not a job inside
        if (!foundInTheMunOfRes && getRandom().nextDouble() <= getMyHousehold().getMyVillage().getCommutOutsideProba()) {
            myJob = labourOffice.getOfferInOutside(getSearchedProf());
        }
        // choice of the village at the selected distance
        if (found) {
            if (!foundInTheMunOfRes) {
                chosen = getMyHousehold().getRandom().nextInt(0, myJob.size() - 1);
                setNextActPat(myJob.get(chosen).getPattern());
                leaveMyActivPattern();
                takeAnActivPattern(getNextActPat(), myJob.get(chosen).getJobsLocations()[0]);
                ///*
                // check if she has became a far commuter // if far commuter at the beginning, remains far
                if (getStatus() == Status.WORKER) {
                    workFar(getMyHousehold().getMyVillage(), getJobLocation());
                }
                if (isFarCommuter()) {
                    getMyHousehold().setLeader(this);
                    getMyHousehold().setNeedOfResidence(true);
                }
            }
        } else {
            setNextActPat(null);
        }
        setLookForJob(false);
        return found;
    }

    /**
     * Method allowing the individual to search for a job from closer to further
     * from the village of residence
     */
    public boolean seekJob() {
        double probaRefuse = 0.0;
        boolean found = false;
        boolean foundInTheMunOfRes = false;
        List<LabourOffice.JobProposition> myJob = new ArrayList<LabourOffice.JobProposition>();
        LabourOffice labourOffice = getMyHousehold().getMyVillage().getMyRegion().getLabourOffice();
        int chosen = -1;
        // the individual is going to look for a job starting from the closer villages to the further, it stops when it founds a job
        SortedMap<Double, SubNetwork> subNetworks = labourOffice.getNetwork().getSubNetwork(getMyHousehold().getMyVillage());
        // save the keys, ordered them in descending order (they are in ascending one at the beginning)
        int nbKeys = subNetworks.size();
        Double[] probas = new Double[nbKeys];
        for (Map.Entry<Double, SubNetwork> entry : subNetworks.entrySet()) {
            nbKeys--;
            probas[nbKeys] = (entry.getKey()).doubleValue();
        }
        nbKeys = subNetworks.size();
        for (int i = 0; i < nbKeys; i++) {
            SubNetwork nextSubNetwork = subNetworks.get(probas[i]);
            if (nextSubNetwork.getDistance() == labourOffice.getNetwork().getClassWidth()) {
                // we have picked out the first distance class, and now we're looking only in the current mun
                myJob = labourOffice.getLocalMunicipalitiesForProfession(nextSubNetwork, getSearchedProf());
                if (myJob.size() > 0) {
                    // it accepts the job following the distribution of acceptance of the distance
                    if (getRandom().nextDouble() <= probaRefuse) {
                        found = false;
                    } else {
                        found = true;
                        chosen = getMyHousehold().getRandom().nextInt(0, myJob.size() - 1);
                        if (myJob.get(chosen).getJobsLocations()[0] == getMyHousehold().getMyVillage()) {
                            foundInTheMunOfRes = true;
                        }
                    }
                }
            } else {
                myJob = labourOffice.getLocalMunicipalitiesForProfession(nextSubNetwork, getSearchedProf());
                if (myJob.size() > 0) {
                    // it accepts the job following the distribution of acceptance of the distance
                    if (getRandom().nextDouble() <= probaRefuse) {
                        found = false;
                    } else {
                        found = true;
                    }
                }
            }
            if (found) {
                break;
            }
            //probaRefuse = probaRefuse + (entry.getKey()).doubleValue();
        }
        // in case it does not work in its own village, it can decide to commute outside whatever it has found or not a job inside
        if (!foundInTheMunOfRes && getRandom().nextDouble() <= getMyHousehold().getMyVillage().getCommutOutsideProba() && found) {
            myJob = labourOffice.getOfferInOutside(getSearchedProf());
            found = true;
        }
        // choice of the village at the selected distance
        if (found) {
            if (!foundInTheMunOfRes) {
                chosen = getMyHousehold().getRandom().nextInt(0, myJob.size() - 1);
            }
            setNextActPat(myJob.get(chosen).getPattern());
            leaveMyActivPattern();
            takeAnActivPattern(getNextActPat(), myJob.get(chosen).getJobsLocations()[0]);
            // check if she has became a far commuter
            if (getStatus() == Status.WORKER) {
                workFar(getMyHousehold().getMyVillage(), getJobLocation());
            }
            if (isFarCommuter()) {
                getMyHousehold().setLeader(this);
                getMyHousehold().setNeedOfResidence(true);
            }
        } else {
            setNextActPat(null);
        }
        setLookForJob(false);
        if (!found) {
            getMyHousehold().getMyVillage().getMyRegion().getCounters().incNbNotFindJob(1);
        } else {
            getMyHousehold().getMyVillage().getMyRegion().getCounters().incNbFindJob(1);
        }
        return found;
    }

    /**
     * Check if an individual has became a far commuter
     */
    public void workFar(Municipality M1, Municipality M2) {
        if (!M1.isOutside() && !M2.isOutside()) {
            /*
             if (M1.euclidianDistanceFrom(M2) > (getParameters().getFarCloseThreshold()) * 1000.0) {
             setFarCommuter(true);
             }
             * 
             */
            if (M1.distanceFrom(M2) > (getParameters().getFarCloseThreshold())) {
                setFarCommuter(true);
            }
        }
    }
}
