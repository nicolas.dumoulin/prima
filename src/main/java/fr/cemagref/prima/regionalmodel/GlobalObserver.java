/*
 * Copyright (C) 2012 dumoulin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObservablesHandler.ObservableFetcher;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.ohoui.annotations.SkipField;
import fr.cemagref.prima.regionalmodel.parameters.ConfiguredByFiles;
import fr.cemagref.prima.regionalmodel.parameters.Parameters;
import fr.cemagref.prima.regionalmodel.tools.ReflectUtils;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.NotImplementedException;

/**
 * This observer allows to print one line for each timestep with:
 *  <ul><li>a set of input parameters (option <tt>parametersToWrite</tt>). If null, all parameters are printed
 *  <li>the date
 *  <li>the values of a set of observables at the given timestep (options <tt>regularObservables</tt>)
 *  <li>the cumulated values of a set of observables at the given timestep since the last printed timestep (options <tt>cumulativeObservables</tt>)
 *  </ul>
 *
 * Other options are:
 *  <ul><li><tt>outputFile</tt> the file in which the output will be written, or System.out if null.
 *  <ul><li><tt>handler</tt> the class containing the observables
 *  <li><tt>dates</tt> the dates at which the observables should be printed. If null, they will be printed at each timestep.
 * @author dumoulin
 */
public class GlobalObserver extends ObserverListener {

    private static final Logger LOGGER = Logger.getLogger(GlobalObserver.class.getName());
    protected char separator = ';';
    protected boolean append;
    private File outputFile;
    private Class handler;
    @Description(name = "parameters to write", tooltip = "List of parameters that will be wrotten at each time step. If null, all parameters are printed.")
    private List<String> parametersToWrite;
    private List<String> regularObservables;
    protected transient List<ObservablesHandler.ObservableFetcher> regularFetchers;
    protected transient Map<ObservablesHandler.ObservableFetcher, int[]> fetchersSizes;
    protected transient Map<ObservablesHandler.ObservableFetcher, List<String>[]> fetchersLabels;
    private List<String> cumulativeObservables;
    protected transient List<ObservablesHandler.ObservableFetcher> cumulativeFetchers;
    private transient List<Integer> cumulativeValues;
    private List<Integer> dates;
    protected transient PrintStream outputStream = System.out;
    private transient StringBuilder parametersHeader;
    private transient StringBuilder parametersValues;

    public Class getHandler() {
        return handler;
    }

    public File getOutputFile() {
        return outputFile;
    }

    @Override
    public void init() {
        // nothing to do, we need to retrieve parameters in init(Parameters) below
    }

    private List<ObservablesHandler.ObservableFetcher> initFetchers(MunicipalitySet region, List<String> names) {
        List<ObservablesHandler.ObservableFetcher> fetchers = new ArrayList<ObservablesHandler.ObservableFetcher>();
        if (names != null) {
            for (String name : names) {
                ObservableFetcher fetcher = region.getMyApplication().getObservableManager().addObservable(handler).getObservableFetcherByName(name);
                if (fetcher != null) {
                    try {
                        fetchersSizes.put(fetcher, fetcher.getSizes(region.getCounters()));
                        fetchersLabels.put(fetcher, fetcher.getLabels(region.getCounters()));
                        fetchers.add(fetcher);
                    } catch (Exception ex) {
                        LOGGER.log(Level.SEVERE, "An error has occured during introspection for the observer: " + fetcher.getDeclaredName(), ex);
                    }
                } else {
                    LOGGER.log(Level.WARNING, "No suitable observable founded with the name: {0}", name);
                }
            }
        }
        return fetchers;
    }

    public void init(MunicipalitySet region, Parameters parameters) {
        try {
            File outputDir = outputFile.getParentFile();
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            // it's rather better to use a buffetred outputstream, and to disable to default buffer of the PrintStream
            outputStream = new PrintStream(new BufferedOutputStream(new FileOutputStream(outputFile, append), 1024), false);
            LOGGER.log(Level.FINE, "Some output will be wroten in {0}", outputFile.getAbsolutePath());
        } catch (FileNotFoundException e) {
            LOGGER.log(Level.WARNING, null, e);
            outputStream = System.out;
        }
        // retrieving fetchers
        if (handler != null) {
            ObservablesHandler observablesHandler = region.getMyApplication().getObservableManager().addObservable(handler);
            observablesHandler.addObserverListener(this);
            fetchersSizes = new HashMap<ObservableFetcher, int[]>();
            fetchersLabels = new HashMap<ObservableFetcher, List<String>[]>();
            regularFetchers = initFetchers(region, regularObservables);
            cumulativeFetchers = initFetchers(region, cumulativeObservables);
            cumulativeValues = new ArrayList<Integer>();
            for (ObservableFetcher fetcher : cumulativeFetchers) {
                int[] sizes = fetchersSizes.get(fetcher);
                if (sizes.length == 0) {
                    cumulativeValues.add(0);
                } else if (sizes.length == 1) {
                    for (int i = 0; i < sizes[0]; i++) {
                        cumulativeValues.add(0);
                    }
                } else {
                    throw new NotImplementedException("Only implemented for scalar and vector observables.");
                }
            }
            for (int i = 0; i < cumulativeFetchers.size(); i++) {
            }
        }
        // storing parameters
        parametersHeader = new StringBuilder();
        parametersValues = new StringBuilder();
        if (parametersToWrite == null) {
            // write all parameters
            for (Field field : Parameters.class.getDeclaredFields()) {
                if (field.isAnnotationPresent(SkipField.class)) {
                    continue;
                }
                if (!Modifier.isTransient(field.getModifiers())) {
                    Object fieldValue = ReflectUtils.getFieldValue(field, parameters);
                    String value = null;
                    if (fieldValue == null) {
                        value = "null";
                    } else if (ConfiguredByFiles.class.isAssignableFrom(fieldValue.getClass())) {
                        // TODO handle parameters configured by several files
                        value = ((ConfiguredByFiles) fieldValue).getFilenames().toString();
                    } else {
                        for (Class type : new Class[]{Integer.class, Double.class, String.class, double.class, int.class}) {
                            if (type.isAssignableFrom(field.getType())) {
                                value = fieldValue.toString();
                                break;
                            }
                        }
                    }
                    if (value != null) {
                        parametersHeader.append(field.getName()).append(separator);
                        parametersValues.append(value).append(separator);
                    } else {
                        for (Field f : fieldValue.getClass().getDeclaredFields()) {
                            if (!Modifier.isTransient(f.getModifiers())) {
                                Object subfieldValue = ReflectUtils.getFieldValue(f, fieldValue);
                                if (subfieldValue == null) {
                                    LOGGER.log(Level.WARNING, "The parameter {0}.{1} is null, and won''t be written in {2}", new Object[]{field.getName(), f.getName(), outputFile});
                                } else {
                                    parametersHeader.append(field.getName()).append(".").append(f.getName()).append(separator);
                                    parametersValues.append(subfieldValue).append(separator);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            // write only specified parameters
            for (String param : parametersToWrite) {
                try {
                    ReflectUtils.FieldOnInstance fieldOnInstance = ReflectUtils.getFieldOnInstance(parameters, param);
                    Object fieldValue = fieldOnInstance.getValue();
                    parametersHeader.append(param).append(separator);
                    parametersValues.append(fieldValue).append(separator);
                } catch (ReflectiveOperationException ex) {
                    LOGGER.log(Level.WARNING, "Error while trying to write the parameter {0} in {1}", new Object[]{param, outputFile});
                }

            }
        }
        if (!append) {
            // writing headers for parameters
            outputStream.print(parametersHeader.toString());
            outputStream.print("time");
            // Now, we print headers for the observables
            processFetcherHeaders(region, regularFetchers);
            processFetcherHeaders(region, cumulativeFetchers);
            outputStream.println();
            outputStream.flush();
        }
    }

    private void processFetcherHeaders(MunicipalitySet region, List<ObservablesHandler.ObservableFetcher> fetchers) {
        if (fetchers != null) {
            for (ObservablesHandler.ObservableFetcher fetcher : fetchers) {
                int[] sizes = fetchersSizes.get(fetcher);
                List<String>[] labels = fetchersLabels.get(fetcher);
                if (sizes.length == 0) {
                    outputStream.print(separator);
                    outputStream.print(fetcher.getDeclaredName());
                } else {
                    if (sizes.length > 1) {
                        throw new NotImplementedException("Only implemented for scalar and vector observables.");
                    } else {
                        for (String label : labels[0]) {
                            outputStream.print(separator);
                            outputStream.print(label);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void addObservable(ObservablesHandler classObservable) {
        // nothing to do, refresh not implemented
    }

    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long date) {
        if (dates == null || dates.contains((int) date)) {
            outputStream.print(parametersValues.toString());
            outputStream.print(date);
            for (ObservablesHandler.ObservableFetcher fetcher : regularFetchers) {
                Object value = fetcher.fetchValueOrNull(instance);
                int[] sizes = fetchersSizes.get(fetcher);
                if (sizes.length == 1) {
                    for (int i = 0; i < sizes[0]; i++) {
                        outputStream.print(separator);
                        try {
                            outputStream.print(Array.get(value, i));
                        } catch (ArrayIndexOutOfBoundsException e) {
                            LOGGER.log(Level.SEVERE, "Error while retrieving observable " + fetcher.getDeclaredName() + " (array) in " + handler + " at index " + i, e);
                            outputStream.print("N/A");
                        }
                    }
                } else {
                    outputStream.print(separator);
                    outputStream.print(value);
                }
            }
            int valuesIndex = 0;
            for (int i = 0; i < cumulativeFetchers.size(); i++) {
                ObservablesHandler.ObservableFetcher fetcher = cumulativeFetchers.get(i);
                Object value = fetcher.fetchValueOrNull(instance);
                int[] sizes = fetchersSizes.get(fetcher);
                if (sizes.length == 0) {
                    outputStream.print(separator);
                    outputStream.print(cumulativeValues.get(valuesIndex) + (Integer) value);
                    cumulativeValues.set(valuesIndex, 0);
                    valuesIndex++;
                } else if (sizes.length == 1) {
                    for (int j = 0; j < sizes[0]; j++) {
                        outputStream.print(separator);
                        outputStream.print(cumulativeValues.get(valuesIndex) + ((int[]) value)[j]);
                        cumulativeValues.set(valuesIndex, 0);
                        valuesIndex++;
                    }
                } else {
                    throw new NotImplementedException("Only implemented for scalar and vector observables.");
                }
            }
            outputStream.println();
            outputStream.flush();
        } else {
            // TODO only if date < last date in the configured list
            int valuesIndex = 0;
            for (int i = 0; i < cumulativeFetchers.size(); i++) {
                ObservablesHandler.ObservableFetcher fetcher = cumulativeFetchers.get(i);
                Object value = fetcher.fetchValueOrNull(instance);
                int[] sizes = fetchersSizes.get(fetcher);
                if (sizes.length == 0) {
                    cumulativeValues.set(valuesIndex, cumulativeValues.get(valuesIndex) + (Integer) value);
                    valuesIndex++;
                } else if (sizes.length == 1) {
                    for (int j = 0; j < sizes[0]; j++) {
                        cumulativeValues.set(valuesIndex, cumulativeValues.get(valuesIndex) + ((int[]) value)[j]);
                        valuesIndex++;
                    }
                } else {
                    throw new NotImplementedException("Only implemented for scalar and vector observables.");
                }
            }
        }
    }

    @Override
    public void close() {
        outputStream.close();
    }
}
