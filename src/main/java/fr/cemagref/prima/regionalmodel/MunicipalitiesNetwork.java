/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class MunicipalitiesNetwork {

    public class SubNetwork {

        private double distance;
        private List<Municipality> municipalities;

        public SubNetwork(double distance, List<Municipality> municipalities) {
            this.distance = distance;
            this.municipalities = municipalities;
        }

        public double getDistance() {
            return distance;
        }

        public List<Municipality> getMunicipalities() {
            return municipalities;
        }
    }
    private final MunicipalitySet region;
    private final Map<Municipality, SortedMap<Double, SubNetwork>> probasByMunInNetwork;
    private final int classWidth;

    public MunicipalitiesNetwork(MunicipalitySet region, SortedMap<Integer, Double> probas) throws BadDataException {
        this.region = region;
        probasByMunInNetwork = new HashMap<Municipality, SortedMap<Double, SubNetwork>>();
        this.classWidth = probas.firstKey();
        Map<Municipality, SortedMap<Integer, List<Municipality>>> network = buildNetworkWithGivenRange(probas.firstKey(), probas.lastKey());
        for (Map.Entry<Municipality, SortedMap<Integer, List<Municipality>>> neighbours : network.entrySet()) {
            SortedMap<Double, SubNetwork> currentProbas = new TreeMap<Double, SubNetwork>();
            for (Map.Entry<Integer, Double> p : probas.entrySet()) {
                int distance = (int) Math.ceil(p.getKey());
                currentProbas.put(p.getValue(), new SubNetwork(distance, Utils.getValueFromUpperBound(neighbours.getValue(), distance)));
            }
            probasByMunInNetwork.put(neighbours.getKey(), currentProbas);
        }
    }

    public MunicipalitiesNetwork(MunicipalitySet region, String munNetworkClassesFilename, String munNetworkProbasFilename) throws BadDataException {
        this.region = region;
        if (munNetworkClassesFilename == null || munNetworkProbasFilename == null || region.getParameters().getMunicipalitiesDistances() == null) {
            Logger.getLogger(MunicipalitiesNetwork.class.getName()).warning("No input file found for municipalities network for job research, fallback to old mode");
            probasByMunInNetwork = null;
            classWidth = 0;
            return;
        }
        // init the network
        probasByMunInNetwork = new HashMap<Municipality, SortedMap<Double, SubNetwork>>();
        // loading municipalities classes
        final Map<Integer, Integer> munClasses = new HashMap<Integer, Integer>();
        try {
            CSVReader classesReader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(munNetworkClassesFilename), null);
            Integer[] line;
            while ((line = CSV.nextIntegersLine(classesReader)) != null) {
                munClasses.put(line[0], line[1]);
            }
            classesReader.close();
        } catch (Exception ex) {
            throw new BadDataException("error while loading municipalities classes", ex);
        }
        // loading distances classes probabilities
        SortedMap<Integer, Double[]> probas = new TreeMap<Integer, Double[]>();
        try {
            CSVReader probasReader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(munNetworkProbasFilename), null);
            String[] line;
            while ((line = CSV.readLine(probasReader)) != null) {
                probas.put(Integer.parseInt(line[0]), Utils.parseDoubleArray(line, 1));
            }
            this.classWidth = probas.firstKey();
            probasReader.close();
        } catch (Exception ex) {
            throw new BadDataException("error while loading municipalities probabilities", ex);
        }
        buildNetwork(munClasses, probas);
    }

    /**
     * Returns the width of each distance classes, that is also equal to the distance of the first class (upper bound).
     * @return 
     */
    public final int getClassWidth() {
        return classWidth;
    }

    /**
     * Build a network by grouping neighbours by distances classes. For each
     * municipality, a map contains the neighbours with:<ul>
     *  <li>the upper bound (inclusive) of the distance class as key</li>
     *  <li>the list of neighbours at this distance as value</li></ul>
     * @param rangesStep the width in kilometers of each class
     * @param max the maximum distance in kilometers to taken into account
     * @return
     * @throws BadDataException 
     */
    private Map<Municipality, SortedMap<Integer, List<Municipality>>> buildNetworkWithGivenRange(int rangesStep, int max) throws BadDataException {
        Map<Municipality, SortedMap<Integer, List<Municipality>>> network = new HashMap<Municipality, SortedMap<Integer, List<Municipality>>>();
        // loading the distances and building the network
        try {
            CSVReader distancesReader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(region.getParameters().getMunicipalitiesDistances()), null);
            String[] readLine = CSV.readLine(distancesReader);
            Integer[] munLabels = Utils.parseIntegerArray(readLine, 1);
            String[] line;
            // for each line of distances
            while ((line = CSV.readLine(distancesReader)) != null) {
                // storing the class of the current municipality
                SortedMap<Integer, List<Municipality>> neighbours = new TreeMap<Integer, List<Municipality>>();
                for (int i = rangesStep; i <= max; i += rangesStep) {
                    neighbours.put(i, new ArrayList<Municipality>());
                }
                // for each distance with each other municipality
                for (int i = 0; i < munLabels.length; i++) {
                    int distance = (int) Math.floor(Double.parseDouble(line[i + 1]));
                    if (distance <= max) {
                        Utils.getValueFromUpperBound(neighbours, distance).add(region.getMunicipality(munLabels[i].toString()));
                    }
                }
                network.put(region.getMunicipality(line[0]), neighbours);
            }
        } catch (Exception ex) {
            throw new BadDataException("error while building a municipalities network", ex);
        }
        return network;
    }

    private void buildNetwork(Map<Integer, Integer> munClasses, SortedMap<Integer, Double[]> probas) throws BadDataException {
        Map<Municipality, SortedMap<Integer, List<Municipality>>> network = buildNetworkWithGivenRange(probas.firstKey(), probas.lastKey());
        for (Map.Entry<Municipality, SortedMap<Integer, List<Municipality>>> neighbours : network.entrySet()) {
            int munClass = munClasses.get(Integer.parseInt(neighbours.getKey().getName()));
            SortedMap<Double, SubNetwork> currentProbas = new TreeMap<Double, SubNetwork>();
            for (Map.Entry<Integer, Double[]> p : probas.entrySet()) {
                int distance = (int) Math.ceil(p.getKey());
                currentProbas.put(p.getValue()[munClass - 1], new SubNetwork(distance, Utils.getValueFromUpperBound(neighbours.getValue(), distance)));
            }
            probasByMunInNetwork.put(neighbours.getKey(), currentProbas);
        }
    }

    public boolean isLoaded() {
        return probasByMunInNetwork != null;
    }

    public SortedMap<Double, SubNetwork> getSubNetwork(Municipality key) {
        return probasByMunInNetwork.get(key);
    }

    /**
     * Retrieve the network from the given municipality, pickup a distance and returns the list of neighbours concerned.
     * @param mun
     * @return 
     */
    public SubNetwork nextSubNetwork(Municipality mun) {
        return region.getMyApplication().getRandom().nextMapObjectWithDistributionInKeys(probasByMunInNetwork.get(mun));
    }

}
