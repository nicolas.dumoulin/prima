/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.parameters.MunicipalityParameters;
import fr.cemagref.prima.regionalmodel.parameters.Parameters;
import fr.cemagref.prima.regionalmodel.parameters.OutsideParameters;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.prima.regionalmodel.Individual.Status;
import fr.cemagref.prima.regionalmodel.MunicipalitiesNetwork.SubNetwork;
import fr.cemagref.prima.regionalmodel.scenarios.Scenario;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.Random;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class corresponding to the regional application defining attributes from the
 * region and managing the whole application by the main loop (iteration())
 *
 * @author Sylvie Huet <sylvie.huet@cemagref.fr>
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class MunicipalitySet {

    public int searchDistThreshold;

    public int getSearchDistThreshold() {
        return (int) (((float) getParameters().getFarCloseThreshold()) / (float) getLabourOffice().getNetwork().getClassWidth());
    }
    public int inactivityEntry = 0;
    public int inactivityOut = 0;
    public int labourMarketEnter = 0;
    public int retirementEnter = 0;
    public int[] emigrantStatus = new int[8]; // in order: farmers, craftmen, executives, interm.prof., employees, workers, retired, other inactives
    public int[] immigrantStatus = new int[8]; // in order: farmers, craftmen, executives, interm.prof., employees, workers, retired, other inactives
    public int[] emigrantAge = new int[8];
    public int[] immigrantAge = new int[8];

    public int getOutMigrantsLastStep() {
        return counters.getOutMigrantsLastStep();
    }
    private static final Logger LOGGER = Logger.getLogger(MunicipalitySet.class.getName());
    protected List<Scenario> scenarios;
    /**
     * Labour office of the region where each individuals can be informed about
     * the availability of the activity
     */
    private LabourOffice labourOffice;

    public int getNbActivSectors() {
        return parameters.getNbSectorsActiv();
    }

    public int getNbProfessions() {
        return parameters.getNbTypesBySectorActiv();
    }

    public Random getRandom() {
        return getMyApplication().getRandom();
    }

    public int getNbActivities() {
        return parameters.getNbActivities();
    }

    public LabourOffice getLabourOffice() {
        return labourOffice;
    }

    public int getNbOfUnactiveSituations() {
        return parameters.getNbOfUnactiveSituations();
    }

    public int getNbOfUnemployedSituations() {
        return parameters.getNbOfUnemployedSituations();
    }

    public double getProbaBirth() {
        return parameters.getProbaBirth();
    }

    public double getSplittingProbability() {
        return parameters.getSplittingProba();
    }

    public double getResSatisfactionMargin() {
        return parameters.getResSatisfacMargin();
    }
    /**
     * capacity of AvailRes give the number of room corresponding to a position
     * in the table availability of residence of the village
     */
    private int[] capacityOfAvailRes;

    public int[] getCapacityOfAvailRes() {
        return capacityOfAvailRes;
    }

    private void setCapacityOfAvailRes(int i, int val) {
        this.capacityOfAvailRes[i] = val;
    }

    public int getCapacityOfAvailRes(int i) {
        return capacityOfAvailRes[i];
    }

    public int getNbSizesResidence() {
        return parameters.getNbSizeRes();
    }

    public double getAvDiffAgeCouple() {
        return parameters.getAvDifAgeCouple();
    }

    public double getStDiffAgeCouple() {
        return parameters.getStDifAgeCouple();
    }

    public int getAgeMinToChild() {
        return parameters.getAgeMinHavingChild();
    }

    public int getAgeMaxToChild() {
        return parameters.getAgeMaxHavingChild();
    }

    public int getStartStep() {
        return parameters.getStartStep();
    }

    public int getNbIteration() {
        return parameters.getNbStep();
    }

    public int getNbMunicipalities() {
        return parameters.getMunicipalitiesParameters().size();
    }
    /**
     * Collection of activity patterns available in the region F.Gargiulo -
     * 15.04.2009
     */
    private ActivityPattern[] allPatterns;

    public ActivityPattern[] getAllPatterns() {
        return allPatterns;
    }
    /**
     * Collection of activity available in the region S. Huet - 24.04.2009
     */
    private Activity[] regionalActivities;

    public Activity[] getRegionalActivities() {
        return regionalActivities;
    }

    public int getIterationValue() {
        return parameters.getStep();
    }
    Application myApplication;

    public Application getMyApplication() {
        return myApplication;
    }

    public void setMyApplication(Application myApplication) {
        this.myApplication = myApplication;
    }

    /**
     * Collection of villages representing my region S. Huet - 7.04.2009
     */
    private List<Municipality> myMunicipalities;

    /**
     * Provides the municipalities of this region.
     *
     * @return the unmodifiable list of municipalities
     */
    public List<Municipality> getMyMunicipalities() {
        return myMunicipalities;
    }
    /**
     * Particular Municipality representing cities ou village outside the region
     * its resident are those working as external in the village of the rural
     * region and not living in its external workers are those living in the
     * rural region and not working in S. Huet - 7.10.2009
     */
    private List<Municipality> outsides;

    public List<Municipality> getOutsides() {
        return outsides;
    }

    /**
     * Return the municipality (from inside or outside) given by its name, or
     * null if not found.
     *
     * @param name
     * @return
     */
    public Municipality getMunicipality(String name) {
        for (Municipality mun : myMunicipalities) {
            if (mun.getName().equals(name)) {
                return mun;
            }
        }
        for (Municipality mun : outsides) {
            if (mun.getName().equals(name)) {
                return mun;
            }
        }
        return null;
    }

    public Household findHouseholdById(long id) {
        Household hh = null;
        for (Municipality mun : myMunicipalities) {
            hh = mun.findHouseholdById(id);
            if (hh != null) {
                break;
            }
        }
        return hh;
    }
    private Parameters parameters;

    public Parameters getParameters() {
        return parameters;
    }

    /**
     * Constructor used for tests
     */
    MunicipalitySet(Activity[] regionalActivities, ActivityPattern[] allPatterns, Municipality... municipalities) throws BadDataException {
        this.parameters = new Parameters();
        this.regionalActivities = regionalActivities;
        List<Integer> professions = new ArrayList<Integer>();
        // compute nb of professions
        for (Activity activity : regionalActivities) {
            if (!professions.contains(activity.getProfession())) {
                professions.add(activity.getProfession());
            }
        }
        outsides = new ArrayList<Municipality>();
        this.parameters.setNbTypesBySectorActiv(professions.size());
        this.allPatterns = allPatterns;
        this.myMunicipalities = Arrays.asList(municipalities);
        labourOffice = new LabourOffice(this);
        labourOffice.initBase();
        labourOffice.initNetwork();
    }

    /**
     * Constructor used for simulation
     */
    public MunicipalitySet(Application myAppli, Parameters parameters, List<Scenario> scenarios) throws IOException, BadDataException, ProcessingException {
        this(myAppli, parameters, scenarios, true);
    }

    /**
     * Constructor used for tests
     */
    MunicipalitySet(Application myAppli, Parameters parameters, List<Scenario> scenarios, boolean writeOutput) throws IOException, BadDataException, ProcessingException {
        this.myApplication = myAppli;
        this.parameters = parameters;
        this.scenarios = scenarios;
        this.currentYear = getStartStep();
        finishToInitTheRegion();
        if (scenarios != null) {
            for (Scenario scenario : scenarios) {
                // init the scenario
                scenario.init(this);
            }
        }
        parameters.getMigration().init(this);
        myAppli.getObservableManager().initObservers();
        parameters.initGlobalObservers(this);
        // print init state
        this.currentYear--;
        parameters.fireGlobalObservables(myAppli.getObservableManager(), this);
        this.currentYear++;
    }

    public void run(int replic) throws ProcessingException {
        for (int iter = 0; iter <= getNbIteration(); iter++) {
            iteration(iter);
        }
        myApplication.getObservableManager().closeObservers();
    }

    public void run() throws ProcessingException {
        run(1);
    }

    private void finishToInitTheRegion() throws IOException, BadDataException {
        initRegionalActivities();
        initAllPatterns(parameters.getNbPat(), getNbActivities());
        // building municipalities
        this.myMunicipalities = new ArrayList<Municipality>(getNbMunicipalities());
        for (MunicipalityParameters munParameters : parameters.getMunicipalitiesParameters()) {
            getMyMunicipalities().add(new Municipality(this, munParameters.getMunicipalityName()));
        }
        labourOffice = new LabourOffice(this);
        labourOffice.initBase();
        labourOffice.initNetwork();
        if (parameters.getOutsideParameters().size() != 1) {
            throw new RuntimeException("TODO Model is built for supporting only one outside");
        }
        outsides = new ArrayList<Municipality>();
        for (OutsideParameters outsideParameter : parameters.getOutsideParameters()) {
            Municipality outside = new Municipality(this, outsideParameter.getName());
            outsides.add(outside);
            Municipality[] proximityJob = new Municipality[outsideParameter.getNeighbours().size()];
            int proximityJobIndex = 0;
            for (String neighbour : outsideParameter.getNeighbours()) {
                // TODO index municipalities by their names in a map for better performance
                for (Municipality mun : myMunicipalities) {
                    if (mun.getName().equals(neighbour)) {
                        proximityJob[proximityJobIndex] = mun;
                        proximityJobIndex++;
                        break;
                    }
                }
            }
            outside.setProximityJob(proximityJob);
            initOutside(outsideParameter);
        }
        initCapacityAvailableResidence();
        // loading network
        Map<String, SortedMap<Double, Municipality>> network = new TreeMap<String, SortedMap<Double, Municipality>>();
        CSVReader distancesReader = CSV.getReader(myApplication.getFileFromRelativePath(parameters.getMunicipalitiesDistances()), null);
        Integer[] munLabels = Utils.parseIntegerArray(CSV.readLine(distancesReader), 1);
        String[] line;
        while ((line = CSV.readLine(distancesReader)) != null) {
            SortedMap<Double, Municipality> neighbours = new TreeMap<Double, Municipality>();
            for (int i = 0; i < munLabels.length; i++) {
                neighbours.put(Double.parseDouble(line[i + 1]), getMunicipality(munLabels[i].toString()));
            }
            network.put(line[0], neighbours);
        }
        // loading municipalities data
        for (int i = 0; i < parameters.getMunicipalitiesParameters().size(); i++) {
            MunicipalityParameters munParameters = parameters.getMunicipalitiesParameters().get(i);
            LOGGER.log(Level.FINER, "Loading data for municipality {0}", munParameters.getMunicipalityName());
            int countHousehold = initOneVillage(munParameters, i, network.get(munParameters.getMunicipalityName()));
            Municipality mun = getMyMunicipalities().get(i);
            Logger munLogger = mun.getLogger();
            if (munLogger.isLoggable(Level.FINE)) {
                StringBuilder munPrint = new StringBuilder("Init Municipality ").append(mun.getName()).append("\n");
                munPrint.append("  nb individuals=").append(munParameters.getCountIndiv());
                munPrint.append(" against parameterize=").append(munParameters.getPopsize());
                munPrint.append(" nb households=").append(countHousehold).append("\n");
                munPrint.append("  ").append(mun.editAvailableRes()).append("\n");
                munPrint.append("  ").append(mun.editDemography(0)).append("\n");
                munPrint.append("  ").append(mun.editHouseholdDetailSizes()).append("\n");
                munPrint.append("  ").append(mun.editHouseholdDetailTypes());
                munLogger.fine(munPrint.toString());
            }
        }
        // init probabilities for commuting to outside
        Map<Integer, Double> probasByDistClass = new HashMap<Integer, Double>();
        try {
            CSVReader reader = CSV.getReader(myApplication.getFileFromRelativePath(parameters.getCommutOutsideProbas()), null);
            while ((line = CSV.readLine(reader)) != null) {
                probasByDistClass.put(Integer.parseInt(line[0]), Double.parseDouble(line[1]));
            }
            reader.close();
            reader = CSV.getReader(myApplication.getFileFromRelativePath(parameters.getDistToBorder()), null);
            while ((line = CSV.readLine(reader)) != null) {
                Municipality municipality = getMunicipality(line[0]);
                int dist = Integer.parseInt(line[1]);
                // TODO find a way for getting this value of 3 (distance range width)
                municipality.setDistanceToBorder(dist * 3);
                municipality.setCommutOutsideProba(probasByDistClass.get(dist));
            }
            reader.close();
        } catch (IOException ex) {
            throw new BadDataException("Error while loading the probas for commuting to outside", ex);
        }

        // Fired people who have an employment which does not correspond to an offer 
        // in the same loop initialise the parent from people who was parent at the initialisation time and only student
        // and then we can finish to init the individual calling the finishToInitMunicipality method
        LOGGER.log(Level.FINE, "Adusting employement against init offer by firing");
        int r = 0;
        int totalFired = 0;
        for (Municipality municipality : getMyMunicipalities()) {
            LOGGER.log(Level.FINER, " processing mun {0}", municipality.getName());
            // giving a parent to orphan children
            for (Household hh : municipality.getMyHouseholds()) {
                for (Individual ind : hh) {
                    if (ind.getParent1() == null && ind.getStatus() == Status.STUDENT) {
                        Individual indiv;
                        do {
                            Municipality munic = getRandom().nextObject(getMyMunicipalities());
                            Household hsh = getRandom().nextObject(munic.getMyHouseholds());
                            indiv = hsh.getAdults().get(0);
                            r++;
                            if (r > 100) {
                                break;
                            }
                        } while (indiv.getAge() < (ind.getAge() + getAgeMinToChild()));
                        ind.setParent1(indiv);
                    }
                }
            }
            municipality.finishToInitMunicipality();
            Arrays.fill(municipality.getOccupiedActivitiesByOutside(), 0);
            //totalFired += municipality.fire(true);
            totalFired += municipality.fireInit();
            getLabourOffice().updateAvailabilities(municipality);
        }
        for (Municipality outside : outsides) {
            //totalFired += outside.fire(true);
            totalFired += outside.fireInit();
            getLabourOffice().updateAvailabilities(outside);
        }
        // try to find again a job to the one who has been fired
        LOGGER.log(Level.FINE, "{0} fired people now look for a new job", totalFired);
        int found = 0;
        List<Household> householdsToUpdate = new ArrayList<Household>();
        for (Municipality mun : getMyMunicipalities()) {
            householdsToUpdate.addAll(mun.getMyHouseholds());
            getLabourOffice().updateAvailabilities(mun);
        }
        getRandom().shuffle(householdsToUpdate);
        for (Household hh : householdsToUpdate) {
            for (Individual ind : hh) {
                if (ind.isLookForJob()) {
                    if (ind.findJob()) {
                        found++;
                    }
                }
            }
        }
        LOGGER.log(Level.FINE, "{0} fired people have found a job ({1} have failed)", new Object[]{found, totalFired - found});
        LOGGER.log(Level.FINE, "Initialisation of job occupation by outsiders");
        // Initialisation of occupation by outsiders equal to "total offers of job - occupied by resident - occupied by ext)
        int serviceThreshold = getNbProfessions() * 3;
        int currentTotService = 0;
        int totOfferService = 0;
        for (Municipality mun : getMyMunicipalities()) {
            for (int i = 0; i < mun.getOccupiedActivitiesByOutside().length; i++) {
                mun.getOccupiedActivitiesByOutside()[i] = mun.getOfferedActivities()[i]
                        - (int) mun.getOccupiedActivitiesByRes()[i]
                        - (int) mun.getOccupiedActivitiesByExt()[i];
                // this correction is applied in case the fire method is not called because it generates negative outside occupations
                if (mun.getOccupiedActivitiesByOutside()[i] < 0) {
                    mun.getOccupiedActivitiesByOutside()[i] = 0;
                }
            }
            // computation of the initial part of the job offer dedicated to service to people living there and the other part which is exogeneous
            int[] offerM = new int[getNbActivities()];
            Arrays.fill(offerM, 0);
            currentTotService = 0;
            for (int i = 0; i < getNbActivities(); i++) {
                if (i >= serviceThreshold) {
                    currentTotService += mun.getOfferedActivities(i);
                }
            }
            totOfferService = (int) (mun.getPopulationSize() * (mun.getIntercept() + (mun.getSlope() * Math.log(mun.getPopulationSize()))));
            if (totOfferService < 0) {
                totOfferService = 0;
            }
            if (totOfferService > currentTotService) {
                totOfferService = currentTotService;
            }
            if (totOfferService > 0) {
                for (int i = serviceThreshold; i < getNbActivities(); i++) {
                    if (mun.getOfferedActivities()[i] == 0) {
                        mun.endogeneousOfferedJob[i] = (int) Math.round(totOfferService * 0.001);
                    } else {
                        mun.endogeneousOfferedJob[i] = Math.round(totOfferService * ((float) mun.getOfferedActivities()[i] / (float) currentTotService));
                    }
                }
            }
            for (int i = 0; i < getNbActivities(); i++) {
                if (mun.getOfferedActivities()[i] == 0 && mun.endogeneousOfferedJob[i] > 0) {
                    mun.exogeneousOfferedJob[i] = mun.getOfferedActivities()[i];
                } else {
                    mun.exogeneousOfferedJob[i] = mun.getOfferedActivities()[i] - mun.endogeneousOfferedJob[i];
                }
            }
            getLabourOffice().updateAvailabilities(mun);
        }
        // logging
        for (Municipality outside : outsides) {
            if (outside.getLogger().isLoggable(Level.FINE)) {
                StringBuilder jobsPrint = new StringBuilder(outside.giveJobsOccupiedByInsideRegionPeople());
                for (int i = 0; i < getNbMunicipalities(); i++) {
                    jobsPrint.append("\n  ");
                    jobsPrint.append(getMyMunicipalities().get(i).giveJobsOccupiedByInsideRegionPeople());
                }
                outside.getLogger().fine(jobsPrint.toString());
            }
            for (int i = 0; i < getNbActivities(); i++) {
                outside.exogeneousOfferedJob[i] = outside.getOfferedActivities()[i] - outside.endogeneousOfferedJob[i];
            }
            getLabourOffice().updateAvailabilities(outside);
        }
    }

    /**
     * Method initActivities First version: S. Huet, 24.04.2009 Changed S. Huet
     * 08.10.2009
     */
    private void initRegionalActivities() {
        regionalActivities = new Activity[getNbActivities()];
        int k = 0;
        for (int j = 0; j < getNbActivSectors(); j++) {
            for (int i = 0; i < getNbProfessions(); i++) {
                regionalActivities[k] = new Activity(j, i);
                k++;
            }
        }
    }

    /**
     * Method initAllPatterns First version: F.Gargiulo 15.04.2009 S. Huet,
     * 21.01.2010, changed to be more generic regarding the maximum nb of
     * activities of individual can have at a time (that is previously fixed at
     * 2)
     */
    private void initAllPatterns(int nbPatterns, int nbActivities) {
        allPatterns = new ActivityPattern[nbPatterns];
        // create the null pattern with all zeros
        boolean[] mypattern = new boolean[nbActivities];
        // initialize the pattern for unemploynment
        allPatterns[0] = new ActivityPattern(mypattern, 0);
        // initialize all the patterns with one activity
        for (int i = 0; i < nbActivities; i++) {
            for (int j = 0; j < nbActivities; j++) {
                mypattern[j] = false;
            }
            mypattern[i] = true;
            allPatterns[i + 1] = new ActivityPattern(mypattern, i + 1);
        }
    }

    /**
     * Method initOneVillage NB : the villageNb allows the identification of the
     * village First version: S. Huet, 7.04.2009
     */
    private int initOneVillage(MunicipalityParameters parameters, int villageNb, SortedMap<Double, Municipality> neighbours) throws IOException {
        Municipality municipality = getMyMunicipalities().get(villageNb);
        municipality.setOfferedActivities(parameters.getOfferedActivities());
        municipality.endogeneousOfferedJob = new int[municipality.getOfferedActivities().length];
        municipality.exogeneousOfferedJob = new int[municipality.getOfferedActivities().length];
        Arrays.fill(municipality.endogeneousOfferedJob, 0);
        Arrays.fill(municipality.exogeneousOfferedJob, 0);
        // Initialize household
        Integer[][] myHsh = parameters.getHouseholds();
        // Initialization of the village
        String[] param = {parameters.getInputFileName(), parameters.getHouseholdsFilename(), parameters.getResidentsActivityFilename()};
        municipality.setFicParameter(param);
        municipality.initVillage(parameters, parameters.getCountIndiv(), myHsh, parameters.getIndividualsActivities(), parameters.getProxJob(), neighbours);

        return myHsh.length;
    }

    /**
     * Method initializing the OutsideRuralRegion village First version: S.
     * Huet, 7.04.2009
     */
    private void initOutside(OutsideParameters parameters) {
        // Read the parameter of the village
        try {
            for (Municipality outside : outsides) {
                String livingPop[][] = new String[0][0];
                int[] availableResidence = new int[getNbSizesResidence()];
                for (int i = 0; i < getNbSizesResidence(); i++) {
                    availableResidence[i] = 0;
                }
                outside.initOutside(livingPop, availableResidence, parameters.getOfferedActivities());
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error during outside init", e);
        }
    }
    private MunicipalitySetCounters counters = new MunicipalitySetCounters(this);

    public MunicipalitySetCounters getCounters() {
        return counters;
    }
    private List<Household> householdsToUpdate;

    public List<Household> getHouseholdsToUpdate() {
        return householdsToUpdate;
    }
    private int currentYear;

    public int getCurrentYear() {
        return currentYear;
    }

    /**
     * Method containig the evolution algorithm for each village First version:
     * F.Gargiulo 16/04/09 Added parameter "nbMunicipalities": S. Ternes
     * 20/04/09 Changed: february 2010 (S. Huet)
     */
    public void iteration(int iter) throws ProcessingException {
        int nbAd = 0;
        int nbHshSingle = 0;
        int nbSingleAdult = 0;
        int nbOldPartnerNonNull = 0;
        inactivityEntry = 0;
        inactivityOut = 0;
        labourMarketEnter = 0;
        retirementEnter = 0;
        for (Municipality mun : getMyMunicipalities()) {
            mun.resetCounters();
            for (Household hh : mun.getMyHouseholds()) {
                nbAd = nbAd + hh.getAdults().size();
                if (hh.getHshType() == Household.Type.SINGLE || hh.getHshType() == Household.Type.MONOPARENTAL) {
                    nbHshSingle = nbHshSingle + 1;
                }
                if (hh.getAdults().size() == 1) {
                    nbSingleAdult++;
                }
                if (hh.getOldPartner() != null) {
                    nbOldPartnerNonNull++;
                }
            }
        }
        currentYear = getStartStep() + iter;
        parameters.updateValues(currentYear);

        LOGGER.log(Level.INFO, "BEGIN OF STEP {0}", currentYear);
        counters.reset();
        parameters.getMigration().resetCounters();
        if (scenarios != null) {
            // For each Municipality, applied the scenario and compute the
            // consequences on land use, jobs, services, individuals and householdsFilename
            for (Scenario scenario : scenarios) {
                scenario.step(this, iter);
            }
        }
        // Making dynamic the employment offer
        if (parameters.getDynamicServiceEmployment() != null) {
            parameters.getDynamicServiceEmployment().step(this, iter);
        }
        // Make a list given the order in which the individuals of the region are going to be updated
        householdsToUpdate = new ArrayList<Household>();
        for (Municipality mun : getMyMunicipalities()) {
            householdsToUpdate.addAll(mun.getMyHouseholds());
            getLabourOffice().updateAvailabilities(mun);
        }
        getRandom().shuffle(householdsToUpdate);
        for (Household hh : householdsToUpdate) {
            // Define need of households regarding work - directly
            // applying the transition to inactivity making available some jobs
            // Employed individiduals can be designed as inactive
            if (!hh.isJustSuppressed()) {
                if (!hh.getResidence().isTransit()) {
                    hh.willingJobs();
                }
            }
        }
        // manage the consequence of the change in availability of jobs
        // Fired people become unemployed only if they had not been already designed as inactive by the willingJobs method
        boolean activeFire = false;
        if (activeFire) {
            for (Municipality municipality : getMyMunicipalities()) {
                municipality.fire(false); // false means that it is not the initialisation time
                getLabourOffice().updateAvailabilities(municipality);
            }
            for (Municipality municipality : getOutsides()) {
                municipality.fire(false);
                getLabourOffice().updateAvailabilities(municipality);
            }
        }
        // Emigration (going to live out of the bunch) and Suppression of the suppress household
        //parameters.getEmigration().step(this, currentYear);
        // Update household regarding work and residence
        int nbCouples = 0;
        int coupleFailures = 0;
        int nbDivorces = 0;
        ///*
        for (Household hh : householdsToUpdate) {
            if (!hh.getResidence().isTransit()) { // if it is not a potential immigrant
                death(hh); //death process in household
                birth(hh); // birth process in household
            }
            if (!hh.isJustSuppressed()) {
                if (!hh.getResidence().isTransit()) {
                    hh.searchJobs(); // it is possible the hh has been already suppressed by the createCouple dynamics
                }
            }
        }
        householdsToUpdate = new ArrayList<Household>();
        for (Municipality mun : getMyMunicipalities()) {
            householdsToUpdate.addAll(mun.getMyHouseholds());
        }

        getRandom().shuffle(householdsToUpdate);
        for (Household hh : householdsToUpdate) {
            //make couples
            if (!hh.isJustSuppressed()) {
                // TODO parameterisation of the 81 (the age an individual stops looking for a partner)
                if (hh.singleAdult() && hh.getAdults().get(0).getAge() < 70) {
                    if (getRandom().nextDouble() < parameters.getProbabilityToMakeCouple()) {
                        if (createNewCouple(hh, currentYear)) {
                            nbCouples++;
                        } else {
                            coupleFailures++;
                        }
                    }
                }
                if (!hh.isJustSuppressed() && !hh.getResidence().isTransit()) {
                    if (hh.getAdults().size() > 1 && hh.getAdults().get(0).getAge() < 70 && hh.getAdults().get(1).getAge() < 70) {
                        if (splitHousehold(hh, getSplittingProbability())) {
                            nbDivorces++; //divorces
                        }
                    }
                }
                // if the household need a residence due to a precedent event
                // or if its residence seems unapproriate (including other occupants)
                if (!hh.isJustSuppressed()) {
                    hh.setNeedOfResidence(needOfResid(hh));
                }
                // if the household need a residence due to a precedent event
                // or if its residence seems unapproriate (including other occupants)
                if (!hh.isJustSuppressed()) {
                    parameters.getMigration().process(hh);
                }
            }
        }
        // Suppression of household updated and resign of the far commuter who is not a leader
        for (Municipality mun : myMunicipalities) {
            for (Iterator<Household> it = mun.getMyHouseholds().iterator(); it.hasNext();) {
                Household hh = it.next();
                if (hh.isJustSuppressed()) {
                    if (!hh.getResidence().isTransit()) {
                        hh.getMyVillage().releaseResidence(hh);
                    }
                    it.remove();
                } else { // resign of the far commuters who are not leaders
                    for (Individual ind : hh.getAdults()) {
                        if (ind.isFarCommuter() && hh.determineLeader() != ind) {
                            // resign ;
                            ind.leaveMyActivPattern();
                            ind.becomeUnemployed(hh.getMyVillage());
                            ind.setFarCommuter(false);
                        }
                    }
                }
            }
        }

        counters.updateCounters();
        // Logging
        for (Municipality mun : getMyMunicipalities()) {
            mun.updatePopSize();
            Logger munLogger = mun.getLogger();
            if (munLogger.isLoggable(Level.FINER)) {
                StringBuilder munPrint = new StringBuilder("Municipality ").append(mun.getName()).append("\n");
                munPrint.append("  ").append(mun.editAvailableRes()).append("\n");
                munPrint.append("  ").append(mun.editDemography(currentYear)).append("\n");
                munPrint.append(mun.editIndividualDetailAges("  ")).append("\n");
                munPrint.append("  ").append(mun.editHouseholdDetailSizes()).append("\n");
                munPrint.append("  ").append(mun.editHouseholdDetailTypes()).append("\n");
                munPrint.append("  Jobs offer   ").append(Arrays.toString(mun.getOfferedActivities()));
                munPrint.append("\n   free          ").append(Arrays.toString(mun.getFreeActivities()));
                munPrint.append("\n   occupied      ").append(Arrays.toString(mun.getOccupiedActivities()));
                munPrint.append("\n   occ by res    ").append(Arrays.toString(mun.getOccupiedActivitiesByRes()));
                munPrint.append("\n   occ by ext    ").append(Arrays.toString(mun.getOccupiedActivitiesByExt()));
                munPrint.append("\n   occ outside   ").append(Arrays.toString(mun.getOccupiedActivitiesByOutside()));
                munLogger.finer(munPrint.toString());
            }
        }
        LOGGER.log(Level.FINER,
                "nb new couples: {0} − couple failures: {1} − nb divorces: {2} − nb birth {3} − nb death {4} - annual killed orphans {5} - expulsed people {6} ",
                new Object[]{nbCouples, coupleFailures, nbDivorces, counters.getNbBirth(),
                    counters.getNbDeath(), counters.getTotalOrphans(), counters.getTotalExpulsed()
                });
        parameters.fireGlobalObservables(myApplication.getObservableManager(), this);
        // Make people becoming older
        for (Municipality municipality : getMyMunicipalities()) {
            municipality.updateHouseholdAges();
        }
        int soldMig = counters.getNbImmigrants() - parameters.getMigration().getMigrantsCount();
        LOGGER.log(Level.FINER,
                "===> ITER " + iter + " nbAdults " + nbAd + " nb menages single " + nbHshSingle + " nb adult single " + nbSingleAdult + " oldpartn non null " + nbOldPartnerNonNull
                + "\nFIN in-migrants " + counters.getNbImmigrants() + " in migrants 60 and more " + counters.getNbImmigrants60()
                + "\n Out-migrants: total " + parameters.getMigration().getMigrantsCount() + " solde migratoire " + soldMig + " actives " + parameters.getMigration().getMigrantsCountO() + " 60 and more " + parameters.getMigration().getMigrantsCount60()
                + " student " + parameters.getMigration().getMigrantsCountS() + " single " + parameters.getMigration().getMigrantsCountSingle()
                + "\n nb partenaires trouvés " + nbCouples + " nb divorces " + nbDivorces + " nb single move inside the bunch " + parameters.getMigration().getMobileSingleCount()
                + " mobile 60 et plus " + parameters.getMigration().getMobileMore60Count() + " mobile Student " + parameters.getMigration().getMobileStudentCount() + " mobile Actif et fam " + parameters.getMigration().getMobileActivEtFamCount()
                + "\nactifs " + counters.getNbActives() + " retired " + counters.getNbRetireds() + " student " + counters.getNbStudents());

        LOGGER.log(Level.INFO, "END OF STEP {0}", currentYear);
    }

    /**
     * Récupération des distances de domicile travail
     */
    public void distResWork() {
        float d;
        int distDistrib[] = new int[17];
        Arrays.fill(distDistrib, 0);
        int z = 0;
        for (Municipality mun : getMyMunicipalities()) {
            for (Household hs : mun.getMyHouseholds()) {
                for (Individual in : hs.getAdults()) {
                    z = 0;
                    if (in.getStatus() == Status.WORKER && in.getJobLocation() != outsides.get(0)) {
                        d = (float) (mun.euclidianDistanceFrom(in.getJobLocation()) / 1000.00f);
                        for (int i = 3; i < 52; i = i + 3) {
                            if (d <= i) {
                                distDistrib[z]++;
                                i = 52;
                            } else {
                                if (d > 51) {
                                    distDistrib[16]++;
                                    i = 52;
                                } else {
                                    z++;
                                }
                            }
                        }
                    }
                }
            }
        }
        for (int i = 0; i < 17; i++) {
            System.err.print(distDistrib[i] + "\t");
        }
        System.err.println();

        for (Municipality mun : getMyMunicipalities()) {
            for (Municipality mun2 : getMyMunicipalities()) {
                System.err.println(mun + "\t" + mun2 + "\t" + mun.euclidianDistanceFrom(mun2));
            }
        }
    }

    /**
     * Check the need of residence of the household considering all the
     * households living in the same residence
     */
    public boolean needOfResid(Household hh) {
        boolean needOfResidence = hh.needOfResidence();
        boolean zeroWantLeave = true;
        if (!needOfResidence) {
            int appropriateSizeOfRes = hh.getMyVillage().appropriatedSizeOfRes(hh.size());
            for (Household roomate : hh.getResidence()) {
                if (roomate.needOfResidence()) {
                    zeroWantLeave = false;
                } else if (roomate != hh) {
                    appropriateSizeOfRes += hh.getMyVillage().appropriatedSizeOfRes(roomate.size());
                }
            }
            if (zeroWantLeave) {
                needOfResidence = !hh.satisfiedByLodging(appropriateSizeOfRes, hh.getResidence().getType());
            }
        }
        return needOfResidence;
    }

    /**
     * Satisfaction regarding the dwelling size
     */
    private boolean satisfiedByLodging(int idealSizeOfRes, int actualTypeOfRes) {
        return (Math.abs(actualTypeOfRes - idealSizeOfRes) <= getResSatisfactionMargin());
    }

    /**
     * Method findPartner: select a household which is not a couple one or not a
     * complex one because we don't know about the dynamics of complex one. Also
     * select an adult which respect a given difference of ages inside the
     * couple (parameters) Observation: we must check the sex for the adults in
     * the future
     *
     * @return the found partner. Can be null if none has been found. First
     * looks in own municipality, then if not found in all households First
     * version: S. Ternes, F. Gargiulo, 22.04.2009 Changed by S. Huet,
     * 1.02.2010, by G. Deffuant 4/11/2010, change by S. Huet
     */
    private Individual findPartner(Household hsh) {
        Individual adult = hsh.getAdults().get(0);
        Municipality placeOfWork = hsh.getMyVillage();
        if (adult.getStatus() == Status.WORKER && !adult.getJobLocation().isOutside()) {
            placeOfWork = adult.getJobLocation();
        }
        // Preference for finding someone in the already residence place
        for (int d = 0; d < parameters.getNbJoinTrials(); d++) {
            //it can find a partner only in the village where it has been affected at first
            Household otherHH = hsh.getMyVillage().getMyHouseholds().get(getRandom().nextInt(0, hsh.getMyVillage().getMyHouseholds().size() - 1));
            // check the conditions to become a couple
            if ((otherHH != hsh) && (!otherHH.isJustSuppressed())
                    && ((!otherHH.getResidence().isTransit()) || (!hsh.getResidence().isTransit()))) { // at least one hh should be not in transit
                if (otherHH.singleAdult()) {
                    Individual partner = otherHH.getAdults().get(0);
                    if (matchingPartners(adult, partner)) {
                        return partner;
                    }
                }
                for (Individual partner : otherHH.getChildren()) {
                    if ((partner.getAge() > getParameters().getAgeMinHavingChild()) && (matchingPartners(adult, partner))) {
                        // second, consider children
                        return partner;
                    }
                }
            }
        }
        // search in a space delimited by the far distance
        SortedMap<Double, SubNetwork> netRes = getParameters().getMigration().activesResidenceNetwork.getSubNetwork(hsh.getMyVillage());
        SortedMap<Double, SubNetwork> netWork = getParameters().getMigration().activesResidenceNetwork.getSubNetwork(placeOfWork);
        // save the keys, ordered them in descending order (they are in ascending one at the beginning)
        int nbKeysR = netRes.size();
        int nbKeysW = netWork.size();
        Double[] probasRes = new Double[nbKeysR];
        Double[] probasWork = new Double[nbKeysW];
        for (Map.Entry<Double, SubNetwork> entry : netRes.entrySet()) {
            nbKeysR--;
            probasRes[nbKeysR] = (entry.getKey()).doubleValue();
        }
        nbKeysR = netRes.size();
        for (Map.Entry<Double, SubNetwork> entry : netWork.entrySet()) {
            nbKeysW--;
            probasWork[nbKeysW] = (entry.getKey()).doubleValue();
        }
        nbKeysW = netWork.size();
        List<Municipality> subnet = new ArrayList<Municipality>();
        List<Municipality> munTemp = null;
        int nbMun = 0;
        int taillePop = 0;
        Municipality city = null;
        for (int i = 0; i < getSearchDistThreshold(); i++) {
            munTemp = (netWork.get(probasWork[i])).getMunicipalities();
            for (Municipality m : munTemp) {
                if (!subnet.contains(m)) {
                    subnet.add(m);
                    nbMun++;
                    taillePop = taillePop + m.getPopulationSize();
                }
            }
            munTemp = (netRes.get(probasRes[i])).getMunicipalities();
            for (Municipality m : munTemp) {
                if (!subnet.contains(m)) {
                    subnet.add(m);
                    nbMun++;
                    taillePop = taillePop + m.getPopulationSize();
                }
            }
        }
        int[] popMun = new int[nbMun];
        for (int i = 0; i < nbMun; i++) {
            popMun[i] = subnet.get(i).getPopulationSize();
        }
        for (int d = 0; d < parameters.getNbJoinTrials(); d++) {
            // if I can't find in my village
            if (subnet.size() > 0) {
                int rand = (getRandom().nextInt(0, taillePop - 1));
                int cum = 0;
                for (int i = 0; i < nbMun; i++) {
                    cum = cum + popMun[i];
                    if (rand < cum) {
                        city = subnet.get(i);
                        i = nbMun;
                    }
                }
                Household otherHH = city.getMyHouseholds().get(getRandom().nextInt(0, city.getMyHouseholds().size() - 1));
                // check the conditions to become a couple
                if ((otherHH != hsh) && (!otherHH.isJustSuppressed()) && !otherHH.getResidence().isTransit()) {
                    if (otherHH.singleAdult()) {
                        Individual partner = otherHH.getAdults().get(0);
                        if (matchingPartners(adult, partner)) {
                            return partner;
                        }
                    }
                    for (Individual partner : otherHH.getChildren()) {
                        if ((partner.getAge() > getParameters().getAgeMinHavingChild()) && (matchingPartners(adult, partner))) {
                            // second, consider children
                            return partner;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Conditions for two adults from single households to become a couple. In
     * this case, only age
     */
    private boolean matchingPartners(Individual adult, Individual partner) {
        if (Math.abs(adult.getAge() - partner.getAge()) > getAvDiffAgeCouple() - getStDiffAgeCouple()
                && Math.abs(adult.getAge() - partner.getAge()) < getAvDiffAgeCouple() + getStDiffAgeCouple()) {
            return true;
        }
        return false;
    }

    /**
     * creation of new couple for a single household First try to find a single
     * household matching with age if not found, with equal probability,
     * duplicate the single adumt with difference of age or leave the simulation
     */
    private boolean createNewCouple(Household hsh, int iter) {
        Individual partner = findPartner(hsh);
        boolean newInhabitants = false;
        if (partner != null) { // there is a new partner
            if (partner.getMyHousehold().getResidence().isTransit()) {
                newInhabitants = true;
                for (Individual ind : partner.getMyHousehold()) {
                    ind.setMigrationYear((short) getCurrentYear());
                    // compute again the age to die if it has already passed
                    if (ind.getAgeToDie() <= ind.getAge()) {
                        ind.initAgeToDie();
                    }
                }
            }
            if (!partner.isAdult()) {
                partner.becomeAdult();
            }
            hsh.getAdults().get(0).setLastJoiningYear((short) getCurrentYear());
            partner.setLastJoiningYear((short) getCurrentYear());
            Household partnerHH = partner.getMyHousehold();
            Household leavingHH = hsh;
            Municipality departure = hsh.getMyVillage();
            Household growingHH = partnerHH;
            // the municipality where the partner lives is in the proximity
            if (partnerHH.getMyVillage() != getOutsides()) { // if outside, the household simply disappears
                // dwelling: take the larger residence
                float partnerHHres = 0.0f;
                float hshRes = 0.0f;
                for (Household roomate : partnerHH.getResidence()) {
                    if (!roomate.needOfResidence()) {
                        partnerHHres = partnerHHres + roomate.listOfMembers.size();
                    }
                }
                partnerHHres = (partnerHH.getResidence().getType() + 1) / (partnerHHres + hsh.listOfMembers.size());
                for (Household roomate : hsh.getResidence()) {
                    if (!roomate.needOfResidence()) {
                        hshRes = hshRes + roomate.listOfMembers.size();
                    }
                }
                hshRes = (hsh.getResidence().getType() + 1) / (hshRes + partnerHH.listOfMembers.size());
                if (partnerHHres < hshRes) {
                    leavingHH = partnerHH;
                    growingHH = hsh;
                    departure = leavingHH.getMyVillage();
                }
                if (growingHH.getResidence().getNbHhResidents() > 1) {
                    growingHH.setCoupleAtParents(true);
                }
                // We copy the members of our household into the found one
                for (Individual individual : leavingHH) {
                    Individual newInd = new Individual(individual);
                    if (individual.getStatus() == Status.WORKER) {
                        individual.getJobLocation().removeWorker(individual);
                        individual.getJobLocation().addWorker(newInd);
                    }
                    growingHH.addMember(newInd);
                    if (partnerHH.getMyVillage() != hsh.getMyVillage()) {
                        if (individual.getMyActivityPattern() != null) {
                            if (newInd.getStatus() == Status.UNEMPLOYED) {
                                newInd.setJobLocation(growingHH.getMyVillage());
                            } else { // she is not unemployed
                                if (individual.getJobLocation() == leavingHH.getMyVillage()) {
                                    leavingHH.getMyVillage().decreaseOccupiedActivityRes(individual.getMyActivityPattern().getJob());
                                    leavingHH.getMyVillage().increaseOccupiedActivityExt(individual.getMyActivityPattern().getJob());
                                } else if (individual.getJobLocation() == growingHH.getMyVillage()) {
                                    growingHH.getMyVillage().decreaseOccupiedActivityExt(individual.getMyActivityPattern().getJob());
                                    growingHH.getMyVillage().increaseOccupiedActivityRes(individual.getMyActivityPattern().getJob());
                                } else { // check if the moved individual become a far commuter
                                    if (individual.getStatus() == Status.WORKER) {
                                        newInd.workFar(growingHH.getMyVillage(), individual.getJobLocation());
                                    }
                                }
                            }
                        }
                    }
                    if (newInd.isLookForJob()) {
                        // the household of the moved individual can have not already made searchJob() while the new household has already done searchJob() during the iteration
                        newInd.seekJob();
                    }
                }
                growingHH.updateHshFeaturesAfterJoining(iter);
                // update the counter of moving people
                Municipality arrival = growingHH.getMyVillage();
                if (arrival == departure) {
                    // moving inside the same village
                    if (!newInhabitants) {
                        arrival.getCounters().incNbHouseholdMoveInsideSameMun(1);
                        arrival.getCounters().incNbIndividualMoveInsideSameMun(leavingHH.size());
                    }
                    departure.suppressHousehold(leavingHH, false, false);
                } else {
                    // the houshold leave its village
                    double dist = arrival.euclidianDistanceFrom(departure);
                    arrival.getCounters().incNbHouseholdMoveIn(1);
                    arrival.getCounters().incNbIndividualMoveIn(leavingHH.size());
                    if (!newInhabitants) {
                        departure.getCounters().incNbHouseholdMoveOut(1);
                        departure.getCounters().incNbIndividualMoveOut(leavingHH.size());
                        arrival.getCounters().incNbHouseholdMoveInside(1);
                        arrival.getCounters().incNbIndividualMoveInsideOtherMun(leavingHH.size(), dist);
                    }
                    // 1 is the "join" event
                    //leavingHH.setJustSuppressed(true);
                    departure.suppressHousehold(leavingHH, false, true);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Death process applied to a household. If no adults remains in an
     * household, the children are suppressed from the system.
     */
    private void death(Household hsh) {
        if (!hsh.isJustSuppressed()) {
            //int orphans = 0;
            Municipality mun = hsh.getMyVillage();
            List<Individual> suppressed = hsh.death();
            mun.getCounters().incNumDeath(suppressed.size());
            if (suppressed.size() == hsh.size()) {
                // if all the members are dead => remove the household
                mun.suppressHousehold(hsh, true, false);
            } else {
                for (Individual ind : suppressed) {
                    hsh.suppressMemberLeavingActivity(ind);
                }
                if (!suppressed.isEmpty()) {
                    hsh.updateHshFeaturesAfterDeath();
                    if (hsh.getAdults().isEmpty()) {
                        //orphans+=hsh.getSize();
                        mun.getCounters().incNbKilledOrphans(hsh.getSize());
                        mun.suppressHousehold(hsh, true, false);
                    }
                }
            }
        }
    }
    /*
     * Birth: an household is enlarged GD 11/2010
     */

    private void birth(Household hsh) {
        if (!hsh.isJustSuppressed()) {
            Municipality mun = hsh.getMyVillage();
            if (hsh.birth()) {
                mun.getCounters().incNumBirth(1);
                Individual newIndy = new Individual(hsh, 0, Status.STUDENT);
                if (newIndy.getAge() == newIndy.getAgeToDie()) {
                    // the child is dead at birth :-/
                    mun.getCounters().incNumDeath(1);
                } else {
                    // The household type is update by addMember
                    hsh.addMember(newIndy);
                    hsh.updateHshFeaturesAfterBirth();
                }
            }
        }
    }

    /**
     * splitting households by divorce: couple members quit each other (create a
     * new household) for couple type 2 (couple without children) and 3 (with
     * children) Each individual leaves , the children remains with one of the
     * adult
     */
    private boolean splitHousehold(Household hsh, double splittingProba) {
        if (hsh.getHshTypeCode() == 2 || hsh.getHshTypeCode() == 3) {
            if (!hsh.isJustSuppressed()) {
                Municipality mun = hsh.getMyVillage();
                boolean splitByDivorce = hsh.splitByDivorce(splittingProba);
                if (splitByDivorce) {
                    List<Individual> adults = hsh.getAdults();
                    adults.get(0).setLastSplittingYear((short) getCurrentYear());
                    adults.get(1).setLastSplittingYear((short) getCurrentYear());
                    int leavingAdultIndex = 0;
                    if ((adults.get(0).getJobLocation() != null) && (adults.get(1).getJobLocation() != null)
                            && (mun.closest(adults.get(0).getJobLocation(), adults.get(1).getJobLocation()) == adults.get(0).getJobLocation())) {
                        leavingAdultIndex = 1;
                    } else if (adults.get(1).getJobLocation() != null) {
                        leavingAdultIndex = 1;
                    }
                    List<Individual> listofmembers = new ArrayList<Individual>();
                    Individual leavingAdult = adults.get(leavingAdultIndex);
                    listofmembers.add(leavingAdult);
                    Household newHsh = new Household(mun, listofmembers, hsh.getResidence());
                    newHsh.setNeedOfResidence(true);
                    //hsh.setNeedOfResidence(true);
                    ///*
                    if (hsh.isCoupleAtParents()) {
                        hsh.setNeedOfResidence(true);
                    }
                    //*/
                    mun.getCounters().incNewHouseholdNb(1);
                    mun.getMyHouseholds().add(newHsh);
                    hsh.suppressMember(leavingAdult);
                    // children dispatching
                    List<Individual> children = hsh.getChildren();
                    int nbChildrenForLeavingInd = getRandom().nextInt(0, children.size());
                    int[] indexes = getRandom().randomList(children.size());
                    for (int i = 0; i
                            < nbChildrenForLeavingInd; i++) {
                        Individual child = children.get(indexes[i]);
                        newHsh.addMember(child);
                        hsh.suppressMember(child);
                    }
                    newHsh.updateHshFeaturesAfterDivorce(hsh);
                    hsh.updateHshFeaturesAfterDivorce(newHsh);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Method counting the quantity of each pattern (to debugg the pattern
     * occupancy)
     */
    public String countPattern(int vil) {
        int[] countPat = new int[getAllPatterns().length];
        for (Household hh : getMyMunicipalities().get(vil).getMyHouseholds()) {
            if (!hh.isJustSuppressed()) {
                for (Individual ind : hh) {
                    Status status = ind.getStatus();
                    if ((status != Status.RETIRED) && (status != Status.STUDENT)) {
                        if (ind.getMyActivityPattern() != null) {
                            countPat[ind.getMyActivityPattern().getIndex()]++;
                        }
                    }
                }
            }
        }
        StringBuilder buff = new StringBuilder("Count pattern: ");
        for (int count : countPat) {
            buff.append(count).append(" ");
        }
        return buff.append("\n").toString();
    }

    /**
     * Initialise the capacity i term of number of people for each type of
     * residence As the ideal size for an household follows in France the rule
     * (nbIndividualsOfTheHouseholds*1)+1 room then for a flat size of x + 1 =
     * rooms, the ideal size of household is x individuals This method gives a
     * result to know the appropriate size of a residence for a given size of
     * household First version: S. Huet, 28.07.2009
     */
    private void initCapacityAvailableResidence() {
        capacityOfAvailRes = new int[getNbSizesResidence()];
        for (int i = 0; i < getNbSizesResidence(); i++) {
            if (i == 0) {
                setCapacityOfAvailRes(i, 1);
            } else {
                setCapacityOfAvailRes(i, i);
            }
        }
    }

    /**
     * To print err the activity pattern
     */
    public String printAllPatternsOfActivity() {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < getAllPatterns().length; i++) {
            buf.append("\tpattern #").append(i).append(": ");
            buf.append(allPatterns[i]);
        }
        return buf.toString();
    }

    public static void main(String[] args) {
        for (Method m : MunicipalitySet.class.getDeclaredMethods()) {
            Observable ann = m.getAnnotation(Observable.class);
            if (ann
                    != null) {
                System.out.println("|" + m.getName() + " |" + ann.description() + " |");
            }
        }
    }
}
