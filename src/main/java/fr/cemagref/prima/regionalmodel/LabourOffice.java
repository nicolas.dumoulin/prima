/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.prima.regionalmodel.MunicipalitiesNetwork.SubNetwork;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Provides the availability of jobs in the region.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class LabourOffice {

    private List<Municipality>[] availabilitiesByJob;
    private List<ActivityPattern>[] activityPatternsByProfession;
    private List<ActivityPattern>[] monoActivityPatternsByProfession;
    private ActivityPattern[] monoActivityPatternsByJob;
    private MunicipalitySet region;
    private MunicipalitiesNetwork network;

    public LabourOffice(MunicipalitySet region) {
        this.region = region;
    }

    public void initBase() throws BadDataException {
        // init
        activityPatternsByProfession = new List[region.getNbProfessions()];
        monoActivityPatternsByProfession = new List[region.getNbProfessions()];
        monoActivityPatternsByJob = new ActivityPattern[region.getRegionalActivities().length];
        // init the base of municipalities by job
        availabilitiesByJob = new List[region.getRegionalActivities().length];
        for (int i = 0; i < availabilitiesByJob.length; i++) {
            List<Municipality> list = new LinkedList<Municipality>();
            availabilitiesByJob[i] = list;
            // init from municipalities data
            for (Municipality municipality : region.getMyMunicipalities()) {
                if (municipality.getFreeActivities(i) > 0) {
                    list.add(municipality);
                }
            }
        }
        // init the activity patterns
        for (int profession = 0; profession < region.getNbProfessions(); profession++) {
            List<Integer> activitiesIndexes = new ArrayList<Integer>();
            int index = 0;
            // retrieves activity that match the profession
            // SH : TODO THIS METHOD IS EQUIVALENT TO GETJOBS ???
            for (Activity activity : region.getRegionalActivities()) {
                if (activity.getProfession() == profession) {
                    activitiesIndexes.add(index);
                }
                index++;
            }
            // retrieves the pattern that match the activity
            activityPatternsByProfession[profession] = new ArrayList<ActivityPattern>();
            monoActivityPatternsByProfession[profession] = new ArrayList<ActivityPattern>();
            for (ActivityPattern activityPattern : region.getAllPatterns()) {
                for (Integer activityIndex : activitiesIndexes) {
                    if (activityPattern.getPattern()[activityIndex]) {
                        activityPatternsByProfession[profession].add(activityPattern);
                        if (activityPattern.getJob() > -1) {
                            monoActivityPatternsByProfession[profession].add(activityPattern);
                            monoActivityPatternsByJob[activityIndex] = activityPattern;
                        }
                        break;
                    }
                }
            }
        }
    }

    public void initNetwork() throws BadDataException {
        network = new MunicipalitiesNetwork(region, region.getParameters().getJobResearchMunNetworkClasses(), region.getParameters().getJobResearchMunNetworkProbas());
    }

    public boolean isNetworkLoaded() {
        return network != null;
    }

    public MunicipalitiesNetwork getNetwork() {
        return network;
    }

    /**
     * Find the municipalities in which a job is available.
     * @param pattern The type of job
     * @return The list of matching municipalities
     */
    public JobProposition getMunicipalitiesForJob(int job) {
        return new JobProposition(monoActivityPatternsByJob[job], availabilitiesByJob[job].toArray(new Municipality[]{}));
    }

    /**
     * Find the municipalities in which an activity pattern is available.
     * A municipality have to match all the
     * jobs of the pattern to be selected.
     * It is assumed that looking at the regional level, in case of pluriactive activity pattern,
     * the two jobs have to be located in the same municipality
     * @param pattern The activity pattern looked for
     * @return The list of matching municipalities
     */
    public List<JobProposition> getMunicipalitiesForActivityPattern(ActivityPattern pattern) {
        List<Municipality> municipalities = new ArrayList<Municipality>();
        int job = pattern.getJob();
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        // get all municipalities for the first job of the pattern
        if (job > -1) {
            municipalities.addAll(availabilitiesByJob[job]);
        }
        for (Municipality mun : municipalities) {
            jobPropositions.add(new JobProposition(pattern, new Municipality[]{mun}));
        }
        return jobPropositions;
    }

    /**
     * Find the municipalities providing activity corresponding to a given
     *  profession. Only monoactivity patterns are taken into consideration.
     *
     * May return two jobProposition for same municipalities, but for two
     * different patterns that match the profession.
     * @param profession The profession looked for
     * @return The list of matching municipalities
     */
    public List<JobProposition> getMunicipalitiesForProfession(int profession) {
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        // only look for pattern with only one activity
        for (ActivityPattern actPat : monoActivityPatternsByProfession[profession]) {
            jobPropositions.addAll(getMunicipalitiesForActivityPattern(actPat));
        }
        return jobPropositions;
    }

    /**
     * Find the municipalities in a neighbourhood in which an activity pattern is available.
     * A municipality have to match only one of the jobs of the pattern to be selected.
     * But, if no all the jobs of the pattern have been matched by a municipality,
     * <tt>null</tt> is returned.
     * @param pattern The activity pattern looked for
     * @param municipalities the neighbourood
     * @return The list of matching municipalities, may be null
     */
    public List<JobProposition> getLocalMunicipalitiesForActivityPattern(ActivityPattern pattern, List<Municipality> municipalities) {
        int job = pattern.getJob();
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        // looking for municipalities offering each job of the pattern
        List<List<Municipality>> matchingMunicipalitiesByJobs = new ArrayList<List<Municipality>>();
        List<Municipality> matchingMunicipalitiesForAJob;
        matchingMunicipalitiesForAJob = new ArrayList<Municipality>();
        matchingMunicipalitiesByJobs.add(matchingMunicipalitiesForAJob);
        // for each municipality matching this job of the pattern
        for (Municipality foundMun : availabilitiesByJob[job]) {
            // if municipality in neighbourhood
            if (municipalities.contains(foundMun)) {
                // OK, let's add it to the list
                matchingMunicipalitiesForAJob.add(foundMun);
            }
        }
        // municipalities will be added in this list to avoid array reconstruction
        //  in JobProposition instance
        // building proposition for first job of the pattern
        List<Municipality> municipalitiesForJobProposition;
        municipalitiesForJobProposition = new ArrayList<Municipality>();
        matchingMunicipalitiesForAJob = matchingMunicipalitiesByJobs.remove(0);
        for (Municipality matchingMunicipality : matchingMunicipalitiesForAJob) {
            municipalitiesForJobProposition.add(matchingMunicipality);
        }
        // Now, we can build the JobProposition instances
        if (municipalitiesForJobProposition.isEmpty()) {
            return jobPropositions;
        }
        for (Municipality municipalitys : municipalitiesForJobProposition) {
            jobPropositions.add(new JobProposition(pattern, municipalitys));
        }
        return jobPropositions;
    }

    public List<JobProposition> getOfferInOutside(int profession) {
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        for (ActivityPattern actPat : monoActivityPatternsByProfession[profession]) {
            for (Municipality outside : region.getOutsides()) {
                jobPropositions.add(new JobProposition(actPat, outside));
            }
        }
        return jobPropositions;
    }

    /**
     * Find the municipalities in the municipality and in its reachable outside
     *  the <b>monoactive</b> where activity patterns corresponding to a given profession
     *  are available. Only monoactivity patterns are taken into consideration.
     * @param vil The municipalities determining the neighbourhood where to search
     * @param profession The profession looked for
     * @return The list of matching municipalities
     */
    public List<JobProposition> getLocalMunicipalitiesForProfession(Municipality vil, int profession) {
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        SubNetwork nextSubNetwork = network.nextSubNetwork(vil);
        for (ActivityPattern actPat : monoActivityPatternsByProfession[profession]) {
            if (actPat.getJob() > -1) {
                List<JobProposition> localMunicipalitiesForActivityPattern = getLocalMunicipalitiesForActivityPattern(actPat, nextSubNetwork.getMunicipalities());
                if (localMunicipalitiesForActivityPattern != null) {
                    jobPropositions.addAll(localMunicipalitiesForActivityPattern);
                }
            }
        }
        return jobPropositions;
    }

    public List<JobProposition> getLocalMunicipalitiesForProfession(SubNetwork subnetwork, int profession) {
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        for (ActivityPattern actPat : monoActivityPatternsByProfession[profession]) {
            List<JobProposition> localMunicipalitiesForActivityPattern = getLocalMunicipalitiesForActivityPattern(actPat, subnetwork.getMunicipalities());
            if (localMunicipalitiesForActivityPattern != null) {
                jobPropositions.addAll(localMunicipalitiesForActivityPattern);
            }
        }
        return jobPropositions;
    }

    /**
     * Return the jobs available for a profession in a municipality.
     * @param mun
     * @param profession
     * @return 
     */
    public List<JobProposition> getJobPropositionsInMunicipality(Municipality mun, int profession) {
        List<JobProposition> jobPropositions = new ArrayList<JobProposition>();
        for (ActivityPattern actPat : monoActivityPatternsByProfession[profession]) {
            if (mun.getFreeActivities(actPat.getJob()) > 0) {
                jobPropositions.add(new JobProposition(actPat, mun));
            }
        }
        return jobPropositions;
    }

    /**
     * Update the job availability of the municipality, if a job is taken off.
     * @param job
     * @param municipality
     */
    public void takeJob(int job, Municipality inMunicipality) {
        if (inMunicipality.getFreeActivities(job) == 0) {
            availabilitiesByJob[job].remove(inMunicipality);
        }
    }

    /**
     * Update the job availability of the municipality, if a job is released.
     * @param job
     * @param municipality
     */
    public void releaseJob(int job, Municipality inMunicipality) {
        if ((inMunicipality.getFreeActivities(job) > 0) && !(availabilitiesByJob[job].contains(inMunicipality))) {
            availabilitiesByJob[job].add(inMunicipality);
        }
    }

    /**
     * Recompute the data of the labor office for a job against the availability
     * of jobs in this municipality.
     * @param job
     * @param municipality
     */
    public void updateAvailabilities(int job, Municipality municipality) {
        if (availabilitiesByJob[job].contains(municipality)) {
            if (municipality.getFreeActivities(job) <= 0) {
                availabilitiesByJob[job].remove(municipality);
            }
        } else {
            if (municipality.getFreeActivities(job) > 0) {
                availabilitiesByJob[job].add(municipality);
            }
        }
        //int availCompteur = (int) (municipality.getOfferedActivities()[job] - (municipality.getOccupiedActivitiesByRes()[job]
        //+ municipality.getOccupiedActivitiesByExt()[job] + municipality.getOccupiedActivitiesByOutside()[job]));
        //if (job == 5) System.err.println("job "+job+" available in "+availabilitiesByJob[job].toString()+" avail with counters "+availCompteur+" for mun "+municipality) ;
    }

    public void updateAvailabilities(Municipality municipality) {
        for (int i = 0; i < availabilitiesByJob.length; i++) {
            updateAvailabilities(i, municipality);
        }
    }

    public class JobProposition {

        private ActivityPattern pattern;
        private Municipality[] jobsLocations;

        JobProposition(ActivityPattern pattern, Municipality... jobsLocations) {
            this.pattern = pattern;
            this.jobsLocations = jobsLocations;
        }

        /**
         * Provides the municipalities that match for the jobs of the wanted pattern.
         * The order of the municipalities correspond to the order of jobs in the pattern.
         * @return an array of municipalities
         */
        public Municipality[] getJobsLocations() {
            return jobsLocations;
        }

        public ActivityPattern getPattern() {
            return pattern;
        }

        @Override
        public String toString() {
            StringBuilder buf = new StringBuilder(pattern.toString());
            for (Municipality municipality : jobsLocations) {
                buf.append(", ").append(municipality);
            }
            return buf.toString();
        }
    }
}
