/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios;

import fr.cemagref.prima.regionalmodel.Household;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class LocalImmigrationEvent extends AbstractImmigrationEvent<Municipality> {

    protected int nbHHImmigrants;
    protected int migratoryBalance;

    public LocalImmigrationEvent(ImmigrationUpdater immigrationUpdater, String param) {
        super(immigrationUpdater);
        this.nbHHImmigrants = Integer.parseInt(param);
    }

    @Override
    public final void process(Municipality municipality) throws ProcessingException {
        clearPreviousImmigrantsIfNeeded(municipality);
        // Action for the past period (when we exactly know the number of in-migrant households
        List<Household> immigrants = listPossibleMigrants(municipality.getMyRegion(), nbHHImmigrants, false);
        LOGGER.log(Level.FINER, "In municipality {0}: {1} HH immigrants incoming", new Object[]{municipality.getName(), immigrants.size()});
        municipality.addTheMigrants(immigrants);
        int immigIndiv = 0;
        int immigrantLess31 = 0;
        for (Household hh : municipality.getMyHouseholds()) {
            if (hh.getResidence().isTransit()) {
                postprocessImmigrants(hh);
                immigIndiv = immigIndiv + hh.size();
                for (Individual ind : hh) {
                    if (ind.getAge() < 31) {
                        immigrantLess31++;
                    }
                }
            }
        }
        LOGGER.log(Level.FINER, "In municipality {0}: {1} immigrants incoming", new Object[]{municipality.getName(), immigIndiv});
        LOGGER.log(Level.FINER, "In municipality {0}: {1} immigrants incoming being less than 31", new Object[]{municipality.getName(), immigrantLess31});
    }
}
