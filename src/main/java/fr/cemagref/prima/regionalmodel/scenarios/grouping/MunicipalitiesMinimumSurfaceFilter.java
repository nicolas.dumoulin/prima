/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.cemagref.prima.regionalmodel.scenarios.grouping;

import fr.cemagref.prima.regionalmodel.Municipality;

/**
 * A filter for selecting only municipalities above a fixed surface.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class MunicipalitiesMinimumSurfaceFilter implements MunicipalitiesFilter {

    protected double minimumSurface;

    public boolean accept(Municipality municipality) {
        return (municipality.getTotalSurface() >= minimumSurface);
    }

}
