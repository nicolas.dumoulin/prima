/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios;

import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.Municipality;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class TableUpdaterEvent implements Event<Municipality> {

    private Method updaterMethod;
    private Method finalUpdaterMethod;
    private TableUpdater.UpdateMode mode;
    private Object objectToUpdate;
    private Integer[] deltas;
    protected final static Logger LOGGER = Logger.getLogger(TableUpdaterEvent.class.getName());

    public TableUpdaterEvent(Method updaterMethod, Method finalUpdaterMethod, TableUpdater.UpdateMode mode, Object objectToUpdate, int category, int delta) throws BadDataException {
        this.updaterMethod = updaterMethod;
        this.finalUpdaterMethod = finalUpdaterMethod;
        this.mode = mode;
        this.objectToUpdate = objectToUpdate;
        this.deltas = new Integer[category + 1];
        Arrays.fill(deltas, 0);
        this.deltas[category] = delta;
    }

    public TableUpdaterEvent(Method updaterMethod, Method finalUpdaterMethod, TableUpdater.UpdateMode mode, Object objectToUpdate, Integer[] deltas) throws BadDataException {
        this.updaterMethod = updaterMethod;
        this.finalUpdaterMethod = finalUpdaterMethod;
        this.mode = mode;
        this.objectToUpdate = objectToUpdate;
        this.deltas = deltas;
    }

    @Override
    public void process(Municipality municipality) throws ProcessingException {
        LOGGER.log(Level.FINER, "update for {0} through {1}", new Object[]{municipality.getName(), this.updaterMethod.getName()});
        // update the table
        for (int i = 0; i < deltas.length; i++) {
            try {
                boolean methodAccessible = updaterMethod.isAccessible();
                updaterMethod.setAccessible(true);
                updaterMethod.invoke(objectToUpdate, i, deltas[i], mode);
                updaterMethod.setAccessible(methodAccessible);
            } catch (IllegalAccessException ex) {
                throw buildProcessingException(municipality, ex);
            } catch (IllegalArgumentException ex) {
                throw buildProcessingException(municipality, ex);
            } catch (InvocationTargetException ex) {
                throw buildProcessingException(municipality, ex);
            }
        }
        if (finalUpdaterMethod != null) {
            try {
                boolean methodAccessible = finalUpdaterMethod.isAccessible();
                finalUpdaterMethod.setAccessible(true);
                finalUpdaterMethod.invoke(objectToUpdate);
                finalUpdaterMethod.setAccessible(methodAccessible);
            } catch (IllegalAccessException ex) {
                throw buildProcessingException(municipality, ex);
            } catch (IllegalArgumentException ex) {
                throw buildProcessingException(municipality, ex);
            } catch (InvocationTargetException ex) {
                throw buildProcessingException(municipality, ex);
            }
        }
    }

    protected final ProcessingException buildProcessingException(Municipality mun, Exception cause) {
        return new ProcessingException("Error while processing scenario on method " + updaterMethod.getName()
                + "\n  in municipality " + mun.getName() + "\n  deltas to apply: " + Arrays.toString(deltas), cause);
    }

    public int getDelta(int category) {
        return deltas[category];
    }

    public void incDelta(int category, int i) {
        deltas[category] += i;
    }

    public int getDeltasSize() {
        return deltas.length;
    }

    @Override
    public String toString() {
        return TableUpdaterEvent.class.getSimpleName() + " with updater method: " + updaterMethod.getName();
    }
}
