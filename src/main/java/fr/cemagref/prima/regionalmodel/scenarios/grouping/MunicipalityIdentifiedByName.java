/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios.grouping;

import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.prima.regionalmodel.Municipality;
import java.util.regex.Pattern;
import org.openide.util.lookup.ServiceProvider;

/**
 * Filter that find a municipality by its name, including regular expression matching.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = MunicipalitiesFilter.class)
public class MunicipalityIdentifiedByName implements MunicipalitiesFilter {

    @Description(name = "Name of municipality to match", tooltip = "Regular expressions can be used for matching names")
    private String name;
    @Description(name = "Exclusion filter", tooltip = "If checked, the filter will select municipalities that doesn't match the given name")
    private boolean exclude = false;
    private transient Pattern pattern;

    public Object readResolve() {
        pattern = Pattern.compile(name);
        return this;
    }

    public MunicipalityIdentifiedByName() {
    }

    public static MunicipalityIdentifiedByName createInstance(String name, boolean exclude) {
        MunicipalityIdentifiedByName filter = new MunicipalityIdentifiedByName();
        filter.name = name;
        filter.exclude = exclude;
        filter.readResolve();
        return filter;
    }

    @Override
    public boolean accept(Municipality municipality) {
        return exclude != pattern.matcher(municipality.getName()).matches();
    }
}
