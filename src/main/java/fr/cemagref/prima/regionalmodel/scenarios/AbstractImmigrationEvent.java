/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.scenarios;

import fr.cemagref.prima.regionalmodel.Household;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public abstract class AbstractImmigrationEvent<T> implements Event<T> {

    protected final static Logger LOGGER = Logger.getLogger(AbstractImmigrationEvent.class.getName());
    protected ImmigrationUpdater immigrationUpdater;

    public AbstractImmigrationEvent(ImmigrationUpdater immigrationUpdater) {
        this.immigrationUpdater = immigrationUpdater;
    }

    /**
     * Method to generate potential migrants S. Huet, 3.11.2010 by adding some
     * randomly chosen household having the convenient properties
     *
     * @param municipalitySet
     * @param nb
     * @param countIndividuals if true, return a list of households with a sum
     * of <tt>nb</tt> individuals, otherwise it returns <tt>nb</tt> households
     * @return
     */
    protected List<Household> listPossibleMigrants(MunicipalitySet municipalitySet, int nb, boolean countIndividuals) {
        List<Household> households = new ArrayList<Household>(nb);
        // for the past period
        for (int i = 0; i < nb;) {
            Municipality mun = municipalitySet.getRandom().nextObject(municipalitySet.getMyMunicipalities());
            if (mun.getMyHouseholds().size() > 0) {
                Household hh = municipalitySet.getRandom().nextObject(mun.getMyHouseholds());
                if (evaluate(hh)) {
                    if (countIndividuals) {
                        Household hsh = new Household(hh, true) ;
                        hsh.createMigrants(hh);
                        households.add(hsh);
                        i = i + hh.size();
                    } else {
                        Household hsh = new Household(hh, true) ;
                        hsh.createMigrants(hh);
                        households.add(hsh);
                        i++;
                    }
                }
            }
        }
        return households;
    }

    protected void clearPreviousImmigrantsIfNeeded(List<Municipality> municipalities) {
        if (immigrationUpdater.clearPreviousMigrants()) {
            for (Municipality mun : municipalities) {
                clearPreviousImmigrantsIfNeeded(mun);
            }
        }
    }

    protected void clearPreviousImmigrantsIfNeeded(Municipality municipality) {
        if (immigrationUpdater.clearPreviousMigrants()) {
            for (Iterator<Household> it = municipality.getMyHouseholds().iterator(); it.hasNext();) {
                Household hh = it.next();
                if (hh.getResidence().isTransit()) {
                    it.remove();
                }
            }
        }
    }

    /**
     * Method to decide if an household can represent an immigrant household S.
     * Huet, 3.11.2010
     */
    protected boolean evaluate(Household hh) {
        return (!hh.getResidence().isTransit());
    }

    /**
     * This method is called after the insertion of immigrants. It is a hook to
     * perform additionnal processing of these new households.
     *
     * @param hh
     */
    protected void postprocessImmigrants(Household hh) {
        hh.setNeedOfResidence(true);
    }
}
