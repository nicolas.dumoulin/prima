/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.tools;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Municipality;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility class that wraps CSVReader from opencsv library and add convenient
 * methods.
 *
 * <p>Default separator is ';'. If numerical values are wroten with a ',', it
 * will be replaced by a '.' (international format). <p>Any line beginning with
 * the character '#' is considered as a comment.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class CSV {

    public static class FileCSVReader extends CSVReader {

        private File file;

        public FileCSVReader(File file, char separator) throws FileNotFoundException {
            super(new FileReader(file), separator);
            this.file = file;
        }

        public File getFile() {
            return file;
        }
    }
    private static final char DEFAULT_SEPARATOR = ';';

    public static FileCSVReader getReader(String filename, Character separator) throws FileNotFoundException {
        return getReader(new File(filename), separator);
    }

    public static FileCSVReader getReader(File file, Character separator) throws FileNotFoundException {
        return new FileCSVReader(file, separator == null ? DEFAULT_SEPARATOR : separator);
    }

    /**
     * Create a CSVReader for reading an file concerning a municipality, with
     * the default delimiter ";".
     *
     * @param skipHeaders if true, the first line will be skipped in the readed
     * file
     * @param municipality the municipality concerned
     * @param filename the name of the file to read in the directory of the
     * municipality
     * @return
     * @throws BadDataException in case of reading errors
     */
    public static CSVReader getReader(boolean skipHeaders, Municipality municipality, String filename) throws BadDataException {
        return getReader(skipHeaders, municipality, filename, DEFAULT_SEPARATOR);
    }

    /**
     * Create a CSVReader for reading an file concerning a municipality.
     *
     * @param skipHeaders if true, the first line will be skipped in the readed
     * file
     * @param municipality the municipality concerned
     * @param filename the name of the file to read in the directory of the
     * municipality
     * @param separator the delimiter to use for separating entries. If null,
     * the default delimiter ";" will be used.
     * @return
     * @throws BadDataException in case of reading errors
     */
    public static CSVReader getReader(boolean skipHeaders, Municipality municipality, String filename, Character separator) throws BadDataException {
        try {
            File municipalityFilename = new File(municipality.getMyRegion().getMyApplication().getMunicipalityDirectory(municipality), filename);
            CSVReader reader = new CSVReader(new FileReader(municipalityFilename), separator == null ? DEFAULT_SEPARATOR
                    : separator);
            if (skipHeaders) {
                // skip first line (headers)
                reader.readNext();
            }
            return reader;
        } catch (IOException ex) {
            throw new BadDataException("Error during initialization", ex);
        }
    }

    public static void readYearlyData(CSVReader reader, ArrayParser parser) throws IOException, BadDataException {
        String[] line;
        while ((line = readLine(reader)) != null) {
            int date = Integer.parseInt(line[0]);
            parser.parse(date, Arrays.copyOfRange(line, 1, line.length));
        }
    }

    /**
     * Read the next no-commented line in a csv file. Return null if no such a
     * line remains.
     *
     * @param reader
     * @return the data readen, can be null.
     * @throws IOException
     */
    public static String[] readLine(CSVReader reader) throws IOException {
        String[] line = reader.readNext();
        while (line != null && (line.length == 0 || (line.length == 1 && line[0].length() == 0) || line[0].startsWith("#"))) {
            // skip comment lines
            line = reader.readNext();
        }
        if (line != null && (line.length == 0 || (line.length == 1 && line[0].length() == 0) || line[0].startsWith("#"))) {
            line = null;
        }
        return line;
    }

    public static Integer[] nextIntegersLine(CSVReader reader) throws BadDataException {
        List<Integer> list = parseNextLine(reader, new ColumnParser<Integer>() {
            @Override
            public Integer parse(int column, String s) {
                return Integer.parseInt(s);
            }
        });
        return list == null ? null : list.toArray(new Integer[0]);
    }

    public static Double[] nextDoublesLine(CSVReader reader) throws BadDataException {
        List<Double> list = parseNextLine(reader, new ColumnParser<Double>() {
            @Override
            public Double parse(int column, String s) {
                return Double.parseDouble(s);
            }
        });
        return list == null ? null : list.toArray(new Double[0]);
    }

    public static <T> List<T> parseNextLine(CSVReader reader, ColumnParser<T> parser) throws BadDataException {
        try {
            String[] line = readLine(reader);
            List<T> data = null;
            if (line != null) {
                try {
                    data = new ArrayList<T>(line.length);
                    for (int i = 0; i < line.length; i++) {
                        if (line[i].trim().length() > 0) {
                            data.add(parser.parse(i, line[i].trim().replace(",", ".")));
                        }
                    }
                } catch (Exception ex) {
                    if (reader instanceof FileCSVReader) {
                        throw new BadDataException("Error while parsing this CSV data: " + Arrays.toString(line) + " from the file " + ((FileCSVReader) reader).getFile(), ex);
                    } else {
                        throw new BadDataException("Error while parsing this CSV data: " + Arrays.toString(line), ex);
                    }
                }
            }
            return data;
        } catch (Exception ex) {
            if (reader instanceof FileCSVReader) {
                throw new BadDataException("Error while reading the CSV data from the file " + ((FileCSVReader) reader).getFile(), ex);
            } else {
                throw new BadDataException("Error while reading the CSV data", ex);
            }
        }
    }

    public static interface ColumnParser<T> {

        public T parse(int column, String s);
    }

    public static interface ArrayParser<T> {

        public void parse(int date, String[] copyOfRange) throws BadDataException;
    }
}
