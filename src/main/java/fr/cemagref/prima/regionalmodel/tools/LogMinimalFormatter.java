/*
 *  Copyright (C) 2010 dumoulin
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.tools;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author dumoulin
 */
public class LogMinimalFormatter extends Formatter {

    private static final String prefix = "[";
    private static final String suffix = "] ";
    public static final String endline = "\n";
    private static final SimpleFormatter simpleFormatter = new SimpleFormatter();
    private static final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");

    @Override
    public String format(LogRecord record) {
        if (record.getLevel() == Level.SEVERE) {
            return simpleFormatter.format(record);
        } else {
            StringBuilder buff;
            if (record.getMessage() == null) {
                buff = new StringBuilder(30);
            } else {
                buff = new StringBuilder(record.getMessage().length() + 30);
            }
            buff.append(prefix);
            buff.append(formatter.format(new Date(record.getMillis())).toString());
            buff.append(":");
            buff.append(record.getLevel());
            buff.append(":");
            buff.append(record.getLoggerName().replace("fr.cemagref.prima.regionalmodel", "p.rm"));
            buff.append(suffix);
            buff.append(super.formatMessage(record));
            buff.append(endline);
            if (record.getThrown() != null) {
                StringWriter sink = new StringWriter();
                record.getThrown().printStackTrace(new PrintWriter(sink, true));
                buff.append(sink.toString());
            }
            return buff.toString();
        }
    }
}
