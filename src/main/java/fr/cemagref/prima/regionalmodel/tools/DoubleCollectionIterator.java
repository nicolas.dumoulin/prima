/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.tools;

import java.util.List;

/**
 * Utility classes for iterating on an array of Double, double, Float or float
 * for retrieving a double value.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public interface DoubleCollectionIterator<T> {

    public double get(T array, int index);

    public int size(T array);
    public final static DoubleCollectionIterator<List<Double>> fromDoubleClassList = new DoubleCollectionIterator<List<Double>>() {

        @Override
        public double get(List<Double> array, int index) {
            return array.get(index);
        }

        @Override
        public int size(List<Double> array) {
            return array.size();
        }
    };

    abstract static class DoubleArrayIterator<T> implements DoubleCollectionIterator<T> {

        @Override
        public int size(T array) {
            return ((Object[]) array).length;
        }
    }
    public final static DoubleCollectionIterator<Double[]> fromDoubleClass = new DoubleArrayIterator<Double[]>() {

        @Override
        public double get(Double[] array, int index) {
            return array[index];
        }
    };
    public final static DoubleCollectionIterator<double[]> fromDoublePrimitive = new DoubleArrayIterator<double[]>() {

        @Override
        public double get(double[] array, int index) {
            return array[index];
        }

        @Override
        public int size(double[] array) {
            return array.length;
        }
    };
    public final static DoubleCollectionIterator<Float[]> fromFloatClass = new DoubleArrayIterator<Float[]>() {

        @Override
        public double get(Float[] array, int index) {
            return array[index];
        }
    };
    public final static DoubleCollectionIterator<float[]> fromFloatPrimitive = new DoubleArrayIterator<float[]>() {

        @Override
        public double get(float[] array, int index) {
            return array[index];
        }

        @Override
        public int size(float[] array) {
            return array.length;
        }
    };
}
