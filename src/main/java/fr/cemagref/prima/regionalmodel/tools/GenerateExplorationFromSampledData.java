/*
 * Copyright (C) 2012 dumoulin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.tools;

import au.com.bytecode.opencsv.CSVReader;
import java.io.IOException;
import java.io.PrintStream;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Read a table of sampled input values and write the corresponding XML for the
 * exploration input file.
 * The CSV file must contain on the first line the name of the parameter for each column.
 * All the columns are used as input parameter for the generated set of simulation parameters.
 *
 * @author dumoulin
 */
public class GenerateExplorationFromSampledData {

    public static void main(String[] args) throws IOException {
        PrintStream output = System.out;
        String filename = null;
        if (args.length == 1) {
            filename = args[0];
        } else {
            JFileChooser jfc = new JFileChooser("");
            jfc.setFileFilter(new FileNameExtensionFilter("CSV file", "csv", "txt"));
            if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                filename = jfc.getSelectedFile().getAbsolutePath();
            }
            output.println("Paste the following XML in your exploration input file behind the element \"<Exploration>\":\n");
        }
        CSVReader reader = CSV.getReader(filename, ';');
        String[] headers = CSV.readLine(reader);
        String[] line;
        output.println("<parametersValueBySimulation>");
        while ((line = CSV.readLine(reader)) != null) {
            output.println("  <list>");
            for (int i = 0; i < line.length; i++) {
                output.println("    <fr.cemagref.prima.regionalmodel.Exploration_-ParameterValue>");
                output.println("      <name>" + headers[i] + "</name>");
                String value = line[i];
                String type = null;
                if (value.matches("[0-9]*,[0-9]+")) {
                    type = "double";
                    value = value.replace(",", ".");
                } else if (value.matches("[0-9]*\\.[0-9]+")) {
                    type = "double";
                } else if (value.matches("[0-9]*\\.[0-9]+E[+-][0-9]+")) {
                    type = "double";
                } else if (value.matches("[0-9]*")) {
                    type = "int";
                } else {
                    type = "string";
                }
                output.print("      <value");
                if (type != null) {
                    output.print(" class=\"" + type + "\"");
                }
                output.println(">" + value + "</value>");
                output.println("    </fr.cemagref.prima.regionalmodel.Exploration_-ParameterValue>");
            }
            output.println("  </list>");
        }
        output.println("</parametersValueBySimulation>");
    }
}
