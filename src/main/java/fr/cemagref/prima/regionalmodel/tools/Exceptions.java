/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.tools;

import java.awt.Component;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class Exceptions {

    public static void displayException(Component parent, String title, String message, Exception ex, Logger logger) {
        StringBuilder s;
        if (message == null || message.length() == 0) {
            s = new StringBuilder("An error has occured !");
        } else {
            s = new StringBuilder(message);
        }
        s.append("\n").append(ex.getMessage());
        Throwable cause = ex.getCause();
        while (cause != null) {
            s.append("\n  caused by: ");
            s.append(cause.getMessage());
            cause = cause.getCause();
        }
        s.append("\n\nYou can report this error by sending the full console output to developper team.");
        JOptionPane.showMessageDialog(parent, s.toString(), "Error", JOptionPane.ERROR_MESSAGE);
        logger.log(Level.SEVERE, message, ex);
    }

    public static void displayException(Component parent, Exception ex, Logger logger) {
        displayException(parent, "Error", "", ex, logger);
    }
}
