/*
 * Copyright (C) 2012 dumoulin
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * ObservablesConfigurator.java
 *
 * Created on 3 févr. 2012, 11:17:31
 */
package fr.cemagref.prima.regionalmodel.tools;

import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 *
 * @author dumoulin
 */
public class ObservablesConfigurator extends javax.swing.JFrame {

    private DefaultListModel listModel;
    private Class[] availableClasses = new Class[]{MunicipalitySet.class};

    /** Creates new form ObservablesConfigurator */
    public ObservablesConfigurator() {
        listModel = new DefaultListModel();
        initComponents();
        classComboBox.setSelectedIndex(0);
        jList1.setCellRenderer(new CheckListRenderer());
        jList1.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                int index = jList1.locationToIndex(e.getPoint());
                CheckableItem item = (CheckableItem) jList1.getModel().getElementAt(index);
                item.setSelected(!item.isSelected());
                Rectangle rect = jList1.getCellBounds(index, index);
                jList1.repaint(rect);
            }
        });

        populateObservablesList();
    }

    private void populateObservablesList() {
        listModel.clear();
        Class currentClass = availableClasses[classComboBox.getSelectedIndex()];
        List<CheckableItem> items = new ArrayList<CheckableItem>();
        for (Method m : currentClass.getDeclaredMethods()) {
            Observable ann = m.getAnnotation(Observable.class);
            if (ann != null) {
                items.add(new CheckableItem(m.getName(), ann.description()));
            }
        }
        Collections.sort(items, new Comparator<CheckableItem>() {

            @Override
            public int compare(CheckableItem o1, CheckableItem o2) {
                return o1.getLabel().compareToIgnoreCase(o2.getLabel());
            }
        });
        for (CheckableItem item : items) {
            listModel.addElement(item);
        }
    }

    private void printXML() {
        jTextArea1.setText("");
        for (Enumeration elts = listModel.elements(); elts.hasMoreElements();) {
            CheckableItem item = (CheckableItem) elts.nextElement();
            if (item.isSelected()) {
                jTextArea1.append("<string>");
                jTextArea1.append(item.getLabel());
                jTextArea1.append("</string>\n");
            }
        }
    }

    private class CheckListRenderer extends JCheckBox implements ListCellRenderer {

        @Override
        public Component getListCellRendererComponent(JList list, Object item, int index, boolean isSelected, boolean hasFocus) {
            setEnabled(list.isEnabled());
            setSelected(((CheckableItem) item).isSelected());
            setFont(list.getFont());
            setText(item.toString());
            setToolTipText(((CheckableItem) item).getDescription());
            return this;
        }
    }

    private class CheckableItem {

        private String label;
        private String description;
        private boolean isSelected;

        public CheckableItem(String label) {
            this.label = label;
            isSelected = false;
        }

        public CheckableItem(String label, String description) {
            this(label);
            this.description = description;
        }

        public void setSelected(boolean b) {
            isSelected = b;
            printXML();
        }

        public boolean isSelected() {
            return isSelected;
        }

        public String getDescription() {
            return description;
        }

        public String getLabel() {
            return label;
        }

        @Override
        public String toString() {
            return label;
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        classComboBox = new javax.swing.JComboBox();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle(org.openide.util.NbBundle.getMessage(ObservablesConfigurator.class, "ObservablesConfigurator.title")); // NOI18N

        classComboBox.setModel(new DefaultComboBoxModel(availableClasses));

        jSplitPane1.setDividerLocation(500);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jList1.setModel(listModel);
        jScrollPane2.setViewportView(jList1);

        jSplitPane1.setTopComponent(jScrollPane2);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jSplitPane1.setRightComponent(jScrollPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 573, Short.MAX_VALUE)
                    .addComponent(classComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, 573, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(classComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                new ObservablesConfigurator().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox classComboBox;
    private javax.swing.JList jList1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextArea jTextArea1;
    // End of variables declaration//GEN-END:variables
}
