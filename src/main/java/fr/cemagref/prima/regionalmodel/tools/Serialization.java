/*
 * Copyright (C) 2013 Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.tools;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class Serialization {

    public static void serializeObject(Object object, String filename) throws ProcessingException {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(new GZIPOutputStream(fos));
            try {
                oos.writeObject(object);
                oos.flush();
            } finally {
                try {
                    oos.close();
                } finally {
                    fos.close();
                }
            }
        } catch (IOException ex) {
            throw new ProcessingException("Erro while trying to serialize an object of type " + object.getClass() + " in " + filename, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                throw new ProcessingException("Erro while trying to serialize an object of type " + object.getClass() + " in " + filename, ex);
            }
        }
    }

    public static Object deserializeObject(String filename) throws ProcessingException {
        Object object = null;
        try {
            FileInputStream fis = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(new GZIPInputStream(fis));
            try {
                object = ois.readObject();
            } finally {
                try {
                    ois.close();
                } finally {
                    fis.close();
                }
            }
        } catch (Exception ex) {
             throw new ProcessingException("Erro while trying to deserialize an object from " + filename, ex);
        }
        return object;
    }
}
