/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.gui.eventsbymunicipalities;

import fr.cemagref.prima.regionalmodel.Municipality;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class MunicipalityTreeNode implements TreeNode, TreeNodeWithChildren<EventTreeNode> {

    private Municipality municipality;
    private List<EventTreeNode> events;

    public MunicipalityTreeNode(Municipality municipality, List<EventTreeNode> events) {
        this.municipality = municipality;
        this.events = events;
    }

    MunicipalityTreeNode(Municipality municipality) {
        this(municipality, new ArrayList<EventTreeNode>());
    }

    public boolean add(EventTreeNode e) {
        return events.add(e);
    }

    @Override
    public String getDisplayName() {
        return municipality.getName();
    }

    public List<EventTreeNode> getEvents() {
        return events;
    }

    public Municipality getMunicipality() {
        return municipality;
    }

    @Override
    public List<EventTreeNode> getChildren() {
        return events;
    }
}
