/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.gui.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class ChartMenuAction extends AbstractAction {

    private JComponent chart;
    private JPanel panel;
    private int index;

    public ChartMenuAction(String label, JComponent chart, JPanel panel, int index, boolean enabled) {
        super(label);
        this.chart = chart;
        this.panel = panel;
        this.index = index;
        putValue(SELECTED_KEY, true);
        panel.add(chart, index);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if ((Boolean) getValue(SELECTED_KEY)) {
            panel.add(chart, index);
        } else {
            panel.remove(index);
        }
        panel.validate();
    }
}
