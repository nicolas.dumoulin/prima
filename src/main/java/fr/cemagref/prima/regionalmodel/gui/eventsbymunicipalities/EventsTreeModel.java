/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.gui.eventsbymunicipalities;

import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.scenarios.Event;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class EventsTreeModel implements TreeModel {

    private List<TreeModelListener> treeModelListeners;
    private MunicipalitiesTreeNode root;

    public EventsTreeModel(List<Event<List<Municipality>>> events, String tableName) {
        this.treeModelListeners = new ArrayList<TreeModelListener>();
        this.root = new MunicipalitiesTreeNode(events, tableName);
    }

    @Override
    public Object getRoot() {
        return root;
    }

    @Override
    public Object getChild(Object parent, int index) {
        return ((TreeNodeWithChildren)parent).getChildren().get(index);
    }

    @Override
    public int getChildCount(Object parent) {
        return ((TreeNodeWithChildren)parent).getChildren().size();
    }

    @Override
    public boolean isLeaf(Object node) {
        return node instanceof EventTreeNode || getChildCount(node) == 0;
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        // Nothing
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        return ((TreeNodeWithChildren)parent).getChildren().indexOf(child);
    }

    public void addChild() {
        // TODO finer event on child updated
        for (TreeModelListener treeModelListener : treeModelListeners) {
            treeModelListener.treeStructureChanged(new TreeModelEvent(this, new Object[]{root}));
        }
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        treeModelListeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        treeModelListeners.remove(l);
    }
}
