/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.gui;

import fr.cemagref.observation.gui.Drawable;
import fr.cemagref.observation.kernel.ObservableManager;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.prima.regionalmodel.MunicipalitySetCounters;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Class that display several chart observers. It intends to be used in parameter
 * "regionObservers" by specifying the observable to include for plotting.
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class ChartsPanel extends ObserverListener implements Drawable {

    private List<String> observables;
    private transient List<ObservablesHandler.ObservableFetcher> fetchers;
    private transient JPanel panel;
    private transient ObservablesHandler regionObservable;

    @Override
    public void addObservable(ObservablesHandler classObservable) {}

    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long t) {}

    @Override
    public void init() {
        panel = new JPanel(new GridLayout(0, 2));
    }

    public void initSubObservers(ObservableManager observableManager) {
        panel.removeAll();
        regionObservable = observableManager.addObservable(MunicipalitySetCounters.class);
        fetchers = new ArrayList<ObservablesHandler.ObservableFetcher>();
        if (observables != null) {
            for (String name : observables) {
                fetchers.add(regionObservable.getObservableFetcherByName(name));
            }
        } else {
            fetchers.addAll(Arrays.asList(regionObservable.getObservableFetchers()));
        }
        for (ObservablesHandler.ObservableFetcher fetcher : fetchers) {
            PopulationChart chart = new PopulationChart(fetcher);
            chart.init();
            regionObservable.addObserverListener((ObserverListener) chart);
            panel.add(chart.getDisplay());
        }
    }

    @Override
    public void close() {}

    @Override
    public String getTitle() {
        return "Municipality set charts";
    }

    @Override
    public JComponent getDisplay() {
        return panel;
    }
}
