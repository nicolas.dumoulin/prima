/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;
import org.openide.util.lookup.ServiceProvider;

/**
 * Parameter that returns a boolean value according to the current year, to the
 * age of the individual and a list of probas by age given as input.
 *
 * <p>Input format: a csv file with on the first line the age range lower
 * bounds, and for each line the the probas associated for each year
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class DecisionByYearByAgeAndProbas extends DecisionByAgeAndProbas {

    private transient SortedMap<Integer, SortedMap<Integer, Double>> probaByYearByAge;
    private transient int minAge, maxAge;

    @Override
    public void init(Application application) throws BadDataException {
        this.application = application;
        try {
            probaByYearByAge = new TreeMap<Integer, SortedMap<Integer, Double>>();
            String[] data;
            CSVReader reader = CSV.getReader(application.getFileFromRelativePath(filename), separator);
            Integer[] ages = CSV.nextIntegersLine(reader);
            minAge = ages[1];
            maxAge = ages[ages.length - 1];
            while ((data = CSV.readLine(reader)) != null) {
                SortedMap<Integer, Double> probasByAge = new TreeMap<Integer, Double>();
                for (int i = 1; i < ages.length; i++) {
                    probasByAge.put(ages[i], Double.parseDouble(data[i]));
                }
                probaByYearByAge.put(Integer.parseInt(data[0]), probasByAge);
            }
            reader.close();
        } catch (IOException ex) {
            throw new BadDataException("Error while loading " + filename, ex);
        }
    }

    @Override
   public Boolean getValue(Individual ind, int age) {
        int year = ind.getMyHousehold().getMyVillage().getMyRegion().getCurrentYear();
        year = year + age - ind.getAge();
        SortedMap<Integer, Double> probasByAge = probaByYearByAge.get(Math.min(year, probaByYearByAge.lastKey()));
        Double proba = Utils.getValueFromLowerBound(probasByAge, age);
        //if (ind.getAge() > 88) System.err.println("proba "+proba+" age indiv "+ind.getAge()+" id "+ind.getId()) ;
        return application.getRandom().nextDouble() < proba;
    }

    @Override
    public int getMinimumAge() {
        return minAge;
    }

    @Override
    public int getMaximumAge() {
        return maxAge;
    }

    @Override
    public Boolean getValue(Individual individual) {
        return getValue(individual, individual.getAge());
    }
}
