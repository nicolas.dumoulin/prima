/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.observation.gui.Drawable;
import fr.cemagref.observation.kernel.ObservableManager;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.observation.observers.CSVObserver;
import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.ohoui.annotations.SkipField;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.GlobalObserver;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalityCounters;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.MunicipalitySetCounters;
import fr.cemagref.prima.regionalmodel.dynamics.DynamicServiceEmployment;
import fr.cemagref.prima.regionalmodel.dynamics.Migration;
import fr.cemagref.prima.regionalmodel.scenarios.Scenario;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.Random;
import fr.cemagref.prima.regionalmodel.tools.ReflectUtils;
import fr.cemagref.prima.regionalmodel.tools.Serialization;
import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class Parameters {

    @Retention(RetentionPolicy.RUNTIME)
    private static @interface IsFile {
    }
    private static transient final Logger LOGGER = Logger.getLogger(Parameters.class.getName());
    @Description(name = "Index of the seed", tooltip = "Index of the seed to use for initializing the pseudo-random numbers generator")
    private Integer seedIndex;
    @Description(name = "Simulation start", tooltip = "Indicates at which step the simulation will start")
    private int startStep;
    @Description(name = "Simulation duration", tooltip = "Number of steps that will be elapsed during the simulation")
    private int nbStep;
    @Description(name = "Step duration", tooltip = "")
    private int step;
    private transient List<MunicipalityParameters> villagesParameters;
    private transient List<OutsideParameters> outsideParameters;
    private int nbSectorsActiv;
    private int nbTypesBySectorActiv;
    private transient int nbActivities;
    private int nbMaxOfActivityByPattern;
    private int nbSizeRes;
    private Value<Integer, Individual> ageToDie;
    private int ageMinHavingChild;
    private int ageMaxHavingChild;
    private transient double probaBirth;
    //private double nbChild;
    public Value<Double, Individual> nbChild;
    private double avDifAgeCouple;
    private double stDifAgeCouple;
    private double probabilityToMakeCouple;
    private transient int nbPat;
    private Value<Integer, Individual> firstProfession;
    private int nbOfUnactiveSituations;
    private int nbOfUnemployedSituations;
    private Value<Integer, Individual> ageEnterLabourMarket;
    private Value<Integer, Individual> ageToRetire;
    private Value<Boolean, Individual> remainInactive;
    private Value<Boolean, Individual> isInactiveAfterUnemployment;
    private Value<Boolean, Individual> isInactiveAfterEmployment;
    private Value<Integer, Individual> profIfUnactive;
    private Value<Integer, Individual> profIfWorking;
    private Value<Boolean, Integer> filterFireOnActivity;
    @IsFile
    private String municipalitiesDistances;
    @IsFile
    private String jobResearchMunNetworkClasses;
    @IsFile
    private String jobResearchMunNetworkProbas;
    @IsFile
    private String distToBorder;
    @IsFile
    private String commutOutsideProbas;
    private Migration migration;
    private double splittingProba;
    private double nbJoinTrials;
    private double farCloseThreshold;
    private double dispoThreshold; // indication for the price of the lodging in a city (correspond to nb of residence free/nb of residence offers)
    private double studentCityThreshold; // minimum size of a city susceptible to attract student as a study place
    private int resSatisfactMargin;
    private double probToAcceptNewResidence;
    private MunicipalitiesParametersLoader municipalitiesParameters;
    @IsFile
    private String scenarioFile;
    private List<Scenario> scenarios;
    private DynamicServiceEmployment dynamicServiceEmployment;
    private transient List<Updatable> updatableValuesCache;
    @SkipField
    private List<ObserverListener> regionObservers;
    @SkipField
    private List<GlobalObserver> globalObservers;

    public Parameters() {
    }

    public void loadFiles(Application application) throws BadDataException, ProcessingException {
        initRNG(application.getRandom());
        // load villages parameters
        municipalitiesParameters.loadData(application);
        villagesParameters = municipalitiesParameters.getMunicipalitiesParameters();
        outsideParameters = municipalitiesParameters.getOutsidesParameters();
        // init parameters
        for (Field field : Parameters.class.getDeclaredFields()) {
            if (Value.class.isAssignableFrom(field.getType())) {
                Object o = ReflectUtils.getFieldValue(field, this);
                if (o != null && Value.class.isAssignableFrom(o.getClass())) {
                    ((Value) o).init(application);
                }
            }
        }
        // load observers
        loadObservers(application);
    }

    public List<String> getInputFiles() {
        List<String> filenames = new ArrayList<String>();
        for (Field field : Parameters.class.getDeclaredFields()) {
            Object value = ReflectUtils.getFieldValue(field, this);
            if (value != null) {
                if (field.isAnnotationPresent(IsFile.class)) {
                    filenames.add((String)value);
                } else if (ConfiguredByFiles.class.isAssignableFrom(value.getClass())) {
                    filenames.addAll(((ConfiguredByFiles) value).getFilenames());
                }
            }
        }
        // TODO not all the filename parameters are retrieved
        return filenames;
    }

    public static void main(String[] args) throws Exception {
        Application app = new Application("dataIn/Cantal/ficEnt1_3D.xml");
        System.out.println(app.getParameters().getInputFiles());
    }

    /**
     * Save the villages parameters (including outside) into a compressed binary
     * file.
     *
     * @param filename
     * @throws ProcessingException
     */
    public void saveVillagesParametersToBinary(String filename) throws ProcessingException {
        List<List> data = new ArrayList<List>();
        data.add(villagesParameters);
        data.add(outsideParameters);
        Serialization.serializeObject(data, filename);
    }

    private void initRNG(Random random) throws ProcessingException {
        if (seedIndex == null) {
            random.resetNextSubstream(0);
            LOGGER.log(Level.WARNING, "Random numbers generator will be initialized with the default seed : {0}", random.getSeedToString());
        } else {
            random.resetNextSubstream(seedIndex);
            LOGGER.log(Level.INFO, "Random numbers generator has been initialized with the following seed : {0}", random.getSeedToString());
        }
    }

    public void loadObservers(Application application) throws ProcessingException {
        application.getObservableManager().clear();
        if (regionObservers != null) {
            ObservablesHandler regionObservable = application.getObservableManager().addObservable(MunicipalitySetCounters.class);
            for (ObserverListener observer : regionObservers) {
                if (!(observer instanceof Drawable) || application.isGui()) {
                    regionObservable.addObserverListener(observer);
                }
                if (observer instanceof CSVObserver) {
                    // Force output file to be created in the same directory than the input directory
                    try {
                        File inputFile = (File) ReflectUtils.getFieldValue("outputFile", observer);
                        ReflectUtils.setField("outputFile", observer, new File(application.getDataDirectory().getParentFile().getParentFile(), inputFile.getPath()));
                    } catch (Exception ex) {
                        throw new ProcessingException("Error during output file modification", ex);
                    }
                }
            }
        }
    }

    public void initGlobalObservers(MunicipalitySet region) throws ProcessingException {
        if (globalObservers != null) {
            for (GlobalObserver globalObserver : globalObservers) {
                globalObserver.init(region, this);
            }
        }
    }

    public void fireGlobalObservables(ObservableManager observableManager, MunicipalitySet region) {
        if (globalObservers != null) {
            List<ObservablesHandler> firedObservables = new ArrayList<ObservablesHandler>();
            for (GlobalObserver obs : globalObservers) {
                if (obs.getHandler() == MunicipalitySetCounters.class) {
                    ObservablesHandler observable = observableManager.addObservable(MunicipalitySetCounters.class);
                    if (!firedObservables.contains(observable)) {
                        observable.fireChanges(region.getCounters(), region.getCurrentYear());
                        firedObservables.add(observable);
                    }
                } else if (obs.getHandler() == MunicipalityCounters.class) {
                    ObservablesHandler observable = observableManager.addObservable(MunicipalityCounters.class);
                    if (!firedObservables.contains(observable)) {
                        for (Municipality mun : region.getMyMunicipalities()) {
                            observable.fireChanges(mun.getCounters(), region.getCurrentYear());
                        }
                        firedObservables.add(observable);
                    }
                } else if (obs.getHandler() == Municipality.class) {
                    ObservablesHandler observable = observableManager.addObservable(Municipality.class);
                    if (!firedObservables.contains(observable)) {
                        for (Municipality mun : region.getMyMunicipalities()) {
                            observable.fireChanges(mun, region.getCurrentYear());
                        }
                        firedObservables.add(observable);
                    }
                }
            }
        }
    }

    public Object readResolve() throws Exception {
        // fetch all instances of temporal value for putting in cache
        updatableValuesCache = new ArrayList<Updatable>();
        for (Field field : this.getClass().getDeclaredFields()) {
            Object o = field.get(this);
            if (o != null && Updatable.class.isAssignableFrom(o.getClass())) {
                updatableValuesCache.add((Updatable) o);
            }
        }
        updateValues(startStep);
        nbActivities = nbSectorsActiv * nbTypesBySectorActiv;
        //this.probaBirth = (double) (nbChild) / (double) ((float) (ageMaxHavingChild - ageMinHavingChild + 1));
        updateProbaBirth();
        // Reading the matrix of probability to change of activity pattern
        int nbAct = nbSectorsActiv * nbTypesBySectorActiv;
        nbPat = 1 + nbAct;
        for (int i = 1; i < nbMaxOfActivityByPattern; i++) {
            nbPat = nbPat + (nbAct * (nbAct - i)) / (nbMaxOfActivityByPattern - i);
        }
        LOGGER.log(Level.FINE, "nb Patterns {0}", nbPat);
        return this;
    }

    public void updateProbaBirth() {
        // we consider that the nbchild parameter is independent of individuals (getNbChild(null))
        this.probaBirth = (double) (getNbChild(null)) / (double) ((float) (ageMaxHavingChild - ageMinHavingChild + 1));
    }

    /**
     * Update the temporal values parameters caches, see
     * {@link TemporalValue#updateCurrentValue(int)}
     *
     * @param time
     */
    public void updateValues(int time) {
        for (Updatable value : updatableValuesCache) {
            value.update(time);
        }
        updateProbaBirth();
    }

    public void setNbTypesBySectorActiv(int nbTypesBySectorActiv) {
        this.nbTypesBySectorActiv = nbTypesBySectorActiv;
    }

    public Integer getSeedIndex() {
        return seedIndex;
    }

    public Value<Integer, Individual> getFirstProfession() {
        return firstProfession;
    }

    public Value<Boolean, Individual> getIsInactiveAfterEmployment() {
        return isInactiveAfterEmployment;
    }

    public Value<Boolean, Individual> getIsInactiveAfterUnemployment() {
        return isInactiveAfterUnemployment;
    }

    public int getNbMaxOfActivityByPattern() {
        return nbMaxOfActivityByPattern;
    }

    public Value<Integer, Individual> getProfIfUnactive() {
        return profIfUnactive;
    }

    public Value<Integer, Individual> getProfIfWorking() {
        return profIfWorking;
    }

    public Value<Boolean, Individual> getRemainInactive() {
        return remainInactive;
    }

    public int getResSatisfactMargin() {
        return resSatisfactMargin;
    }

    public List<MunicipalityParameters> getVillagesParameters() {
        return villagesParameters;
    }

    /**
     * Age max to have child
     */
    public int getAgeMaxHavingChild() {
        return ageMaxHavingChild;
    }

    /**
     * Age min to have child
     */
    public int getAgeMinHavingChild() {
        return ageMinHavingChild;
    }

    /**
     * Average difference of age in a couple
     */
    public double getAvDifAgeCouple() {
        return avDifAgeCouple;
    }

    /**
     * probability for a single to make a new a couple
     */
    public double getProbabilityToMakeCouple() {
        return probabilityToMakeCouple;
    }

    public double getProbaBirth() {
        return probaBirth;
    }

    public Value<Integer, Individual> getAgeEnterLabourMarket() {
        return ageEnterLabourMarket;
    }

    public int getAgeEnterLabourMarket(Individual ind) {
        return ageEnterLabourMarket.getValue(ind);
    }

    public Value<Integer, Individual> getAgeToRetire() {
        return ageToRetire;
    }

    public int getAgeToRetire(Individual ind) {
        return ageToRetire.getValue(ind);
    }

    public boolean isInactiveAfterEmployment(Individual ind) {
        return isInactiveAfterEmployment.getValue(ind);
    }

    public boolean isInactiveAfterUnemployment(Individual ind) {
        return isInactiveAfterUnemployment.getValue(ind);
    }

    public boolean remainInactive(Individual ind) {
        return remainInactive.getValue(ind);
    }

    public int getProfIfUnactive(Individual ind) {
        return profIfUnactive.getValue(ind);
    }

    public int getProfIfWorking(Individual ind) {
        return profIfWorking.getValue(ind);
    }

    public boolean getFilterFireOnActivity(int activity) {
        if (filterFireOnActivity == null) {
            return false;
        }
        return filterFireOnActivity.getValue(activity);
    }

    public String getMunicipalitiesDistances() {
        return municipalitiesDistances;
    }

    public String getJobResearchMunNetworkClasses() {
        return jobResearchMunNetworkClasses;
    }

    public String getJobResearchMunNetworkProbas() {
        return jobResearchMunNetworkProbas;
    }

    public String getCommutOutsideProbas() {
        return commutOutsideProbas;
    }

    public String getDistToBorder() {
        return distToBorder;
    }

    public List<MunicipalityParameters> getMunicipalitiesParameters() {
        return villagesParameters;
    }

    public List<OutsideParameters> getOutsideParameters() {
        return outsideParameters;
    }

    public int getAgeToDie(Individual ind) {
        return ageToDie.getValue(ind);
    }

    public Value<Integer, Individual> getAgeToDie() {
        return ageToDie;
    }

    /**
     * Average number of children by individuals
     */
    public double getNbChild(Individual ind) {
        return nbChild.getValue(ind);
    }

    public Integer getFirstProfession(Individual ind) {
        return firstProfession.getValue(ind);
    }

    /**
     * Give the number of line in the matrix of transition for the labour
     * situation (probaActivityPattern) corresponding to an unactive status
     */
    public int getNbOfUnactiveSituations() {
        return nbOfUnactiveSituations;
    }

    /**
     * Give the number of line in the matrix of transition for the labour
     * situation (probaActivityPattern) corresponding to an unemployed status
     */
    public int getNbOfUnemployedSituations() {
        return nbOfUnemployedSituations;
    }

    public int getNbPat() {
        return nbPat;
    }

    public int getNbSectorsActiv() {
        return nbSectorsActiv;
    }

    /**
     * Number of sizes considered for residences. The cardinal which corresponds
     * to nbSizesResidence is a "nbSizesResidence" and more size
     */
    public int getNbSizeRes() {
        return nbSizeRes;
    }

    /**
     * Step of the simulation start
     *
     * @return
     */
    public int getStartStep() {
        return startStep;
    }

    public void setStartStep(int startStep) {
        this.startStep = startStep;
    }

    /**
     * Nb of iteration to do
     */
    public int getNbStep() {
        return nbStep;
    }

    public int getNbTypesBySectorActiv() {
        return nbTypesBySectorActiv;
    }

    public int getNbActivities() {
        return nbActivities;
    }

    public Migration getMigration() {
        return migration;
    }

    /**
     * Probability to divorce, to quit each other inside a couple
     */
    public double getSplittingProba() {
        return splittingProba;
    }

    public double getStDifAgeCouple() {
        return stDifAgeCouple;
    }

    /**
     * Nb of years in one iteration, particularly used to increase age of people
     */
    public int getStep() {
        return step;
    }

    /**
     * Number of times by iteration an individual tries to find an individual
     * respecting the conditions to become its partner (join method)
     */
    public int getNbJoinTrials() {
        return (int) Math.round(nbJoinTrials);
    }

    public double getFarCloseThreshold() {
        return farCloseThreshold;
    }

    public double getDispoThreshold() {
        return dispoThreshold;
    }

    public double getStudentCityThreshold() {
        return studentCityThreshold;
    }

    public double getProbToAcceptNewResidence() {
        return probToAcceptNewResidence;
    }

    public double getResSatisfacMargin() {
        return resSatisfactMargin;
    }

    public List<Scenario> getScenarios() {
        return scenarios;
    }

    /**
     * File containing the scenarios
     */
    public String getScenarioFile() {
        return scenarioFile;
    }

    public DynamicServiceEmployment getDynamicServiceEmployment() {
        return dynamicServiceEmployment;
    }
}
