/*
 * Copyright (C) 2013 Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.Serialization;
import java.io.File;
import java.util.Arrays;
import java.util.List;

/**
 * Load the villages (and outsides) data from a compressed binary archive. This
 * binary file can be produced with the class MunicipalitiesParametersConverter.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class BinaryMunicipalitiesParametersLoader implements MunicipalitiesParametersLoader, ConfiguredByFiles {

    private transient List<MunicipalityParameters> villagesParameters;
    private transient List<OutsideParameters> outsideParameters;
    private String municipalitiesFilename;

    @Override
    public List<MunicipalityParameters> getMunicipalitiesParameters() {
        return villagesParameters;
    }

    @Override
    public List<OutsideParameters> getOutsidesParameters() {
        return outsideParameters;
    }

    @Override
    public List<String> getFilenames() {
        return Arrays.asList(municipalitiesFilename);
    }

    @Override
    public void loadData(Application application) throws ProcessingException {
        List<List> data = (List<List>) Serialization.deserializeObject(application.getDataDirectory() + File.separator + municipalitiesFilename);
        villagesParameters = data.get(0);
        outsideParameters = data.get(1);
    }
}
