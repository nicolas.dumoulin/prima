/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import org.openide.util.lookup.ServiceProvider;

/**
 * <p>Defines a parameter value by a normal law.</p>
 * 
 * <p>Example of XML configuration
 * <pre>
 * &lt;yourParameter class="IntValueFromNormalLaw">
 *   &lt;mean>6.0&lt;/mean>
 *   &lt;stdDev>4.0&lt;/stdDev>
 * &lt;/yourParameter>
 * </pre>
 * </p>
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class IntValueFromNormalLaw<O extends Object> implements Value<Integer, O> {

    private transient Application application;
    private float mean;
    private float stdDev;

    @Override
    public Integer getValue(Object O) {
        return (int) (float) application.getRandom().pickOutNormalLaw(mean, stdDev);
    }

    public float getMean() {
        return mean;
    }

    public float getStdDev() {
        return stdDev;
    }

    @Override
    public void init(Application application) throws BadDataException {
        this.application = application;
    }
}
