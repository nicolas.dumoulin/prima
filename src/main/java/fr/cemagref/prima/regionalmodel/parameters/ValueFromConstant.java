/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import org.openide.util.lookup.ServiceProvider;

/**
 * Example of XML configuration
 * <pre>
 * &lt;yourParameter class="ValueFromConstant">
 *   &lt;constant class="int">3&lt;/constant>
 * &lt;/yourParameter>
 * </pre>
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = Value.class)
public class ValueFromConstant<N extends Number, O extends Object> implements Value<N, O> {

    private N constant;

    public ValueFromConstant() {
    }

    public ValueFromConstant(N constant) {
        this.constant = constant;
    }

    @Override
    public N getValue(O object) {
        return constant;
    }

    @Override
    public void init(Application application) throws BadDataException {
    }
}
