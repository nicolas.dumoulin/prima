/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Parameters for one village
 *
 * TODO load with xstream and check validity of arrays size
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class MunicipalityParameters implements Serializable {

    private static final long serialVersionUID = 1L;
    private transient Logger logger;
    private int popsize;
    private int countIndiv;
    private String inputFileName;
    private String householdsFilename;
    private String residentsActivityFilename;
    private int totalSurface;
    private int[] offeredActivities;
    private float[] revenusActiv;
    private int[] residenceOffer;
    private String municipalityName;
    private String proxJobFile;
    private double lon, lat;
    private double intercept, slope; // parameters of the function to compute the number of employments in the service sector
    private Integer[][] households;
    private String[][] individualsActivities;
    private String[] proxJob;

    public MunicipalityParameters(Parameters parentParameters, File municipalityDir, String villageFilename, String populationFilename, String proxJobFilename, String activityFilename) throws IOException {
        String municipalityDirName = municipalityDir.getAbsolutePath() + File.separator;
        municipalityName = municipalityDir.getName();
        readResolve();
        // reading main input file found in municipality dir
        inputFileName = municipalityDirName + villageFilename;
        Scanner scanner = new Scanner(new File(inputFileName));
        scanner.useDelimiter("\\s+");
        scanner.nextLine();
        popsize = scanner.nextInt();
        householdsFilename = municipalityDirName + populationFilename;
        proxJobFile = municipalityDirName + proxJobFilename;
        residentsActivityFilename = municipalityDirName + activityFilename;
        totalSurface = scanner.nextInt();
        offeredActivities = new int[parentParameters.getNbActivities()];
        for (int i = 0; i < parentParameters.getNbActivities(); i++) {
            offeredActivities[i] = scanner.nextInt();
        }
        residenceOffer = new int[parentParameters.getNbSizeRes()];
        for (int i = 0; i < parentParameters.getNbSizeRes(); i++) {
            residenceOffer[i] = scanner.nextInt();
        }
        scanner.useLocale(Locale.US);
        lon = scanner.nextDouble();
        lat = scanner.nextDouble();
        if (parentParameters.getDynamicServiceEmployment() != null) {
            intercept = scanner.nextDouble();
            slope = scanner.nextDouble();
        }
        scanner.close();
        // Initialize household
        households = readPopFile(householdsFilename);
        // Initialize individual patterns
        individualsActivities = readActivityFile(countIndiv, households, residentsActivityFilename);
        // Initialization of the accessible villages from the village villageNb
        CSVReader reader = CSV.getReader(proxJobFile, null);
        proxJob = reader.readNext();
        reader.close();
    }

    private Object readResolve() {
        this.logger = Logger.getLogger(Municipality.class.getName() + "." + municipalityName);
        return this;
    }

    public double getIntercept() {
        return intercept;
    }

    public double getSlope() {
        return slope;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public int getPopsize() {
        return popsize;
    }

    public String getHouseholdsFilename() {
        return householdsFilename;
    }

    public String getInputFileName() {
        return inputFileName;
    }

    public int[] getOfferedActivities() {
        return offeredActivities;
    }

    public String getResidentsActivityFilename() {
        return residentsActivityFilename;
    }

    public float[] getRevenusActiv() {
        return revenusActiv;
    }

    public int[] getResidenceOffer() {
        return residenceOffer;
    }

    public int getTotalSurface() {
        return totalSurface;
    }

    public String getMunicipalityName() {
        return municipalityName;
    }

    public String getProxJobFile() {
        return proxJobFile;
    }

    public Integer[][] getHouseholds() {
        return households;
    }

    public String[][] getIndividualsActivities() {
        return individualsActivities;
    }

    public String[] getProxJob() {
        return proxJob;
    }

    public int getCountIndiv() {
        return countIndiv;
    }

    /**
     * Read the pop file generated by the class StartPopulation Create a table
     * of household for which each line contains the type of household in
     * position 0 and the other positions contains the age of the individuals of
     * the household
     */
    private Integer[][] readPopFile(String nameOfFile) throws IOException {
        List<Integer[]> data = new ArrayList<Integer[]>();
        this.countIndiv = 0;
        int size;
        CSVReader reader = CSV.getReader(nameOfFile, ' ');
        Integer[] line;
        try {
            while ((line = CSV.nextIntegersLine(reader)) != null) {
                size = line[1];
                this.countIndiv += size;
                Integer[] ldata = new Integer[size + 1];
                ldata[0] = line[0];
                for (int i = 1; i <= size; i++) {
                    ldata[i] = line[i + 1];
                }
                data.add(ldata);
            }
        } catch (BadDataException ex) {
            throw new IOException(ex);
        }
        reader.close();
        return data.toArray(new Integer[][]{});
    }

    /**
     * Read the file activity of individuals The format of the file is described
     * in a doc file (instructions for the activity file.doc)
     */
    private String[][] readActivityFile(int popSize, Integer[][] hsh, String nameOfFile) {
        int i = 0, j = 0, size = 0, ind = 0, pos = 0, nbOccupiedActiv = 1;
        String[][] myActives = new String[popSize][7];
        int truc = 0;
        String temp = "0";
        try {
            int status = 0;
            FileReader myFile = new FileReader(nameOfFile);
            StreamTokenizer mySt = new StreamTokenizer(myFile);
            mySt.resetSyntax();    // Configure the tokenizer
            mySt.wordChars(33, 255);             // accept all characters
            mySt.whitespaceChars(0, ' '); // set white space as separator
            mySt.eolIsSignificant(false);  // ignore end of line character
            for (i = 0; i < myActives.length; i++) {
                Arrays.fill(myActives[i], "-1000");
            }
            mySt.nextToken();
            while (mySt.ttype != StreamTokenizer.TT_EOF) {
                for (i = 0; i < hsh.length; i++) {
                    size = hsh[i].length - 1;
                    pos = 0;
                    for (j = 0; j < size; j++) {
                        // Corresponds to one line of the activity file
                        truc = Integer.parseInt((mySt.sval));
                        myActives[ind][pos] = (new Integer(truc)).toString(); // status
                        status = truc;
                        mySt.nextToken();
                        pos++;
                        switch (status) { // the following depends on the status
                            // of the individual (stocked in myActives[ind][pos])
                            case 0: // student
                                pos = 0; // nothing at this time
                                break;
                            case 1: // active
                                if (mySt.ttype == StreamTokenizer.TT_NUMBER) {
                                    truc = (int) mySt.nval;
                                    myActives[ind][pos] = Integer.toString(truc); // municipality where it works
                                } else {
                                    myActives[ind][pos] = mySt.sval; // municipality where it works
                                }
                                if (myActives[ind][pos].length() == 4) {
                                    myActives[ind][pos] = temp.concat(myActives[ind][pos]);
                                }
                                mySt.nextToken();
                                pos++;
                                myActives[ind][pos] = (new Integer(nbOccupiedActiv)).toString();
                                pos++;
                                for (int o = 0; o < nbOccupiedActiv; o++) { // read the activity pattern
                                    myActives[ind][pos] = mySt.sval; // activity sector
                                    mySt.nextToken();
                                    pos++;
                                    myActives[ind][pos] = mySt.sval;// socioprofessionnal category
                                    mySt.nextToken();
                                    pos++;
                                }
                                pos = 0;
                                break;
                            case 2: // unemployed
                                if (mySt.ttype == StreamTokenizer.TT_NUMBER) {
                                    truc = (int) mySt.nval;
                                    myActives[ind][pos] = Integer.toString(truc); // municipality where it works
                                } else {
                                    myActives[ind][pos] = mySt.sval; // municipality where it works
                                }
                                // Socio professional category (for France)
                                mySt.nextToken();
                                pos = 0;
                                break;
                            case 3: // retired
                                pos = 0; // nothing at this time
                                break;
                            case 4: // others
                                pos = 0; // nothing at this time
                                break;
                        }
                        ind++;
                    }
                }
                logger.log(Level.FINER, "readActivityFile: ind {0} pos {1}popSize {2}", new Object[]{ind, pos, popSize});
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Problem to read the activity file", e);
        }
        return myActives;
    }
}
