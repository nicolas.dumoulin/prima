/*
 * Copyright (C) 2013 Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class DefaultMunicipalitiesParametersLoader implements MunicipalitiesParametersLoader {

    private transient List<MunicipalityParameters> villagesParameters;
    private transient List<OutsideParameters> outsideParameters;
    
    private String municipalityFilename;
    /**
     * File containing the list of municipality considered as close from finding
     * a job point of view
     */
    private String proxJobFilename;
    /**
     * File containing the households of a municipality
     */
    private String populationFilename;
    /**
     * File containing the list of individuals with their activities in a
     * municipality
     */
    private String activityFilename;
    
    private final static transient Logger LOGGER = Logger.getLogger(DefaultMunicipalitiesParametersLoader.class.getName());

    @Override
    public List<MunicipalityParameters> getMunicipalitiesParameters() {
        return villagesParameters;
    }

    @Override
    public List<OutsideParameters> getOutsidesParameters() {
        return outsideParameters;
    }

    @Override
    public void loadData(Application application) {
        File[] dirs = application.getDataDirectory().listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory() && !".svn".equals(pathname.getName());
            }
        });
        // sort dirs to get the same order at each run
        Arrays.sort(dirs, new Comparator<File>() {
            @Override
            public int compare(File filea, File fileb) {
                return filea.getName().compareToIgnoreCase(fileb.getName());
            }
        });
        // counting the "outside"
        int outNb = 0;
        for (File municipalityDir : dirs) {
            if (municipalityDir.getName().toLowerCase().startsWith("outside")) {
                outNb++;
            }
        }

        villagesParameters = new ArrayList<MunicipalityParameters>(dirs.length - outNb);
        outsideParameters = new ArrayList<OutsideParameters>(outNb);
        //LOGGER.log(Level.INFO, "{0} municipalities will be loaded", dirs.length - outNb);
        for (File municipalityDir : dirs) {
            LOGGER.log(Level.FINER,
                    "Processing directory {0}", municipalityDir);
            try {
                if (municipalityDir.getName()
                        .toLowerCase().startsWith("outside")) {
                    outsideParameters.add(new OutsideParameters(application.getParameters(), municipalityDir));
                } else {
                    villagesParameters.add(
                            new MunicipalityParameters(application.getParameters(), municipalityDir, municipalityFilename, populationFilename, proxJobFilename, activityFilename));
                }
            } catch (IOException ex) {
                LOGGER.log(Level.WARNING,
                        "This directory doesn't seem to contain municipalities file and has been skipped :" + municipalityDir + " (" + municipalityDir.getName() + ")",
                        ex);
            } catch (RuntimeException ex) {
                LOGGER.log(Level.SEVERE, "Error while trying to load municipality data in {0}", municipalityDir);
                throw ex;
            }
        }
    }
    
}
