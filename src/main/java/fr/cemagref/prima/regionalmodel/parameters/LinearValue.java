/*
 * Copyright (C) 2012 Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.parameters;

import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;

/**
 * <p>Defines a parameter value by a linear function <tt>y=a*x+b</tt>, where:
 * <ul><li><tt>a</tt> is the slope of the line</li>
 * <li><tt>x</tt> is the current year translated so that the first step is 0</li>
 * <li><tt>b</tt> is the y-intercept of the line</li></ul></p>
 * 
 * <p>You can use the extra-parameter <tt>constantDate</tt> for
 * stopping the linear function, so that the parameter value won't be computed
 * again and will remains the same from this date to the end of the simulation.
 * In fact, the value will be constant even from the year preceding
 * <tt>constantDate</tt>. In other words, at <tt>constantDate</tt> the value will
 * remain the precedent one.</p>
 * 
 * <p>Examples of XML
 * configuration:
 * <pre>
 * &lt;yourParameter class="LinearValue">
 *   &lt;slope>1.2&lt;/slope>
 *   &lt;intercept>0.1&lt;/intercept>
 * &lt;/yourParameter>
 * </pre>
 * <pre>
 * &lt;yourParameter class="LinearValue">
 *   &lt;slope>1.2&lt;/slope>
 *   &lt;intercept>0.1&lt;/intercept>
 *   &lt;constantDate>2001&lt;/constantDate>
 * &lt;/yourParameter>
 * </pre></p>
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class LinearValue<T extends Object> implements Value<Double, T>, Updatable {

    private double slope, intercept;
    private Integer constantDate;
    private transient int startStep;
    private transient double currentValue;

    public LinearValue(double slope, double intercept) {
        this.slope = slope;
        this.intercept = intercept;
        this.startStep = -1;
    }

    public Object readResolve() {
        startStep = -1;
        return this;
    }

    @Override
    public void init(Application application) throws BadDataException {
        startStep = application.getParameters().getStartStep();
    }

    @Override
    public Double getValue(T object) {
        return currentValue;
    }

    @Override
    public void update(int time) {
        if (startStep == -1) {
            // special case for updating the value as the model isn't completly initialized
            currentValue = intercept;
        } else {
            if ((constantDate == null) || (time < constantDate)) {
                currentValue = slope * (time - startStep) + intercept;
            }
        }
    }
}
