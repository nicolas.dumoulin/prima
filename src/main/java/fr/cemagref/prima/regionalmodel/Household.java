/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.prima.regionalmodel.Individual.Status;
import fr.cemagref.prima.regionalmodel.tools.Random;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Sylvie Huet <sylvie.huet@cemagref.fr>; Nicolas Dumoulin
 * <nicolas.dumoulin@cemagref.fr>
 */
public class Household implements Iterable<Individual> {

    private static final Logger LOGGER = Logger.getLogger(Household.class.getName());
    private static int cpt = 0;
    private int id;
    /**
     * Municipality of the next residence of the household (when it is going to
     * probabilisticMove)
     */
    private transient Municipality nextResMunicipality;
    /**
     * NextSizeOfResidence (-1 means no residence)
     */
    private transient int nextSizeOfResidence;
    private Residence residence;
    /**
     * Household types: correspond to the code used by StartPopulation when it
     * generates a population <ul><li>0: single <li>1: monoparenthal family
     * <li>2: couple without children <li>3: couple with children <li>4: complex
     * household (other households)
     */
    public Type hshType;

    public static enum Type {

        SINGLE, MONOPARENTAL, COUPLE_WITHOUT_CHILDREN, COUPLE_WITH_CHILDREN, COMPLEX;
    }
    /**
     * Total list of members of the household
     */
    protected List<Individual> listOfMembers;
    /**
     * When the individuals of a couple quits each other, they remind about
     * their old partner the time necessary to physically quit each other (i.e.
     * living in a different residence) That is to avoid they take a new partner
     * and live in the residence where the old partner is still living
     */
    private Household oldPartner = null;
    private boolean coupleAtParents = false;
    private Individual leader = null;

    public Individual getLeader() {
        return leader;
    }

    public void setLeader(Individual leader) {
        this.leader = leader;
    }

    public Household getOldPartner() {
        return oldPartner;
    }
    /**
     * Year of meeting: iteration number during which the individual of the
     * couple of the household has met each other
     */
    int meetingYear;
    /**
     * Say if the household wants to change its residence due to a professional
     * migration or household splitting. Warning: if it is false, it doesn't
     * mean that the household doesn't want at all to change its residence.
     */
    private transient boolean needOfResidence;
    /**
     * Muncipality where the household lives
     */
    private transient Municipality myVillage;
    /**
     * @param justSuppressed indicates that the household has to be suppressed
     * at the end of the current iteration
     */
    private transient boolean justSuppressed;

    public Random getRandom() {
        return myVillage.getMyRegion().getMyApplication().getRandom();
    }

    public long getId() {
        return id;
    }

    public Municipality getNextResMunicipality() {
        return nextResMunicipality;
    }

    public void setNextResMunicipality(Municipality nextResMunicipality) {
        this.nextResMunicipality = nextResMunicipality;
    }

    public int getNextSizeOfResidence() {
        return nextSizeOfResidence;
    }

    public void setNextSizeOfResidence(int nextSizeOfResidence) {
        this.nextSizeOfResidence = nextSizeOfResidence;
    }

    public Residence getResidence() {
        return residence;
    }

    public void setTransitResidence() {
        this.residence = Residence.createTransitResidence(this);
    }

    /**
     * Add the household to a residence and notify the old partner if any (now,
     * they can have a new partner)
     *
     * @param residence
     */
    public void setResidence(Residence residence) {
        if (this.residence != null && this.residence.isTransit()) {
            for (Individual ind : listOfMembers) {
                //myVillage.getCounters().incNbHouseholdMoveIn(1);
                myVillage.getCounters().incNbIndividualMoveIn(1);
                ind.setMigrationYear((short) myVillage.getMyRegion().getCurrentYear());
                // compute again the age to die if it has already passed
                if (ind.getAgeToDie() <= ind.getAge()) {
                    ind.initAgeToDie();
                }
            }
            myVillage.getCounters().incNbHouseholdMoveIn(1);
        }
        this.residence = residence;
        residence.add(this);
        if ((oldPartner != null) && (residence != oldPartner.getResidence())) {
            oldPartner.oldPartner = null;
            oldPartner = null;
        }
    }

    /**
     * Household types: <ul><li>0: single <li>1: monoparenthal family <li>2:
     * couple without children <li>3: couple with children <li>4: complex
     * household (other households)
     */
    public int getHshTypeCode() {
        return hshType == null ? 0 : hshType.ordinal();
    }

    public Type getHshType() {
        return hshType;
    }

    public int getMeetingYear() {
        return meetingYear;
    }

    public boolean needOfResidence() {
        return needOfResidence;
    }

    public void setNeedOfResidence(boolean needOfResidence) {
        this.needOfResidence = needOfResidence;
    }

    /**
     *
     * @return an iterator on members of the Household
     */
    @Override
    public Iterator<Individual> iterator() {
        return listOfMembers.iterator();
    }

    public int indexOf(Individual o) {
        return listOfMembers.indexOf(o);
    }

    /**
     * Get a working copy of the list of members. Usefull for iterating over
     * members while we can remove a member, so that your are sure to get the
     * correct members.
     *
     * @return
     */
    public List<Individual> getCopyOfMembers() {
        List<Individual> members = new ArrayList<Individual>(listOfMembers.size());
        members.addAll(listOfMembers);
        return members;
    }

    public Individual getMember(int index) {
        return listOfMembers.get(index);
    }

    /**
     *
     * @return the number of individuals in this household
     */
    public int size() {
        return listOfMembers.size();
    }

    public final List<Individual> getAdults() {
        List<Individual> adults = new ArrayList<Individual>();
        for (Individual member : listOfMembers) {
            if (member.isAdult()) {
                adults.add(member);
            }
        }
        return adults;
    }

    public final List<Individual> getChildren() {
        List<Individual> children = new ArrayList<Individual>();
        for (Individual member : listOfMembers) {
            if (!member.isAdult()) {
                children.add(member);
            }
        }
        return children;
    }

    public boolean isCoupleAtParents() {
        return coupleAtParents;
    }

    public void setCoupleAtParents(boolean coupleAtParents) {
        this.coupleAtParents = coupleAtParents;
    }

    /**
     * Give the size of the household
     */
    public int getSize() {
        return listOfMembers.size();
    }

    /**
     * Determine the leader of the household. If a leader has already be
     * determined in the current timestep, he is returned. If there is only one
     * adult, he is promoted as leader and returned. If there is two adults, we
     * pick on randomly.
     *
     * The leader is the one is used to determine where searching for a new
     * residence. This search considers a distance starting from the place of
     * work of the leader.
     *
     * @return the selected adult for leadership.
     */
    public Individual determineLeader() {
        if (leader == null) {
            List<Individual> adults = getAdults();
            if (adults.size() == 1) {
                leader = adults.get(0);
            } else {
                if (getAdults().size() == 0) {
                    System.err.println(this + " ind number of children " + getChildren().size() + " need for residence " + needOfResidence() + " js " + this.justSuppressed + " size of hsh " + size());
                }
                leader = adults.get(getRandom().nextInt(0, 1));
            }
        }
        return leader;
    }

    /**
     * Reset the leader of the household, so the method {@link determineLeader()}
     * will rebuild a new leader.
     */
    public void resetLeader() {
        leader = null;
    }

    public Municipality getMyVillage() {
        return myVillage;
    }

    public void setMyVillage(Municipality myVillage) {
        this.myVillage = myVillage;
    }

    public boolean isJustSuppressed() {
        return justSuppressed;
    }

    public void setJustSuppressed(boolean justSuppressed) {
        this.justSuppressed = justSuppressed;
    }

    public Object readResolve() {
        for (Individual ind : listOfMembers) {
            ind.setMyHousehold(this);
        }
        return this;
    }

    /**
     * Constructor for the household at the initialization time
     */
    public Household(Municipality myVill, Integer[] household) {
        this.id = ++cpt;
        this.myVillage = myVill;
        // The first number indicates the family types
        this.hshType = Type.values()[household[0]];
        this.meetingYear = -1;
        int hshSize = household.length - 1;
        listOfMembers = new ArrayList<Individual>(hshSize);
        for (int i = 1; i <= hshSize; i++) {
            boolean child = ((hshType == Type.COUPLE_WITH_CHILDREN) && (i > 2)) || ((hshType == Type.MONOPARENTAL) && (i > 1));
            listOfMembers.add(new Individual(this, !child, household[i]));
        }
        /*
         * // The computation of certain age required that the household is
         * already constructed and then the age are initialised (example
         * Auvergne for age to enter on the labour market) for (Individual ind:
         * listOfMembers) { ind.initIndividual(); }
         *
         */
        this.residence = null;
        this.justSuppressed = false;
    }

    /**
     * Constructor for the households of the Outside rural region village
     * containing only household of single living there and working in the other
     * regional village
     */
    public Household(Municipality myVill, int age) {
        this(myVill, new Integer[]{age});
    }

    /**
     * Constructor for the updating and moving households of the Outside rural
     * region village containing only household of single living there and
     * working in the other regional village - if it is a migrant household, the
     * new household is a duplication with a new household number
     */
    public Household(Household household, boolean migrant) {
        listOfMembers = new ArrayList<Individual>();
        if (!migrant) {
            this.id = household.id;
            this.residence = household.residence;
            listOfMembers.addAll(household.listOfMembers);
            this.nextSizeOfResidence = household.getNextSizeOfResidence();
            this.nextResMunicipality = household.getNextResMunicipality();
            this.leader = household.leader;
            for (Individual member : listOfMembers) {
                member.setMyHousehold(this);
            }
        } else {
            this.id = ++cpt;
            this.residence = Residence.createTransitResidence(this);
        }
        myVillage = household.myVillage;
        hshType = household.hshType;
        meetingYear = household.meetingYear;
    }

    public Household(Municipality myVill, List<Individual> myInd, Residence residence) {
        this(myVill, myInd);
        this.residence = residence;
        if (residence != null) {
            residence.add(this);
        }
        if (myInd.isEmpty()) {
            hshType = null;
        } else if (myInd.size() == 1) {
            hshType = Type.SINGLE;
        } else {
            if (getAdults().size() == 1) {
                hshType = Type.MONOPARENTAL;
            } else if (getChildren().size() > 0) {
                hshType = Type.COUPLE_WITH_CHILDREN;
            } else {
                hshType = Type.COUPLE_WITHOUT_CHILDREN;
            }
        }
    }

    private Household(Municipality myVill, List<Individual> myInd) {
        this.id = ++cpt;
        this.myVillage = myVill;
        listOfMembers = myInd;
        for (Individual individual : myInd) {
            individual.setMyHousehold(this);
        }
    }

    /**
     * Method searching a job if decided previously. If a job is found, changes
     * are applied.
     */
    public void searchJobs() {
        for (Individual member : getCopyOfMembers()) {
            if (member.isLookForJob()) {
                member.seekJob();
            }
        }
    }

    /**
     * Build the intersection between the neighbourhood of the job of the leader
     * and the neighbourhood of the residence of the household.
     *
     * @return the list of municipalities in the intersection
     */
    public List<Municipality> getJobAndResidenceProximityIntersection() {
        if ((determineLeader().getJobLocation() == null) || (determineLeader().getJobLocation() == myVillage)) {
            return Arrays.asList(myVillage.getProximityJob());
        } else {
            List intersect = new ArrayList<Municipality>(Arrays.asList(determineLeader().getJobLocation().getProximityJob()));
            intersect.retainAll(Arrays.asList(myVillage.getProximityJob()));
            return intersect;
        }
    }

    /**
     * Attach to an existing residence. Method used at initialization when the
     * household has not been able to find a residence. The method gets an
     * existing residence at random and attaches the household in this residence
     */
    public void attachToExistingResidence() {
        int resType = size() - 1;
        if (size() >= myVillage.getResidencesTypeCount()) {
            resType = myVillage.getResidencesTypeCount() - 1;
        } else {
            do {
                resType = getRandom().nextInt(size(), myVillage.getResidencesTypeCount()) - 1;
            } while (myVillage.getOccupiedResidencesCount(resType) < 1);
        }
        residence = myVillage.pickAnOccupiedResidence(resType);
        residence.add(this);
    }

    /**
     * Method to init the residence of the initial population S. Huet,
     * 29.07.2009 - taille initialisée aléatoirement
     */
    public void initResidenceOld() {
        ///* random choice
        int[] size = {0, 1, 2, 3};
        List posSize = new ArrayList();
        for (int i = 0; i < size.length; i++) {
            posSize.add(new Integer(size[i]));
        }
        int taille = 0;
        //System.err.print("poss "+posSize) ;
        for (int i = 0; i < posSize.size(); i++) {
            taille = ((Integer) posSize.get(i)).intValue();
            if (getMyVillage().hasFreeResidences(taille)) {
                if (getResidence() != null && !isJustSuppressed()) {
                    getMyVillage().releaseResidence(this);
                }
                setNextSizeOfResidence(taille);
                getMyVillage().takeResidence(this);
                i = posSize.size();
            }
        }
        if (getResidence() == null) {
            getMyVillage().getCounters().incNbOfDwellingLacking(1);
        }
    }

    /**
     * Method to init the residence of the initial population S. Huet,
     * 29.07.2009 - taille attribuée en fonction de l'age moyen des adultes du
     * ménage plus il est élevé, plus la taille est grande
     */
    public void initResidence() {
        ///* random choice
        int[] size = {0, 1, 2, 3};
        List posSize = new ArrayList();
        for (int i = 0; i < size.length; i++) {
            posSize.add(new Integer(size[i]));
        }
        int taille = 0;
        float avAge = averageAge();
        if (avAge < 26) {
            taille = 0;
        } else {
            if (avAge < 31) {
                taille = 1;
            } else {
                if (avAge < 36) {
                    taille = 2;
                } else { // higher than 26
                    taille = 3;
                }
            }
        }
        if (getMyVillage().hasFreeResidences(taille)) {
            if (getResidence() != null && !isJustSuppressed()) {
                getMyVillage().releaseResidence(this);
            }
            setNextSizeOfResidence(taille);
            getMyVillage().takeResidence(this);
        }
        // if not available, try in larger size
        if (getResidence() == null) { // try larger size
            for (int i = taille; i < posSize.size(); i++) {
                if (getMyVillage().hasFreeResidences(i)) {
                    if (getResidence() != null && !isJustSuppressed()) {
                        getMyVillage().releaseResidence(this);
                    }
                    setNextSizeOfResidence(i);
                    getMyVillage().takeResidence(this);
                    i = posSize.size();
                }
            }
        }
        // if not available, try in smaller size
        if (getResidence() == null) { // try larger size
            for (int i = taille-1; i > -1 ; i--) {
                if (getMyVillage().hasFreeResidences(i)) {
                    if (getResidence() != null && !isJustSuppressed()) {
                        getMyVillage().releaseResidence(this);
                    }
                    setNextSizeOfResidence(i);
                    getMyVillage().takeResidence(this);
                    i = -1;
                }
            }
        }
        if (getResidence() == null) {
            getMyVillage().getCounters().incNbOfDwellingLacking(1);
        }
    }
    /**
     * Codes used to init status when reading input files.
     */
    private static final Status[] STATUS_CODES = new Status[]{Status.STUDENT, Status.WORKER, Status.UNEMPLOYED, Status.RETIRED, Status.INACTIVE};

    /**
     * Method to init the status, the place of work and the pattern of
     * individuals of the household The order of the pat table is the same than
     * the one the individuals have been stocked in the household list of
     * members The format of data is described in a doc file "Instructions for
     * the activity file.doc" S. Huet 08.10.2009
     */
    public void initPatternsOfMyInd(String[][] pat) {
        int i = 0, j = 0, k = 0, m = 0, a = 0, start = 0;
        boolean[] goodPat = new boolean[getMyVillage().getMyRegion().getAllPatterns().length];
        int[] actPosit;
        int nbAct = 0, actPas = 0;
        // At the beginning, we consider that all activities are practised in the same municipality
        List<Individual> temp = getCopyOfMembers();
        for (i = 0; i < temp.size(); i++) {
            Arrays.fill(goodPat, true);
            Individual member = temp.get(i);
            member.setStatus(STATUS_CODES[Integer.parseInt(pat[i][0])]);
            if (member.getStatus() == Status.RETIRED && !member.isAdult()) {
                member.becomeAdult();
            }
            if (member.getStatus() != Status.STUDENT) {
                member.setAgeToEnterOnTheLabourMarket(member.getAge() - 1);
                // this value is chosen because it will have no impact during the simulation time (the individual has already an activity)
            }
            switch (member.getStatus()) {
                case WORKER: // active
                    nbAct = (new Integer(pat[i][2])).intValue();
                    // real size depends on the activity pattern
                    // by default it lives and works in the same municipality for all its activities
                    Municipality mun = getMyVillage();
                    if (!pat[i][1].equals(getMyVillage().getName())) {
                        // it works outside the village for all its activities
                        mun = getMyVillage().getMyRegion().getMunicipality(pat[i][1]);
                        if (mun == null) {
                            mun = getMyVillage().getMyRegion().getOutsides().get(0);
                            // FIXME non found municipalities are attributed to the first outside
                            LOGGER.warning("Municipality " + pat[i][1] + " has not been found. First outside region has been attributed as job location.");
                        }
                    }
                    member.setJobLocation(mun);
                    member.setPreviousJobLocation(mun);
                    mun.addWorker(member);
                    // The CSP (socioprofessional category) are at the even position beginning at the position 4
                    k = 0;
                    for (j = 4; j < pat[i].length; j++) {
                        if ((new Integer(pat[i][j])).intValue() != -1000) {
                            if (j % 2 == 0) {
                                assert k == 0 : "multi-pattern unsupported";
                                member.setProfession((new Integer(pat[i][j])).intValue());
                                k++;
                            }
                        }
                    }
                    // initialisation du pattern calculation of the position of the occupied activity in the pattern
                    actPosit = new int[nbAct];
                    int p = 0;
                    for (k = 3; k < pat[i].length; k++) {
                        if ((new Integer(pat[i][k])).intValue() != -1000) {
                            if (k % 2 != 0) {
                                actPosit[p] = (((new Integer(pat[i][k])).intValue() * getMyVillage().getMyRegion().getNbProfessions())
                                        + ((new Integer(pat[i][k + 1])).intValue() + 1)) - 1;
                                p++;
                            }
                        }
                    }
                    // sort the position of the occupied activity
                    Arrays.sort(actPosit);
                    // Find the corresponding pattern
                    m = 0;
                    for (j = 0; j < getMyVillage().getMyRegion().getAllPatterns().length; j++) {
                        start = 0;
                        actPas = 0;
                        m = 0;
                        while (actPas < nbAct) {
                            for (k = start; k < actPosit[m]; k++) { // check the positions 0
                                if (getMyVillage().getMyRegion().getAllPatterns()[j].getPattern()[k]) {
                                    goodPat[j] = false;
                                    k = actPosit[m];
                                }
                            }
                            if (goodPat[j]) {
                                if (!getMyVillage().getMyRegion().getAllPatterns()[j].getPattern()[actPosit[m]]) {
                                    goodPat[j] = false;
                                }
                            }
                            actPas++;
                            if (actPas < nbAct) {
                                start = actPosit[m] + 1;
                                m++;
                            }
                        }
                        // Check the last false positions if they exist
                        if (goodPat[j]) {
                            for (k = (actPosit[m] + 1); k < getMyVillage().getMyRegion().getAllPatterns()[j].getPattern().length; k++) {
                                if (getMyVillage().getMyRegion().getAllPatterns()[j].getPattern()[k]) {
                                    goodPat[j] = false;
                                    k = getMyVillage().getMyRegion().getAllPatterns()[j].getPattern().length;
                                }
                            }
                        }
                    }
                    // affect the pattern to the individual and impact on the various counters
                    for (j = 0; j < getMyVillage().getMyRegion().getAllPatterns().length; j++) {
                        if (goodPat[j]) {
                            member.setMyActivityPattern(getMyVillage().getMyRegion().getAllPatterns()[j]);
                            // the number of occupied activities increase as we assign a pattern to an individual
                            if (member.getJobLocation() == getMyVillage()) {
                                getMyVillage().increaseOccupiedActivityRes(member.getMyActivityPattern().getJob());
                            } else {
                                member.getJobLocation().increaseOccupiedActivityExt(member.getMyActivityPattern().getJob());
                            }
                        }
                    }
                    //}
                    // ensure that this worker is an adult
                    if (!member.isAdult()) {
                        member.becomeAdult();
                    }
                    break;
                case UNEMPLOYED:
                    member.setProfession(new Integer(pat[i][1]));
                    // number 0 is the pattern of unemployed
                    member.setMyActivityPattern(getMyVillage().getMyRegion().getAllPatterns()[0]);
                    member.setJobLocation(getMyVillage());
                    //getMyVillage().setPatternOccupancy(0, getMyVillage().getPatternOccupancy()[0] + 1);
                    break;
                case INACTIVE:
                    /*
                     * if (member.getAge() > 59) {
                     * member.setStatus(Status.RETIRED); }
                     *
                     */
                    // Attribution of a random profession to work with at the beginning: done later in the last code version
                    // member.affectAFirstProfession();
                    break;
                default:
                    break;
            }
            //System.err.println("profession donnée à l'init "+member.getProfession()+" id hh "+getId()+" id indiv "+member.getId()+ "status "+member.getStatus()+" lieu de travail "+member.getJobLocation()+" lieu res "+member.getMyHousehold().getMyVillage().getName()) ;
        }
    }

    /**
     * Look for each unactive, worker or unemployed of the household if they are
     * going to change their labour situation (means change of status (unactive
     * to active or the contrary) or of activityPattern For each individual, the
     * current situation is identified and the link is done between this current
     * situation and the line of probability to use in the transitionMatrix in
     * order to send to the decideAboutLabourSituation() method the good
     * probabilities to decide PAY ATTENTION IN CASE OF PLURIACTIVITY: It has to
     * be notice that for the Inactive and Unemployed status, only the first
     * value of the attribute[] profession of the individual is considered to
     * decide about the current state in the transitionMatrix (if the past
     * professional detail is considered by this matrix). It means that, if
     * previously, the individual was pluriactive, it is only the profession
     * linked to the first activity encountered in the past ActivityPattern
     * which is considered to decide about the probability of transition to
     * another state.
     */
    public void willingJobs() {
        for (Individual member : getCopyOfMembers()) {
            member.pickTheNextJobSituationNew(getMyVillage());
        }
    }


    /*
     * *************************************************
     * Functions for changing household structure
     */
    /**
     * Method spliByDivorce for couple and couple with children The splitting
     * follows a constant probability
     */
    public boolean splitByDivorce(double probSplit) {
        boolean split = false;
        // Here we consider it is always the first adult (the first individual
        // of the household) who quits the other individual

        if (getRandom().nextDouble() <= probSplit) {
            split = true;

        }
        return split;

    }

    /*
     * Method death
     */
    public List<Individual> death() {
        List<Individual> suppressed = new ArrayList<Individual>();
        for (Individual ind : listOfMembers) {
            if (ind.getAge() >= ind.getAgeToDie()) {
                //System.err.println(ind.getAgeToDie()+"\t"+ind.getAge()) ;
                suppressed.add(ind);
            }
        }
        return suppressed;
    }

    /**
     * Method : for defining if a couple of parents (in case the household has a
     * couple of parents) have a baby at this time
     *
     * @author sylvie.huet 17.03.2010
     * @return a boolean indicating if the couple of parents have a baby
     */
    public boolean birth() {
        boolean newborn = false;
        try {
            if (hshType == Type.COUPLE_WITHOUT_CHILDREN || hshType == Type.COUPLE_WITH_CHILDREN) {
                Individual indiv = getAdults().get(getRandom().nextInt(0, 1));
                if (indiv.getAge() >= myVillage.getMyRegion().getAgeMinToChild()
                        && indiv.getAge() <= myVillage.getMyRegion().getAgeMaxToChild()) {
                    if (getRandom().nextDouble() <= myVillage.getMyRegion().getProbaBirth()) {
                        newborn = true;
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("exception " + e + "household " + this);
        }
        return newborn;
    }

    public void suppressMemberLeavingActivity(Individual member) {
        member.leaveMyActivPattern();
        suppressMember(member);
    }

    public void suppressMember(Individual member) {
        // Is the member to suppress is a child, in case, it has also to be suppressed of the list of child
        listOfMembers.remove(member);
        // TODO is it necessary?
        for (Individual ind : listOfMembers) {
            ind.setMyHousehold(this);
        }
        if (getAdults().isEmpty()) {
            setJustSuppressed(true);
            List<Individual> toBecomeAdult = new ArrayList<Individual>();
            for (Individual ind : listOfMembers) {
                // TODO add (or find) a parameter instead of "15"
                if (ind.getAge() > 15 && ind.getAge() < ind.getAgeToDie()) {
                    toBecomeAdult.add(ind);
                }
            }
            for (Individual ind : toBecomeAdult) {
                ind.becomeAdult();
            }
        }
    }

    /**
     * Function for adding a new individual in a household
     */
    public void addMember(Individual newMember) {
        newMember.setMyHousehold(this);
        listOfMembers.add(newMember);
    }

    /**
     * Says if an individual is looking for a partner.
     *
     * @param member
     * @return
     */
    public boolean isLookingForAPartner(Individual member) {
        if (member.isAdult()) {
            if (((hshType == Type.SINGLE) || (hshType == Type.MONOPARENTAL)) && (oldPartner == null)) {
                return true;
            }
        } else {
            if (member.getAge() > 15) {
                return true;
            }
        }
        return false;
    }

    /**
     * true if household is single and possible old partner left.
     *
     * @param member
     * @return
     */
    public boolean singleAdult() {
        if ((getAdults().size() == 1) && (oldPartner == null)) {
            return true;
        }
        return false;
    }

    /**
     * true if household is single and possible old partner left.
     *
     * @param member
     * @return
     */
    public boolean couple() {
        if (getHshType() == Type.COUPLE_WITHOUT_CHILDREN || getHshType() == Type.COUPLE_WITH_CHILDREN) {
            return true;
        }
        return false;
    }

    /**
     * Method returning a boolean saying if a household wants to change of
     * residence SH: 18.02.2010
     */
    public boolean needToChangeRes(int idealTypeOfRes) {
        boolean change = false;
        if (getResidence() == null) {
            change = true;
        } else {
            // Assume households is satisfied if the size of the flat is more or
            // less 1 room from the ideal size
            if (getResidence().getType() <= idealTypeOfRes + 1) {
                if (getResidence().getType() >= idealTypeOfRes - 1) {
                    change = false;
                } else {
                    change = true;
                }
            } else {
                change = true;
            }
        }
        return change;
    }

    /**
     * Update of the parental household type when a child become adult after his
     * first job
     */
    public void updateHshFeaturesWhenChildLeaving() {
        if (hshType != Type.COMPLEX) {
            int nbOtherChild = getChildren().size();
            if (nbOtherChild == 0) {
                if (hshType == Type.MONOPARENTAL) { // that is a single parent household and the last children leaves
                    hshType = Type.SINGLE;
                }
                if (hshType == Type.COUPLE_WITH_CHILDREN) { // that is a parent with children household and the last children leaves
                    hshType = Type.COUPLE_WITHOUT_CHILDREN;
                }
            }
        }
    }

    /**
     * Update of the household type after leaving the other adult of the couple.
     * Reset the meeting year to -1.
     */
    public void updateHshFeaturesAfterDivorce(Household oldPartner) {
        assert hshType == Type.COMPLEX || getAdults().size() == 1 : this + "" + oldPartner;
        this.oldPartner = oldPartner;
        coupleAtParents = false;
        resetLeader();
        if (listOfMembers.size() == 1) { // that is a new household without children, thus a single
            hshType = Type.SINGLE;
        } else if (hshType != Type.COMPLEX) {
            // that is a new household with children, thus a monoparentaly family
            hshType = Type.MONOPARENTAL;
        }
        meetingYear = -1;
    }

    /**
     * Update of the household type after one of the two old people has migrate
     * to a retirement home probably Then the resting individual is considered
     * as a single Reset the meeting year to -1.
     */
    public void updateHshFeaturesAfterRetiredMigration() {
        coupleAtParents = false;
        resetLeader();
        if (listOfMembers.size() == 1) { // that is a new household without children, thus a single
            hshType = Type.SINGLE;
        } else if (getAdults().size() == 1) {
            // that is a new household with children, thus a monoparentaly family
            hshType = Type.MONOPARENTAL;
        } else if (listOfMembers.size() == 2 && hshType != Type.COMPLEX) { // a child is dead
            // last child is dead
            hshType = Type.COUPLE_WITHOUT_CHILDREN;
        }
        meetingYear = -1;
    }

    /**
     * Update of the household type after a death.
     */
    public void updateHshFeaturesAfterDeath() {
        int nbAdults = getAdults().size();
        if (nbAdults == 0) {
            // only children in the household, so it is suppressed
            getMyVillage().suppressHousehold(this, true, false);
            setJustSuppressed(true);
        } else if (listOfMembers.size() == 1) {
            // only one adult remains, easy case
            hshType = Type.SINGLE;
            resetLeader();
        } else if (nbAdults == 1) {
            hshType = Type.MONOPARENTAL;
            resetLeader();
        } else if (listOfMembers.size() == 2 && hshType != Type.COMPLEX) { // a child is dead
            // last child is dead
            hshType = Type.COUPLE_WITHOUT_CHILDREN;
        }
    }

    /**
     * Update of the household type after a death for potential inmigrants only.
     */
    public void updateHshFeaturesAfterDeath(boolean potential) {
        int nbAdults = getAdults().size();
        if (nbAdults == 0) {
            // only children in the household, so it is suppressed
            //getMyVillage().suppressHousehold(this, true, false);
            setJustSuppressed(true);
        } else if (listOfMembers.size() == 1) {
            // only one adult remains, easy case
            hshType = Type.SINGLE;
            resetLeader();
        } else if (nbAdults == 1) {
            hshType = Type.MONOPARENTAL;
            resetLeader();
        } else if (listOfMembers.size() == 2 && hshType != Type.COMPLEX) { // a child is dead
            // last child is dead
            hshType = Type.COUPLE_WITHOUT_CHILDREN;
        }
    }

    /**
     * Update of the household type after a birth
     */
    public void updateHshFeaturesAfterBirth() {
        if (hshType == Type.COUPLE_WITHOUT_CHILDREN) {
            hshType = Type.COUPLE_WITH_CHILDREN;
        }
        if (hshType == Type.SINGLE) {
            hshType = Type.MONOPARENTAL;
        }
    }

    /**
     * Update of the household type when the household join another adult
     */
    public void updateHshFeaturesAfterJoining(int iter) {
        meetingYear = iter;
        resetLeader();
        if (hshType == Type.SINGLE) {
            if (getChildren().isEmpty()) {
                hshType = Type.COUPLE_WITHOUT_CHILDREN;
            } else {
                hshType = Type.COUPLE_WITH_CHILDREN;
            }
        } else if (hshType == Type.MONOPARENTAL) {
            hshType = Type.COUPLE_WITH_CHILDREN;
        }
    }

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder("Household ").append(id);
        buff.append(" ").append(hshType);
        if (justSuppressed) {
            buff.append(" justSuppressed ");
        }
        if (getResidence() != null) {
            if (getResidence().isTransit()) {
                buff.append(" isTransit ");
            }
        }
        buff.append(" in ").append(getMyVillage().getName()).append("\n");
        for (Individual member : listOfMembers) {
            buff.append("  #").append(member.getId()).append(" ").append(member.getAge()).append("(").append(member.isAdult() ? "adult" : "child").append(")");
            buff.append(",").append(member.getStatus());
            if (member.getStatus() == Status.WORKER) {
                buff.append(" in ").append(member.getJobLocation());
            }
            buff.append(",").append(member.getMyActivityPattern());
            buff.append("\n");
        }
        return buff.toString();
    }

    /**
     * Method to create by duplication the potentially migrant individuals
     */
    public void createMigrants(Household memb) {
        Individual[] individ = new Individual[memb.size()];
        int i = 0;
        for (Individual indiv : memb) {
            individ[i] = new Individual(indiv, true);
            individ[i].setProfession(indiv.getProfession());
            individ[i].setMyHousehold(this);
            individ[i].updateAgeLabourMarket();
            individ[i].updateAgeRetirement();
            if (individ[i].getStatus() == Status.WORKER) {
                individ[i].setStatus(Status.UNEMPLOYED);
                individ[i].becomeUnemployed(getMyVillage());
                individ[i].setLookForJob(true);
            }
            if (individ[i].getStatus() == Status.UNEMPLOYED) {
                individ[i].setJobLocation(this.getMyVillage());
                individ[i].takeAnActivPattern(getMyVillage().getMyRegion().getAllPatterns()[0], individ[i].getJobLocation());
                individ[i].setNextActPat(null);
                individ[i].setLookForJob(true);
            }
            if (individ[i].getStatus() == Status.STUDENT) {
                if (individ[i].getAge() > 14) {
                    individ[i].setStatus(Status.UNEMPLOYED);
                    individ[i].setJobLocation(this.getMyVillage());
                    individ[i].takeAnActivPattern(getMyVillage().getMyRegion().getAllPatterns()[0], individ[i].getJobLocation());
                    individ[i].setNextActPat(null);
                    individ[i].setLookForJob(true);
                } else {
                    individ[i].setJobLocation(null);
                    individ[i].setNextActPat(null);
                    individ[i].setLookForJob(false);
                }
            }
            if (individ[i].getStatus() == Status.RETIRED) {
                individ[i].setJobLocation(null);
                individ[i].setNextActPat(null);
                individ[i].setLookForJob(false);
            }
            i++;
        }
        for (i = 0; i < individ.length; i++) {
            listOfMembers.add(individ[i]);
        }
    }

    /**
     * Satisfaction regarding the dwelling size - depends on the distance to the
     * ideal size of dwelling, to the average age of the household Use to assess
     * the level of satisfaction of an household regarding its current dwelling
     */
    public boolean satisfiedByLodging(int idealSizeOfRes, int actualTypeOfRes) {
        if (oldPartner != null) {
            return false;
        } else {
            return satisfyingSizeLodging(idealSizeOfRes, actualTypeOfRes, true);
        }
    }

    /**
     * Give the level of satisfaction for an type of res called actualTypeOfRes
     * for an assumed actualTypeOfRes The boolean decision indicates if the
     * household should decide quitting its current household (the age has a
     * negative effect on the moving decision) or if the household should say if
     * it can be satisfied by a proposition of size for a new logding (the age
     * has the contrary effect, older it is, less open-minded for the size of
     * the dwelling it is)
     */
    public boolean satisfyingSizeLodging(int idealSizeOfRes, int actualTypeOfRes, boolean decision) {
        double val = 0.0;
        if (decision) { // to decide quitting the current size
            /*
             * decision based on age val = 1 - (((float)
             * (Math.abs(idealSizeOfRes - actualTypeOfRes)) / (float)
             * (getMyVillage().getMyRegion().getParameters().getNbSizeRes() -
             * 1)) Math.exp(-((averageAge() - 15.0) *
             * getMyVillage().getMyRegion().getParameters().getProbToAcceptNewResidence())));
             * return (getRandom().nextDouble() <= val);
             */
            ///* decision based on the nature of the need (increasing or decreasing)
            if ((idealSizeOfRes - actualTypeOfRes) > 0) { // need to increase the house then depends on the number of rooms lacking
                val = 1 - ((float) (Math.abs(idealSizeOfRes - actualTypeOfRes)) / (float) (getMyVillage().getMyRegion().getParameters().getNbSizeRes() - 1));
            } else { // need nothing or to decrease, then constant probability to move (named by error getProbToAcceptNewResidence) TO CHANGE
                val = 1 - (float) getMyVillage().getMyRegion().getParameters().getProbToAcceptNewResidence();
            }
            return (getRandom().nextDouble() <= val);
            //*/
        } else { // to decide which size should be convenient to choose, here actual type of res represents the proposed size
            // all is convenient if it is at least larger than the ideal size - built on the fact that rare are people looking for smaller (and it can be the ideal size)
            // pay attention, here the actualTypeOfRes corresponds to the potential type of res
            if (getResidence() != null && getResidence().getType() != -1) {
                if ((getResidence().getType() - idealSizeOfRes) > 0) { // the household needs to decrease the size of her residence
                    //if (actualTypeOfRes <= getResidence().getType() && ((getResidence().getType() - actualTypeOfRes) < 2)) {
                    if (actualTypeOfRes <= getResidence().getType()) {
                        return true;
                    }
                } else { // the household wants to increase the size of her residence or keep the same size
                    //if (actualTypeOfRes >= getResidence().getType() && ((actualTypeOfRes - getResidence().getType()) < 2)) {
                    if (actualTypeOfRes >= getResidence().getType()) {
                        return true;
                    }
                }
            } else { // has no residence before
                //if (actualTypeOfRes >= idealSizeOfRes && ((actualTypeOfRes - idealSizeOfRes) < 2)) {
                if (actualTypeOfRes >= idealSizeOfRes) {
                    return true;
                }
            }
            return false;

        }
        // TODO Parameterise 15 which is the first age to become an adult
        // TODO Change the name of the parameter et probToAcceptNewResidence en beta
    }

    /**
     * Return a list of dwelling sizes susceptible to satisfy the household
     */
    public List possibleSizeLodging() {
        int base = getMyVillage().appropriatedSizeOfRes(getSize());
        List posSize = new ArrayList();
        for (int i = 0; i < getMyVillage().getMyRegion().getParameters().getNbSizeRes(); i++) {
            if (satisfyingSizeLodging(base, i, false)) {
                posSize.add(i);
            }
        }
        return posSize;
    }

    /**
     * Computation of the average age of the adults of the household
     */
    public float averageAge() {
        float aa = 0.0f;
        // Managing especially the case where there is only a student who is not yet an adult since it has not yet completely decided to move (same for retired)
        if (this.size() == 1 && getCopyOfMembers().get(0).getStatus() == Status.STUDENT && getCopyOfMembers().get(0).getAge() >= 18 && getCopyOfMembers().get(0).getAge() <= 29) {
            aa = getCopyOfMembers().get(0).getAge();
        } else {
            if (this.size() == 1 && getCopyOfMembers().get(0).getAge() >= 60) {
                aa = getCopyOfMembers().get(0).getAge();
            } else {
                int totAge = 0;
                for (Individual ind : this.getCopyOfMembers()) {
                    if (ind.isAdult()) {
                        totAge = totAge + ind.getAge();
                    }
                }
                aa = ((float) totAge / (float) this.getAdults().size());
            }
        }
        return aa;
    }
}
