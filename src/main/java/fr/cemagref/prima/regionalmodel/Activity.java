package fr.cemagref.prima.regionalmodel;

/**
 * The set of possible activities (different jobs) that the population of 
 * the village could have. The activities that people can choose can be 
 * described in terms of sector, level of revenue, surface and place of 
 * work (inside or outside the village).
 */
public class Activity {

    /**
     * Sector of activity. Can be something as Agriculture, Industry, Building and Services
     */
    private int activitySector;

    public int getActivitySector() {
        return activitySector;
    }

    /**
     * The profession required in the sector of activity. For example, in Auvergne 
     * the profession is represented by the socio-professional category coded as
     * 0 agriculteur exploitant, 1 artisan et al, 2 cadres,
     * 3 professions intermediaires, 4 employés, 5 ouvriers
     */
    private int profession;

    public int getProfession() {
        return profession;
    }

    /*
     *Constructor Activity
     */
    public Activity(int activSect, int idProf) {
        this.activitySector = activSect;
        this.profession = idProf;
    }
}

