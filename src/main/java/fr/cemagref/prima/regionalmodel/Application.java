/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

/**
 *
 * @author sylvie.huet
 * 8.04.09
 */
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.prima.regionalmodel.gui.MainWindow;
import fr.cemagref.prima.regionalmodel.parameters.Parameters;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import com.thoughtworks.xstream.XStream;
import fr.cemagref.observation.gui.Drawable;
import fr.cemagref.observation.kernel.ObservableManager;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.observation.observers.ConsoleObserver;
import fr.cemagref.prima.regionalmodel.gui.ChartsPanel;
import fr.cemagref.prima.regionalmodel.parameters.AgeDeterminationFromAgeAdult;
import fr.cemagref.prima.regionalmodel.parameters.AgeDeterminationFromDecisionByAge;
import fr.cemagref.prima.regionalmodel.parameters.AgeDeterminationFromDecisionByProfessionAndAge;
import fr.cemagref.prima.regionalmodel.parameters.DecisionByAgeAndProbas;
import fr.cemagref.prima.regionalmodel.parameters.DecisionByYearByAgeAndProbas;
import fr.cemagref.prima.regionalmodel.parameters.IntValueFromExponentialLaw;
import fr.cemagref.prima.regionalmodel.parameters.IntValueFromNormalLaw;
import fr.cemagref.prima.regionalmodel.parameters.LinearValue;
import fr.cemagref.prima.regionalmodel.parameters.ProfessionFromUniformLaw;
import fr.cemagref.prima.regionalmodel.parameters.ProfessionTransitionParameters;
import fr.cemagref.prima.regionalmodel.parameters.TemporalValue;
import fr.cemagref.prima.regionalmodel.parameters.Value;
import fr.cemagref.prima.regionalmodel.parameters.ValueFromConstant;
import fr.cemagref.prima.regionalmodel.parameters.ValueFromGivenDistribution;
import fr.cemagref.prima.regionalmodel.parameters.ValueFromNormalLaw;
import fr.cemagref.prima.regionalmodel.scenarios.DistributedTableUpdater;
import fr.cemagref.prima.regionalmodel.scenarios.ImmigrationUpdater;
import fr.cemagref.prima.regionalmodel.scenarios.Scenario;
import fr.cemagref.prima.regionalmodel.scenarios.TableUpdater.UpdateMode;
import fr.cemagref.prima.regionalmodel.scenarios.grouping.ExcludeOutsideMunicipalities;
import fr.cemagref.prima.regionalmodel.scenarios.grouping.MunicipalitiesMinimumSurfaceFilter;
import fr.cemagref.prima.regionalmodel.scenarios.grouping.MunicipalityIdentifiedByName;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.Random;
import fr.cemagref.prima.regionalmodel.tools.ReflectUtils;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Main class of the simulator. As it used the commons-cli library for parsing the
 * command line arguments, running the program without arguments will give you a
 * description of the required arguments.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class Application {

    private static final Logger LOGGER = Logger.getLogger(Application.class.getName());
    private boolean gui = false;
    private File inputFile;
    private File dataDirectory;
    private Parameters parameters;
    private List<Scenario> scenarios;
    private Random random;
    private ObservableManager observableManager;
    private String version;
    public static String nameFile;
    public static String nameFile1;
    public static String nameFile2;
    public static String myInPath = "dataIn" + File.separator;
    private final static XStream xstream = new XStream();

    static {
        for (Class type : new Class[]{Parameters.class, ValueFromConstant.class, ValueFromGivenDistribution.class,
                    AgeDeterminationFromAgeAdult.class, AgeDeterminationFromDecisionByAge.class,
                    AgeDeterminationFromDecisionByProfessionAndAge.class, DecisionByAgeAndProbas.class, DecisionByYearByAgeAndProbas.class,
                    IntValueFromExponentialLaw.class, IntValueFromNormalLaw.class, ProfessionFromUniformLaw.class, ProfessionTransitionParameters.class,
                    ValueFromConstant.class, ValueFromGivenDistribution.class, ValueFromNormalLaw.class, TemporalValue.class, LinearValue.class}) {
            xstream.alias(type.getSimpleName(), type);
        }
        for (Class type : new Class[]{MunicipalitiesMinimumSurfaceFilter.class, MunicipalityIdentifiedByName.class,
                    ExcludeOutsideMunicipalities.class}) {
            xstream.alias(type.getSimpleName(), type);
        }
        for (Class type : new Class[]{ImmigrationUpdater.class, DistributedTableUpdater.class, UpdateMode.class}) {
            xstream.alias(type.getSimpleName(), type);
        }
        xstream.processAnnotations(PeriodicCSVObserver.class);
        xstream.addImplicitCollection(ConsoleObserver.class, "dates", "date", Integer.class);
    }

    public Application() {
        random = new Random();
        observableManager = new ObservableManager();
        version = "";
        try {
            Properties props = new Properties();
            props.load(getClass().getResourceAsStream("application.properties"));
            String projectVersion = props.getProperty("project.version");
            version = projectVersion;
            if (projectVersion.contains("SNAPSHOT")) {
                version = projectVersion + " - " + props.getProperty("build.timestamp");
            }
        } catch (IOException ex) {
            LOGGER.log(Level.WARNING, "Application version cannot be loaded", ex);
        }

    }

    public Application(String inputFilename) throws FileNotFoundException {
        this();
        loadInputFile(inputFilename);
    }

    public Application(File dataDirectory, Parameters parameters) throws FileNotFoundException {
        this();
        this.dataDirectory = dataDirectory;
        if (parameters != null) {
            loadParameters(parameters);
        }
    }

    public Application(String[] args) throws IOException, BadDataException, ProcessingException {
        this();
        // command line options parsing
        Option optionBatchMode = OptionBuilder.withLongOpt("batch-mode").withDescription("Use the batch mode (no GUI)").isRequired(false).create("b");
        Option optionInputFile = OptionBuilder.withLongOpt("input").withDescription("Input file").isRequired(false).hasArgs(1).withArgName("filename").create("i");
        Options options = new Options();
        options.addOption(optionInputFile).addOption(optionBatchMode);
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            new HelpFormatter().printHelp(" ", options);
            throw new BadDataException("Error while parsing command line arguments. Given args was: " + Arrays.toString(args), e);
        }
        // Load parameters
        gui = !cmd.hasOption(optionBatchMode.getOpt());
        if (cmd.getOptionValue(optionInputFile.getOpt()) == null) {
            if (!gui) {
                new HelpFormatter().printHelp(" ", options);
                throw new BadDataException("Please provide an input file. Given args was: " + Arrays.toString(args));
            }
        } else {
            loadInputFile(cmd.getOptionValue(optionInputFile.getOpt()));
        }
    }

    /**
     * Set the input file of the application and load the parameters.
     * @param inputFile
     * @throws FileNotFoundException 
     */
    public final void loadInputFile(String inputFile) throws FileNotFoundException {
        LOGGER.log(Level.INFO, "Running Prima Regional Model version {0}", version);
        this.inputFile = new File(inputFile);
        dataDirectory = this.inputFile.getParentFile();
        loadParameters((Parameters) fromXML(new FileReader(inputFile)));
    }

    private void loadParameters(Parameters parameters) throws FileNotFoundException {
        this.parameters = parameters;
        if (parameters.getScenarios() != null) {
            this.scenarios = parameters.getScenarios();
        } else {
            this.scenarios = (List<Scenario>) fromXML(new FileReader(getFileFromRelativePath(parameters.getScenarioFile())));
        }
    }

    public Random getRandom() {
        return random;
    }

    public ObservableManager getObservableManager() {
        return observableManager;
    }

    public static void toXML(Object obj, OutputStream out) {
        xstream.toXML(obj, out);
    }

    public static void toXML(Object obj, Writer out) {
        xstream.toXML(obj, out);
    }

    public static String toXML(Object obj) {
        return xstream.toXML(obj);
    }

    public static Object fromXML(InputStream input) {
        return xstream.fromXML(input);
    }

    public static Object fromXML(Reader xml) {
        return xstream.fromXML(xml);
    }

    public static Object fromXML(String xml) {
        return xstream.fromXML(xml);
    }

    public File getInputFile() {
        return inputFile;
    }

    public File getDataDirectory() {
        return dataDirectory;
    }

    public void setDataDirectory(File dataDirectory) {
        this.dataDirectory = dataDirectory;
    }

    public File getFileFromRelativePath(String filename) {
        return new File(getDataDirectory(), filename);
    }

    public boolean isGui() {
        return gui;
    }

    public String getVersion() {
        return version;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    public List<Scenario> getScenarios() {
        return scenarios;
    }

    public File getMunicipalityDirectory(Municipality municipality) {
        return new File(getDataDirectory(), municipality.getName());
    }

    public void loadFiles() throws BadDataException, ProcessingException {
        parameters.loadFiles(this);
    }

    public MunicipalitySet run() throws IOException, BadDataException, ProcessingException {
        if (gui) {
            MainWindow window = new MainWindow(this);
            window.setVisible(true);
        } else {
            // remove charts observer
            ObservablesHandler observablesHandler = observableManager.getObservable(MunicipalitySet.class);
            if (observablesHandler != null) {
                for (Iterator<ObserverListener> it = observablesHandler.getObservers().iterator(); it.hasNext();) {
                    ObserverListener observerListener = it.next();
                    if (observerListener instanceof Drawable) {
                        it.remove();
                    }
                }
            }
            MunicipalitySet region = new MunicipalitySet(this, parameters, scenarios);
            LOGGER.fine("MunicipalitySet loaded, ready to run!");
            region.run();
            return region;
        }
        return null;
    }

    public MunicipalitySet run(int replic) throws IOException, BadDataException, ProcessingException {
        MunicipalitySet region = new MunicipalitySet(this, parameters, scenarios);
        region.run(replic);
        return region;
    }

    /**
     * The option on the command line -g allows to benefit from an interface to run the model and visualise some indicators
     */
    public static void main(String[] args) throws Exception {
        // logging init
        try {
            LogManager.getLogManager().readConfiguration(Application.class.getResourceAsStream("logging.properties"));
        } catch (IOException ex) {
            LOGGER.log(Level.WARNING,
                    "Problems to load the logging configuration file", ex);
        }
        // load and run
        Application application = new Application(args);
        if (application.parameters != null) {
            application.loadFiles();
        }
        application.run();
    }
}
