/*
 * Copyright (C) 2013 Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class MunicipalitySetCounters {

    private MunicipalitySet municipalitySet;
    private int oldPopsize;
    private int totalOrphans;
    private int totalExpulsed;
    private int nbBirth;
    private int nbDeath;
    public int nbNotFindJob = 0;
    public int nbFindJob = 0;
    public int outMigrantsLastStep; //necessary for computation of number of potential in-migrants

    @Observable(description = "Stop being unemployed during the year")
    public int getStopBeingUnemployed() {
        return stopBeingUnemployed;
    }
    private int stopBeingUnemployed;

    @Observable(description = "Become unemployed during the year")
    public int getBecomeUnemploy() {
        return becomeUnemploy;
    }
    private int becomeUnemploy;

    public MunicipalitySetCounters(MunicipalitySet municipalitySet) {
        this.municipalitySet = municipalitySet;
    }

    public MunicipalitySet getMunicipalitySet() {
        return municipalitySet;
    }

    public void reset() {
        stopBeingUnemployed = 0;
        becomeUnemploy = 0;
        totalExpulsed = 0;
        totalOrphans = 0;
        outMigrantsLastStep = getNbOutMigrants();
        oldPopsize = getPopulationSize();
    }

    public void updateCounters() {
        nbBirth = 0;
        nbDeath = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            nbBirth = nbBirth + mun.getCounters().getNumBirth();
            nbDeath = nbDeath + mun.getCounters().getNumDeath();
            totalOrphans = totalOrphans + mun.getCounters().getNbKilledOrphans();
            totalExpulsed = totalExpulsed + mun.getCounters().getNbHouseholdExpulsed();
        }
    }

    public int getOutMigrantsLastStep() {
        return outMigrantsLastStep;
    }

    public int getTotalOrphans() {
        return totalOrphans;
    }

    public int getTotalExpulsed() {
        return totalExpulsed;
    }

    public void incBecomeUnemploy(int inc) {
        this.becomeUnemploy += inc;
    }

    public void incStopBeingUnemployed(int inc) {
        this.stopBeingUnemployed += inc;
    }

    public void incNbNotFindJob(int inc) {
        this.nbNotFindJob += inc;
    }

    public void incNbFindJob(int inc) {
        this.nbFindJob += inc;
    }

    @Observable(description = "Total job offer for insiders comprised the outside offer")
    public int getJobOfferForInsidersWithOutside() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipalitySet.getNbActivities(); i++) {
                size += municipality.getOfferedActivities()[i] - municipality.getOccupiedActivitiesByOutside()[i];
            }
        }
        for (Municipality municipality : municipalitySet.getOutsides()) {
            for (int i = 0; i < municipalitySet.getNbActivities(); i++) {
                size += municipality.getOfferedActivities()[i] - municipality.getOccupiedActivitiesByOutside()[i];
            }
        }
        return size;
    }

    public void updatePopulationSize() {
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            municipality.updatePopSize();
        }
    }

    @Observable(description = "Population size")
    public int getPopulationSize() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size += municipality.getPopulationSize();
        }
        return size;
    }

    public String[] getPopulationsSizeLabels(Integer dimension) {
        String[] labels = new String[municipalitySet.getMyMunicipalities().size()];
        for (int i = 0; i < municipalitySet.getMyMunicipalities().size(); i++) {
            labels[i] = "Pop" + municipalitySet.getMyMunicipalities().get(i).getName();
        }
        return labels;
    }

    @Observable(description = "Population size for each municipalities", labelsMethod = "getPopulationsSizeLabels", size = {0})
    public int[] getPopulationsSize() {
        int[] sizes = new int[municipalitySet.getMyMunicipalities().size()];
        for (int i = 0; i < municipalitySet.getMyMunicipalities().size(); i++) {
            sizes[i] = municipalitySet.getMyMunicipalities().get(i).getPopulationSize();
        }
        return sizes;
    }

    public String[] getPopulationsSizeSmallAndBigMunsLabels(Integer dimension) {
        String[] labels = new String[municipalitySet.getMyMunicipalities().size()];
        int index = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            if (mun.getPopulationSizeAtInit() < 1000) {
                labels[index] = "PopSmall_" + mun.getName();
                index++;
            }
        }
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            if (mun.getPopulationSizeAtInit() >= 1000) {
                labels[index] = "PopBig_" + mun.getName();
                index++;
            }
        }
        return labels;
    }

    @Observable(description = "Population size for small muns (less than 1000) and then big", labelsMethod = "getPopulationsSizeSmallAndBigMunsLabels", size = {0})
    public int[] getPopulationsSizeSmallAndBigMuns() {
        int[] sizes = new int[municipalitySet.getMyMunicipalities().size()];
        int index = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            if (mun.getPopulationSizeAtInit() < 1000) {
                sizes[index] = mun.getPopulationSize();
                index++;
            }
        }
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            if (mun.getPopulationSizeAtInit() >= 1000) {
                sizes[index] = mun.getPopulationSize();
                index++;
            }
        }
        return sizes;
    }

    @Observable(description = "Population size 15040 ")
    public int getPopulationSize15040() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15040"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Condat (15054)")
    public int getPopulationSize15054() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15054"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Lugarde (15110)")
    public int getPopulationSize15110() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15110"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Marcenat (15114)")
    public int getPopulationSize15114() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15114"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Montboudif (15129)")
    public int getPopulationSize15129() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15129"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Montgreleix (15132)")
    public int getPopulationSize15132() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15132"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Montgreleix (15173)")
    public int getPopulationSize15173() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15173"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Arpajon sur Cère (15012)")
    public int getPopulationSize15012() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15012"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Aurillac (15014)")
    public int getPopulationSize15014() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15014"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Ytrac (15267)")
    public int getPopulationSize15267() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15267"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Vézac (15255)")
    public int getPopulationSize15255() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15255"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size St Flour (15187)")
    public int getPopulationSize15187() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15187"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Maurs (15122)")
    public int getPopulationSize15122() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15122"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Massiac (15119)")
    public int getPopulationSize15119() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15119"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size Chaudes Aigues (15045)")
    public int getPopulationSize15045() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15045"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Population size St Mamet La Salvetat (15196)")
    public int getPopulationSize15196() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            if (municipality.getName().equalsIgnoreCase(new String("15196"))) {
                size += municipality.getPopulationSize();
            }
        }
        return size;
    }

    @Observable(description = "Annual number of individuals expulsed after a decrease of the dwelling")
    public int getNbIndividualsExpulsed() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size += municipality.getCounters().getNbIndividualsExpulsed();
        }
        return size;
    }

    @Observable(description = "Annual natural corrected balance (birth - death - killed orphans)")
    public int getAnnualNaturalBalanceWithKilledOrphans() {
        return nbBirth - nbDeath - totalOrphans;
    }

    @Observable(description = "Annual killed orphans")
    public int getAnnualKilledOrphans() {
        return totalOrphans;
    }

    @Observable(description = "Annual natural balance (birth - death)")
    public int getAnnualNaturalBalance() {
        return nbBirth - nbDeath;
    }

    @Observable(description = "Nb birth")
    public int getNbBirth() {
        return nbBirth;
        //return nbBirth;
    }

    @Observable(description = "Nb death")
    public int getNbDeath() {
        return nbDeath;
    }

    @Observable(description = "Annual natural balance (birth - death)")
    public int getTotalNaturalBalance() {
        return nbBirth - nbDeath;
    }

    @Observable(description = "Migration balance (in - out migrations) (without those born and out-migrate the same year)")
    public int getMigratoryBalance() {
        return getNbImmigrants() - getNbOutMigrants();
    }

    @Observable(description = "Nb of individuals out migrants (without those born and out-migrate the same year)")
    public int getNbOutMigrants() {
        return (municipalitySet.getParameters().getMigration().getMigrantsCount());
    }

    @Observable(description = "Nb of individuals active out migrants")
    public int getNbActiveOutMigrants() {
        return municipalitySet.getParameters().getMigration().getActiveMigrantsCount();
    }

    @Observable(description = "Nb of individuals inactive out migrants")
    public int getNbInactiveOutMigrants() {
        return municipalitySet.getParameters().getMigration().getInactiveMigrantsCount();
    }

    @Observable(description = "Nb of individuals out migrants 0-14")
    public int getNbOutMigrants0_14() {
        return municipalitySet.getParameters().getMigration().getMigrantsCount0_14();
    }

    @Observable(description = "Nb of individuals out migrants 15-24")
    public int getNbOutMigrants15_24() {
        return municipalitySet.getParameters().getMigration().getMigrantsCount15_24();
    }

    @Observable(description = "Nb of individuals out migrants 25-29")
    public int getNbOutMigrants25_29() {
        return municipalitySet.getParameters().getMigration().getMigrantsCount25_29();
    }

    @Observable(description = "Nb of individuals out migrants 30-39")
    public int getNbOutMigrants30_39() {
        return municipalitySet.getParameters().getMigration().getMigrantsCount30_39();
    }

    @Observable(description = "Nb of individuals out migrants 40-59")
    public int getNbOutMigrants40_59() {
        return municipalitySet.getParameters().getMigration().getMigrantsCount40_59();
    }

    @Observable(description = "Nb of individuals out migrants 60-74")
    public int getNbOutMigrants60_74() {
        return municipalitySet.getParameters().getMigration().getMigrantsCount60_74();
    }

    @Observable(description = "Nb of individuals out migrants 75 and more")
    public int getNbOutMigrants75AndMore() {
        return municipalitySet.getParameters().getMigration().getMigrantsCount75AndMore();
    }

    @Observable(description = "Nb of retired out migrants")
    public int getNbOutMigrantRetired() {
        return municipalitySet.getParameters().getMigration().getMigrantsCountR();
    }

    @Observable(description = "Nb of student out migrants")
    public int getNbOutMigrantStudent() {
        return municipalitySet.getParameters().getMigration().getMigrantsCountS();
    }

    @Observable(description = "Nb of Actives out migrants")
    public int getNbOutMigrantActives() {
        return municipalitySet.getParameters().getMigration().getMigrantsCountO();
    }

    @Observable(description = "Nb of individuals out migrants of the region")
    public int getNbIndividualMoveOut() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbHouseholdMoveOut();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in migrants in the region 0-14")
    public int getNbImmigrants0_14() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbImmigrants0_14();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in migrants in the region 15_24")
    public int getNbImmigrants15_24() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbImmigrants15_24();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in migrants in the region 25_29")
    public int getNbImmigrants25_29() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbImmigrants25_29();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in migrants in the region 30_39")
    public int getNbImmigrants30_39() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbImmigrants30_39();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in migrants in the region 40_59")
    public int getNbImmigrants40_59() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbImmigrants40_59();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in migrants in the region 60_74")
    public int getNbImmigrants60_74() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbImmigrants60_74();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in migrants in the region 75AndMore")
    public int getNbImmigrants75AndMore() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbImmigrants75andMore();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in migrants")
    public int getNbImmigrants() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbImmigrants();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in active in migrants")
    public int getNbActiveImmigrants() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbActiveImmigrants();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in inactive in migrants")
    public int getNbInActiveImmigrants() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbInactiveImmigrants();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in migrants 60 and more")
    public int getNbImmigrants60() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbImmigrants60();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals in migrants single")
    public int getNbImmigrantsSingle() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbImmigrantsSingle();
        }
        System.err.println("nb immigrants single " + tot);
        return tot;
    }

    @Observable(description = "Parameter NbChild")
    public float getNbChild() {
        float nbC = (float) municipalitySet.getParameters().getNbChild(null);
        return nbC;
    }

    @Observable(description = "Unemployed (w/o immigrants)")
    public int getUnemployedWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                            size++;
                        }
                    }
                }
            }
        }
        return size;
    }
    int nbRetiredF = 0;

    @Observable(description = "Nb Retired farmers this year)")
    public int getNbRetiredFarmersThisYear() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAgeToGoOnRetirement() == ind.getAge()) {
                            if (ind.getProfession() == 0) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        nbRetiredF = nbRetiredF + size;
        return size;
    }

    @Observable(description = "Nb Retired farmers cumulated")
    public int getNbRetiredFarmersCumulated() {
        getNbRetiredFarmersThisYear();
        return nbRetiredF;
    }
    int nbRetiredC = 0;

    @Observable(description = "Nb Retired craftmen this year)")
    public int getNbRetiredCraftmenThisYear() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAgeToGoOnRetirement() == ind.getAge()) {
                            if (ind.getProfession() == 1) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        nbRetiredC = nbRetiredC + size;
        return size;
    }

    @Observable(description = "Nb Retired employees this year)")
    public int getNbRetiredEmployeesThisYear() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAgeToGoOnRetirement() == ind.getAge()) {
                            if (ind.getProfession() == 4) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        nbRetiredC = nbRetiredC + size;
        return size;
    }

    @Observable(description = "Nb Retired executives this year)")
    public int getNbRetiredExecutivesThisYear() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAgeToGoOnRetirement() == ind.getAge()) {
                            if (ind.getProfession() == 2) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        nbRetiredC = nbRetiredC + size;
        return size;
    }

    @Observable(description = "Nb Retired prof interm this year)")
    public int getNbRetiredIntermProfThisYear() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAgeToGoOnRetirement() == ind.getAge()) {
                            if (ind.getProfession() == 3) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        nbRetiredC = nbRetiredC + size;
        return size;
    }

    @Observable(description = "Nb Retired")
    public int getNbRetireds() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.RETIRED) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Students > 18")
    public int getNbStudentsSup18() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.STUDENT && ind.getAge() > 18) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Students")
    public int getNbStudents() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.STUDENT) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Children < 15")
    public int getNbChildrenLower15() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAge() < 15) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "From 15 to 29")
    public int getNbFrom15to29() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAge() > 14 && ind.getAge() < 30) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "From 30 to 44")
    public int getNbFrom30to44() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAge() > 29 && ind.getAge() < 45) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "From 45 to 59")
    public int getNbFrom45to59() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAge() > 44 && ind.getAge() < 60) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "From 60 to 74")
    public int getNbFrom60to74() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAge() > 59 && ind.getAge() < 75) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Old > 74")
    public int getNbOldHigher74() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAge() > 74) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Old > 60")
    public int getNbOldHigher60() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAge() > 60) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Actives")
    public int getNbActives() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED || ind.getStatus() == Individual.Status.WORKER) {
                            r++;
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Actives 15_64 years' old")
    public int getNbActives15_64() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED || ind.getStatus() == Individual.Status.WORKER) {
                            if (ind.getAge() > 15 && ind.getAge() < 65) {
                                r++;
                            }
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Actives having an occupation 15_64 years' old")
    public int getNbActivesOccupied15_64() {
        int r = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.WORKER) {
                            if (ind.getAge() > 15 && ind.getAge() < 65) {
                                r++;
                            }
                        }
                    }
                }
            }
        }
        return r;
    }

    @Observable(description = "Nb Retired craftmen cumulated")
    public int getNbRetiredCraftmenCumulated() {
        getNbRetiredCraftmenThisYear();
        return nbRetiredC;
    }
    int nbRetiredW = 0;

    @Observable(description = "Nb Retired workers this year)")
    public int getNbRetiredWorkerThisYear() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAgeToGoOnRetirement() == ind.getAge()) {
                            if (ind.getProfession() == 5) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        nbRetiredW = nbRetiredW + size;
        return size;
    }

    @Observable(description = "Nb Retired workers cumulated")
    public int getNbRetiredWorkerCumulated() {
        getNbRetiredWorkerThisYear();
        return nbRetiredW;
    }

    @Observable(description = "Free jobs for farmers (w/o outsides)")
    public int getFreeJobsForFarmers() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                /*
                 * if (i == 0 || i == 6 || i == 12 || i == 18) {
                 * System.err.println(municipality.getName() + " offer " +
                 * municipality.getOfferedActivities(i) + " res " +
                 * municipality.getOccupiedActivitiesByRes(i) + " ext " +
                 * municipality.getOccupiedActivitiesByExt(i) + " out " +
                 * municipality.getOccupiedActivitiesByOutside(i)); }
                 */
                if (i == 0) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 6) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 12) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 18) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Offered jobs for farmers")
    public int getOfferedJobsForFarmers() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 0) {
                    size = size + (int) municipality.getOfferedActivities(i);
                }
                if (i == 6) {
                    size = size + (int) municipality.getOfferedActivities(i);
                }
                if (i == 12) {
                    size = size + (int) municipality.getOfferedActivities(i);
                }
                if (i == 18) {
                    size = size + (int) municipality.getOfferedActivities(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Offered jobs")
    public int getOfferedJobs() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                size = size + (int) municipality.getOfferedActivities(i);
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by workers")
    public int getOccupiedJobsByWorkers() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipality.getOccupiedActivities().length; i++) {
                if (i == 5) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 11) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 17) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 23) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
            }
        }
        for (Municipality municipality : municipalitySet.getOutsides()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 5) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 11) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 17) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 23) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by craftmen")
    public int getOccupiedJobsByCraftmen() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 1) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 7) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 13) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 19) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
            }
        }
        for (Municipality municipality : municipalitySet.getOutsides()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 1) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 7) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 13) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 19) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by people living outside")
    public int getOccupiedJobsByOutside() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipality.getOccupiedActivities().length; i++) {
                size = size + (int) municipality.getOccupiedActivitiesByOutside(i);
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by employees")
    public int getOccupiedJobsByEmployees() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipality.getOccupiedActivities().length; i++) {
                if (i == 4) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 10) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 16) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 22) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
            }
        }
        for (Municipality municipality : municipalitySet.getOutsides()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 4) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 10) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 16) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 22) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by prof. interm.")
    public int getOccupiedJobsByProfInterm() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipality.getOccupiedActivities().length; i++) {
                if (i == 3) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 9) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 15) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 21) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
            }
        }
        for (Municipality municipality : municipalitySet.getOutsides()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 3) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 9) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 15) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 21) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by farmers")
    public int getOccupiedJobsByFarmers() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 0) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 6) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 12) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
                if (i == 18) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i) + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
            }
        }
        for (Municipality municipality : municipalitySet.getOutsides()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 0) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 6) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 12) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
                if (i == 18) {
                    size = size + (int) municipality.getOccupiedActivitiesByRes(i) + (int) municipality.getOccupiedActivitiesByExt(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by Indiv outside")
    public int getOccupiedJobsByIndivFromOutside() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                size = size + (int) municipality.getOccupiedActivitiesByOutside(i);
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by outside farmers")
    public int getOccupiedJobsByOutsideFarmer() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 0 || i == 6 || i == 12 || i == 18) {
                    size = size + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Occupied jobs by outside craftmen")
    public int getOccupiedJobsByOutsideCraftmen() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 1 || i == 7 || i == 13 || i == 19) {
                    size = size + (int) municipality.getOccupiedActivitiesByOutside(i);
                }
            }
        }
        return size;
    }
    //*/

    @Observable(description = "Free jobs for workers (w/o outsides)")
    public int getFreeJobsForWorkers() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getOccupiedActivities().length; i++) {
                if (i == 5) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 11) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 17) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 23) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Free jobs - total")
    public int getJeJobs() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipality.getOccupiedActivities().length; i++) {
                size = size + (int) municipality.getFreeActivities(i);
            }
        }
        return size;
    }

    @Observable(description = "Free jobs - total")
    public int getFreeJobs() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipality.getOccupiedActivities().length; i++) {
                size = size + (int) municipality.getFreeActivities(i);
            }
        }
        return size;
    }

    @Observable(description = "Free jobs for craftmen (w/o outsides)")
    public int getFreeJobsForCraftmen() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipality.getOccupiedActivities().length; i++) {
                if (i == 1) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 7) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 13) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 19) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Free jobs for executives (w/o outsides)")
    public int getFreeJobsForExecutives() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipality.getOccupiedActivities().length; i++) {
                if (i == 2) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 8) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 14) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 20) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Free jobs for Interm Prof (w/o outsides)")
    public int getFreeJobsForIntermProf() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipality.getOccupiedActivities().length; i++) {
                if (i == 3) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 9) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 15) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 21) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Free jobs for employees (w/o outsides)")
    public int getFreeJobsForEmployees() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipality.getOccupiedActivities().length; i++) {
                if (i == 4) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 10) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 16) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
                if (i == 22) {
                    size = size + (int) municipality.getFreeActivities(i);
                }
            }
        }
        return size;
    }

    @Observable(description = "Unemployed farmers (w/o immigrants)")
    public int getUnemployedFarmersWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                            if (ind.getProfession() == 0) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Unemployed craftmen (w/o immigrants)")
    public int getUnemployedCraftmenWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                            if (ind.getProfession() == 1) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Unemployed executives (w/o immigrants)")
    public int getUnemployedExecutivesWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                            if (ind.getProfession() == 2) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Unemployed intermediairy prof (w/o immigrants)")
    public int getUnemployedIntermProfWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                            if (ind.getProfession() == 3) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Unemployed employees (w/o immigrants)")
    public int getUnemployedEmployeesWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                            if (ind.getProfession() == 4) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Unemployed workers (w/o immigrants)")
    public int getUnemployedWorkersWithoutImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                            if (ind.getProfession() == 5) {
                                size++;
                            }
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Unemployed (with immigrants)")
    public int getUnemployedWithImmigrantsCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                        size++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Nb of inactives")
    public int getNbInactives() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household) {
                    if (ind.getStatus() == Individual.Status.INACTIVE) {
                        size++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Retired")
    public int getRetiredCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.RETIRED) {
                            size++;
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "services jobs offer")
    public int getServicesJobOffer() {
        int count = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = municipalitySet.getNbProfessions() * 3; i < municipalitySet.getNbActivities(); i++) {
                count += municipality.getOfferedActivities(i);
            }
        }
        return count;
    }

    @Observable(description = "endogeneous services jobs offer")
    public int getEndogServicesJobOffer() {
        int count = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = municipalitySet.getNbProfessions() * 3; i < municipalitySet.getNbActivities(); i++) {
                count += municipality.endogeneousOfferedJob[i];
            }
        }
        return count;
    }

    @Observable(description = "exogeneous services jobs offer")
    public int getExogServicesJobOffer() {
        int count = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = municipalitySet.getNbProfessions() * 3; i < municipalitySet.getNbActivities(); i++) {
                count += municipality.exogeneousOfferedJob[i];
            }
        }
        return count;
    }

    @Observable(description = "potential immigrants")
    public int getNbPotentialImmigrants() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size += municipality.getCounters().getNbPotentialImmigrants();
        }
        return size;
    }

    @Observable(description = "Couples nb indiv avec ou sans enfants")
    public int getNbCouples() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    if ((household.getHshTypeCode() == 2) || (household.getHshTypeCode() == 3)) {
                        size++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Couples nb hsh avec enfants")
    public int getNbCouplesWithChildren() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    if ((household.getHshTypeCode() == 3)) {
                        size++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Couples nb hsh sans enfants")
    public int getNbCouplesWithoutChildren() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    if ((household.getHshTypeCode() == 2)) {
                        size++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Nb Adults")
    public int getNbAdults() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                size += household.getAdults().size();
            }
        }
        return size;
    }

    @Observable(description = "Nb Singles without potential immigrants")
    public int getNbSinglesWoPotImmig() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    if (household.singleAdult()) {
                        size++;
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Nb Singles available for making couple")
    public int getNbSingles() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (household.singleAdult()) {
                    size++;
                }
            }
        }
        return size;
    }

    @Observable(description = "Nb Singles total")
    public int getNbSinglesTot() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (household.getAdults().size() == 1) {
                    size++;
                }
            }
        }
        return size;
    }

    @Observable(description = "Nb menage par residence (moyenne)")
    public int getNbHshByRes() {
        float size = 0;
        int size2 = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getResidencesTypeCount(); i++) {
                size += municipality.getResidenceOffer(i) - municipality.getFreeResidencesCount(i);
            }
            size2 += municipality.getMyHouseholds().size();
        }
        size = (float) size2 / size;
        return (int) size;
    }

    @Observable(description = "Nb times one individual has not found a job")
    public int getNbNotFindJob() {
        return nbNotFindJob;
    }

    @Observable(description = "Nb times one individual has found a job")
    public int getNbFindJob() {
        return nbFindJob;
    }

    @Observable(description = "Nb res occupied)")
    public int getNbOccupiedRes() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i
                    < municipality.getResidencesTypeCount(); i++) {
                size += municipality.getResidenceOffer(i) - municipality.getFreeResidencesCount(i);
            }
        }
        return size;
    }

    @Observable(description = "Distribution of age by range of 5 years (first range: [0,5[)", size = {20})
    public int[] getDistribAgeBy5() {
        int width = 5;
        int max = 101;
        int nbRange = max / width;
        max = nbRange * width;
        int[] data = new int[nbRange];
        Arrays.fill(data, 0);
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAge() > max - width) {
                            data[data.length - 1]++;
                        } else {
                            data[ind.getAge() / width]++;
                        }
                    }
                }
            }
        }
        return data;
    }

    @Observable(description = "Distribution of sizes of households", size = {6})
    public int[] getDistribHshSize() {
        int maxSize = 6;
        int data[] = new int[maxSize];
        Arrays.fill(data, 0);
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    if (household.size() > maxSize) {
                        data[data.length - 1]++;
                    } else {
                        data[household.size() - 1]++;
                    }
                }
            }
        }
        return data;
    }

    public String[] getDistribHshTypeLabels(Integer dimension) {
        List<String> labels = new ArrayList<String>(Household.Type.values().length);
        for (Household.Type label : Household.Type.values()) {
            labels.add(label.toString());
        }
        return labels.toArray(new String[]{});
    }

    @Observable(description = "Distribution of type of housholds", labelsMethod = "getDistribHshTypeLabels", size = {0})
    public int[] getDistribHshType() {
        int data[] = new int[5];
        Arrays.fill(data, 0);
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    data[household.getHshType().ordinal()]++;
                }
            }
        }
        return data;
    }

    public String[] getDistribHshTypeINSEELabels(Integer dimension) {
        List<String> labels = new ArrayList<String>(15);
        for (String name : new String[]{"dTypeHshModel", "dTypeFamilyVillage", "dTypeHsHVillage"}) {
            for (int i = 0; i < 5; i++) {
                labels.add(name + "[" + i + "]");
            }
        }
        return labels.toArray(new String[]{});
    }

    @Observable(description = "Distributions of type of housholds (model + INSEE)", labelsMethod = "getDistribHshTypeINSEELabels", size = {0})
    public int[] getDistribHshTypeINSEE() {
        int[] data = new int[15];
        Arrays.fill(data, 0);
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            int[] munData = computeINSEEHouseholdType(municipality);
            for (int i = 0; i < munData.length; i++) {
                data[i] += munData[i];
            }
        }
        return data;
    }

    /**
     * This method computes the type of households regarding the definition
     * given by the French National Statistical Institute (INSEE) for the Census
     * The INSEE types are: <ul><li>0: single <li>1: single parent family <li>2:
     * couple without children <li>3: couple with children <li>4: complex
     * household (other households)</ul> The type are the same than in the model
     * but the definition of each type is not the same We counts what the INSEE
     * calls family and what the INSEE calls the household
     */
    private int[] computeINSEEHouseholdType(Municipality mun) {
        int size = 5;
        long indexMostAged = -1;
        boolean childAttachedToTheMostAged = false;
        List<Household> single = new ArrayList();
        List<Household> couple = new ArrayList();
        List<Household> singleParent = new ArrayList();
        int[] dTypeHshModel = new int[size]; // to return
        int[] dTypeFamilyVillage = new int[size]; // to return
        int nbFamily = 0;
        // Number of family by residence (nombre de occupied residences ayant 1, 2, 3, 4, 5 and more families)
        double[] nbFamilyByRes = new double[municipalitySet.getNbSizesResidence()]; // average to return
        int[] dTypeHsHVillage = new int[size]; // to return
        // distribution of household type following the model
        for (Household HouseH : mun.getMyHouseholds()) {
            if (!HouseH.getResidence().isTransit()) {
                dTypeHshModel[HouseH.getHshTypeCode()]++;
            }
        }
        System.arraycopy(dTypeHshModel, 0, dTypeFamilyVillage, 0, size);
        boolean linked = false;
        // computation of type and family following the INSEE definitions
        //<editor-fold defaultstate="collapsed" desc="main loop">
        for (int i = 0; i < municipalitySet.getNbSizesResidence(); i++) {
            for (Residence res : mun.getOccupiedResidences(i)) {
                linked = false;
                single = new ArrayList();
                couple = new ArrayList();
                singleParent = new ArrayList();
                childAttachedToTheMostAged = false;
                nbFamily = res.getNbHhResidents();
                if (res.getNbHhResidents() > 1) {
                    for (Household hh : res) {
                        if (hh.getHshTypeCode() == 0) {
                            single.add(hh);
                        } else {
                            // s'il y un couple alors statut de la household (de la residence) = couple
                            if (hh.getHshTypeCode() == 2 || hh.getHshTypeCode() == 3) {
                                couple.add(hh);
                            } else {
                                if (hh.getHshTypeCode() == 1) {
                                    singleParent.add(hh);
                                }
                            }
                        }
                    }
                    Collections.sort(single, new Comparator<Household>() {
                        @Override
                        public int compare(Household o1, Household o2) {
                            return o2.getAdults().get(0).getAge() - o1.getAdults().get(0).getAge();
                        }
                    });
                    // trouver le couple le plus agé et affecter son type à distrib Household
                    if (couple.size() > 0) {
                        Collections.sort(couple, new Comparator<Household>() {
                            @Override
                            public int compare(Household o1, Household o2) {
                                return (o2.getAdults().get(0).getAge() + o2.getAdults().get(1).getAge())
                                        - (o1.getAdults().get(0).getAge() + o1.getAdults().get(1).getAge());
                            }
                        });
                        indexMostAged = couple.get(0).getId();
                        // le plus vieux est le premier
                    } else {
                        if (singleParent.size() > 0) {
                            indexMostAged = singleParent.get(0).getId();
                        } else {
                            if (single.size() > 0) {
                                indexMostAged = single.get(0).getId();
                            }
                        }
                    }
                    if (single.isEmpty()) { // pas d'enfant à éventuellement rattacher
                        dTypeHsHVillage[householdINSEEStatus(couple, singleParent, indexMostAged)]++;
                    } else { // enfant à éventuellement rattacher
                        boolean[] done = new boolean[single.size()];
                        Arrays.fill(done, false);
                        int j = 0;
                        for (Household sing : single) {
                            if (!done[j]) {
                                boolean par = false;
                                for (Household hh : res) {
                                    if (!par) {
                                        if (hh != sing) {
                                            for (Individual ind : hh.getAdults()) {
                                                if (!par) {
                                                    if (sing.getAdults().get(0).getParent1() == ind || sing.getAdults().get(0).getParent2() == ind) {
                                                        par = true;
                                                        if (hh.getId() == indexMostAged) {
                                                            childAttachedToTheMostAged = true;
                                                        }
                                                        dTypeFamilyVillage[0]--; // One single less
                                                        done[j] = true;
                                                        nbFamily--;
                                                        // hh changes of status accordingly to the fact the single sing is attached to the family hh
                                                        switch (hh.getHshTypeCode()) {
                                                            case 0: // becomes 1
                                                                dTypeFamilyVillage[0]--;
                                                                dTypeFamilyVillage[1]++;
                                                                for (int l = 0; l < single.size(); l++) {
                                                                    if (single.get(l) == hh) {
                                                                        done[l] = true;
                                                                    }
                                                                }
                                                                if (childAttachedToTheMostAged && !linked) {
                                                                    dTypeHsHVillage[1]++;
                                                                    linked = true;
                                                                }
                                                                break;
                                                            case 1:
                                                                if (childAttachedToTheMostAged && !linked) {
                                                                    dTypeHsHVillage[1]++;
                                                                    linked = true;
                                                                }
                                                                break;
                                                            case 2: // becomes 3
                                                                dTypeFamilyVillage[2]--;
                                                                dTypeFamilyVillage[3]++;
                                                                if (childAttachedToTheMostAged && !linked) {
                                                                    dTypeHsHVillage[3]++;
                                                                    linked = true;
                                                                }
                                                                break;
                                                            case 3:
                                                                if (childAttachedToTheMostAged && !linked) {
                                                                    dTypeHsHVillage[3]++;
                                                                    linked = true;
                                                                }
                                                                break;
                                                            case 4:
                                                                if (childAttachedToTheMostAged && !linked) {
                                                                    dTypeHsHVillage[4]++;
                                                                    linked = true;
                                                                }
                                                                break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            j++;
                        }
                        if (!childAttachedToTheMostAged) {
                            dTypeHsHVillage[householdINSEEStatus(couple, singleParent, indexMostAged)]++;
                        }
                    }
                } else {
                    for (Household hh : res) {
                        dTypeHsHVillage[hh.getHshTypeCode()]++;
                    }
                }
                nbFamilyByRes[i] = nbFamilyByRes[i] + nbFamily;
            }
            // average nb of family by size of lodging
            if (nbFamilyByRes[i] == 0 || mun.getOccupiedResidencesCount(i) == 0) {
                nbFamilyByRes[i] = 0.0;
            } else {
                nbFamilyByRes[i] = nbFamilyByRes[i] / (double) mun.getOccupiedResidencesCount(i);
            }
        }
        //</editor-fold>
        int[] data = new int[size * 3];
        System.arraycopy(dTypeHshModel, 0, data, 0, size);
        System.arraycopy(dTypeFamilyVillage, 0, data, size, size);
        System.arraycopy(dTypeHsHVillage, 0, data, 2 * size, size);
        return data;
    }

    /**
     * Method allowing to determine the INSEE household status when no single
     * family is attached to another family of the residence
     */
    private int householdINSEEStatus(List<Household> couple, List<Household> singleParent, long indexMostAgedCouple) {
        int stat = 0;
        if (couple.size() > 0) {
            if (couple.size() == 1) {
                stat = couple.get(0).getHshTypeCode();
            } else {
                for (Household hh : couple) {
                    if (hh.getId() == indexMostAgedCouple) {
                        stat = hh.getHshTypeCode();
                    }
                }
            }
        } else {
            if (singleParent.size() > 0) {
                stat = 1;
            } else {
                stat = 4;
            }
        }
        return stat;
    }

    @Observable(description = "Distribution of the professions of unemployed and workers", size = {6})
    public int[] getDistribProfessionsUnemployedAndWorkers() {
        int[] data = new int[municipalitySet.getNbProfessions()];
        Arrays.fill(data, 0);
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if ((ind.getStatus() == Individual.Status.UNEMPLOYED) || (ind.getStatus() == Individual.Status.WORKER)) {
                            data[ind.getProfession()]++;
                        }
                    }
                }
            }
        }
        return data;
    }

    @Observable(description = "Distribution of the professions of unemployed", size = {6})
    public int[] getDistribProfessionsUnemployed() {
        int[] data = new int[municipalitySet.getNbProfessions()];
        Arrays.fill(data, 0);
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                            data[ind.getProfession()]++;
                        }
                    }
                }
            }
        }
        return data;
    }

    @Observable(description = "Distribution of the professions of workers", size = {6})
    public int[] getDistribProfessionsWorkers() {
        int[] data = new int[municipalitySet.getNbProfessions()];
        Arrays.fill(data, 0);
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.WORKER) {
                            data[ind.getProfession()]++;
                        }
                    }
                }
            }
        }
        return data;
    }

    @Observable(description = "Distribution of the professions of workers who work outside", size = {6})
    public int[] getDistribProfessionsOutsideWorkers() {
        int[] data = new int[municipalitySet.getNbProfessions()];
        Arrays.fill(data, 0);
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.WORKER && ind.getJobLocation() == municipalitySet.getOutsides().get(0)) {
                            data[ind.getProfession()]++;
                        }
                    }
                }
            }
        }
        return data;
    }

    @Observable(description = "Distribution of the job offers by activity sectors", size = {4})
    public int[] getDistribActivitySectorJobOffers() {
        int temp = 0;
        int[] data = new int[municipalitySet.getNbActivSectors()];
        Arrays.fill(data, 0);
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipalitySet.getNbActivSectors(); i++) {
                temp = 0;
                for (int j = 0; j < municipalitySet.getNbProfessions(); j++) {
                    temp = municipality.getEndogeneousOfferedJob()[j + (municipalitySet.getNbProfessions() * i)] + municipality.getExogeneousOfferedJob()[j + (municipalitySet.getNbProfessions() * i)];
                    data[i] = data[i] + temp;
                }
            }
        }
        return data;
    }

    @Observable(description = "Distribution of the job occupations by activity sectors", size = {4})
    public int[] getDistribActivitySectorJobOccupations() {
        int[] data = new int[municipalitySet.getNbActivSectors()];
        Arrays.fill(data, 0);
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.WORKER) {
                            data[ind.getSectorOfActivity()]++;
                        }
                    }
                }
            }
        }
        return data;
    }

    public String[] getDistribPlaceOfWorkLabels(Integer dimension) {
        String[] labels = new String[17 + municipalitySet.getOutsides().size() + 1];
        labels[0] = "workAt[0,3]";
        for (int i = 1; i < 17; i++) {
            labels[i] = "workAt[" + (3 * i + 1) + "," + (3 * (i + 1)) + "]";
        }
        for (int i = 0; i < municipalitySet.getOutsides().size(); i++) {
            labels[17 + i] = municipalitySet.getOutsides().get(i).getName();

        }
        labels[labels.length - 1] = "meanInside";
        return labels;
    }

    @Observable(description = "Distance Distribution of the places of work", labelsMethod = "getDistribPlaceOfWorkLabels", size = {0})
    public double[] getDistribPlaceOfWork() {
        double[] data = new double[19];
        Arrays.fill(data, 0);
        double sum = 0;
        int count = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            if (mun.getMyWorkers() != null) {
                for (List<Individual> workers : mun.getMyWorkers()) {
                    if (workers != null) {
                        for (Individual ind : workers) {
                            double distFloat = mun.euclidianDistanceFrom(ind.getMyHousehold().getMyVillage()) / 1000.00f;
                            // local commuters are excludes from the average distance computation
                            if (ind.getMyHousehold().getMyVillage() != mun) {
                                sum += distFloat;
                                count++;
                            }
                            int d = (int) (distFloat);
                            if (d <= 51) {
                                if (d == 0) {
                                    data[0]++;
                                } else {
                                    data[(d - 1) / 3]++;
                                }
                            } else {
                                data[16]++;
                            }
                        }
                    }
                }
            }
        }
        for (Municipality mun : municipalitySet.getOutsides()) {
            if (mun.getMyWorkers() != null) {
                for (List<Individual> workers : mun.getMyWorkers()) {
                    if (workers != null) {
                        data[17] += workers.size();
                    }
                }
            }
        }
        data[18] = sum / count;
        return data;
    }

    public String[] getDistribResMobilityLabels(Integer dimension) {
        List<String> labels = new ArrayList<String>(municipalitySet.getMyMunicipalities().size() + municipalitySet.getOutsides().size());
        labels.add("moveAt[0,3]");
        for (int i = 1; i < 17; i++) {
            labels.add("moveAt[" + (3 * i + 1) + "," + (3 * (i + 1)) + "]");
        }
        return labels.toArray(new String[]{});
    }

    public String[] getDistribResMobilityHshLabels(Integer dimension) {
        List<String> labels = new ArrayList<String>(municipalitySet.getMyMunicipalities().size() + municipalitySet.getOutsides().size());
        labels.add("moveHshAt[0,3]");
        for (int i = 1; i < 17; i++) {
            labels.add("moveHshAt[" + (3 * i + 1) + "," + (3 * (i + 1)) + "]");
        }
        return labels.toArray(new String[]{});
    }

    public String[] getDistribHshMoversBySizeLabels(Integer dimension) {
        List<String> labels = new ArrayList<String>(municipalitySet.getMyMunicipalities().size() + municipalitySet.getOutsides().size());
        for (int i = 0; i < 4; i++) {
            int t = i + 1;
            labels.add("moveHshBySize" + t);
        }
        return labels.toArray(new String[]{});
    }

    @Observable(description = "Distance Distribution of the residential mobilities", labelsMethod = "getDistribResMobilityLabels", size = {0})
    public int[] getDistDistribResidMobility() {
        int nbRange = 17;
        int[] data = new int[nbRange];
        Arrays.fill(data, 0);
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < nbRange; i++) {
                data[i] = data[i] + mun.getCounters().getDistDistribResMobility()[i];
            }
        }
        return data;
    }

    @Observable(description = "Distance Distribution of the residential mobilities of household", labelsMethod = "getDistribResMobilityHshLabels", size = {0})
    public int[] getDistDistribHshResidMobility() {
        int nbRange = 17;
        int[] data = new int[nbRange];
        Arrays.fill(data, 0);
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < nbRange; i++) {
                data[i] = data[i] + mun.getCounters().getDistDistribResMobilityHsh()[i];
            }
        }
        return data;
    }

    @Observable(description = "Distance Distribution of the moving household by size", labelsMethod = "getDistribHshMoversBySizeLabels", size = {0})
    public int[] getHshMoversSize() {
        int nbRange = 4;
        int[] data = new int[nbRange];
        Arrays.fill(data, 0);
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < nbRange; i++) {
                data[i] = data[i] + mun.getCounters().getDisthshMoversBySize()[i];
            }
        }
        return data;
    }

    public String[] getOutsideJobOccupationLabels(Integer dimension) {
        List<String> names = new ArrayList<String>(municipalitySet.getOutsides().size());
        for (Municipality outside : municipalitySet.getOutsides()) {
            names.add("WorkersIn" + outside.getName());
        }
        return names.toArray(new String[]{});
    }

    @Observable(description = "Job occupation in outsides", labelsMethod = "getOutsideJobOccupationLabels", size = {0})
    public int[] getOutsideJobOccupation() {
        int[] data = new int[municipalitySet.getOutsides().size()];
        Arrays.fill(data, 0);
        int i = 0;
        for (Municipality outside : municipalitySet.getOutsides()) {
            if (outside.getMyWorkers() != null) {
                for (List<Individual> workers : outside.getMyWorkers()) {
                    if (workers != null) {
                        data[i] += workers.size();
                    }
                }
            }
            i++;
        }
        return data;
    }

    @Observable(description = "Nb of households moving inside (same village or not)")
    public int getNbHouseholdMoveInside() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbHouseholdMoveInside();
            tot = tot + mun.getCounters().getNbHouseholdMoveInsideSameMun();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals moving inside (same village or not)")
    public int getNbIndividualMoveInside() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbIndividualMoveInsideOtherMun();
            tot = tot + mun.getCounters().getNbIndividualMoveInsideSameMun();
        }
        return tot;
    }

    @Observable(description = "Nb of households moving inside from a village to another one")
    public int getNbHouseholdMoveInsideOtherMun() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbHouseholdMoveInside();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals moving inside from a village to another one")
    public int getNbIndividualMoveInsideOtherMun() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbIndividualMoveInsideOtherMun();
        }
        return tot;
    }

    @Observable(description = "Nb of households moving inside a village")
    public int getNbHouseholdMoveInsideSameMun() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbHouseholdMoveInsideSameMun();
        }
        return tot;
    }

    @Observable(description = "Nb of individuals moving inside a village")
    public int getNbIndividualMoveInsideSameMun() {
        int tot = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            tot = tot + mun.getCounters().getNbIndividualMoveInsideSameMun();
        }
        return tot;
    }

    public int[] getPopulationSizeDetail() {
        int[] size = new int[5];
        // in the 4th position, we have the number of individual in age of procreation
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getAge() <= 25) {
                            size[0]++;
                        } else {
                            if (ind.getAge() <= 45) {
                                size[1]++;
                            } else {
                                if (ind.getAge() <= 65) {
                                    size[2]++;
                                } else {
                                    size[3]++;
                                }
                            }
                        }
                        if (ind.getAge() >= municipalitySet.getParameters().getAgeMinHavingChild()) {
                            if (ind.getAge() <= municipalitySet.getParameters().getAgeMaxHavingChild()) {
                                size[4]++;
                            }
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Young population (age <= 25)")
    public int getYoungPopulationSize() {
        return getPopulationSizeDetail()[0];
    }

    @Observable(description = "Young population (age <= 45)")
    public int getMid1PopulationSize() {
        return getPopulationSizeDetail()[1];
    }

    @Observable(description = "Young population (age <= 65)")
    public int getMid2PopulationSize() {
        return getPopulationSizeDetail()[2];
    }

    @Observable(description = "Young population (age > 65)")
    public int getOldPopulationSize() {
        return getPopulationSizeDetail()[3];
    }

    @Observable(description = "Annual nb birth by 1/2 individual in procreation age * 100")
    public int getAnnualNatalityLevel() {
        return (int) ((float) nbBirth / (float) (getPopulationSizeDetail()[4] / 2.0f) * 100);
    }

    @Observable(description = "Actives")
    public int getActivesCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                            size++;
                        }
                        if (ind.getStatus() == Individual.Status.WORKER) {
                            size++;
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Occupied Actives")
    public int getActivesOccupiedCount() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.WORKER) {
                            size++;
                        }
                    }
                }
            }
        }
        return size;
    }

    @Observable(description = "Unemployment rate on total actives")
    public int getUnemployementRate() {
        int size = 0;
        int size2 = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                if (!household.getResidence().isTransit()) {
                    for (Individual ind : household) {
                        if (ind.getStatus() == Individual.Status.UNEMPLOYED) {
                            size++;
                            size2++;
                        }
                        if (ind.getStatus() == Individual.Status.WORKER) {
                            size++;
                        }
                    }
                }
            }
        }
        size = (int) (((float) size2 / ((float) (size + size2))) * 100);
        return size;
    }

    @Observable(description = "Average size of households")
    public int getAverageSizeHsh() {
        int size = 0;
        int size2 = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size2 += municipality.getMyHouseholds().size();
            for (Household household : municipality.getMyHouseholds()) {
                size += household.size();
            }
        }
        return (int) (((float) size / (float) size2) * 100);
    }

    @Observable(description = "% of active individuals profession = farmer")
    public int cspFarmer() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Individual.Status.RETIRED && ind.getStatus() != Individual.Status.STUDENT && ind.getStatus() != Individual.Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 0) {
                            size++;
                        }
                    }
                }
            }
        }
        //System.err.println("nombre de fermiers "+size) ;
        return (int) ((float) size / (float) tot * 100);
    }

    @Observable(description = "% of active individuals profession = craftmen")
    public int cspCraftmen() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Individual.Status.RETIRED && ind.getStatus() != Individual.Status.STUDENT && ind.getStatus() != Individual.Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 1) {
                            size++;
                        }
                    }
                }
            }
        }
        return (int) ((float) size / (float) tot * 100);
    }

    @Observable(description = "% of active individuals profession = executives")
    public int cspExecutives() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Individual.Status.RETIRED && ind.getStatus() != Individual.Status.STUDENT && ind.getStatus() != Individual.Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 2) {
                            size++;
                        }
                    }
                }
            }
        }
        return (int) ((float) size / (float) tot * 100);
    }

    @Observable(description = "% of active individuals profession = interm prof")
    public int cspIntermProf() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Individual.Status.RETIRED && ind.getStatus() != Individual.Status.STUDENT && ind.getStatus() != Individual.Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 3) {
                            size++;
                        }
                    }
                }
            }
        }
        return (int) ((float) size / (float) tot * 100);
    }

    @Observable(description = "% of active individuals profession = employee")
    public int cspEmployee() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Individual.Status.RETIRED && ind.getStatus() != Individual.Status.STUDENT && ind.getStatus() != Individual.Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 4) {
                            size++;
                        }
                    }
                }
            }
        }
        return (int) ((float) size / (float) tot * 100);
    }

    @Observable(description = "% of active individuals profession = worker")
    public int cspWorker() {
        int size = 0;
        int tot = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household household : municipality.getMyHouseholds()) {
                for (Individual ind : household.getAdults()) {
                    if (ind.getStatus() != Individual.Status.RETIRED && ind.getStatus() != Individual.Status.STUDENT && ind.getStatus() != Individual.Status.INACTIVE) {
                        tot++;
                        if (ind.getProfession() == 5) {
                            size++;
                        }
                    }
                }
            }
        }
        return (int) ((float) size / (float) tot * 100);
    }

    @Observable(description = "Available residences size 1")
    public int getAvailableResidencesSize1() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < 1; i++) {//getNbSizesResidence()
                size = size + municipality.getFreeResidencesCount(i);
            }
        }
        return size;
    }

    @Observable(description = "Available residences size 2")
    public int getAvailableResidencesSize2() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 1; i < 2; i++) {//getNbSizesResidence()
                size = size + municipality.getFreeResidencesCount(i);
            }
        }
        return size;
    }

    @Observable(description = "Available residences size 3")
    public int getAvailableResidencesSize3() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 2; i < 3; i++) {//getNbSizesResidence()
                size = size + municipality.getFreeResidencesCount(i);
            }
        }
        return size;
    }

    @Observable(description = "Available residences size 4")
    public int getAvailableResidencesSize4() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 3; i < 4; i++) {//getNbSizesResidence()
                size = size + municipality.getFreeResidencesCount(i);
            }
        }
        return size;
    }

    @Observable(description = "Available residences")
    public int getAvailableResidences() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipalitySet.getNbSizesResidence(); i++) {
                size = size + municipality.getFreeResidencesCount(i);
            }
        }
        return size;
    }

    @Observable(description = "Hsh size 1 in needForRes")
    public int hsh1NeedForRes() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household hsh : municipality.getMyHouseholds()) {
                if (hsh.needOfResidence() && hsh.getSize() == 1) {
                    size++;
                }
            }
        }
        return size;
    }

    @Observable(description = "Hsh size 2 in needForRes")
    public int hsh2NeedForRes() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household hsh : municipality.getMyHouseholds()) {
                if (hsh.needOfResidence() && hsh.getSize() == 2) {
                    size++;
                }
            }
        }
        return size;
    }

    @Observable(description = "Hsh size 3 in needForRes")
    public int hsh3NeedForRes() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household hsh : municipality.getMyHouseholds()) {
                if (hsh.needOfResidence() && hsh.getSize() == 3) {
                    size++;
                }
            }
        }
        return size;
    }

    @Observable(description = "Hsh size > 3 in needForRes")
    public int hsh4NeedForRes() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household hsh : municipality.getMyHouseholds()) {
                if (hsh.needOfResidence() && hsh.getSize() > 3) {
                    size++;
                }
            }
        }
        return size;
    }

    @Observable(description = "Occupied residences")
    public int getOccupiedResidences() {
        float size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (Household hsh : municipality.getMyHouseholds()) {
                if (!hsh.getResidence().isTransit()) {
                    if (hsh.getResidence().getNbHhResidents() > 0) {
                        size = size + (1.0f / (float) (hsh.getResidence().getNbHhResidents()));
                    }
                }
            }
        }
        return (int) Math.round(size);
    }

    @Observable(description = "Residence offer")
    public int getResidenceOffer() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipalitySet.getNbSizesResidence(); i++) {
                size = size + municipality.getResidenceOffer(i);
            }
        }
        return size;
    }

    @Observable(description = "Residence offer size 1")
    public int getResidenceOfferS1() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size = size + municipality.getResidenceOffer(0);
        }
        return size;
    }

    @Observable(description = "Residence offer size 2")
    public int getResidenceOfferS2() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size = size + municipality.getResidenceOffer(1);
        }
        return size;
    }

    @Observable(description = "Residence offer size 3")
    public int getResidenceOfferS3() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size = size + municipality.getResidenceOffer(2);
        }
        return size;
    }

    @Observable(description = "Residence offer size 4")
    public int getResidenceOfferS4() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size = size + municipality.getResidenceOffer(3);
        }
        return size;
    }

    @Observable(description = "Free Residence offer")
    public int getFreeResidenceOffer() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            for (int i = 0; i < municipalitySet.getNbSizesResidence(); i++) {
                size = size + municipality.getFreeResidencesCount(i);
            }
        }
        return size;
    }

    @Observable(description = "Free Residence offer size 1")
    public int getFreeResidenceOfferS1() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size = size + municipality.getFreeResidencesCount(0);
        }
        return size;
    }

    @Observable(description = "Free Residence offer size 2")
    public int getFreeResidenceOfferS2() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size = size + municipality.getFreeResidencesCount(1);
        }
        return size;
    }

    @Observable(description = "Free Residence offer size 3")
    public int getFreeResidenceOfferS3() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size = size + municipality.getFreeResidencesCount(2);
        }
        return size;
    }

    @Observable(description = "Free Residence offer size 4")
    public int getFreeResidenceOfferS4() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size = size + municipality.getFreeResidencesCount(3);
        }
        return size;
    }

    @Observable(description = "Nb households with immigrant one")
    public int getHouseholdNb() {
        int size = 0;
        for (Municipality municipality : municipalitySet.getMyMunicipalities()) {
            size = size + municipality.getMyHouseholds().size();
        }
        return size;
    }
    private Map<String, Map<String, Integer>> realCommutingMatrix = null;
    private Map<String, Integer> realCommutingMatrix_NCPerMun = null;
    private int realCommutingMatrix_NC = 0;

    public String[] getCPCLabels(Integer dimension) {
        String[] labels = new String[2 + municipalitySet.getMyMunicipalities().size()];
        labels[0] = "CPC";
        labels[1] = "NC model";
        for (int i = 0; i < municipalitySet.getMyMunicipalities().size(); i++) {
            labels[i + 2] = "CPC for " + municipalitySet.getMyMunicipalities().get(i).getName();
        }
        return labels;
    }

    public String[] getCPCLabelsRegion(Integer dimension) {
        String[] labels = new String[1];
        labels[0] = "CPC";
        return labels;
    }

    @Observable(description = "Common part of commuters of the region", labelsMethod = "getCPCLabelsRegion", size = {0})
    public double[] getCPCRegion() throws BadDataException, IOException {
        double[] data = new double[1];
        if (realCommutingMatrix == null) {
            // read the real matrix
            realCommutingMatrix = new HashMap<String, Map<String, Integer>>();
            realCommutingMatrix_NCPerMun = new HashMap<String, Integer>();
            Integer[] line;
            CSVReader reader = CSV.getReader(municipalitySet.getMyApplication().getFileFromRelativePath("CommutingCantal.csv"), ';');
            while ((line = CSV.nextIntegersLine(reader)) != null) {
                String mun1Name = line[0].toString();
                Map<String, Integer> array = realCommutingMatrix.get(mun1Name);
                if (array == null) {
                    array = new HashMap<String, Integer>();
                    realCommutingMatrix.put(mun1Name, array);
                }
                array.put(line[1].toString(), line[2]);
                Integer ncPerMun = realCommutingMatrix_NCPerMun.get(mun1Name);
                if (ncPerMun == null) {
                    ncPerMun = 0;
                }
                ncPerMun += line[2];
                realCommutingMatrix_NCPerMun.put(mun1Name, ncPerMun);
                realCommutingMatrix_NC += line[2];
            }
            reader.close();
        }
        int ncc = 0, ncOfModel = 0, nccMun1, ncMun1;
        // count the commuters and compute indicators
        for (int i = 0; i < municipalitySet.getMyMunicipalities().size(); i++) {
            nccMun1 = 0;
            ncMun1 = 0;
            Municipality mun1 = municipalitySet.getMyMunicipalities().get(i);
            for (Municipality mun2 : municipalitySet.getMyMunicipalities()) {
                if (mun1 != mun2) {
                    int ncOfModelInMun2 = 0;
                    for (List<Individual> workers : mun2.getMyWorkers()) {
                        if (workers != null) {
                            for (Individual individual : workers) {
                                if (individual.getMyHousehold().getMyVillage() == mun1) {
                                    ncOfModelInMun2++;
                                }
                            }
                        }
                    }
                    Integer realCommutingInMun2 = 0;
                    Map<String, Integer> dataForMun1 = realCommutingMatrix.get(mun1.getName());
                    if (dataForMun1 != null) {
                        Integer val = dataForMun1.get(mun2.getName());
                        if (val != null) {
                            realCommutingInMun2 = val;
                        }
                    }
                    int min = Math.min(ncOfModelInMun2, realCommutingInMun2);
                    nccMun1 += min;
                    ncMun1 += ncOfModelInMun2;
                }
            }
            ncc += nccMun1;
            ncOfModel += ncMun1;
        }
        data[0] = 2 * (double) ncc / (realCommutingMatrix_NC + ncOfModel);
        return data;
    }

    @Observable(description = "Common part of commuters", labelsMethod = "getCPCLabels", size = {0})
    public double[] getCPC() throws BadDataException, IOException {
        double[] data = new double[2 + municipalitySet.getMyMunicipalities().size()];
        if (realCommutingMatrix == null) {
            // read the real matrix
            realCommutingMatrix = new HashMap<String, Map<String, Integer>>();
            realCommutingMatrix_NCPerMun = new HashMap<String, Integer>();
            Integer[] line;
            CSVReader reader = CSV.getReader(municipalitySet.getMyApplication().getFileFromRelativePath("CommutingCantal.csv"), ';');
            while ((line = CSV.nextIntegersLine(reader)) != null) {
                String mun1Name = line[0].toString();
                Map<String, Integer> array = realCommutingMatrix.get(mun1Name);
                if (array == null) {
                    array = new HashMap<String, Integer>();
                    realCommutingMatrix.put(mun1Name, array);
                }
                array.put(line[1].toString(), line[2]);
                Integer ncPerMun = realCommutingMatrix_NCPerMun.get(mun1Name);
                if (ncPerMun == null) {
                    ncPerMun = 0;
                }
                ncPerMun += line[2];
                realCommutingMatrix_NCPerMun.put(mun1Name, ncPerMun);
                realCommutingMatrix_NC += line[2];
            }
            reader.close();
        }
        int ncc = 0, ncOfModel = 0, nccMun1, ncMun1;
        // count the commuters and compute indicators
        for (int i = 0; i < municipalitySet.getMyMunicipalities().size(); i++) {
            nccMun1 = 0;
            ncMun1 = 0;
            Municipality mun1 = municipalitySet.getMyMunicipalities().get(i);
            for (Municipality mun2 : municipalitySet.getMyMunicipalities()) {
                if (mun1 != mun2) {
                    int ncOfModelInMun2 = 0;
                    for (List<Individual> workers : mun2.getMyWorkers()) {
                        if (workers != null) {
                            for (Individual individual : workers) {
                                if (individual.getMyHousehold().getMyVillage() == mun1) {
                                    ncOfModelInMun2++;
                                }
                            }
                        }
                    }
                    Integer realCommutingInMun2 = 0;
                    Map<String, Integer> dataForMun1 = realCommutingMatrix.get(mun1.getName());
                    if (dataForMun1 != null) {
                        Integer val = dataForMun1.get(mun2.getName());
                        if (val != null) {
                            realCommutingInMun2 = val;
                        }
                    }
                    int min = Math.min(ncOfModelInMun2, realCommutingInMun2);
                    nccMun1 += min;
                    ncMun1 += ncOfModelInMun2;
                }
            }
            ncc += nccMun1;
            ncOfModel += ncMun1;
            Integer realCommutingMun1 = realCommutingMatrix_NCPerMun.get(mun1.getName());
            data[2 + i] = 2 * (double) nccMun1 / (realCommutingMun1 == null ? 0 : realCommutingMun1 + ncMun1);
        }
        data[0] = 2 * (double) ncc / (realCommutingMatrix_NC + ncOfModel);
        data[1] = ncOfModel;
        return data;
    }
}
