/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.auvergne;

import fr.cemagref.prima.regionalmodel.Household;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.scenarios.Event;
import fr.cemagref.prima.regionalmodel.scenarios.GlobalImmigrationEvent;
import fr.cemagref.prima.regionalmodel.scenarios.ImmigrationUpdater;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;
import org.openide.util.lookup.ServiceProvider;

/**
 * Specific implementation for the France.
 *
 * Exemple of XML configuration:
 * <pre>
 *     &lt;fr.cemagref.prima.regionalmodel.auvergne.FranceImmigrationUpdater>
 *        &lt;globalInputFilename>immigrants.txt&lt;/globalInputFilename>
 *        &lt;clearPreviousMigrants>true&lt;/clearPreviousMigrants>
 *        &lt;firstEmigrantsAmount>3000&lt;/firstEmigrantsAmount>
 *        &lt;municipalitiesFilter class="ExcludeOutsideMunicipalities"/>
 *        &lt;param1>1.066362&lt;/param1>
 *        &lt;param2>0.00003203865&lt;/param2>
 *        &lt;youngPeopleAgeThreshold>25&lt;/youngPeopleAgeThreshold>
 *        &lt;rateBigTown>0.45&lt;/rateBigTown>
 *        &lt;bigTownID>15014&lt;/bigTownID>
 *        &lt;cspProbasByYear class="tree-map">
 *            &lt;no-comparator/>
 *            &lt;entry>
 *                &lt;int>1999&lt;/int>
 *                &lt;double-array>
 *                    &lt;double>0.041&lt;/double>
 *                    &lt;double>0.086&lt;/double>
 *                    &lt;double>0.052&lt;/double>
 *                    &lt;double>0.178&lt;/double>
 *                    &lt;double>0.331&lt;/double>
 *                    &lt;double>0.312&lt;/double>
 *                &lt;/double-array>
 *            &lt;/entry>
 *            &lt;entry>
 *                &lt;int>2030&lt;/int>
 *                &lt;double-array>
 *                    &lt;double>0.064&lt;/double>
 *                    &lt;double>0.106&lt;/double>
 *                    &lt;double>0.021&lt;/double>
 *                    &lt;double>0.149&lt;/double>
 *                    &lt;double>0.511&lt;/double>
 *                    &lt;double>0.149&lt;/double>
 *                &lt;/double-array>
 *            &lt;/entry>
 *        &lt;/cspProbasByYear>
 *    &lt;/fr.cemagref.prima.regionalmodel.auvergne.FranceImmigrationUpdater>
 * </pre>
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
@ServiceProvider(service = ImmigrationUpdater.class)
public class FranceImmigrationUpdater extends ImmigrationUpdater {

    private SortedMap<Integer, double[]> cspProbasByYear;
    private double param1, param2;
    private int youngPeopleAgeThreshold;
    private double rateBigTown;
    private String bigTownID;

    @Override
    public void init(MunicipalitySet region) throws BadDataException {
        super.init(region);
    }

    @Override
    protected Event<List<Municipality>> buildGlobalEvent(String[] line) {
        return new FranceImmigrationEvent(this, line);
    }

    private class FranceImmigrationEvent extends GlobalImmigrationEvent {

        private List<Double> probaSizeHH;
        private List<Integer> ageClassesSupBound;
        private List<Double> probaAgeClass;

        public FranceImmigrationEvent(ImmigrationUpdater immigrationUpdater, String[] line) {
            super(immigrationUpdater, Integer.parseInt(line[0]));
            int shift = 1;
            probaSizeHH = new ArrayList<Double>();
            for (String value : Arrays.copyOfRange(line, shift + 1, shift + 1 + Integer.parseInt(line[shift]))) {
                probaSizeHH.add(Double.parseDouble(value));
            }
            ageClassesSupBound = new ArrayList<Integer>();
            probaAgeClass = new ArrayList<Double>();
            shift += 1 + probaSizeHH.size();
            int nbAgeClasses = Integer.parseInt(line[shift]);
            // The last bound is
            for (String value : Arrays.copyOfRange(line, 1 + shift, 1 + shift + nbAgeClasses - 1)) {
                ageClassesSupBound.add(Integer.parseInt(value));
            }
            shift += ageClassesSupBound.size();
            ageClassesSupBound.add(Integer.MAX_VALUE);
            for (String value : Arrays.copyOfRange(line, 1 + shift, 1 + shift + nbAgeClasses)) {
                probaAgeClass.add(Double.parseDouble(value));
            }
        }

        @Override
        public void process(List<Municipality> municipalities) throws ProcessingException {
            clearPreviousImmigrantsIfNeeded(municipalities);
            int nbMigrants = migratoryBalance;
            MunicipalitySet region = municipalities.get(0).getMyRegion();
            // for the first step, the nbImmigrants is already precomputed with a given nbOutMigrants
            if (region.getCurrentYear() > region.getStartStep()) {
                nbMigrants += region.getOutMigrantsLastStep();
            }
            if (nbMigrants > 0) {
                // preparing data for processing first households with more than one individual and then single hh
                List<Household> singleHouseholds = new LinkedList<Household>();
                List<Household> otherHouseholds = new LinkedList<Household>();
                List<Household> hseholds = listPossibleMigrants(region, nbMigrants, true);
                int[] toCorrect = checkStatisticsOnPotentialImmigrants(region, hseholds, nbMigrants);
                while (toCorrect[0] != -1) {
                    hseholds = correctAgeDistrib(toCorrect, hseholds, region);
                    toCorrect = checkStatisticsOnPotentialImmigrants(region, hseholds, nbMigrants);
                }
                for (Household household : hseholds) {
                    if (household.size() == 1) {
                        singleHouseholds.add(household);
                    } else {
                        if (household.size() > 0) {
                            otherHouseholds.add(household);
                        }
                    }
                }
                // computing municipalities capacities
                Map<Municipality, Integer> municipalitiesCapacity = new TreeMap<Municipality, Integer>();
                Map<Municipality, Integer> remainingMunicipalitiesForOnlyOneInd = new TreeMap<Municipality, Integer>();
                // weights computation for later normalization
                double[] weights = new double[municipalities.size()];
                double weightsSum = 0;
                for (int i = 0; i < municipalities.size(); i++) {
                    Municipality mun = municipalities.get(i);
                    weights[i] = param1 * mun.getPopulationSize() / region.getCounters().getPopulationSize() - param2 * mun.getDistanceToBorder();
                    weightsSum += weights[i];
                }
                for (int i = 0; i < municipalities.size(); i++) {
                    Municipality mun = municipalities.get(i);
                    // weight application with normalization
                    int capacity = (int) Math.round(weights[i] * nbMigrants / weightsSum);
                    if (capacity > 0) {
                        municipalitiesCapacity.put(mun, capacity);
                    }
                    LOGGER.log(Level.FINEST, "Computation of capacity for {0}:\n   ({1} * {2} / {3} - {4}* {5}) * {6}\n   = {7}",
                            new Object[]{mun.getName(), param1, mun.getPopulationSize(), region.getCounters().getPopulationSize(), param2, mun.getDistanceToBorder(), nbMigrants, municipalitiesCapacity.get(mun)});
                }
                // processing households with more than one individual
                while (!otherHouseholds.isEmpty()) {
                    Household hh = region.getRandom().nextObject(otherHouseholds, true);
                    Municipality mun = null;
                    int limit = municipalitiesCapacity.size();
                    if (!municipalitiesCapacity.isEmpty()) {
                        do {
                            mun = region.getRandom().nextObject(new ArrayList<Municipality>(municipalitiesCapacity.keySet()));
                            limit--;
                        } while (municipalitiesCapacity.get(mun) < hh.size() && limit > 0);
                        if (limit == 0) {
                            // if limit is reached, we take at random a municipality without to check the capacity
                            mun = region.getRandom().nextObject(new ArrayList<Municipality>(municipalitiesCapacity.keySet()));
                        }
                    } else {
                        LOGGER.log(Level.WARNING, "All municipalities have exhausted their immigrants quota, so {0} immigrants households cannot be placed", hh.size());
                        break;
                    }
                    if (mun != null) {
                        LOGGER.log(Level.FINER, "One regular HH inserted, {0} remaining", otherHouseholds.size());
                        mun.addMigrant(hh);
                        postprocessImmigrants(hh);
                        int newCapacity = municipalitiesCapacity.remove(mun) - hh.size();
                        if (newCapacity > 1) {
                            municipalitiesCapacity.put(mun, newCapacity);
                        } else if (newCapacity == 1) {
                            remainingMunicipalitiesForOnlyOneInd.put(mun, 1);
                        }
                    }
                }
                for (Map.Entry<Municipality, Integer> entry : municipalitiesCapacity.entrySet()) {
                    remainingMunicipalitiesForOnlyOneInd.put(entry.getKey(), entry.getValue());
                }
                // processing single households
                while (!singleHouseholds.isEmpty()) {
                    Household hh = region.getRandom().nextObject(singleHouseholds, true);
                    Municipality mun;
                    /*
                     * if (hh.getMember(0).getAge() < youngPeopleAgeThreshold) {
                     * if (region.getRandom().nextDouble() <= rateBigTown) { if
                     * (remainingMunicipalitiesForOnlyOneInd.containsKey(bigTown))
                     * { mun = bigTown; } } else { int limit =
                     * remainingMunicipalitiesForOnlyOneInd.size(); do { mun =
                     * region.getRandom().nextObject(new
                     * ArrayList<Municipality>(remainingMunicipalitiesForOnlyOneInd.keySet()));
                     * limit--; } while (mun == bigTown && limit > 0); if (limit
                     * == 0 || mun == bigTown) { LOGGER.log(Level.WARNING, "All
                     * municipalities (excluding big town) have exhausted their
                     * immigrants quota", hh.size()); break; //} } } else {
                     */
                    mun = region.getRandom().nextObject(new ArrayList<Municipality>(remainingMunicipalitiesForOnlyOneInd.keySet()));
                    //}
                    if (mun != null) {
                        mun.addMigrant(hh);
                        postprocessImmigrants(hh);
                        int newCapacity = remainingMunicipalitiesForOnlyOneInd.remove(mun) - 1;
                        if (newCapacity == 0) {
                            remainingMunicipalitiesForOnlyOneInd.remove(mun);
                        } else {
                            remainingMunicipalitiesForOnlyOneInd.put(mun, newCapacity);
                        }
                    }
                }
            }
        }

        /**
         * Method to decide if an household can represent an immigrant household
         * S. Huet, 3.11.2010 That is overloaded to consider that a given
         * existing household can represent an immigrant if it probably
         * correspond to an immigrant (respecting some size feature and age of
         * individual probability)
         */
        //@Override
        protected boolean evaluateOld(Household hh) {
            boolean good = super.evaluate(hh);
            if (good) {
                // Take the probability for the given size of household
                double probSize = probaSizeHH.get(Math.min(hh.getSize() - 1, probaSizeHH.size() - 1));
                // Compute the average probability for the age of the household member
                double probAge = 0.0f;
                for (Individual indiv : hh) {
                    // look for the age range of the individual
                    for (int j = 0; j < ageClassesSupBound.size(); j++) {
                        if (indiv.getAge() <= ageClassesSupBound.get(j)) {
                            probAge = probAge + probaAgeClass.get(j);
                            break;
                        }
                    }
                }
                probAge = probAge / hh.getSize();
                if (hh.getRandom().nextDouble() > (double) ((probAge + probSize) / 2)) {
                    good = false;
                }
            }
            return good;
        }

        /**
         * Method to decide if an household can represent an immigrant household
         * S. Huet, 3.11.2010 That is overloaded to consider that a given
         * existing household can represent an immigrant if it probably
         * correspond to an immigrant (respecting some size feature and age of
         * individual probability)
         */
        @Override
        protected boolean evaluate(Household hh) {
            boolean good = super.evaluate(hh);
            if (good) {
                // Take the probability for the given size of household
                double probSize = probaSizeHH.get(Math.min(hh.getSize() - 1, probaSizeHH.size() - 1));
                // Compute the average probability for the age of the household member
                /*
                 * double probAge = 0.0f; for (Individual indiv : hh) { // look
                 * for the age range of the individual for (int j = 0; j <
                 * ageClassesSupBound.size(); j++) { if (indiv.getAge() <=
                 * ageClassesSupBound.get(j)) { probAge = probAge +
                 * probaAgeClass.get(j); break; } } } probAge = probAge /
                 * hh.getSize(); //if (hh.getRandom().nextDouble() > (double)
                 * ((probAge + probSize) / 2)) { if (hh.getRandom().nextDouble()
                 * > (double) (probAge)) { good = false; }
                 *
                 */
                if (hh.getRandom().nextDouble() > (double) (probSize)) {
                    good = false;
                }
            }
            return good;
        }

        @Override
        protected void postprocessImmigrants(Household hh) {
            super.postprocessImmigrants(hh);
            int p;
            // Adapt the profession and the activity status
            int[] r = {5, 15, 20, 30, 40, 50, 65, 180};
            for (Individual ind : hh) {
                // Attribution of a profession following the distribution of profession in the "real" data
                double[] probas = Utils.getValueFromUpperBound(cspProbasByYear, ind.getMyHousehold().getMyVillage().getMyRegion().getCurrentYear());
                p = ind.getMyHousehold().getRandom().nextIndexWithDistribution(probas);
                if (ind.getStatus() != Individual.Status.RETIRED) {
                    ind.setProfession(p);
                }
                // Statistics ages
                for (int i = 0; i < r.length; i++) {
                    if (ind.getAge() <= r[i]) {
                        ind.getMyHousehold().getMyVillage().getMyRegion().immigrantAge[i]++;
                        i = r.length;
                    }
                }
                // Statistics about the status regarding the activity of immigrants
                if (ind.getStatus() == Individual.Status.RETIRED) {
                    hh.getMyVillage().getMyRegion().immigrantStatus[6]++;
                } else {
                    if (ind.getStatus() == Individual.Status.INACTIVE) {
                        hh.getMyVillage().getMyRegion().immigrantStatus[7]++;
                    } else {
                        if (ind.getStatus() != Individual.Status.STUDENT) {
                            hh.getMyVillage().getMyRegion().immigrantStatus[ind.getProfession()]++;
                        }
                    }
                }
            }
        }

        public int[] checkStatisticsOnPotentialImmigrants(MunicipalitySet region, List<Household> hshs, int nbMig) {
            int[] distribAge = new int[ageClassesSupBound.size()];
            float[] distribAgef = new float[ageClassesSupBound.size()];
            double[] distribAgeDelta = new double[ageClassesSupBound.size()];
            Arrays.fill(distribAge, 0);
            for (Household hh : hshs) {
                for (Individual indiv : hh) {
                    // look for the age range of the individual
                    for (int j = 0; j < ageClassesSupBound.size(); j++) {
                        if (indiv.getAge() <= ageClassesSupBound.get(j)) {
                            distribAge[j]++;
                            break;
                        }
                    }
                }
            }
            for (int l = 0; l < ageClassesSupBound.size(); l++) {
                distribAgef[l] = (float) distribAge[l] / (float) nbMig;
                distribAgeDelta[l] = probaAgeClass.get(l) - distribAgef[l];
            }
            // A positive error means it lacks of this category - 
            // Save ageRange where there is too much individuals at most and age range where where it lacks at most 
            int[] classToDecToInc = new int[2];
            double maxErrA = Double.MIN_VALUE;
            double minErrA = Double.MAX_VALUE;
            for (int l = 0; l < ageClassesSupBound.size(); l++) {
                if (distribAgeDelta[l] > maxErrA) {
                    maxErrA = distribAgeDelta[l];
                    classToDecToInc[1] = l;
                }
                if (distribAgeDelta[l] < minErrA) {
                    minErrA = distribAgeDelta[l];
                    classToDecToInc[0] = l;
                }
            }
            // stop the improvement when it remains less than 1 individual to possibly correct (impossible solution) (or one household for the household sizes)
            if (((maxErrA * nbMig) < 1.0)) {
                classToDecToInc[0] = -1;
                /*
                 * System.err.print("DISTRIBUTION DE TAILLE DES HOUSEHOLDS
                 * POTENTIELLES " + "\t"); for (int l = 0; l <
                 * probaSizeHH.size(); l++) { System.err.print(distribSizef[l] +
                 * "\t"); } System.err.println(); System.err.print("DISTRIBUTION
                 * D'AGE DES MIGRANTS POTENTIELS " + "\t"); for (int l = 0; l <
                 * ageClassesSupBound.size(); l++) {
                 * System.err.print(distribAgef[l] + "\t"); }
                 * System.err.println();
                 */
            }
            return classToDecToInc;
        }

        public List<Household> correctAgeDistrib(int[] ageToCorrect, List<Household> hshs, MunicipalitySet region) throws ProcessingException {
            //The ageToCorrect[1] contains the age range where we choose an new age for a selected individuals being previously of ageRange ageToCorrect[0]
            boolean attributeChildAge = false;
            // Select the age to give
            int newAge;
            if (ageToCorrect[1] > 0) {
                newAge = region.getRandom().nextInt(ageClassesSupBound.get(ageToCorrect[1] - 1), ageClassesSupBound.get(ageToCorrect[1]));
            } else {
                // means we give an age lower than 14 years'old
                newAge = region.getRandom().nextInt(0, ageClassesSupBound.get(ageToCorrect[1]));
                if (newAge < 15) {
                    attributeChildAge = true;
                }
            }
            // Select an individual who is going to change age
            boolean found = false;
            int count = 0;
            while (!found) {
                count++;
                if (count > 1000000) {
                    throw new ProcessingException("Infinite loop breaked: we failed to redistribute the immigrants for better ages distribution");
                }
                // select an household
                Household hh = region.getRandom().nextObject(hshs);
                while (hh.isJustSuppressed()) {
                    hh = region.getRandom().nextObject(hshs);
                }
                // select an individual
                Individual ind = hh.getRandom().nextObject(hh.getCopyOfMembers());
                // age of indiv is in ageToCorrect[1] ageRange ?
                if (ind.getAge() <= ageClassesSupBound.get(ageToCorrect[0])) {
                    if (ageToCorrect[0] > 0) { // the individual is higher than 14 year's old
                        if (ind.getAge() > ageClassesSupBound.get(ageToCorrect[0] - 1)) {
                            if (ind.isAdult()) {
                                if (attributeChildAge) {
                                    // it has to become a children
                                    ind.adult = false;
                                    ind.setStatus(Individual.Status.STUDENT);
                                    // change the household status
                                    if (ind.getMyHousehold().getAdults().isEmpty()) { // the individual was the only adult, the household has to be added
                                        // to another one since it is now composed only from children
                                        Household househ = region.getRandom().nextObject(hshs);
                                        while (househ == ind.getMyHousehold() || househ.isJustSuppressed()) {
                                            househ = region.getRandom().nextObject(hshs);
                                        }
                                        for (Individual indiv : ind.getMyHousehold().getCopyOfMembers()) {
                                            Individual newInd = new Individual(indiv);
                                            if (indiv == ind) {
                                                newInd.setAge(newAge);
                                            }
                                            househ.addMember(newInd);
                                            indiv.getMyHousehold().suppressMember(indiv);
                                        }
                                        househ.updateHshFeaturesAfterBirth();
                                        for (Individual in : househ.getCopyOfMembers()) {
                                            if (in.getStatus() != Individual.Status.STUDENT && in.getAge() < 15) {
                                                System.err.println("ind id " + in.getId() + " hsh " + in.getMyHousehold());
                                            }
                                        }
                                        ind.getMyHousehold().updateHshFeaturesAfterDeath(true);
                                        if (ind.getMyHousehold().isJustSuppressed()) {
                                            hshs.remove(ind.getMyHousehold());
                                        }
                                    } else {
                                        if (ind.getMyHousehold().getAdults().size() == 1) {
                                            // become a single-parent family
                                            ind.setAge(newAge);
                                            ind.adult = false;
                                            ind.setStatus(Individual.Status.STUDENT);
                                            ind.getMyHousehold().hshType = Household.Type.MONOPARENTAL;
                                        }
                                    }
                                } else {
                                    ind.setAge(newAge);
                                    if (ind.getAgeToEnterOnTheLabourMarket() > newAge) {
                                        ind.setStatus(Individual.Status.STUDENT);
                                    } else {
                                        if (ind.getAgeToGoOnRetirement() > newAge) {
                                            ind.setStatus(Individual.Status.UNEMPLOYED);
                                            ind.setMyActivityPattern(ind.getMyHousehold().getMyVillage().getMyRegion().getAllPatterns()[0]);
                                        } else {
                                            ind.setStatus(Individual.Status.RETIRED);
                                        }
                                    }
                                }
                            } else {
                                // if she's not an adult but she is more than 14
                                ind.setAge(newAge);
                                if (ind.getAgeToEnterOnTheLabourMarket() > newAge) {
                                    ind.setStatus(Individual.Status.STUDENT);
                                } else {
                                    if (ind.getAgeToGoOnRetirement() > newAge) {
                                        ind.setStatus(Individual.Status.UNEMPLOYED);
                                        ind.setMyActivityPattern(ind.getMyHousehold().getMyVillage().getMyRegion().getAllPatterns()[0]);
                                    } else {
                                        ind.setStatus(Individual.Status.RETIRED);
                                    }
                                }
                                if (attributeChildAge) {
                                    ind.setStatus(Individual.Status.STUDENT);
                                }
                            }
                            found = true;
                        }
                    } else { // the individual is currently 14 year's old at most
                        // The age to give is necessarly higher than 14 year's old
                        ind.setAge(newAge);
                        if (ind.getAgeToEnterOnTheLabourMarket() > newAge) {
                            ind.setStatus(Individual.Status.STUDENT);
                        } else {
                            if (ind.getAgeToGoOnRetirement() > newAge) {
                                ind.setStatus(Individual.Status.UNEMPLOYED);
                                ind.setMyActivityPattern(ind.getMyHousehold().getMyVillage().getMyRegion().getAllPatterns()[0]);
                            } else {
                                ind.setStatus(Individual.Status.RETIRED);
                            }
                        }
                        found = true;
                    }
                }
                if (found) {
                    if (ind.getStatus() != Individual.Status.STUDENT && ind.getAge() < 15) {
                        System.err.println("ind id " + ind.getId() + " hsh " + ind.getMyHousehold());
                    }
                }
            }
            return hshs;
        }
    }
}
