/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.cemagref.prima.regionalmodel.auvergne;

import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.dynamics.DynamicServiceEmployment;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class AuvergneDynamicServiceEmployment extends DynamicServiceEmployment {

    @Override
    public void step(MunicipalitySet region, int iter) throws ProcessingException {
        int serviceThreshold = region.getNbProfessions() * 3;
        int currentTotService = 0;
        int totOfferService = 0;
        int delta = 0;
        int toCreateOutside = 0;
        double temp = 0.0 ;
        float tot = 0.0f;
        for (Municipality mun : region.getMyMunicipalities()) {
            currentTotService = 0;
            for (int i = 0; i < region.getNbActivities(); i++) {
                if (i >= serviceThreshold) {
                    currentTotService += mun.endogeneousOfferedJob[i];
                }
            }
            totOfferService = (int)(mun.getPopulationSize() * (mun.getIntercept()+(mun.getSlope()*Math.log(mun.getPopulationSize()))));
            if (totOfferService < 0) totOfferService = 0 ;
            for (int i = serviceThreshold; i < region.getNbActivities(); i++) {
                if (mun.getOfferedActivities()[i] == 0) {
                    delta = ((int) Math.round(totOfferService * 0.001)) - mun.endogeneousOfferedJob[i];
                    mun.endogeneousOfferedJob[i] = (int) Math.round(totOfferService * 0.001);
                } else {
                    delta = Math.round(totOfferService * ((float) mun.endogeneousOfferedJob[i] / (float) currentTotService)) - mun.endogeneousOfferedJob[i];
                    mun.endogeneousOfferedJob[i] = Math.round(totOfferService * ((float) mun.endogeneousOfferedJob[i] / (float) currentTotService));
                }
                mun.setOfferedActivities(i, (mun.exogeneousOfferedJob[i] + mun.endogeneousOfferedJob[i]));
                if (delta > 0) {
                    // then employment creation to share between occupation by people from outside and potential occupation (offer) by insiders
                    tot = mun.getOccupiedActivitiesByOutside(i) + mun.getOccupiedActivitiesByRes(i) + mun.getOccupiedActivitiesByExt(i);
                    if (tot == 0 && mun.getOccupiedActivitiesByOutside(i) == 0) {
                        if (delta == 1) { // try to avoid the bias consisting in putting all the job offer occupied by outside
                            if (region.getRandom().nextDouble() > 0.5) {
                                toCreateOutside = 0;
                            } else {
                                toCreateOutside = 1;
                            }
                        } else {
                            toCreateOutside = Math.round(delta * 0.5f);
                        }
                    } else {
                        if ((tot == mun.getOccupiedActivitiesByOutside(i) * 2) && delta == 1) {
                            if (region.getRandom().nextDouble() > 0.5) {
                                toCreateOutside = 0;
                            } else {
                                toCreateOutside = 1;
                            }
                        } else {
                            toCreateOutside = Math.round(delta * ((float) mun.getOccupiedActivitiesByOutside(i) / tot));
                        }
                    }
                    mun.occupiedActivitiesByOutside[i] = mun.getOccupiedActivitiesByOutside(i) + toCreateOutside;
                }
            }
        }
    }

}
