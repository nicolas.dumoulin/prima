/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.auvergne.parameters;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.parameters.ConfiguredByFiles;
import fr.cemagref.prima.regionalmodel.parameters.Value;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service = Value.class)
public class FirstSCPAttribution implements Value<Integer, Individual>, ConfiguredByFiles {

    private String filename;
    private Character separator;
    private transient Map<Integer, Double[]> probasByAges;

    @Override
    public void init(Application application) throws BadDataException {
        try {
            probasByAges = new HashMap<Integer, Double[]>();
            String[] data;
            CSVReader reader = CSV.getReader(application.getFileFromRelativePath(filename), separator);
            while ((data = CSV.readLine(reader)) != null) {
                probasByAges.put(Integer.parseInt(data[0]), Utils.parseDoubleArray(data, 1));
            }
            reader.close();
        } catch (IOException ex) {
            throw new BadDataException("Error while loading " + filename, ex);
        }
    }

    @Override
    public List<String> getFilenames() {
        return Arrays.asList(filename);
    }

    @Override
    public Integer getValue(Individual individual) {
        Double[] probas = probasByAges.get(individual.getAgeToEnterOnTheLabourMarket()); // mais juste valide pour le cas Auvergne
        if (probas == null) {
            probas = probasByAges.get(30);
            /*
            // the individual is too old for the ages given and is retired
            // building a list of candidate for picking a suitable profession then it takes the distribution of the larger entering age
            List<Individual> candidates = new ArrayList<Individual>();
            for (Household hh : individual.getMyHousehold().getMyVillage().getMyHouseholds()) {
            for (Individual ind : hh) {
            if (ind.getProfession() >=0) {
            candidates.add(ind);
            }
            }
            }
            if (candidates.size() > 0) {
            return candidates.get(Random.nextInt(0, candidates.size() - 1)).getProfession();
            } else {
            // no candidate found in the village, we picking a value at random
            return Random.nextInt(0, individual.getMyHousehold().getMyVillage().getMyRegion().getNbProfessions() - 1);
            }
             *
             */
        }
        int prof = individual.getMyHousehold().getRandom().nextIndexWithDistribution(probas);
        //if (prof == -1) { prof = individual.getMyHousehold().getMyVillage().getMyRegion().getNbProfessions()-1 ; }
        //System.err.println(Arrays.toString(probas)+" prof "+prof) ;
        //return individual.getMyHousehold().getRandom().nextIndexWithDistribution(probas);
        return prof ;
    }
}
