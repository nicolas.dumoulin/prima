/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.auvergne;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.Random;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.Sheet;
import jxl.Workbook;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class GenerateInput {

    private static class Writers {

        private List<BufferedWriter> writers;
        private static final List<String> condatIDs = Arrays.asList("15040", "15054", "15110", "15114", "15129", "15132", "15173");

        public Writers(String munID, String filename) throws IOException {
            this(munID, filename, false);
        }

        public Writers(String munID, String filename, boolean append) throws IOException {
            writers = new ArrayList<BufferedWriter>();
            writers.add(new BufferedWriter(new FileWriter(new File("dataIn/Auvergne/" + munID + filename), append)));
            if (munID.startsWith("15")) {
                writers.add(new BufferedWriter(new FileWriter(new File("dataIn/Cantal/" + munID + filename), append)));
                if (condatIDs.contains(munID)) {
                    writers.add(new BufferedWriter(new FileWriter(new File("dataIn/CondatNew/" + munID + filename), append)));
                }
            }
        }

        public void write(String s) throws IOException {
            for (BufferedWriter writer : writers) {
                writer.write(s);
            }
        }

        public void close() throws IOException {
            for (BufferedWriter writer : writers) {
                writer.close();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Random random = new Random();
        random.setSeed(new long[]{42, 42, 42, 42, 42, 42});
        // lambert geograpical position
        CSVReader reader = new CSVReader(new FileReader("dataIn/Auvergne/datasources/Lambert.csv"), ';');
        reader.readNext();
        Map<String, String[]> lambert = new HashMap<String, String[]>(1320);
        String[] line;
        List<String> munIDs = new ArrayList<String>(1320);
        while ((line = reader.readNext()) != null) {
            for (int i = 0; i < line.length; i++) {
                line[i] = line[i].replace(',', '.');
            }
            lambert.put(line[0], line);
            munIDs.add(line[0]);
        }
        reader.close();
        // intercept and slope
        reader = new CSVReader(new FileReader("dataIn/Auvergne/datasources/CoefficentRegression.csv"), ';');
        reader.readNext();
        List<Double[]> coeffsRegression = new ArrayList<Double[]>();
        while ((line = reader.readNext()) != null) {
            if (Integer.parseInt(line[1]) != coeffsRegression.size() + 1) {
                throw new RuntimeException("Bad indexing in coeffsRegression file. line #" + (coeffsRegression.size() + 1) + " red,"
                        + " but ID #" + Integer.parseInt(line[1]) + " red in this line:\n  " + line);
            }
            Double[] coeffRegression = new Double[2];
            coeffRegression[0] = Double.parseDouble(line[2].replace(",", "."));
            coeffRegression[1] = Double.parseDouble(line[3].replace(",", "."));
            coeffsRegression.add(coeffRegression);
        }
        reader.close();
        reader = new CSVReader(new FileReader("dataIn/Auvergne/datasources/IDDistComFreq.csv"), ';');
        reader.readNext();
        Map<String, Double[]> interceptsSlopes = new HashMap<String, Double[]>(1320);
        while ((line = reader.readNext()) != null) {
            String id = new Formatter().format("%05d", Integer.parseInt(line[0])).toString();
            interceptsSlopes.put(id, coeffsRegression.get(Integer.parseInt(line[1]) - 1));
            // store the list of mun IDs
        }
        reader.close();

        // places in the maisons of retraite ;-)
        CSVReader csvReader = CSV.getReader("dataIn/Auvergne/datasources/maisons_de_retraite.csv", null);
        csvReader.readNext();
        Map<String, Integer> placesInMaisonsOfRetraite2011 = new HashMap<String, Integer>();
        Map<String, Integer> placesInMaisonsOfRetraite1990 = new HashMap<String, Integer>();
        while ((line = csvReader.readNext()) != null) {
            String munID = line[0];
            if (!line[1].isEmpty()) {
                placesInMaisonsOfRetraite1990.put(munID, Integer.parseInt(line[1]));
            }
            placesInMaisonsOfRetraite2011.put(munID, Integer.parseInt(line[2]));
        }
        csvReader.close();
        // housing
        Workbook w = Workbook.getWorkbook(new File("dataIn/Auvergne/datasources/LogementParam.xls"));
        Sheet sheet = w.getSheet(0);
        Map<String, Integer[]> housings = new HashMap<String, Integer[]>(1320);
        for (int i = 1; i < sheet.getRows(); i++) {
            // read occupations for each mun
            String munID = sheet.getCell(0, i).getContents();
            for (int j = 1; j >= 0; j--) {
                Integer[] housing = new Integer[4];
                int sum = 0;
                int free = Integer.parseInt(sheet.getCell(2 + 5 * j, i).getContents());
                for (int col = 0; col < 4; col++) {
                    housing[col] = Integer.parseInt(sheet.getCell(col + 3 + 5 * j, i).getContents());
                    sum += housing[col];
                }
                readAndDistributeHousing(free, housing, sum);
                // adding places in the maisons de retraites
                if (placesInMaisonsOfRetraite1990.containsKey(munID)) {
                    housing[0] += placesInMaisonsOfRetraite1990.get(munID);
                }
                // storing or writing data    
                if (j == 0) {
                    // initial housing
                    housings.put(munID, housing);
                } else {
                    // scenario
                    Writers writers = new Writers(munID, "/dwelling.csv");
                    writers.write("#empty line\n");
                    writers.write("1991");
                    //writers.write("1995");
                    for (Integer h : housing) {
                        writers.write(";");
                        writers.write(h.toString());
                    }
                    writers.write("\n");
                    writers.close();
                }
            }
        }
        w.close();
        // housing in 2006
        w = Workbook.getWorkbook(new File("dataIn/Auvergne/datasources/BTX_IC_LOGEMENTS_2006Base.xls"));
        sheet = w.getSheet(0);
        for (int i = 1; i < sheet.getRows(); i++) {
            String munID = sheet.getCell(0, i).getContents();
            if (munID.length() == 4) {
                munID = "0" + munID;
            }
            Writers writers = new Writers(munID, "/dwelling.csv", true);
            writers.write("2000");
            //writers.write("2003");
            Integer[] housing = new Integer[4];
            int sum = 0;
            int free = Integer.parseInt(sheet.getCell(1, i).getContents());
            for (int col = 0; col < 4; col++) {
                housing[col] = Integer.parseInt(sheet.getCell(col + 2, i).getContents());
                sum += housing[col];
            }
            // housing of type 5 are counted with the type 4 (only 4 categories in our model)
            housing[3] += Integer.parseInt(sheet.getCell(4 + 2, i).getContents());
            sum += Integer.parseInt(sheet.getCell(4 + 2, i).getContents());
            readAndDistributeHousing(free, housing, sum);
            // adding places in the maisons de retraites
            if (placesInMaisonsOfRetraite2011.containsKey(munID)) {
                housing[0] += placesInMaisonsOfRetraite2011.get(munID);
            }
            // writing
            for (Integer h : housing) {
                writers.write(";");
                writers.write(h.toString());
            }
            writers.write("\n");
            writers.close();
        }
        w.close();
        // workers
        Map<String, Integer[]> workers = new HashMap<String, Integer[]>(1320);
        w = Workbook.getWorkbook(new File("dataIn/Auvergne/datasources/A_Prendre_Pour_Scenarios.xls"));
        sheet = w.getSheet(0);
        for (int i = 1; i < sheet.getRows(); i++) {
            if (sheet.getCell(0, i).getContents().equals("1990")) {
                String munID = new Formatter().format("%05d", Integer.parseInt(sheet.getCell(1, i).getContents())).toString();
                Integer[] counts = new Integer[24];
                for (int col = 0; col < counts.length; col++) {
                    counts[col] = Integer.parseInt(sheet.getCell(12 + col, i).getContents());
                }
                workers.put(munID, counts);
            }
        }
        w.close();
        // population
        w = Workbook.getWorkbook(new File("dataIn/Auvergne/datasources/population1990.xls"));
        sheet = w.getSheet(2);
        CSVReader distancesReader = new CSVReader(new FileReader("dataIn/Auvergne/datasources/MatriceDistanceEuclidienneAuv.txt"), ' ');
        distancesReader.readNext();
        File outsideProxJobFile = new File("dataIn/Auvergne/outside0/proxJob.csv");
        BufferedWriter outsideProxJobWriter = new BufferedWriter(new FileWriter(outsideProxJobFile));
        for (int munIndex = 1; munIndex < sheet.getRows(); munIndex++) {
            String munID = new Formatter().format("%05d", Integer.parseInt(sheet.getCell(0, munIndex).getContents())).toString();

            // village.txt
            String popSize = sheet.getCell(58, munIndex).getContents();
            Writers writers = new Writers(munID, "/village.txt");
            writers.write("#empty line\n");
            writers.write(popSize);
            writers.write("\t-1\t");
            Integer[] workersToWrite = workers.get(munID);
            for (int j = 0; j < workersToWrite.length; j++) {
                writers.write(workersToWrite[j].toString());
                writers.write("\t");
            }
            Integer[] housingToWrite = housings.get(munID);
            for (int col = 0; col < 4; col++) {
                writers.write(housingToWrite[col].toString());
                writers.write("\t");
            }
            String[] munXY = lambert.get(munID);
            writers.write(munXY[1]);
            writers.write("\t");
            writers.write(munXY[2]);
            writers.write("\t");
            Double[] interceptSlope = interceptsSlopes.get(munID);
            if (interceptSlope == null) {
                interceptSlope = coeffsRegression.get(0);
                Logger.getLogger(GenerateInput.class.getName()).log(Level.WARNING, "Municipality {0} wasn''t in the IDDistComFreq classification, so the first class has been taken.", munID);
            }
            writers.write(interceptSlope[0].toString());
            writers.write("\t");
            writers.write(interceptSlope[1].toString());
            writers.write("\t");
            writers.write("\n");
            writers.close();

            // proxjob.csv
            writers = new Writers(munID, "/proxJob.csv");
            line = distancesReader.readNext();
            for (int col = 1; col < line.length; col++) {
                double distance = Double.parseDouble(line[col]);
                if (col != munIndex && (distance < 20000)) {
                    writers.write(munIDs.get(col - 1));
                    writers.write(";");
                }
            }
            if (random.nextInt(0, 9) > 8) {
                writers.write("outside0");
                outsideProxJobWriter.write(munID);
                outsideProxJobWriter.write(";");
            }
            writers.write("\n");
            writers.close();
        }
        w.close();
        outsideProxJobWriter.write("\n");
        outsideProxJobWriter.close();
        generateExoJobScenarios();
    }

    private static void readAndDistributeHousing(int free, Integer[] housing, int sum) {
        // redistribute the free residence amount proportionnaly to the occupied amount
        int freeRedistributed = 0;
        double[] rests = new double[4];
        for (int col = 0; col < 4; col++) {
            double delta = free * ((double) housing[col] / sum);
            rests[col] = delta - (int) delta;
            housing[col] += (int) delta;
            freeRedistributed += delta;
        }
        // now we can have some rest that we will give to those who have the higher rest
        while (freeRedistributed < free) {
            int maxRestIndex = 0;
            for (int col = 1; col < rests.length; col++) {
                if (rests[col] > rests[maxRestIndex]) {
                    maxRestIndex = col;
                }
            }
            rests[maxRestIndex] = 0;
            freeRedistributed++;
            housing[maxRestIndex]++;
        }
    }

    public static void generateExoJobScenarios() throws Exception {
        // read the population size for 1999 and 2006
        CSVReader reader = new CSVReader(new FileReader("dataIn/Auvergne/datasources/popAuvergne90_99_06.csv"), ';');
        reader.readNext();
        Map<String, Integer[]> popSize = new HashMap<String, Integer[]>(1320);
        List<String> munIDs = new ArrayList<String>(1320);
        String[] line;
        while ((line = reader.readNext()) != null) {
            Integer[] sizeByYear = new Integer[2];
            String id = new Formatter().format("%05d", Integer.parseInt(line[0])).toString();
            //System.err.println("id "+id) ;
            sizeByYear[0] = new Integer(line[2]);
            sizeByYear[1] = new Integer(line[3]);
            popSize.put(id, sizeByYear);
            // store the list of mun IDs
            munIDs.add(id);
        }
        reader.close();
        // read the total offer of jobs for every villages for 1999 and 2006
        Workbook w = Workbook.getWorkbook(new File("dataIn/Auvergne/datasources/A_Prendre_Pour_Scenarios.xls"));
        Sheet sheet = w.getSheet(0);
        Map<String, Integer[]> workers1999 = new HashMap<String, Integer[]>(1320);
        Map<String, Integer[]> workers2006 = new HashMap<String, Integer[]>(1320);
        for (int i = 1; i < sheet.getRows(); i++) {
            String munID = new String();
            if (sheet.getCell(0, i).getContents().equals("1999")) {
                munID = new Formatter().format("%05d", Integer.parseInt(sheet.getCell(1, i).getContents())).toString();
                Integer[] counts = new Integer[24];
                for (int col = 0; col < counts.length; col++) {
                    counts[col] = Integer.parseInt(sheet.getCell(12 + col, i).getContents());
                }
                workers1999.put(munID, counts);
            }
            if (sheet.getCell(0, i).getContents().equals("2006")) {
                munID = new Formatter().format("%05d", Integer.parseInt(sheet.getCell(1, i).getContents())).toString();
                Integer[] counts = new Integer[24];
                for (int col = 0; col < counts.length; col++) {
                    counts[col] = Integer.parseInt(sheet.getCell(12 + col, i).getContents());
                }
                workers2006.put(munID, counts);
            }
        }
        int nbActivities = 24;
        for (String munID : workers1999.keySet()) {
            // compute the endogeneous offer in service sector from the population size for 1999
            int[] endogeneousOfferedJob99 = new int[nbActivities];
            int[] exogeneousOfferedJob99 = new int[nbActivities];
            Arrays.fill(endogeneousOfferedJob99, 0);
            Arrays.fill(exogeneousOfferedJob99, 0);
            int[] endogeneousOfferedJob06 = new int[nbActivities];
            int[] exogeneousOfferedJob06 = new int[nbActivities];
            Arrays.fill(endogeneousOfferedJob06, 0);
            Arrays.fill(exogeneousOfferedJob06, 0);
            int nbProfessions = 6;
            int serviceThreshold = nbProfessions * 3;
            int totOfferService99 = 0;
            int currentTotService99 = 0;
            int totOfferService06 = 0;
            int currentTotService06 = 0;
            for (int i = 0; i < nbActivities; i++) {
                if (i >= serviceThreshold) {
                    currentTotService99 += workers1999.get(munID)[i];
                    currentTotService06 += workers2006.get(munID)[i];
                }
            }
            FileReader villageFile = new FileReader("dataIn/Auvergne/" + munID + "/village.txt");
            char sep = '\t';
            reader = new CSVReader(villageFile, sep);
            reader.readNext();
            line = reader.readNext();
            double slope = new Double(line[33]).doubleValue();
            double intercept = new Double(line[32]).doubleValue();
            reader.close();
            totOfferService99 = (int) (popSize.get(munID)[0] * (intercept + (slope * Math.log(popSize.get(munID)[0]))));
            totOfferService06 = (int) (popSize.get(munID)[1] * (intercept + (slope * Math.log(popSize.get(munID)[1]))));
            if (totOfferService99 < 0) {
                totOfferService99 = 0;
            }
            if (totOfferService06 < 0) {
                totOfferService06 = 0;
            }
            //System.err.println("munID " + munID + " total offre service " + totOfferService99 + " pop size " + popSize.get(munID)[0] + " slope " + slope + " intercept " + intercept);
            if (totOfferService99 > currentTotService99) {
                totOfferService99 = currentTotService99;
            }
            if (totOfferService99 > 0) {
                for (int i = serviceThreshold; i < nbActivities; i++) {
                    if (workers1999.get(munID)[i] == 0) {
                        endogeneousOfferedJob99[i] = (int) Math.round(totOfferService99 * 0.001);
                    } else {
                        endogeneousOfferedJob99[i] = Math.round(totOfferService99 * ((float) workers1999.get(munID)[i] / (float) currentTotService99));
                    }
                    //System.err.println("i " + i + " total offre service " + totOfferService99 + " workers99 " + workers1999.get(munID)[i] + " currentTotServ " + currentTotService99 + " endog " + endogeneousOfferedJob99[i]);
                }
            }
            if (totOfferService06 > currentTotService06) {
                totOfferService06 = currentTotService06;
            }
            if (totOfferService06 > 0) {
                for (int i = serviceThreshold; i < nbActivities; i++) {
                    if (workers2006.get(munID)[i] == 0) {
                        endogeneousOfferedJob06[i] = (int) Math.round(totOfferService06 * 0.001);

                    } else {
                        endogeneousOfferedJob06[i] = Math.round(totOfferService06 * ((float) workers2006.get(munID)[i] / (float) currentTotService06));
                    }
                }
            }
            // compute the exogeneous offer (the scenario for job offer) from the endogeneous and the total offer
            for (int i = 0; i < nbActivities; i++) {
                if (workers1999.get(munID)[i] == 0 && endogeneousOfferedJob99[i] > 0) {
                    exogeneousOfferedJob99[i] = workers1999.get(munID)[i];
                } else {
                    exogeneousOfferedJob99[i] = workers1999.get(munID)[i] - endogeneousOfferedJob99[i];
                }
                if (workers2006.get(munID)[i] == 0 && endogeneousOfferedJob06[i] > 0) {
                    exogeneousOfferedJob06[i] = workers2006.get(munID)[i];
                } else {
                    exogeneousOfferedJob06[i] = workers2006.get(munID)[i] - endogeneousOfferedJob06[i];
                }
            }
            //System.err.println(munID+" \t"+exogeneousOfferedJob99[17]+" \tworkers99\t "+workers1999.get(munID)[17]+" \tendog 99\t "+endogeneousOfferedJob99[17] ) ;
            // write down the scenario file for one municipality and the two dates 1999 (write 1991 at the beginning of the period) and 2006 (written 2000 at the beginning of the period)
            Writers writers = new Writers(munID, "/exogJobOffers.csv");
            writers.write("#Date and Total job offers expressed in quantity of offers\n");
            writers.write("1991");
            for (int j = 0; j < nbActivities; j++) {
                writers.write(";" + exogeneousOfferedJob99[j]);
            }
            writers.write("\n");
            writers.write("2000");
            for (int j = 0; j < nbActivities; j++) {
                writers.write(";" + exogeneousOfferedJob06[j]);
            }
            writers.write("\n");
            writers.close();
        }
        // generate a file for the case we don't use the dynamic employment
        for (String munID : workers1999.keySet()) {
            int[] offeredJob99 = new int[nbActivities];
            int[] offeredJob06 = new int[nbActivities];
            // compute the exogeneous offer (the scenario for job offer) from the endogeneous and the total offer
            for (int i = 0; i < nbActivities; i++) {
                offeredJob99[i] = workers1999.get(munID)[i];
                offeredJob06[i] = workers2006.get(munID)[i];
            }
            //System.err.println(munID+" \t"+exogeneousOfferedJob99[17]+" \tworkers99\t "+workers1999.get(munID)[17]+" \tendog 99\t "+endogeneousOfferedJob99[17] ) ;
            // write down the scenario file for one municipality and the two dates 1999 (write 1991 at the beginning of the period) and 2006 (written 2000 at the beginning of the period)
            Writers writers = new Writers(munID, "/totalJobOffers.csv");
            writers.write("Date and Total job offers expressed in quantity of offers\n");
            writers.write("1991");
            for (int j = 0; j < nbActivities; j++) {
                writers.write(";" + offeredJob99[j]);
            }
            writers.write("\n");
            writers.write("2000");
            for (int j = 0; j < nbActivities; j++) {
                writers.write(";" + offeredJob06[j]);
            }
            writers.write("\n");
            writers.close();
        }


    }
}
