/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.auvergne;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.prima.regionalmodel.Application;
import fr.cemagref.prima.regionalmodel.parameters.MunicipalityParameters;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class GenerateMunicipalitiesNetworkFiles {

    public static enum Target {

        CANTAL("dataIn/Cantal/distances.csv") {

            @Override
            public boolean filter(String munID) {
                return munID.startsWith("15");
            }
        }, CONDAT("dataIn/CondatNew/distances.csv") {

            List<String> muns = Arrays.asList("15040", "15054", "15110", "15114", "15129", "15132", "15173");

            @Override
            public boolean filter(String munID) {
                return muns.contains(munID);
            }
        };
        private final String distanceFilename;

        private Target(String distanceFilename) {
            this.distanceFilename = distanceFilename;
        }

        public abstract boolean filter(String munID);
    }

    public static void main(String[] args) throws Exception {
        Target target = Target.CONDAT;

        FileWriter fileWriter = new FileWriter(target.distanceFilename);
        fileWriter.write("# euclidian distances between municipalities\n");

        List<String> muns = new ArrayList<String>();
        List<String> munsSelected = new ArrayList<String>();
        CSVReader reader = CSV.getReader("dataIn/Auvergne/datasources/IDDistComFreq.csv", null);
        String[] line;
        reader.readNext();
        while ((line = CSV.readLine(reader)) != null) {
            muns.add(line[0]);
            if (target.filter(line[0])) {
                munsSelected.add(line[0]);
            }
        }
        reader.close();

        fileWriter.write("from/to");
        for (String label : munsSelected) {
            fileWriter.write(";" + label);
        }
        fileWriter.write("\n");

        reader = CSV.getReader("dataIn/Auvergne/datasources/MatriceDistanceEuclidienneAuv.txt", ' ');
        reader.readNext();
        List<List<Double>> distances = new ArrayList<List<Double>>(munsSelected.size());
        int fromIdx = 0;
        while ((line = CSV.readLine(reader)) != null) {
            if (target.filter(muns.get(fromIdx))) {
                fileWriter.write(muns.get(fromIdx));
                Double[] array = Utils.parseDoubleArray(line, 1);
                List<Double> currentDistances = new ArrayList<Double>(munsSelected.size());
                for (int i = 0; i < array.length; i++) {
                    if (target.filter(muns.get(i))) {
                        fileWriter.write(";" + array[i] / 1000);
                        currentDistances.add(array[i]);
                    }
                }
                fileWriter.write("\n");
                distances.add(currentDistances);
            }
            fromIdx++;
        }
        reader.close();
        fileWriter.close();
        System.out.println(distances.size() + " muninicipalities written");
    }

    public static void main2(String[] args) throws Exception {
        if (args.length != 2) {
            System.out.println("Arguments: <simulation input file> <distance classes file>\n"
                    + "  <distance classes>: a CSV file giving in the first column the distance classes between municipalities");
            System.exit(1);
        }
        List<Integer> distancesBounds = new ArrayList<Integer>();
        CSVReader reader = CSV.getReader(args[1], null);
        String[] line;
        while ((line = CSV.readLine(reader)) != null) {
            distancesBounds.add(Integer.parseInt(line[0]));
        }
        reader.close();
        // load municipalities
        Application application = new Application(new String[]{"-b", "-i", args[0]});
        application.loadFiles();

        double lat1 = 0, lng1 = 0, lat2 = 0, lng2 = 0;
        for (MunicipalityParameters mp : application.getParameters().getMunicipalitiesParameters()) {
            if (mp.getMunicipalityName().equals("15268")) {
                lat1 = mp.getLat();
                lng1 = mp.getLon();
            } else if (mp.getMunicipalityName().equals("15129")) {
                lat2 = mp.getLat();
                lng2 = mp.getLon();
            }
        }
        System.out.println(distance(lat1, lng1, lat2, lng2) + " km");
        System.out.println(distance2(lat1, lng1, lat2, lng2) + " km");
        //StaticGeometry.distance(null, null);
        //GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), "");
    }

    public static double distance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;

        return dist;
    }

    private static double distance2(double lat1, double lng1, double lat2, double lng2) {
        double theta = lng1 - lng2;
        double dist = Math.sin(Math.toRadians(lat1))
                * Math.sin(Math.toRadians(lat2))
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
        dist = Math.acos(dist);
        dist = Math.toDegrees(dist);
        dist = dist * 60 * 1.1515 * 1.609344;
        return (dist);
    }
}
