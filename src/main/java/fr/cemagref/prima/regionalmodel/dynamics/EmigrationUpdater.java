/*
 *  Copyright (C) 2010 sylvie.huet
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.dynamics;

import fr.cemagref.prima.regionalmodel.Household;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.lookup.ServiceProvider;

public abstract class EmigrationUpdater extends Dynamics<MunicipalitySet> {

    protected final static transient Logger LOGGER = Logger.getLogger(EmigrationUpdater.class.getName());

    public transient int emigrantsHH ;
    public transient int emigrants ;

    @Override
    public void step(MunicipalitySet municipalitySet, int iter) throws ProcessingException {
        int countRemainingPotentialImmig = 0;
        emigrantsHH = 0;
        emigrants = 0;
        int emigrantLess31 = 0;
        for (Municipality mun : municipalitySet.getMyMunicipalities()) {
            for (Household hh : mun.getMyHouseholds()) {
                if (!hh.isJustSuppressed()) {
                    emigration(hh);
                    if (hh.isJustSuppressed()) {
                        emigrantsHH++;
                        emigrants = emigrants + hh.size();
                        for (Individual ind : hh.getCopyOfMembers()) {
                            if (ind.getAge() < 31) {
                                emigrantLess31++;
                            }
                        }
                    }
                }
                if ((!hh.isJustSuppressed()) && (hh.getResidence().isTransit())) {
                    countRemainingPotentialImmig++;
                }
            }
        }
        LOGGER.log(Level.FINER, "nb of HH emigrants: {0}", emigrantsHH);
        LOGGER.log(Level.FINER, "nb of individual emigrants: {0}", emigrants);
        LOGGER.log(Level.FINER, "nb of individual emigrants less than 31: {0}", emigrantLess31);
        LOGGER.log(Level.FINER, "potential HH immigrants remaining {0}", countRemainingPotentialImmig);
    }

    protected abstract void emigration(Household hh);

    @ServiceProvider(service = EmigrationUpdater.class)
    public static class DefaultEmigrationUpdater extends EmigrationUpdater {

        private float probToQuitRegion;

        @Override
        protected void emigration(Household hh) {
            boolean leave = false;
            // The emigration can be caused by the non satisfaction of the need of residence (or the need of job or ...)
            if (!hh.isJustSuppressed()) {
                if (!hh.getResidence().isTransit()) {
                    if (hh.getRandom().nextDouble() <= probToQuitRegion) {
                        leave = true;
                    }
                    if (leave) {
                        hh.getMyVillage().getCounters().incNbHouseholdMoveOut(1);
                        hh.getMyVillage().getCounters().incNbIndividualMoveOut(hh.size());
                        hh.getMyVillage().suppressHousehold(hh, true, false);
                    }
                }
            }
        }
    }
}
