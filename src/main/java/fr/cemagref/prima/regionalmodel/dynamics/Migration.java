/*
 *  Copyright (C) 2011 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel.dynamics;

import au.com.bytecode.opencsv.CSVReader;
import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.prima.regionalmodel.Household;
import fr.cemagref.prima.regionalmodel.Individual;
import fr.cemagref.prima.regionalmodel.Individual.Status;
import fr.cemagref.prima.regionalmodel.MunicipalitiesNetwork;
import fr.cemagref.prima.regionalmodel.MunicipalitiesNetwork.SubNetwork;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.CSV;
import fr.cemagref.prima.regionalmodel.tools.Utils;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class Migration {

    @Description(name = "Exogenous migration probas", tooltip = "File containing the probas of migration for student and retired people")
    //public int findLodgingFail = 0 ;
    //public int findLodgingSuccess = 0 ;
    private String exogenousMigrationProbasFileName;
    private transient SortedMap<Integer, Double> studentMigrationProbasByYear;
    private transient SortedMap<Integer, Double> retiredMigrationProbasByYear;
    private String distancesToBorderFileName;
    private String migrationOutsideProbasFileName;
    private transient Map<Municipality, Double> migrationOutsideProbas;
    private String distMigrationStudentRetiredFilename;
    private transient MunicipalitiesNetwork studentResidenceNetwork;
    private transient MunicipalitiesNetwork retiredResidenceNetwork;
    private String munDistResidenceClassesFilename;
    private String munDistResidenceProbasFilename;
    public transient MunicipalitiesNetwork activesResidenceNetwork;
    private transient MunicipalitySet region;
    private transient int migrantsCount = 0;
    private transient int migrantsCountR = 0;
    private transient int migrantsCount60 = 0;
    private transient int migrantsCountS = 0;
    private transient int migrantsCountO = 0;
    private transient int migrantsCountSingle = 0;
    private transient int mobileSingleCount = 0;
    private transient int mobileRetiredCount = 0;
    private transient int mobileActiveEtFamCount = 0; // without people more than 60
    private transient int mobileStudentCount = 0;
    private transient int mobileMore60Count = 0;
    private transient int migrantsCount0_14 = 0;
    private transient int migrantsCount15_24 = 0;
    private transient int migrantsCount25_29 = 0;
    private transient int activeOutmigrants;
    private transient int inactiveOutmigrants;

    public int getActiveMigrantsCount() {
        return activeOutmigrants;
    }

    public int getInactiveMigrantsCount() {
        return inactiveOutmigrants;
    }

    public int getMigrantsCount0_14() {
        return migrantsCount0_14;
    }

    public int getMigrantsCount15_24() {
        return migrantsCount15_24;
    }

    public int getMigrantsCount25_29() {
        return migrantsCount25_29;
    }

    public int getMigrantsCount30_39() {
        return migrantsCount30_39;
    }

    public int getMigrantsCount40_59() {
        return migrantsCount40_59;
    }

    public int getMigrantsCount60_74() {
        return migrantsCount60_74;
    }

    public int getMigrantsCount75AndMore() {
        return migrantsCount75AndMore;
    }
    private transient int migrantsCount30_39 = 0;
    private transient int migrantsCount40_59 = 0;
    private transient int migrantsCount60_74 = 0;
    private transient int migrantsCount75AndMore = 0;

    public void init(MunicipalitySet region) throws FileNotFoundException, IOException, BadDataException {
        this.region = region;
        // load migration probas for student and retired
        studentMigrationProbasByYear = new TreeMap<Integer, Double>();
        retiredMigrationProbasByYear = new TreeMap<Integer, Double>();
        CSVReader reader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(exogenousMigrationProbasFileName), null);
        for (SortedMap<Integer, Double> map : new SortedMap[]{studentMigrationProbasByYear, retiredMigrationProbasByYear}) {
            String[] line = CSV.readLine(reader);
            int i = 1;
            // for each year given in the line
            while (i + 1 < line.length) {
                if (!line[i + 1].isEmpty()) {
                    map.put(Integer.parseInt(line[i]), Double.parseDouble(line[i + 1]));
                }
                i += 2;
            }
        }
        reader.close();
        // load probas for migrating outside the region
        migrationOutsideProbas = new HashMap<Municipality, Double>();
        List<Double> probas = new ArrayList<Double>();
        reader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(migrationOutsideProbasFileName), null);
        String[] line;
        while ((line = CSV.readLine(reader)) != null) {
            int index = Integer.parseInt(line[0]);
            Utils.ensureListCapacity(probas, index + 1);
            probas.set(index, Double.parseDouble(line[1]));
        }
        reader.close();
        reader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(distancesToBorderFileName), null);
        while ((line = CSV.readLine(reader)) != null) {
            migrationOutsideProbas.put(region.getMunicipality(line[0]), probas.get(Integer.parseInt(line[1])));
        }
        reader.close();
        // load migration distances for student and retired
        SortedMap<Integer, Double> migrationStudentDistProbas = new TreeMap<Integer, Double>();
        SortedMap<Integer, Double> migrationRetiredDistProbas = new TreeMap<Integer, Double>();
        reader = CSV.getReader(region.getMyApplication().getFileFromRelativePath(distMigrationStudentRetiredFilename), null);
        while ((line = CSV.readLine(reader)) != null) {
            int distance = Integer.parseInt(line[0]);
            migrationRetiredDistProbas.put(distance, Double.parseDouble(line[1]));
            migrationStudentDistProbas.put(distance, Double.parseDouble(line[2]));
        }
        reader.close();
        studentResidenceNetwork = new MunicipalitiesNetwork(region, migrationStudentDistProbas);
        retiredResidenceNetwork = new MunicipalitiesNetwork(region, migrationRetiredDistProbas);
        // load residence network for adults
        activesResidenceNetwork = new MunicipalitiesNetwork(region, munDistResidenceClassesFilename, munDistResidenceProbasFilename);
    }
    /*
     * public void initMigrantsCount(int mig) { migrantsCount = mig; }
     */

    public int getMobileSingleCount() {
        return mobileSingleCount;
    }

    public int getMobileMore60Count() {
        return mobileMore60Count;
    }

    public int getMobileRetiredCount() {
        return mobileRetiredCount;
    }

    public int getMobileStudentCount() {
        return mobileStudentCount;
    }

    public int getMobileActivEtFamCount() {
        return mobileActiveEtFamCount;
    }

    public int getMigrantsCount() {
        return migrantsCount;
    }

    public int getMigrantsCountO() {
        return migrantsCountO;
    }

    public int getMigrantsCountR() {
        return migrantsCountR;
    }

    public int getMigrantsCount60() {
        return migrantsCount60;
    }

    public int getMigrantsCountS() {
        return migrantsCountS;
    }

    public int getMigrantsCountSingle() {
        return migrantsCountSingle;
    }

    public void resetCounters() {
        migrantsCount = 0;
        activeOutmigrants = 0;
        inactiveOutmigrants = 0;
        migrantsCount0_14 = 0;
        migrantsCount15_24 = 0;
        migrantsCount25_29 = 0;
        migrantsCount30_39 = 0;
        migrantsCount40_59 = 0;
        migrantsCount60_74 = 0;
        migrantsCount75AndMore = 0;
        migrantsCountO = 0;
        migrantsCountS = 0;
        migrantsCountR = 0;
        migrantsCount60 = 0;
        migrantsCountSingle = 0;
        mobileSingleCount = 0;
        mobileRetiredCount = 0;
        mobileMore60Count = 0;
    }

    public void process(Household hh) {
        boolean atLeastOneActive = false;
        // case where all members are retired
        if (!hh.getResidence().isTransit()) {
            for (Individual ind : hh.getCopyOfMembers()) {
                // processing student
                if (ind.getStatus() == Status.STUDENT && ind.getAge() >= 18 && ind.getAge() <= 29) {
                    if ((!ind.isAdult() | (hh.getHshType() == Household.Type.SINGLE))) {
                        if (region.getRandom().nextDouble() < Utils.getValueFromUpperBound(studentMigrationProbasByYear, region.getCurrentYear())) {
                            // building a temporary hh with only the child for testing if he can become adult
                            Household oldHh = ind.getMyHousehold(); // Be careful, we need to use the hh associated to the ind,
                            // because it can be different than hh parameter (becomeadult if all adults are gone)
                            Household newHh = new Household(oldHh.getMyVillage(), new ArrayList<Individual>(Arrays.asList(ind)), null);
                            List<Municipality> availableResidences = getPlacesToResideStudent(studentResidenceNetwork, newHh, true);
                            ind.setMyHousehold(oldHh);
                            if (availableResidences.size() > 0) {
                                if (!migrateOutside(ind.getMyHousehold())) {
                                    // the individuals looks if it moves outside only if 
                                    // it has found out a lodging (that is to be close to the data
                                    // from which the probabilities are extracted
                                    oldHh.setNextSizeOfResidence(newHh.getNextSizeOfResidence());
                                    oldHh.setNextResMunicipality(newHh.getNextResMunicipality());
                                    ind.becomeAdult();
                                    newHh = ind.getMyHousehold();
                                    newHh.setNextSizeOfResidence(oldHh.getNextSizeOfResidence());
                                    newHh.setNextResMunicipality(oldHh.getNextResMunicipality());
                                    takeResidence(availableResidences, newHh);
                                    if (!oldHh.getResidence().isTransit()) {
                                        mobileStudentCount++;
                                        for (Individual in : oldHh.getCopyOfMembers()) {
                                            if (in.getAge() >= 60) {
                                                mobileMore60Count++;
                                                mobileStudentCount--;
                                            }
                                        }
                                    }
                                } else {
                                    ind.getMyHousehold().suppressMember(ind);
                                    ind.getMyHousehold().updateHshFeaturesAfterDeath();
                                    migrantsCount++;
                                    hh.getMyVillage().getCounters().incNbOutMigrants(1);
                                    incNbOutMigrantsAges(ind);
                                    migrantsCountS++;
                                    for (Individual in : ind.getMyHousehold().getCopyOfMembers()) {
                                        if (in.getAge() >= 60) {
                                            migrantsCount60++;
                                            migrantsCountS--;
                                        }
                                    }
                                    if (ind.getMyHousehold().getAdults().size() == 1) {
                                        migrantsCountSingle++;
                                    }
                                }
                            }
                        }
                    }
                } else if (ind.getAge() >= 60) {
                    if (region.getRandom().nextDouble() < Utils.getValueFromUpperBound(retiredMigrationProbasByYear, region.getCurrentYear())) {
                        // building a temporary hh with only the retired people for testing if he can leave the HH
                        Household oldHh = ind.getMyHousehold(); // Be careful, we need to use the hh associated to the ind,
                        // because it can be different than hh parameter (becomeadult if all adults are gone)
                        Household newHh = new Household(oldHh.getMyVillage(), new ArrayList<Individual>(Arrays.asList(ind)), null);
                        //List<Municipality> availableResidences = getAvailableResidences(retiredResidenceNetwork, newHh, true);
                        List<Municipality> availableResidences = getPlacesToReside(retiredResidenceNetwork, newHh, true, newHh.getMyVillage());
                        ind.setMyHousehold(oldHh);
                        if (availableResidences.size() > 0) {
                            if (!migrateOutside(hh)) {
                                // the individuals looks if it moves outside only if 
                                // it has found out a lodging (that is to be close to the data
                                // from which the probabilities are extracted
                                oldHh.setNextSizeOfResidence(newHh.getNextSizeOfResidence());
                                oldHh.setNextResMunicipality(newHh.getNextResMunicipality());
                                if (!ind.isAdult()) {
                                    ind.becomeAdult();
                                    newHh = ind.getMyHousehold();
                                } else {
                                    newHh = new Household(oldHh.getMyVillage(), new ArrayList<Individual>(Arrays.asList(ind)), oldHh.getResidence());
                                }
                                newHh.setNextSizeOfResidence(oldHh.getNextSizeOfResidence()); // au lieu de hh
                                newHh.setNextResMunicipality(oldHh.getNextResMunicipality()); // au lieu de hh
                                // the retired is moving in another municipality
                                oldHh.getMyVillage().getCounters().incNewHouseholdNb(1);
                                oldHh.getMyVillage().getMyHouseholds().add(newHh);
                                oldHh.suppressMember(ind);
                                mobileRetiredCount++;
                                for (Individual in : oldHh.getCopyOfMembers()) {
                                    if (in.getAge() >= 60) {
                                        mobileMore60Count++;
                                    }
                                }
                                if (oldHh.getAdults().isEmpty()) {
                                    oldHh.getMyVillage().getCounters().incNbKilledOrphans(oldHh.getSize());
                                    oldHh.getMyVillage().suppressHousehold(oldHh, true, false);
                                } else {
                                    oldHh.updateHshFeaturesAfterRetiredMigration();
                                }
                                takeResidence(availableResidences, newHh);
                            } else {
                                ind.leaveMyActivPattern();
                                hh.suppressMember(ind);
                                migrantsCount++;
                                hh.getMyVillage().getCounters().incNbOutMigrants(1);
                                incNbOutMigrantsAges(ind);
                                migrantsCountR++;
                                for (Individual in : hh.getCopyOfMembers()) {
                                    if (in.getAge() >= 60) {
                                        migrantsCount60++;
                                    }
                                }
                                if (hh.getAdults().size() == 1) {
                                    migrantsCountSingle++;
                                }
                                if (hh.getAdults().isEmpty()) {
                                    hh.getMyVillage().getCounters().incNbKilledOrphans(hh.getSize());
                                    hh.getMyVillage().suppressHousehold(hh, true, false);
                                } else {
                                    hh.updateHshFeaturesAfterDeath();
                                }
                            }
                        }
                    }
                } else { // processing actives and inactives having the good age
                    atLeastOneActive = true;
                }
            }
        }
        if (!hh.isJustSuppressed() && hh.needOfResidence()) {
            if (atLeastOneActive || hh.getResidence().isTransit()) {
                Municipality placeOfWork = hh.determineLeader().getJobLocation();
                if (placeOfWork != null) {
                    if (placeOfWork.isOutside()) {
                        placeOfWork = null;
                        for (Individual ind : hh) {
                            if (ind.isAdult() && ind.getStatus() == Status.WORKER && !ind.getJobLocation().isOutside()) {
                                placeOfWork = ind.getJobLocation();
                                break;
                            }
                        }
                    }
                }
                if (placeOfWork != null && !hh.getResidence().isTransit()) {
                    List<Municipality> possibleResidences = getPlacesToReside(activesResidenceNetwork, hh, false, placeOfWork);
                    //List<Municipality> possibleResidences = getPlacesToReside(activesResidenceNetwork, hh, false, hh.getMyVillage());
                    Municipality chosenMun = null;
                    //findLodgingFail++ ;
                    if (possibleResidences != null && possibleResidences.size() > 0) {
                        //findLodgingSuccess++ ;
                        //findLodgingFail-- ;
                        chosenMun = region.getRandom().nextObject(possibleResidences);
                        //
                        // the individuals looks if it moves outside only if 
                        // it has found out a lodging (that is to be close to the data
                        // from which the probabilities are extracted
                        if ((chosenMun != hh.getMyVillage()) && migrateOutside(hh)) {
                            // migration outside can only occurs if the current mun has not been chosen
                            hh.getMyVillage().suppressHousehold(hh, true, false);
                            migrantsCount += hh.size();
                            hh.getMyVillage().getCounters().incNbOutMigrants(hh.size());
                            incNbOutMigrantsAges(hh);
                            migrantsCountO += hh.size();
                            for (Individual in : hh.getCopyOfMembers()) {
                                if (in.getAge() >= 60) {
                                    migrantsCount60++;
                                    migrantsCountO--;
                                }
                            }
                            if (hh.getAdults().size() == 1) {
                                migrantsCountSingle++;
                            }
                        } else {
                            if (!possibleResidences.isEmpty()) {
                                if (!hh.getResidence().isTransit()) {
                                    mobileActiveEtFamCount = mobileActiveEtFamCount + hh.size();
                                    for (Individual in : hh.getCopyOfMembers()) {
                                        if (in.getAge() >= 60) {
                                            mobileMore60Count++;
                                            mobileActiveEtFamCount--;
                                        }
                                    }
                                }
                            }
                            takeResidence(chosenMun, hh);
                        }
                    }
                } else { // if nobody works even if they are not students or retired (transit for example)
                    List<Municipality> possibleResidences = getPlacesToReside(activesResidenceNetwork, hh, false, hh.getMyVillage());
                    if (!hh.getResidence().isTransit()) {
                        Municipality chosenMun = null;
                        if (possibleResidences.size() > 0) {
                            chosenMun = region.getRandom().nextObject(possibleResidences);
                            //}
                            // the individuals looks if it moves outside only if 
                            // it has found out a lodging (that is to be close to the data
                            // from which the probabilities are extracted
                            if ((chosenMun != hh.getMyVillage()) && migrateOutside(hh)) {
                                // migration outside can only occurs if the first class of muns (containing the current mun) isn't picked out
                                hh.getMyVillage().suppressHousehold(hh, true, false);
                                migrantsCount += hh.size();
                                hh.getMyVillage().getCounters().incNbOutMigrants(hh.size());
                                incNbOutMigrantsAges(hh);
                                migrantsCountO += hh.size();
                                for (Individual in : hh.getCopyOfMembers()) {
                                    if (in.getAge() >= 60) {
                                        migrantsCount60++;
                                        migrantsCountO--;
                                    }
                                }
                                if (hh.getAdults().size() == 1) {
                                    migrantsCountSingle++;
                                }
                            } else {
                                //List<Municipality> possibleResidences = getAvailableResidences(subnetwork, hh);
                                if (!possibleResidences.isEmpty()) {
                                    if (!hh.getResidence().isTransit()) {
                                        mobileActiveEtFamCount = mobileActiveEtFamCount + hh.size();
                                        for (Individual in : hh.getCopyOfMembers()) {
                                            if (in.getAge() >= 60) {
                                                mobileMore60Count++;
                                                mobileActiveEtFamCount--;
                                            }
                                        }
                                    }
                                }
                                takeResidence(chosenMun, hh);
                            }
                        }
                    } else { // a transit can't migrate outside since she has not already migrate inside!
                        List<Municipality> munic = new ArrayList();
                        munic.add(hh.getMyVillage());
                        List<Municipality> possibleResidence = getAvailableResidences(munic, hh);
                        takeResidence(possibleResidence, hh);
                    }
                }
            } else { // the need for residence can be satisfied locally (in the municipality where the household already lives)
                // That is finally only for household composed from adults being 60 and more - then the need for residence
                // has to be computed again since one or more of them has(have) perhaps already migrate during this time step
                if (region.needOfResid(hh)) {
                    List<Municipality> subnetwork = new ArrayList();
                    subnetwork.add(hh.getMyVillage());
                    List<Municipality> possibleResidences = getAvailableResidences(subnetwork, hh);
                    if (hh.getResidence().getType() != hh.getNextSizeOfResidence()) {
                        if (possibleResidences != null && !possibleResidences.isEmpty()) {
                            if (!hh.getResidence().isTransit()) {
                                mobileRetiredCount = mobileRetiredCount + hh.size();
                                for (Individual in : hh.getCopyOfMembers()) {
                                    if (in.getAge() >= 60) {
                                        mobileMore60Count++;
                                    }
                                }
                            }
                        }
                    }
                    takeResidence(possibleResidences, hh);
                }
            }
        }
    }

    public List<Municipality> getPlacesToResideDist(MunicipalitiesNetwork subNetworks, Household hh, boolean excludeCurrentMun, Municipality start) {
        double probaRefuse = 0.0;
        boolean found = false;
        List<Municipality> muns = null;
        List<Municipality> munic;
        SortedMap<Double, SubNetwork> net = subNetworks.getSubNetwork(start);
        // save the keys, ordered them in descending order (they are in ascending one at the beginning)
        int nbKeys = net.size();
        Double[] probas = new Double[nbKeys];
        for (Map.Entry<Double, SubNetwork> entry : net.entrySet()) {
            nbKeys--;
            probas[nbKeys] = (entry.getKey()).doubleValue();
        }
        nbKeys = net.size();
        for (int i = 0; i < nbKeys; i++) {
            munic = getAvailableResidences(net.get(probas[i]).getMunicipalities(), hh);
            if (excludeCurrentMun) {
                muns = new ArrayList<Municipality>();
                for (Municipality m : munic) {
                    if (m != hh.getMyVillage()) {
                        muns.add(m);
                    }
                }
            } else {
                muns = munic;
            }
            if (muns.size() > 0) {
                // it accepts this distance following the distribution of acceptance of the distance
                if (region.getRandom().nextDouble() <= probaRefuse) {
                    found = false;
                } else {
                    found = true;
                }
            }

            if (found) {
                break;
            }
            //probaRefuse = probaRefuse + (entry.getKey()).doubleValue();
        }
        return muns;
    }

    public List<Municipality> getPlacesToReside(MunicipalitiesNetwork subNetworks, Household hh, boolean excludeCurrentMun, Municipality start) {
        List<Municipality> muns = new ArrayList<Municipality>();
        List<Municipality> subnet = new ArrayList<Municipality>();
        List<Municipality> munic;
        SortedMap<Double, SubNetwork> net = subNetworks.getSubNetwork(start);
        // save the keys, ordered them in descending order (they are in ascending one at the beginning)
        int nbKeys = net.size();
        Double[] probas = new Double[nbKeys];
        for (Map.Entry<Double, SubNetwork> entry : net.entrySet()) {
            nbKeys--;
            probas[nbKeys] = (entry.getKey()).doubleValue();
        }
        List<Municipality> munTemp = null;
        for (int i = 0; i < region.getSearchDistThreshold(); i++) {
            munTemp = (net.get(probas[i])).getMunicipalities();
            for (Municipality m : munTemp) {
                subnet.add(m);
            }
            munic = getAvailableResidences(subnet, hh);
            if (excludeCurrentMun) {
                for (Municipality m : munic) {
                    if (m != hh.getMyVillage()) {
                        muns.add(m);
                    }
                }
            } else {
                muns = munic;
            }
            if (muns.size() > 0) {
                // we check the farclose distance is not reach for the possible other worker of the household
                for (Individual ind : hh.getAdults()) {
                    if (ind.getStatus() == Status.WORKER && ind.getJobLocation() != region.getOutsides().get(0)) {
                        int d = (int) (muns.get(0).distanceFrom(ind.getJobLocation()));
                        if (d > region.getParameters().getFarCloseThreshold()) {
                            muns.clear();
                            break;
                        }
                    }
                }
                return muns;
            }
        }
        return muns;
    }

    public List<Municipality> getPlacesToResideStudent(MunicipalitiesNetwork subNetworks, Household hh, boolean excludeCurrentMun) {
        List<Municipality> muns = null;
        List<Municipality> munic = null;
        munic = new ArrayList<Municipality>();
        List<Municipality> subnet = new ArrayList<Municipality>();
        for (int i = 0; i < region.getMyMunicipalities().size(); i++) {
            if (region.getMyMunicipalities().get(i).getPopulationSize() > region.getParameters().getStudentCityThreshold()) {
                subnet.add(region.getMyMunicipalities().get(i));
            }
        }
        muns = getAvailableResidencesStudent(subnet, hh);
        if (excludeCurrentMun) {
            for (Municipality m : muns) {
                if (m != hh.getMyVillage()) {
                    munic.add(m);
                }
            }
        } else {
            munic = muns;
        }
        if (munic.size() > 0) {
            return munic;
        }
        return munic;
    }

    private List<Municipality> getAvailableResidencesStudent(List<Municipality> muns, Household hh) {
        // consider a municipality if prices becomes too high
        // the price is assessed through the disponibility level (dispolev) the number of free residence/total free residence offer
        List<Municipality> availableRes = new ArrayList<Municipality>();
        // new version
        Municipality[] offers = new Municipality[1];
        List posSize = hh.possibleSizeLodging();
        int limit = 0;
        int bound = 0;
        if (hh.getOldPartner() == null) {
            bound = 1;
        } else {
            bound = 1;
        }
        if (posSize.isEmpty()) {
            System.err.println(hh + " averageAge " + hh.averageAge());
        }
        while (availableRes.isEmpty() && limit < bound) {
            int searched = ((Integer) posSize.get(region.getRandom().nextInt(0, posSize.size() - 1))).intValue();
            hh.setNextSizeOfResidence(searched);
            limit++;
            availableRes = new ArrayList<Municipality>();
            Individual partner = null;
            for (Municipality mun : muns) {
                if (hh.couple()) {
                    for (Individual ind : hh.getAdults()) {
                        if (ind != hh.getLeader()) {
                            partner = ind;
                            break;
                        }
                    }
                    if (partner.getStatus() == Status.WORKER && mun.distanceFrom(partner.getJobLocation()) < (region.getParameters().getFarCloseThreshold())) {
                        int count = mun.getFreeResidencesCount(hh.getNextSizeOfResidence());
                        // we copy the mun as many times as we have found free residences
                        offers = new Municipality[count];
                        Arrays.fill(offers, mun);
                        availableRes.addAll(Arrays.asList(offers));
                    } else {
                        int count = mun.getFreeResidencesCount(hh.getNextSizeOfResidence());
                        // we copy the mun as many times as we have found free residences
                        offers = new Municipality[count];
                        Arrays.fill(offers, mun);
                        availableRes.addAll(Arrays.asList(offers));
                    }
                } else {
                    int count = mun.getFreeResidencesCount(hh.getNextSizeOfResidence());
                    // we copy the mun as many times as we have found free residences
                    offers = new Municipality[count];
                    Arrays.fill(offers, mun);
                    availableRes.addAll(Arrays.asList(offers));
                }
            }
        }
        return availableRes;
    }

    private List<Municipality> getAvailableResidences(List<Municipality> muns, Household hh) {
        // consider a municipality if prices becomes too high
        // the price is assessed through the disponibility level (dispolev) the number of free residence/total free residence offer
        List<Municipality> availableRes = new ArrayList<Municipality>();
        // new version
        Municipality[] offers = new Municipality[1];
        List posSize = hh.possibleSizeLodging();
        int limit = 0;
        int bound = 0;
        if (hh.getOldPartner() == null) {
            bound = 1;
        } else {
            bound = 1;
        }
        if (posSize.isEmpty()) {
            System.err.println(hh + " averageAge " + hh.averageAge());
        }
        while (availableRes.isEmpty() && limit < bound) {
            int searched = ((Integer) posSize.get(region.getRandom().nextInt(0, posSize.size() - 1))).intValue();
            hh.setNextSizeOfResidence(searched);
            limit++;
            availableRes = new ArrayList<Municipality>();
            Individual partner = null;
            for (Municipality mun : muns) {
                boolean dispolev = true;
                // for household size higher than 1 - limitation for the 3 employement center places (4 plus grandes villes du Cantal)
                if (hh.getSize() > 1) {
                    // test sélection basée sur désir de maison
                    if (mun.getName().equalsIgnoreCase("15014")) { // Aurillac
                        if (region.getRandom().nextDouble() > region.getParameters().getDispoThreshold()) {
                            dispolev = false;
                        }
                    }
                    if (mun.getName().equalsIgnoreCase("15187")) { // St Flour
                        if (region.getRandom().nextDouble() > region.getParameters().getDispoThreshold()) {
                            dispolev = false;
                        }
                    }
                    if (mun.getName().equalsIgnoreCase("15012")) { // Arpajon sur Cère
                        if (region.getRandom().nextDouble() > region.getParameters().getDispoThreshold()) {
                            dispolev = false;
                        }
                    }
                    ///*
                    if (mun.getName().equalsIgnoreCase("15120")) { // Mauriac
                        if (region.getRandom().nextDouble() > region.getParameters().getDispoThreshold()) {
                            dispolev = false;
                        }
                    }
                    // */
                }
                if (dispolev) {
                    if (hh.couple()) {
                        for (Individual ind : hh.getAdults()) {
                            if (ind != hh.getLeader()) {
                                partner = ind;
                                break;
                            }
                        }
                        if (partner.getStatus() == Status.WORKER && mun.distanceFrom(partner.getJobLocation()) < (region.getParameters().getFarCloseThreshold())) {
                            int count = mun.getFreeResidencesCount(hh.getNextSizeOfResidence());
                            // we copy the mun as many times as we have found free residences
                            offers = new Municipality[count];
                            Arrays.fill(offers, mun);
                            availableRes.addAll(Arrays.asList(offers));
                        } else { // c'est meme chose, en fait c'est inactif !!!! COMMENT SH //////////////////////////////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                            int count = mun.getFreeResidencesCount(hh.getNextSizeOfResidence());
                            // we copy the mun as many times as we have found free residences
                            offers = new Municipality[count];
                            Arrays.fill(offers, mun);
                            availableRes.addAll(Arrays.asList(offers));
                        }
                    } else {
                        int count = mun.getFreeResidencesCount(hh.getNextSizeOfResidence());
                        // we copy the mun as many times as we have found free residences
                        offers = new Municipality[count];
                        Arrays.fill(offers, mun);
                        availableRes.addAll(Arrays.asList(offers));
                    }
                }
            }
        }
        return availableRes;
    }

    private void takeResidence(Municipality munic, Household hh) {
        if (munic != null) {
            hh.setNextResMunicipality(munic);
            if (hh.getAdults().size() == 1) {
                mobileSingleCount++;
            }
            hh.getMyVillage().move(hh);
        }
    }

    private void takeResidence(List<Municipality> availableRes, Household hh) {
        if (availableRes.size() > 0) {
            hh.setNextResMunicipality(region.getRandom().nextObject(availableRes));
            if (hh.getAdults().size() == 1) {
                mobileSingleCount++;
            }
            hh.getMyVillage().move(hh);
        }
    }

    private boolean migrateOutside(Household hh) {
        boolean r = true;
        if (hh.getResidence().isTransit()) {
            return false;
        } else {
            r = region.getMyApplication().getRandom().nextDouble() < migrationOutsideProbas.get(hh.getMyVillage());
            if (r) {
                for (Individual indiv : hh.getCopyOfMembers()) {
                    if (indiv.getStatus() == Status.UNEMPLOYED || indiv.getStatus() == Status.WORKER) {
                        activeOutmigrants++;
                    }
                    if (indiv.getStatus() == Status.INACTIVE) {
                        inactiveOutmigrants++;
                    }
                }
            }
            return r;
            /*
             * if (hh.getMyVillage().getPopulationSize() >
             * (region.getParameters().getStudentCityThreshold())) {
             * //System.err.println(hh.getMyVillage().getName()+" "+hh+" proba
             * "+migrationOutsideProbas.get(hh.getMyVillage())) ; double bound =
             * (migrationOutsideProbas.get(hh.getMyVillage()) * 1.26) ; //1.26
             * return region.getMyApplication().getRandom().nextDouble() < bound
             * ; } else { return
             * region.getMyApplication().getRandom().nextDouble() <
             * migrationOutsideProbas.get(hh.getMyVillage()); }
             *
             */
        }
    }

    public void incNbOutMigrantsAges(Household househ) {
        for (Individual indiv : househ.getCopyOfMembers()) {
            incNbOutMigrantsAges(indiv);
        }
    }

    public void incNbOutMigrantsAges(Individual indiv) {
        if (indiv.getAge() < 15) {
            this.migrantsCount0_14++;
        } else {
            if (indiv.getAge() < 25) {
                this.migrantsCount15_24++;
            } else {
                if (indiv.getAge() < 30) {
                    this.migrantsCount25_29++;
                } else {
                    if (indiv.getAge() < 40) {
                        this.migrantsCount30_39++;
                    } else {
                        if (indiv.getAge() < 60) {
                            this.migrantsCount40_59++;
                        } else {
                            if (indiv.getAge() < 75) {
                                this.migrantsCount60_74++;
                            } else {
                                this.migrantsCount75AndMore++;
                            }
                        }
                    }
                }
            }
        }
    }
}
