/*
 *  Copyright (C) 2011 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import com.thoughtworks.xstream.XStream;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.prima.regionalmodel.parameters.Parameters;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.ReflectUtils;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class Exploration {

    private static final Logger LOGGER = Logger.getLogger(Exploration.class.getName());

    /**
     * Represent a list of possible values for a parameter
     */
    //<editor-fold defaultstate="collapsed" desc="ParameterValues">
    protected static class ParameterValues {

        private String name;
        private Object[] values;
        private transient int index = 0;

        protected ParameterValues() {
        }

        public ParameterValues(String name, Object... values) {
            this.name = name;
            this.values = values;
        }

        public void incIndex() {
            index++;
        }

        public int getIndex() {
            return index;
        }

        public void resetIndex() {
            index = 0;
        }

        public List<ParameterValue> getNextValue() {
            return Arrays.asList(new ParameterValue(name, values[index]));
        }

        public int size() {
            return values.length;
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ComposedParametersValues">
    protected static class ComposedParametersValues extends ParameterValues {

        private List<ParameterValues> parametersValues;

        public ComposedParametersValues(List<ParameterValues> parametersValues) throws BadDataException {
            this.parametersValues = parametersValues;
            int size = -1;
            for (ParameterValues parameterValues : parametersValues) {
                if (size != -1 && size != parameterValues.size()) {
                    throw new BadDataException("The parameters doesn't have the same amount of values!");
                }
                size = parameterValues.size();
            }
        }

        @Override
        public List<ParameterValue> getNextValue() {
            List<ParameterValue> result = new ArrayList<ParameterValue>(parametersValues.size());
            for (ParameterValues parameterValues : parametersValues) {
                result.addAll(parameterValues.getNextValue());
            }
            return result;
        }

        @Override
        public void incIndex() {
            for (ParameterValues parameterValues : parametersValues) {
                parameterValues.incIndex();
            }
        }

        @Override
        public int getIndex() {
            return parametersValues.get(0).getIndex();
        }

        @Override
        public int size() {
            return parametersValues.get(0).size();
        }

        @Override
        public void resetIndex() {
            for (ParameterValues parameterValues : parametersValues) {
                parameterValues.resetIndex();
            }

        }
    }
    //</editor-fold>

    /**
     * Represent the value to use for a simulation for a parameter
     */
    //<editor-fold defaultstate="collapsed" desc="ParameterValue">
    public static class ParameterValue {

        private String name;
        private Object value;

        public ParameterValue(String name, Object value) {
            this.name = name;
            this.value = value;
        }

        private ParameterValue(ParameterValues pv) {
            this.name = pv.name;
            this.value = pv.values[pv.index];
        }

        @Override
        public String toString() {
            return name + ":" + value.toString();
        }
    }
    //</editor-fold>
    private transient String inputFile;
    private int nbReplicates;
    private String baseInputFile;
    private Boolean copyInputDir;
    private File baseOutputDir;
    private List<ParameterValues> parametersValues;
    private List<List<ParameterValue>> parametersValueBySimulation;
    private List<GlobalObserver> globalObservers;
    private transient int currentSimulation, totalSimulations;

    public Exploration(int nbReplicates, String baseInputFile, String baseOutputDir) {
        this.nbReplicates = nbReplicates;
        this.baseInputFile = baseInputFile;
        this.baseOutputDir = new File(baseOutputDir);
        this.totalSimulations = nbReplicates;
    }

    public Exploration(int nbReplicates, File baseInputFile, File baseOutputDir) {
        this.nbReplicates = nbReplicates;
        this.baseInputFile = baseInputFile.getAbsolutePath();
        this.baseOutputDir = baseOutputDir;
        this.totalSimulations = nbReplicates;
    }

    public Exploration(int nbReplicates, String baseInputFile, String baseOutputDir, Boolean copyInputDir) {
        this(nbReplicates, baseInputFile, baseOutputDir);
        this.copyInputDir = copyInputDir;
    }

    public static List<List<ParameterValue>> completePlan(List<ParameterValues> parametersValues) {
        List<List<ParameterValue>> parametersValueBySimulation = new ArrayList<List<ParameterValue>>();
        boolean end = false;
        while (!end) {
            List<ParameterValue> simParameters = new ArrayList<ParameterValue>();
            for (ParameterValues pv : parametersValues) {
                simParameters.addAll(pv.getNextValue());
            }
            parametersValueBySimulation.add(simParameters);
            // prepare the next value
            for (ParameterValues pv : parametersValues) {
                pv.incIndex();
                if (pv.getIndex() == pv.size()) {
                    if (pv == parametersValues.get(parametersValues.size() - 1)) {
                        end = true;
                        break;
                    }
                    pv.resetIndex();
                } else {
                    break;
                }
            }
        }
        return parametersValueBySimulation;
    }

    public static void initLoggers() {
        try {
            LogManager.getLogManager().readConfiguration(Exploration.class.getResourceAsStream("logging-exploration.properties"));
        } catch (IOException ex) {
            LOGGER.log(Level.WARNING, "Problems to load the logging configuration file", ex);
        }
    }

    public void run() throws IOException, BadDataException, ProcessingException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, Exception {
        File outputDir = createOutputDir(baseOutputDir);
        long begin = System.currentTimeMillis();
        FileUtils.copyFileToDirectory(new File(inputFile), outputDir);
        // store timestamp
        //PrintStream timestampPs = new PrintStream(new File(outputDir, "timestamp.txt"));
        //timestampPs.println(System.currentTimeMillis());
        //timestampPs.close();
        // store input dir
        if (this.copyInputDir == null || this.copyInputDir) {
            FileUtils.copyDirectory(new File(baseInputFile).getParentFile(), new File(outputDir, "input"), new NotFileFilter(new WildcardFileFilter(".svn**")));
        }
        System.out.println("Running exploration in " + outputDir);
        if ((parametersValues == null || parametersValues.isEmpty()) && parametersValueBySimulation == null) {
            // no factors given, we'll run only replication of default value
            runReplicates(null, 0);
        } else {
            PrintStream valuesIndexStream = null;
            // complete plan

            if (parametersValueBySimulation == null) {
                parametersValueBySimulation = completePlan(parametersValues);
                // ready for running simulations
                valuesIndexStream = new PrintStream(new File(baseOutputDir, "parameters_values.csv"));
                // print simulation parameters headers
                StringBuilder buff = new StringBuilder();
                buff.append("ID");
                for (ParameterValues parameterValues : parametersValues) {
                    buff.append(";").append(parameterValues.name);
                }
                valuesIndexStream.println(buff);
            }
            // run simulations
            int index = 0;
            totalSimulations = parametersValueBySimulation.size() * nbReplicates;
            currentSimulation = 0;
            for (List<ParameterValue> runParameters : parametersValueBySimulation) {
                index++;
                if (valuesIndexStream != null) {
                    // print headers
                    StringBuilder buff = new StringBuilder();
                    buff.append(index);
                    for (ParameterValue pv : runParameters) {
                        buff.append(";").append(pv.value);
                    }
                    valuesIndexStream.println(buff);
                }
                runReplicates(runParameters, index);
            }
        }
        long elapsed = System.currentTimeMillis() - begin;
        System.out.println("Time elapsed: " + String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes(elapsed),
                TimeUnit.MILLISECONDS.toSeconds(elapsed)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(elapsed))));

    }

    public static File createOutputDir(String dirname) throws IOException, HeadlessException {
        return createOutputDir(dirname, true);
    }

    public static File createOutputDir(File dir) throws IOException, HeadlessException {
        return createOutputDir(dir, true);
    }

    public static File createOutputDir(String dirname, boolean exit) throws IOException, HeadlessException {
        return createOutputDir(new File(dirname), exit);
    }

    public static File createOutputDir(File dirname, boolean exit) throws IOException, HeadlessException {
        if (dirname.exists()) {
            String msg = "<html><body>The output directory <b>" + dirname.getAbsolutePath() + "</b> already exists!";
            if (GraphicsEnvironment.isHeadless()) {
                System.err.println(msg.replaceAll("<[^>]*>", "") + "\nPlease remove this directory or set the parameter outputDir to another place.");
                System.exit(1);
            } else {
                JLabel label = new JLabel(msg + "<br>Do you want to <b>delete</b> it " + "before to run this new exploration?");
                // set non-bold font
                Font font = UIManager.getFont("OptionPane.font");
                label.setFont(new Font(font.getFontName(), Font.PLAIN, font.getSize()));
                if (JOptionPane.showConfirmDialog(null, label, "Exploration already exists", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    FileUtils.deleteDirectory(dirname);
                } else {
                    if (exit) {
                        LOGGER.log(Level.SEVERE, msg);
                        System.exit(1);
                    }
                }
            }
        }
        dirname.mkdirs();
        return dirname;
    }

    public interface PreRunHook {

        public void process(Application application);
    }

    public List<List<File>> runReplicates(List<ParameterValue> runParameters, int index, PreRunHook... hooks) throws Exception {
        return runReplicates(runParameters, index, null, hooks);
    }

    public List<List<File>> runReplicates(List<ParameterValue> runParameters, int index, List<List<String>> outputFilenames, PreRunHook... hooks) throws Exception {
        List<List<File>> outputFiles = new ArrayList<List<File>>();
        // run replicates
        for (int i = 0; i < nbReplicates; i++) {
            List<File> currentOutputFiles = new ArrayList<File>();
            outputFiles.add(currentOutputFiles);
            List<String> currentOutputFilenames = null;
            if (outputFilenames != null) {
                currentOutputFilenames = outputFilenames.get(i);
            }
            currentSimulation++;
            LOGGER.log(Level.INFO, "Simulation {0} / {1}", new Object[]{currentSimulation, totalSimulations});
              // load default parameters
            Parameters parameters = (Parameters) Application.fromXML(new FileReader(baseInputFile));
            // set the seed and other exploration parameters
            setParam(parameters, "seedIndex", i);
            if (runParameters != null) {
                for (ParameterValue pv : runParameters) {
                    setParam(parameters, pv.name, pv.value);
                }
            }
            // disable all configured observers
            setParam(parameters, "regionObservers", new ArrayList<ObserverListener>());
            // add a global observer
            if (globalObservers != null) {
                setParam(parameters, "globalObservers", globalObservers);
            }
            int fileId = 0;
            for (GlobalObserver observer : (List<GlobalObserver>) ReflectUtils.getFieldValue("globalObservers", parameters)) {
                String name;
                if (currentOutputFilenames != null && fileId <= currentOutputFilenames.size() - 1) {
                    name = currentOutputFilenames.get(fileId);
                    fileId++;
                } else {
                    name = index + "_";
                    if (observer.getOutputFile() != null) {
                        String[] split = observer.getOutputFile().getName().split("\\.")[0].split("_");
                        name += split[split.length - 1] + ".txt";
                    } else {
                        name += fileId++ + ".txt";
                    }
                }
                File outputFile = new File(baseOutputDir, name);
                currentOutputFiles.add(outputFile);
                ReflectUtils.setField("outputFile", observer, outputFile);
                if (i == 0) {
                    ReflectUtils.setField("append", observer, false);
                } else {
                    ReflectUtils.setField("append", observer, true);
                }
            }
            // achieve init and run
            Application application = new Application(new File(baseInputFile).getParentFile(), parameters);
            application.getParameters().readResolve();
            application.loadFiles();
            for (PreRunHook hook : hooks) {
                hook.process(application);
            }
            try {
                application.run(i);
            } catch (Exception ex) {
                File errorFile = new File(baseOutputDir, index + "_input_error.xml");
                PrintWriter errorParamsWriter = new PrintWriter(errorFile);
                Application.toXML(application.getParameters(), errorParamsWriter);
                errorParamsWriter.close();
                LOGGER.log(Level.SEVERE, "The simulation #" + currentSimulation
                        + " has failed. The input file has been saved to " + errorFile.getAbsolutePath(), ex);
                System.err.flush();
            }
        }
        return outputFiles;
    }

    public void setParam(Parameters parameters, String path, Object value) throws NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        ReflectUtils.setFieldValueFromPath(parameters, path, value);
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("This program have to receive the exploration input file as unique argument.");
            System.exit(1);
        }
        XStream xstream = new XStream();
        for (Class type : new Class[]{Exploration.class, ParameterValues.class, ComposedParametersValues.class}) {
            xstream.alias(type.getSimpleName(), type);
        }
        xstream.addImplicitCollection(Exploration.class, "parametersValues", "factors", ParameterValues.class);
        xstream.addImplicitCollection(ComposedParametersValues.class, "parametersValues", "factor", ParameterValues.class);
        FileReader exploFileReader = new FileReader(args[0]);
        Exploration ex = (Exploration) xstream.fromXML(exploFileReader);
        ex.initLoggers();
        exploFileReader.close();
        ex.inputFile = args[0];
        ex.run();
    }
}
