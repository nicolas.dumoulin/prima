/*
 *  Copyright (C) 2012 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import com.thoughtworks.xstream.annotations.XStreamImplicit;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObservablesHandler.ObservableFetcher;
import fr.cemagref.observation.observers.CSVObserver;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * <p>This class can observe several observables on a period of time, between two dates.
 * The values of each observables will be cumulated over the period and printed at the end of the period.
 * 
 * <p>Example of XML paremterization:
 *         <pre>
 *      &lt;fr.cemagref.prima.regionalmodel.PeriodicCSVObserver>
 *           &lt;sysout>false&lt;/sysout>
 *           &lt;outputFile>../dataOut/cantalOut2.csv&lt;/outputFile>
 *           &lt;separator>;&lt;/separator>
 *           &lt;period>
 *               &lt;start>1990&lt;/start>
 *               &lt;end>1993&lt;/end>
 *           &lt;/period>
 *           &lt;observables>
 *               &lt;string>getPopulationSize&lt;/string>
 *               &lt;string>getMigratoryBalance&lt;/string>
 *           &lt;/observables>
 *       &lt;/fr.cemagref.prima.regionalmodel.PeriodicCSVObserver></pre>
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class PeriodicCSVObserver extends CSVObserver {

    @XStreamImplicit(itemFieldName = "period")
    private List<Period> period;
    private transient Map<Period, Map<ObservablesHandler.ObservableFetcher, Integer>> valuesByPeriod;

    @Override
    public void init() {
        super.init();
        valuesByPeriod = new TreeMap<Period, Map<ObservablesHandler.ObservableFetcher, Integer>>();
        for (Period p : period) {
            valuesByPeriod.put(p, new TreeMap<ObservablesHandler.ObservableFetcher, Integer>());
        }
    }

    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long date) {
        for (Period p : period) {
            if (p.contains(date)) {
                Map<ObservableFetcher, Integer> values = valuesByPeriod.get(p);
                for (ObservableFetcher fetcher : fetchers) {
                    Integer value = (Integer) super.getValue(fetcher, instance);
                    if (date == p.start) {
                        values.put(fetcher, value);
                    } else {
                        values.put(fetcher, values.get(fetcher) + value);
                    }

                }
                if (date == p.end) {
                    StringBuffer buf = new StringBuffer();
                    StringBuffer sbSeparator = new StringBuffer(" " + this.separator + " ");
                    // print current Time
                    buf.append(p.start).append("-").append(p.end);
                    // print value of each field
                    for (ObservablesHandler.ObservableFetcher fetcher : fetchers) {
                        buf.append(sbSeparator);
                        Object value = values.get(fetcher);
                        buf.append(value == null ? "N/A" : value);
                    }
                    outputStream.println(buf);
                }
            }
        }
    }

    static class Period {

        int start, end;

        boolean contains(long date) {
            return date >= start && date <= end;
        }
    }

    public static void main(String[] args) {
        PeriodicCSVObserver periodicCSVObserver = new PeriodicCSVObserver();
        periodicCSVObserver.period = new ArrayList<Period>();
        Period period = new Period();
        periodicCSVObserver.period.add(period);
        period.start = 1990;
        period.end = 1999;
        Application.toXML(periodicCSVObserver, System.out);
    }
}
