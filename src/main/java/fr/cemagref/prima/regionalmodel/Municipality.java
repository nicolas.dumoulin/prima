/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.prima.regionalmodel;

import fr.cemagref.prima.regionalmodel.parameters.MunicipalityParameters;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.cemagref.prima.regionalmodel.tools.Updatable;
import fr.cemagref.prima.regionalmodel.tools.BadDataException;
import fr.cemagref.prima.regionalmodel.Individual.Status;
import fr.cemagref.prima.regionalmodel.tools.Random;
import fr.cemagref.prima.regionalmodel.dynamics.Dynamics;
import fr.cemagref.prima.regionalmodel.scenarios.TableUpdater.UpdateMode;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class Municipality implements Comparable<Municipality> {

    private final Logger myLogger;
    private String name;
    /**
     * Geographical coordinates
     */
    private double lon, lat;
    
    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
     * Compute the Euclidian distance from the geographical coordinates
     */
    public double euclidianDistanceFrom(Municipality mun) {
        return Math.sqrt(Math.pow(this.getLon() - mun.getLon(), 2) + Math.pow(this.getLat() - mun.getLat(), 2));
    }

    /**
     * Return the distance in the space of the neighbouring of the
     * municipalities
     */
    public double distanceFrom(Municipality mun) {
        double dist = Double.MAX_VALUE;
        // take the network of this
        SortedMap<Double, MunicipalitiesNetwork.SubNetwork> netRes = getMyRegion().getLabourOffice().getNetwork().getSubNetwork(this);
        int nbKeysR = netRes.size();
        Double[] probasRes = new Double[nbKeysR];
        for (Map.Entry<Double, MunicipalitiesNetwork.SubNetwork> entry : netRes.entrySet()) {
            nbKeysR--;
            probasRes[nbKeysR] = (entry.getKey()).doubleValue();
        }
        // read the network of this to find out mun
        for (int i = 0; i < netRes.size(); i++) {
            List<Municipality> munTemp = null;
            munTemp = (netRes.get(probasRes[i])).getMunicipalities();
            if (munTemp.contains(mun)) {
                dist = (netRes.get(probasRes[i])).getDistance() ;
                break;
            }
        }
        //System.err.println("distance "+dist+" mun dep "+this.getName()+" mun param "+mun.getName()) ;
        return dist ;
    }
    /**
     * It stores the municipalies considered as neighbour for each type of
     * Service. The proximity for an hospital doesn't mean the same than for a
     * bakery. It isn't used for instance.
     */
    protected Map<Service, Municipality[]> proximityByService;
    protected List<Dynamics<Municipality>> dynamics;
    /**
     * Total surface avaiable in the village which is dedicated for agriculture
     * (farms).
     */
    private double totalSurface;
    /**
     * Distance to the border of the municipality set
     */
    private int distanceToBorder;
    /**
     * This vector shows the total number of residences, for each type of
     * residence in the village. For example, let's consider that were defined
     * three diferent types of residences: <ul><li>type 1: residences with 1 or
     * 2 bedrooms <li>type 2: residences with 3 or 4 bedrooms <li>type 3:
     * residences with more than 4 bedrooms</ul> In this example, the vector has
     * dimension 3, so: <ul><li>residencesOffer[0]: number of residences of type
     * 1; <li>residencesOffer[1]: number of residences of type 2;
     * <li>residencesOffer[2]: number of residences of type 3.
     */
    private int[] residencesOffer;
    private List<Residence>[] freeResidences;
    private List<Residence>[] occupiedResidences;
    /**
     * Number of individuals living in the village
     */
    private int populationSize;
    private int populationSizeAtInit;
    /**
     * Table containing the level of occupation activities of the activities for
     * the village (constrained by the availability) It is affected by the fact
     * individuals have patterns of activity using or not the activities This
     * corresponds only to the activities occupied by the external individuals
     * to the village but at the same time living in the considered region
     */
    public int[] occupiedActivitiesByOutside;
    /**
     * Table containing the level of occupation activities of the activities for
     * the village (constrained by the availability) It is affected by the fact
     * individuals have patterns of activity using or not the activities This
     * corresponds only to the activities occupied by the external individuals
     * to the village but at the same time living in the considered region
     */
    public float[] occupiedActivitiesByExt;
    /**
     * Table containing the level of occupation activities of the activities for
     * the village (constrained by the availability) It is affected by the fact
     * individuals have patterns of activity using or not the activities This
     * corresponds only to the activities occupied by the residents of the
     * village
     */
    public float[] occupiedActivitiesByRes;
    /**
     * Table containing the offered (ex called availability) activities in the
     * village. An activity is a job in the model. It corresponds to the
     * potential of employments offered by the village It is affected by the
     * political and economical decision at the village and higher levels It
     * does not include the employment available in the accessible villages or
     * cities
     */
    private int[] totalOfferedJob;
    /**
     * Table containing the endogeneous offered (ex called availability)
     * activities in the village. An activity is a job in the model. It
     * corresponds to the potential of employments offered by the village It is
     * affected by the political and economical decision at the village and
     * higher levels It does not include the employment available in the
     * accessible villages or cities. This part describes employments linked to
     * the presence of people in a given place. It is updated endogeneously via
     * a function proposed by Lenormand and al (2011). See
     * computeDynamicEmployServices() in MunicipalitySet
     */
    public int[] endogeneousOfferedJob;
    /**
     * Table containing the exogeneous offered (ex called availability)
     * activities in the village. An activity is a job in the model. It
     * corresponds to the potential of employments offered by the village It is
     * affected by the political and economical decision at the village and
     * higher levels It does not include the employment available in the
     * accessible villages or cities. This part is updated by the scenario.
     */
    public int[] exogeneousOfferedJob;

    public int[] getEndogeneousOfferedJob() {
        return endogeneousOfferedJob;
    }

    public void setEndogeneousOfferedJob(int[] endogeneousOfferedJob) {
        this.endogeneousOfferedJob = endogeneousOfferedJob;
    }

    public int[] getExogeneousOfferedJob() {
        return exogeneousOfferedJob;
    }

    public void setExogeneousOfferedJob(int[] exogeneousOfferedJob) {
        this.exogeneousOfferedJob = exogeneousOfferedJob;
    }
    /**
     * Parameters for the function making dynamic the employment offer in the
     * service sector: slope
     */
    private double slope;
    /**
     * Parameters for the function making dynamic the employment offer in the
     * service sector: intercept
     */
    private double intercept;

    public double getIntercept() {
        return intercept;
    }

    public double getSlope() {
        return slope;
    }
    /**
     * Table of Villages containing instance of Villages accessible to this
     * Municipality
     */
    private Municipality[] proximityJob;
    private double commutOutsideProba;
    /**
     * close municipalities for partner searching
     */
    private List<Municipality> closeMunicipalities;
    /**
     * Table containing the households living in the village
     */
    protected List<Household> myHouseholds;
    private List<Individual>[] myWorkers;
    private MunicipalitySet myRegion;

    public Logger getLogger() {
        return myLogger;
    }

    public void init() throws BadDataException {
        for (Dynamics<Municipality> dyn : dynamics) {
            dyn.init(this);
        }
    }

    public void step(int iter) throws ProcessingException {
        for (Dynamics<Municipality> dyn : dynamics) {
            dyn.step(this, iter);
        }
    }

    public String getName() {
        return name;
    }

    public Iterable<Residence> getOccupiedResidences(int type) {
        return occupiedResidences[type];
    }

    public Residence pickAnOccupiedResidence(int type) {
        return occupiedResidences[type].get(getRandom().nextInt(0, occupiedResidences[type].size() - 1));
    }

    public int getOccupiedResidencesCount(int type) {
        return occupiedResidences[type].size();
    }

    /**
     * Find the closest municipality to the current one using the euclidien
     * distance.
     *
     * @param mun1
     * @param mun2
     * @return
     */
    public Municipality closest(Municipality mun1, Municipality mun2) {
        double dist1 = Math.pow(mun1.lon - this.lon, 2) + Math.pow(mun1.lat - this.lat, 2);
        double dist2 = Math.pow(mun2.lon - this.lon, 2) + Math.pow(mun2.lat - this.lat, 2);
        return dist1 < dist2 ? mun1 : mun2;
    }

    public void updatePopSize() {
        int pop = 0;
        for (Household hs : getMyHouseholds()) {
            if (!hs.getResidence().isTransit()) {
                if (!hs.isJustSuppressed()) {
                    pop = pop + hs.listOfMembers.size();
                }
            }
        }
        this.populationSize = pop;
    }

    public int getPopulationSize() {
        return populationSize;
    }

    public int getPopulationSizeAtInit() {
        return populationSizeAtInit;
    }

    public int[] getOccupiedActivitiesByOutside() {
        return occupiedActivitiesByOutside;
    }

    public int getOccupiedActivitiesByOutside(int index) {
        return occupiedActivitiesByOutside[index];
    }

    @Updatable
    /*
     * public void updateOccupiedActivitiesByOutside(Integer category, Integer
     * value, UpdateMode mode) { int delta =
     * UpdateMode.computeDelta(occupiedActivitiesByOutside[category], value,
     * mode); if (occupiedActivitiesByOutside[category] + delta < 0) { throw new
     * UnsupportedOperationException("Error in activities scenario: delta can be
     * " + "greatest than the current value (category=" + category + ", value="
     * + occupiedActivitiesByOutside[category] + ", delta=" + delta + ")"); }
     * else { occupiedActivitiesByOutside[category] += delta; } }
     */
    public float[] getOccupiedActivitiesByExt() {
        return occupiedActivitiesByExt;
    }

    public float getOccupiedActivitiesByExt(int index) {
        return occupiedActivitiesByExt[index];
    }

    public float[] getOccupiedActivitiesByRes() {
        return occupiedActivitiesByRes;
    }

    public float getOccupiedActivitiesByRes(int index) {
        return occupiedActivitiesByRes[index];
    }

    public int[] getOfferedActivities() {
        return totalOfferedJob;
    }

    public int getOfferedActivities(int index) {
        return totalOfferedJob[index];
    }

    public void setOfferedActivities(int[] offeredActivities) {
        this.totalOfferedJob = offeredActivities;
    }

    public void setOfferedActivities(int i, int offeredActivities) {
        this.totalOfferedJob[i] = offeredActivities;
    }
    /*
     * @Updatable public void updateOfferedActivities(Integer category, Integer
     * value, UpdateMode mode) { int delta =
     * UpdateMode.computeDelta(totalOfferedJob[category], value, mode); if
     * (totalOfferedJob[category] + delta < 0) { throw new
     * UnsupportedOperationException("Error in activities scenario: delta can be
     * " + "greatest than the current value (category=" + category + ", value="
     * + totalOfferedJob[category] + ", delta=" + delta + ")"); } else {
     * totalOfferedJob[category] += delta; } }
     */

    @Updatable
    public void updateOfferedActivities(Integer category, Integer value, UpdateMode mode) {
        // begin by fired those who occupied a non free job (in case boolean activeFire is not true in the iteration method of the MunicipalitySet)
        fire(false); // false means that it is not the initialisation time
        getMyRegion().getLabourOffice().updateAvailabilities(this);
        int toCreateOutside = 0;
        float tot = 0.0f;
        int delta = UpdateMode.computeDelta(exogeneousOfferedJob[category], value, mode);
        /*
         * System.err.println(getName()+" size of pop "+getPopulationSize() +"
         * endo "+Arrays.toString(getEndogeneousOfferedJob())+" exo
         * "+Arrays.toString(getExogeneousOfferedJob())+ "
         * total"+Arrays.toString(getOfferedActivities())) ;
         */
        if (exogeneousOfferedJob[category] + delta < 0) {
            throw new UnsupportedOperationException("Error in activities scenario: delta can be "
                    + "greatest than the current value (category=" + category + ", value="
                    + exogeneousOfferedJob[category] + ", delta=" + delta + ")");
        } else {
            if (delta > 0) {
                // then employment creation to share between occupation by people from outside and potential occupation (offer) by insiders
                tot = getOccupiedActivitiesByOutside(category) + getOccupiedActivitiesByRes(category) + getOccupiedActivitiesByExt(category);
                if (tot == 0 && getOccupiedActivitiesByOutside(category) == 0) {
                    if (delta == 1) { // try to avoid the bias consisting in putting all the job offer occupied by outside
                        if (getRandom().nextDouble() > 0.5) {
                            toCreateOutside = 0;
                        } else {
                            toCreateOutside = 1;
                        }
                    } else {
                        toCreateOutside = Math.round(delta * 0.5f);
                    }
                } else {
                    if ((tot == getOccupiedActivitiesByOutside(category) * 2) && delta == 1) {
                        if (getRandom().nextDouble() > 0.5) {
                            toCreateOutside = 0;
                        } else {
                            toCreateOutside = 1;
                        }
                    } else {
                        toCreateOutside = Math.round(delta * ((float) getOccupiedActivitiesByOutside(category) / tot));
                    }
                }
                occupiedActivitiesByOutside[category] = getOccupiedActivitiesByOutside(category) + toCreateOutside;
            }
            exogeneousOfferedJob[category] += delta;
            totalOfferedJob[category] = exogeneousOfferedJob[category] + endogeneousOfferedJob[category];
        }
    }

    private Random getRandom() {
        return getMyRegion().getMyApplication().getRandom();
    }

    public Municipality[] getProximityJob() {
        return proximityJob;
    }

    public void setProximityJob(Municipality... proximityJob) {
        this.proximityJob = proximityJob;
    }

    public double getCommutOutsideProba() {
        return commutOutsideProba;
    }

    public void setCommutOutsideProba(double commutOutsideProba) {
        this.commutOutsideProba = commutOutsideProba;
    }

    /**
     * List of municipalities that are considered close to the current one
     * according to the parameter {@code farCloseThreshold}
     *
     * @return
     */
    public List<Municipality> getCloseMunicipalities() {
        return closeMunicipalities;
    }

    public List<Household> getMyHouseholds() {
        return myHouseholds;
    }

    public List<Individual>[] getMyWorkers() {
        return myWorkers;
    }

    public Household findHouseholdById(long id) {
        for (Household hh : myHouseholds) {
            if (hh.getId() == id) {
                return hh;
            }
        }
        return null;
    }

    public boolean removeWorker(Individual ind) {
        return myWorkers[ind.getAgeToEnterOnTheLabourMarket()].remove(ind);
    }

    public boolean addWorker(Individual ind) {
        int age = ind.getAgeToEnterOnTheLabourMarket();
        if (age < 0) {
            age = 0;
        }
        if (myWorkers[age] == null) {
            myWorkers[age] = new LinkedList<Individual>();
        }
        return myWorkers[age].add(ind);
    }

    public MunicipalitySet getMyRegion() {
        return myRegion;
    }

    public void setMyRegion(MunicipalitySet myRegion) {
        this.myRegion = myRegion;
    }
    private MunicipalityCounters counters;

    public void resetCounters() {
        counters.resetCounters();
    }

    public MunicipalityCounters getCounters() {
        return counters;
    }
    /**
     * name of file descriptif the parameter of the village
     */
    String[] ficParameter;

    public String[] getFicParameter() {
        return ficParameter;
    }

    public void setFicParameter(String[] ficParameter) {
        this.ficParameter = ficParameter;
    }

    private Municipality(String name) {
        this.name = name;
        this.counters = new MunicipalityCounters(this);
        this.myLogger = Logger.getLogger(Municipality.class.getName() + "." + name);
    }

    /**
     * The constructor for the village where we temporarily stocked people who
     * want to probabilisticMove
     */
    public Municipality(MunicipalitySet myReg, String name) {
        this(name);
        this.myRegion = myReg;
        // first index is for unemployed people
        this.totalOfferedJob = new int[myRegion.getNbActivities()];
        Arrays.fill(this.totalOfferedJob, 0);
        this.myWorkers = new List[200];
        initOccupiedActivitiesCounters();
    }

    /**
     * Constructor for tests
     *
     * @param name
     * @param availabActiv
     */
    public Municipality(String name, int[] availabActiv) {
        this(name);
        this.totalOfferedJob = availabActiv;
        initOccupiedActivitiesCounters();
    }

    private void initOccupiedActivitiesCounters() {
        occupiedActivitiesByExt = new float[totalOfferedJob.length];
        Arrays.fill(occupiedActivitiesByExt, 0.0f);
        occupiedActivitiesByRes = new float[totalOfferedJob.length];
        Arrays.fill(occupiedActivitiesByRes, 0.0f);
        occupiedActivitiesByOutside = new int[totalOfferedJob.length];
        Arrays.fill(occupiedActivitiesByOutside, 0);
    }

    public int getResidencesTypeCount() {
        return residencesOffer.length;
    }

    public int getResidenceOffer(int i) {
        return residencesOffer[i];
    }

    @Updatable
    protected void updateAvailableResidence(Integer category, Integer value, UpdateMode mode) {
        int delta = UpdateMode.computeDelta(residencesOffer[category], value, mode);
        if (residencesOffer[category] + delta < 0) {
            throw new UnsupportedOperationException("Error in residence scenario: delta can be "
                    + "greatest than the current value (category=" + category + ", value="
                    + residencesOffer[category] + ", delta=" + delta + ")");
        } else {
            residencesOffer[category] += delta;
            if (delta > 0) {
                // add residences
                for (int i = 0; i < delta; i++) {
                    freeResidences[category].add(new Residence(category));
                }
            } else if (delta < 0) {
                int nb = Math.abs(delta);
                if (nb > freeResidences[category].size()) {
                    // remove residences
                    freeResidences[category].clear();
                } else {
                    for (int i = 0; i < nb; i++) {
                        // remove residences
                        freeResidences[category].remove(0);
                    }
                }
            }
        }
    }

    @Updatable
    protected void updateAvailableResidence() {
        List<Residence> redundantHouses = new ArrayList<Residence>();
        for (int cat = 0; cat < getResidencesTypeCount(); cat++) {
            int redundantCount = occupiedResidences[cat].size() - residencesOffer[cat];
            if (redundantCount > 0) {
                redundantHouses.addAll(getRandom().pickElements(occupiedResidences[cat], redundantCount));
            }
        }
        int freeCat = 0;
        // Now we will try to find a new house for hh before to expulse them definitively
        List<Household> expulsed = new ArrayList<Household>();
        for (Residence redundantHouse : redundantHouses) {
            for (Household hh : redundantHouse) {
                while (freeCat < getResidencesTypeCount() && (freeResidences[freeCat].isEmpty())) {
                    freeCat++;
                }
                if (freeCat == getResidencesTypeCount()) {
                    // no more residence available in the municipality
                    // the household is expulsed
                    getLogger().log(Level.FINER, "Expulsion of HH {0}", hh.getId());
                    occupiedResidences[hh.getResidence().getType()].remove(hh.getResidence());
                    expulsed.add(hh);
                    hh.setJustSuppressed(true);
                    for (Individual ind : hh) {
                        if (ind.getMyActivityPattern() != null) {
                            ind.leaveMyActivPattern();
                        }
                    }
                    counters.incNbHouseholdExpulsed(1);
                    counters.incNbIndividualsExpulsed(hh.size());
                } else {
                    // we move the household in a free residence
                    hh.setNextSizeOfResidence(freeCat);
                    occupiedResidences[hh.getResidence().getType()].remove(hh.getResidence());
                    takeResidence(hh);
                }
            }
        }
        // suppression at the end for avoiding concurrent modification exceptions
        //if (expulsed.size() > 0) System.err.println(getName()+" nb expulsés "+expulsed.size()) ;
        for (Household hh : expulsed) {
            // we should remove them from the residence for avoiding their processing
            // in the suppresion of households at the end of the iteration, for
            // controlling entirely the removing of the residence
            hh.getResidence().remove(hh);
        }
    }

    public List<Residence> getFreeResidences(int type) {
        return freeResidences[type];
    }

    public boolean hasFreeResidences(int type) {
        return !freeResidences[type].isEmpty();
    }

    public int getFreeResidencesCount(int type) {
        return freeResidences[type].size();
    }

    public double getTotalSurface() {
        return totalSurface;
    }

    public void setTotalSurface(double val) {
        this.totalSurface = val;
    }

    public int getDistanceToBorder() {
        return distanceToBorder;
    }

    public void setDistanceToBorder(int distanceToBorder) {
        this.distanceToBorder = distanceToBorder;
    }
    public int[] patternOccupancy;

    public void setPatternOccupancy(int[] patternOccupancy) {
        this.patternOccupancy = patternOccupancy;
    }

    public void setPatternOccupancy(int i, int val) {
        this.patternOccupancy[i] = val;
    }

    public void incPatternOccupancy(Integer i, Integer delta) {
        this.patternOccupancy[i] += delta;
    }

    @Updatable
    public void updatePatternOccupancy(Integer i, Integer value, UpdateMode mode) {
        this.patternOccupancy[i] += UpdateMode.computeDelta(this.patternOccupancy[i], value, mode);
    }

    public int[] getPatternOccupancy() {
        return patternOccupancy;
    }

    /**
     * This function allows to represent specific rural policies for the
     * village. For example, by this function it could be redefined the total
     * surface for agriculture available in the village. Further others rules
     * can be manage by this function. So, it represents the dynamics of
     * changing the available land for a specific use.
     */
    public void changeLandUse() {
    }

    /**
     * This method manage with the eventual fire after the application of the
     * scenario which can have been reduced the number of job offered for an
     * activity)
     *
     * @author S. Huet - 16.03.2010
     */
    public int fire(boolean init) {
        float[] nbToFire = computeNbOfIndivToFire();
        int i = 0;
        if (this.isOutside() && init) { // every outside commuters is fired at the beginning allowing a good redistribution of the occupied csp compared to the csp job offer
            for (i = 0; i < getOfferedActivities().length; i++) {
                nbToFire[i] = -getOccupiedActivities()[i];
            }
        }
        int totalToFire = 0;
        float probaFireOutside = 0.0f;
        int nb = 0;
        int toFireOutside = 0;
        for (i = 0; i < getOfferedActivities().length; i++) {
            if (nbToFire[i] != 0) {
                List<Individual> indiv = possibleIndivToFire(i);
                probaFireOutside = (float) getOccupiedActivitiesByOutside(i) / (float) (getOccupiedActivitiesByOutside(i)
                        + getOccupiedActivitiesByRes(i) + getOccupiedActivitiesByExt(i));
                toFireOutside = Math.round(-nbToFire[i] * probaFireOutside);
                occupiedActivitiesByOutside[i] = getOccupiedActivitiesByOutside(i) - toFireOutside;
                nbToFire[i] = nbToFire[i] + toFireOutside;
                totalToFire += nbToFire[i];
                while (nbToFire[i] < 0) {
                    // fire a randomly chosen individual
                    nb = getRandom().nextInt(0, indiv.size() - 1);
                    Individual selectedIndividual = indiv.get(nb);
                    selectedIndividual.setLastFiringMun(this);
                    selectedIndividual.setLastFiringYear((short) myRegion.getCurrentYear());
                    // if it is monoactive, it becomes unemployed
                    if (!selectedIndividual.willBeInactive()) {
                        selectedIndividual.leaveMyActivPattern();
                        selectedIndividual.becomeUnemployed(selectedIndividual.getMyHousehold().getMyVillage());
                        if (init) { // The fire is done to solve the fact that the individual has at the beginning a profession in a place and a sector where this profession is not available
                            selectedIndividual.setLookForJob(true);
                            selectedIndividual.setSearchedProf(selectedIndividual.getProfession());
                        }
                        indiv.remove(nb);
                        nbToFire[i] = nbToFire[i] + 1.0f;
                    }
                }
            }
        }
        for (Household hh : getMyHouseholds()) {
            for (Individual ind : hh) {
                if (ind.willBeInactive()) {
                    ind.leaveMyActivPattern();
                    ind.becomeUnactive(this);
                }
            }
        }
        return totalToFire;
    }

    public int fireInit() {
        int totalFired = 0;
        //for (i = 0; i < getOfferedActivities().length; i++) {
        for (Household hsh : getMyHouseholds()) {
            for (Individual commuter : hsh.getAdults()) {
                // fire every commuters in order to find out a correct place to work in
                if (commuter.getStatus() == Status.WORKER && commuter.getJobLocation() != hsh.getMyVillage()) {
                    commuter.setLastFiringMun(this);
                    commuter.setLastFiringYear((short) myRegion.getCurrentYear());
                    // if it is monoactive, it becomes unemployed
                    if (!commuter.willBeInactive()) {
                        commuter.leaveMyActivPattern();
                        commuter.becomeUnemployed(commuter.getMyHousehold().getMyVillage());
                        commuter.setLookForJob(true);
                        commuter.setSearchedProf(commuter.getProfession());
                        totalFired++;
                    }
                }
            }
        }
        return totalFired;
    }

    /**
     * This method compute, for each offered activities, the number of
     * individuals it is necessary to fire after the application of the scenario
     * at a given iteration (corresponds to a change of municipal policy
     * regarding employment)
     *
     * @author S. Huet - 16.03.2010
     */
    public float[] computeNbOfIndivToFire() {
        int i = 0;
        float[] nbToFire = new float[getOfferedActivities().length];
        Arrays.fill(nbToFire, 0);
        for (i = 0; i < getOfferedActivities().length; i++) {
            if (!getMyRegion().getParameters().getFilterFireOnActivity(i)) {
                nbToFire[i] = getOfferedActivities()[i] - getOccupiedActivitiesByExt()[i] - getOccupiedActivitiesByRes()[i] - getOccupiedActivitiesByOutside()[i];
                if (nbToFire[i] > 0) {
                    nbToFire[i] = 0;
                }
            }
        }
        return nbToFire;
    }

    /**
     * Return the list of municipalities that have this municipality as
     * proximityJob.
     *
     * @return
     */
    public List<Municipality> getProximityJobOrigin() {
        List<Municipality> muns = new ArrayList<Municipality>(proximityJob.length);
        for (Municipality mun : myRegion.getMyMunicipalities()) {
            for (Municipality proxMun : mun.getProximityJob()) {
                if (proxMun == this) {
                    muns.add(mun);
                    break;
                }
            }
        }
        for (Municipality mun : myRegion.getOutsides()) {
            for (Municipality proxMun : mun.getProximityJob()) {
                if (proxMun == this) {
                    muns.add(mun);
                    break;
                }
            }
        }
        return muns;
    }

    /**
     * This method compute the list of individuals susceptible to be fired
     * because they have an activity which has been suppressed by the scenario
     * and they do it in the present municipality
     *
     * @param activ integer representing the activity position in the offered
     * activity vector
     * @author S. Huet - 16.03.2010
     */
    private List<Individual> possibleIndivToFire(int activ) {
        List<Individual> indiv = new ArrayList<Individual>();
        for (List<Individual> workers : myWorkers) {
            if (workers != null) {
                for (Individual individual : workers) {
                    if (individual.getMyActivityPattern().getJob() == activ) {
                        indiv.add(individual);
                    }
                }
            }
        }
        return indiv;
    }

    /**
     * Method initHouseholds with a residential status The parameters to init
     * households are: First version: S. Huet, 28.07.2009
     */
    public void initHouseholds(Integer[][] myHsh) {
        // Initialize household
        myHouseholds = new LinkedList<Household>();
        for (Integer[] hh : myHsh) {
            myHouseholds.add(new Household(this, hh));
        }
        for (int index : getRandom().randomList(myHsh.length)) {
            myHouseholds.get(index).initResidence(); // attribution of a residence since the households are initialised without a residence (status = -1)
            // if the residence have not been found then add attach the household to an existing residence
            if (myHouseholds.get(index).getResidence() == null) {
                myHouseholds.get(index).attachToExistingResidence();
            }
        }
    }

    public int getNbOfUnsatisfiedByRes() {
        int nbOfUnsatisfiedByRes = 0;
        for (Household hh : myHouseholds) {
            if (!hh.isJustSuppressed()) {
                if (hh.needToChangeRes(appropriatedSizeOfRes(hh.size()))) {
                    nbOfUnsatisfiedByRes++;
                }
            }
        }
        return nbOfUnsatisfiedByRes;
    }

    /**
     * Method updating the households age Last version: S. Huet, 12.03.2010
     */
    public void updateHouseholdAges() {
        for (Household hh : new ArrayList<Household>(myHouseholds)) {
            // For each individual in the Household
            if (!hh.getResidence().isTransit()) { // Potential migrants don't age
                for (Individual ind : hh.getCopyOfMembers()) {
                    ind.update(this);
                }
            }
        }
    }

    /**
     * Method which gave the appropriated size of flat (in term of index in the
     * matrix of available residence for a given size of household For France:
     * we consider the average rule of ideal flat for a given household is (1
     * room by individual) + 1 room: for example: an household of 3 individuals
     * needs an ideal flat of (3*1)+1 = 4 rooms (NB : the INSEE counts all the
     * rooms of the flat (i.e. the kitchen is a room if it has a surface of at
     * least 12 m2) First version: S. Huet, 11.04.2009
     */
    public int appropriatedSizeOfRes(int sizeHsh) {
        int index = getMyRegion().getCapacityOfAvailRes().length - 1;
        // Consider the number of class of size of household divised by the
        // number of availability
        for (int i = 0; i < getMyRegion().getCapacityOfAvailRes().length; i++) {
            if (sizeHsh <= getMyRegion().getCapacityOfAvailRes(i)) {
                index = i;
                i = getMyRegion().getCapacityOfAvailRes().length;
            }
        }
        return index;
    }

    /**
     * Method init the village First version: S. Huet, 28.07.2009 Changed: S.
     * Huet 12.10.09
     */
    public void initVillage(MunicipalityParameters parameters, int popSize,
            Integer[][] myHsh, String[][] myIndivActiv, String[] proxJob, SortedMap<Double, Municipality> neighbours) {
        myLogger.log(Level.FINEST, "Init municipality {0}", name);
        setTotalSurface(parameters.getTotalSurface());
        this.populationSize = popSize;
        this.populationSizeAtInit = popSize;
        this.lon = parameters.getLon();
        this.lat = parameters.getLat();
        this.slope = parameters.getSlope();
        this.intercept = parameters.getIntercept();
        this.closeMunicipalities = new ArrayList<Municipality>();
        for (Map.Entry<Double, Municipality> entry : neighbours.entrySet()) {
            if (entry.getKey() > (double) getMyRegion().getParameters().getFarCloseThreshold()) {
                break;
            }
            this.closeMunicipalities.add(entry.getValue());
        }
        initVillageResidence(parameters.getResidenceOffer());
        initJobProximity(proxJob);
        initHouseholds(myHsh);
        initIndividualPatterns(myIndivActiv);
    }

    /**
     * Method to finish the initialisation of the individuals after each
     * individual has one or two parents identified - 24.11.2011
     */
    public void finishToInitMunicipality() {
        for (Household hsh : getMyHouseholds()) {
            for (Individual ind : hsh) {
                //ind.initIndividual();
                if (ind.getAgeToEnterOnTheLabourMarket() == -1) {
                    ind.initAgeToEnterOnLabourMarket();
                }
                ind.initAgeToDie();
                if (ind.getStatus() == Status.STUDENT) {
                    if (ind.getAge() >= ind.getAgeToEnterOnTheLabourMarket()) {
                        ind.updateAgeLabourMarket();
                    }
                }
            }
            // The affectation of a first profession to those who has not already got a profession has to be done
            // since every individual have already an age of entering on the labour market and most of them has a profession
            for (Individual ind : hsh) {
                if (ind.getProfession() == -1) {
                    if (ind.getStatus() == Status.STUDENT) {
                        ind.affectAFirstProfession();
                    } else {
                        // for retired and inactive, the profession is attributed at random
                        // TODO it would be better to affect a profession which is picked out from the distribution of profession in the active population
                        ind.setProfession((int) (getRandom().nextDouble() * 6.0));
                    }
                }
                if (ind.getAgeToGoOnRetirement() == -1) {
                    if (ind.getStatus() != Status.RETIRED) {
                        ind.initAgeToGoOnRetirement();
                    }
                }
                if (ind.getStatus() != Status.RETIRED) {
                    if (ind.getAge() > ind.getAgeToGoOnRetirement()) {
                        ind.updateAgeRetirement();
                    }
                } else {
                    if (ind.getStatus() == Status.RETIRED) {
                        ind.setAgeToGoOnRetirement(ind.getAge() - 1);
                    }
                }
            }

        }
    }

    /**
     * Method to init the village representing the outside of the rural part of
     * the region and the outside of the region First version: S. Huet,
     * 07.10.2009
     */
    public void initOutside(String livingHsh[][], int[] availRes, int[] jobOffers) {
        myHouseholds = new LinkedList<Household>();
        initVillageResidence(availRes);
        setOfferedActivities(jobOffers);
        endogeneousOfferedJob = new int[getOfferedActivities().length];
        exogeneousOfferedJob = new int[getOfferedActivities().length];
        Arrays.fill(endogeneousOfferedJob, 0);
        Arrays.fill(exogeneousOfferedJob, 0);
    }

    /**
     * Method calculating the total number of people living in the bunch
     * (municipality set) and working in this municipality
     */
    public String giveJobsOccupiedByInsideRegionPeople() {
        float[] occupiedInside = new float[getOccupiedActivitiesByExt().length];
        StringBuilder buff = new StringBuilder("Jobs occupied by people living inside the region and working in " + getName() + ": ");
        for (int i = 0; i < getOccupiedActivitiesByExt().length; i++) {
            occupiedInside[i] = getOccupiedActivitiesByExt()[i] + getOccupiedActivitiesByRes()[i];
            buff.append(occupiedInside[i]).append("\t");
        }
        return buff.toString();
    }

    /**
     * Method to init the list of municipality which are close to this one
     * regarding the job purpose S. HUET 19.05.2010
     */
    private void initJobProximity(String[] jobProx) {
        //proximityJob = new Municipality[jobProx.length];
        ArrayList<Municipality> proxList = new ArrayList<Municipality>();
        for (int i = 0; i < jobProx.length; i++) {
            if (!this.getName().equals(jobProx[i])) {
                Municipality mun = getMyRegion().getMunicipality(jobProx[i]);
                if (mun != null) {
                    proxList.add(mun);
                }
            }

        }
        proximityJob = proxList.toArray(new Municipality[0]);
    }

    /**
     * Method initIndividualPatterns Associate at each worker or unemployed an
     * activity pattern following data given in a file The format of the file is
     * described in a doc file "Instructions for the activity file.doc" S. HUET
     * 8.10.2009
     */
    private void initIndividualPatterns(String[][] myIndivActiv) {
        int k = 0;
        String[][] myHshPats;
        int size = getMyHouseholds().size();
        for (int i = 0; i < size; i++) {
            Household hh = getMyHouseholds().get(i);
            myHshPats = new String[hh.size()][myIndivActiv[0].length];
            for (int j = 0; j < hh.size(); j++) {
                System.arraycopy(myIndivActiv[k], 0, myHshPats[j], 0, myIndivActiv[0].length);
                k++;
            }
            hh.initPatternsOfMyInd(myHshPats);
        }
    }

    /**
     * Initialisation of the village residence offer
     */
    public void initVillageResidence(int[] residenceOffer) {
        residencesOffer = new int[residenceOffer.length];
        freeResidences = new List[residenceOffer.length];
        occupiedResidences = new List[residenceOffer.length];
        for (int i = 0; i < residenceOffer.length; i++) {
            int val = residenceOffer[i];
            if (myRegion != null) {
                freeResidences[i] = new LinkedList<Residence>();
                occupiedResidences[i] = new LinkedList<Residence>();
                for (int j = 0; j < val; j++) {
                    freeResidences[i].add(new Residence(i));
                }
            }
            this.residencesOffer[i] = val;
        }
    }

    /**
     * Method to print out the state of the households composing the village
     */
    public String printHouseHoldsComposition() {
        StringBuilder buf = new StringBuilder(getMyHouseholds().size() * 100);
        buf.append(name).append("\n");
        for (Household hh : myHouseholds) {
            buf.append(hh.getResidence().getType()).append("\n");
            buf.append(hh);
        }
        return buf.toString();
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isOutside() {
        return name.toLowerCase().startsWith("outside");
    }

    /**
     * Method to suppress an household which will be concretely suppressed at
     * the end of the current iteration
     */
    public void suppressHousehold(Household hsehold, boolean die, boolean changeMunicipality) {
        // free the pattern and change availability
        if (die) {
            for (Individual ind : hsehold) {
                ind.leaveMyActivPattern();
            }
        }
        if (!hsehold.getResidence().isTransit())counters.incNbHouseholdSuppressed(1);
        hsehold.getMyVillage().releaseResidence(hsehold);
        hsehold.setJustSuppressed(true);
    }

    /**
     * Method to increase the occupation level of an activity occupied by a
     * resident S. Huet, 06.10.2009
     */
    public void increaseOccupiedActivityRes(int activ) {
        occupiedActivitiesByRes[activ]++;
    }

    /**
     * Method to decrease the occupation level of an activity occupied by a
     * resident S. Huet, 06.10.2009
     */
    public void decreaseOccupiedActivityRes(int activ) {
        occupiedActivitiesByRes[activ]--;
    }

    /**
     * Method to decrease the occupation level of an activity occupied by a
     * external individual S. Huet, 0.10.2009
     */
    public void increaseOccupiedActivityExt(int activ) {
        occupiedActivitiesByExt[activ]++;
    }

    /**
     * Method to decrease the occupation level of an activity occupied by a
     * external individual S. Huet, 07.10.2009
     */
    public void decreaseOccupiedActivityExt(int activ) {
        occupiedActivitiesByExt[activ]--;
    }

    /**
     * Method to indicate at a given moment the number of free activities in the
     * village S. Huet, 07.10.2009
     *
     * @param activ The index of the activity
     */
    public float getFreeActivities(int activ) {
        float free = 0.0f;
        if (getOccupiedActivitiesByOutside(activ) >= 0) {
            free = getOfferedActivities()[activ] - (getOccupiedActivitiesByExt()[activ] + getOccupiedActivitiesByRes()[activ] + getOccupiedActivitiesByOutside()[activ]);
        } else {
            free = getOfferedActivities()[activ] - (getOccupiedActivitiesByExt()[activ] + getOccupiedActivitiesByRes()[activ]);
        }
        return free;
    }

    /**
     * Method to indicate at a given moment the number of free activities in the
     * village
     */
    public float[] getFreeActivities() {
        float[] free = new float[myRegion.getNbActivities()];
        for (int i = 0; i < free.length; i++) {
            free[i] = getFreeActivities(i);
        }
        return free;
    }

    public float[] getOccupiedActivities() {
        float[] occupied = new float[myRegion.getNbActivities()];
        for (int i = 0; i < occupied.length; i++) {
            occupied[i] = occupiedActivitiesByExt[i] + occupiedActivitiesByOutside[i] + occupiedActivitiesByRes[i];
        }
        return occupied;
    }

    public void takeResidence(Household myHouseH) {
        Residence residence = this.freeResidences[myHouseH.getNextSizeOfResidence()].remove(0);
        myHouseH.setResidence(residence);
        occupiedResidences[myHouseH.getNextSizeOfResidence()].add(residence);
    }

    public void releaseResidence(Household myHouseH) {
        if (myHouseH.getResidence().isTransit()) {
            counters.incNbImmigrants(myHouseH.getSize());
            counters.incNbImmigrantsInAges(myHouseH);
            if (myHouseH.getAdults().size() == 1) {
                counters.incNbImmigrantsSingle(1);
            }
            for (Individual in : myHouseH.getCopyOfMembers()) {
                if (in.getAge() >= 60) {
                    counters.incNbImmigrants60(1);
                }
            }
        }
        releaseResidence(myHouseH, null);
    }

    /**
     * Release properly the residence in the municipality.
     *
     * @param myHouseH hh that quits his residence.
     * @param it iterator for safe removing. If null, the hh is removed directly
     * from the list.
     */
    public void releaseResidence(Household myHouseH, Iterator<Household> it) {
        boolean alreadySup = true;
        for (Household hh : myHouseH.getResidence()) {
            if (hh.getId() == myHouseH.getId()) {
                alreadySup = false;
                break;
            }
        }
        if (!alreadySup) {
            if (it == null) {
                myHouseH.getResidence().remove(myHouseH);
            } else {
                it.remove();
            }
            if (myHouseH.getResidence().getNbHhResidents() == 0) {
                if (myHouseH.getResidence().getType() >= 0) { // it is to exclude the transitory residence which can be put at free
                    myHouseH.getMyVillage().freeResidences[myHouseH.getResidence().getType()].add(myHouseH.getResidence());
                    myHouseH.getMyVillage().occupiedResidences[myHouseH.getResidence().getType()].remove(myHouseH.getResidence());
                }
            }
        }
    }

    public void move(Household myHouseH) {
        for (Individual ind : myHouseH) {
            ind.setLastMovingYear((short) myRegion.getCurrentYear());
            // check if an individual become a far commuter due to the move of her household
            if (ind.getStatus() == Status.WORKER && ind.getJobLocation() != this) {
                ind.workFar(this, ind.getJobLocation());
            }
        }
        // update the residence counters of each village, the quitted and the new one
        double dist = myHouseH.getMyVillage().euclidianDistanceFrom(myHouseH.getNextResMunicipality());
        releaseResidence(myHouseH);
        if (myHouseH.getNextResMunicipality() == myHouseH.getMyVillage()) { // the household only change of residence without changing municipality
            myHouseH.getMyVillage().takeResidence(myHouseH);
            if (!myHouseH.getResidence().isTransit()) {
                myHouseH.getMyVillage().getCounters().incNbHouseholdMoveInsideSameMun(1);
                myHouseH.getMyVillage().getCounters().incNbIndividualMoveInsideSameMun(myHouseH.size());
            }
            myHouseH.setNextSizeOfResidence(-1);
            myHouseH.setNextResMunicipality(null);
        } else { // the household change of residence and municipality
            myHouseH.getMyVillage().getCounters().incNbHouseholdMoveInside(1);
            myHouseH.getMyVillage().getCounters().incNbIndividualMoveInsideOtherMun(myHouseH.size(), dist);
            myHouseH.getMyVillage().getCounters().incNbHouseholdMoveOut(1);
            myHouseH.getMyVillage().getCounters().incNbIndividualMoveOut(myHouseH.size());
            // for unemployed member of household, the "unemployed" pattern is now counted in the new municipality, firstly we suppress him from the counter in the departure
            // municipality
            if (myHouseH.getNextResMunicipality() != getMyRegion().getOutsides()) {
                //myHouseH.getNextResMunicipality().getCounters().incNbHouseholdMoveIn(1);
                //myHouseH.getNextResMunicipality().getCounters().incNbIndividualMoveIn(myHouseH.size());
                ActivityPattern[] actpat = new ActivityPattern[myHouseH.size()];
                Municipality[] places = new Municipality[myHouseH.size()];
                int r = 0;
                for (Individual individual : myHouseH) {
                    if (individual.getStatus() == Status.UNEMPLOYED || individual.getStatus() == Status.WORKER) {
                        actpat[r] = individual.getMyActivityPattern();
                        if (individual.getStatus() == Status.WORKER) {
                            places[r] = individual.getJobLocation();
                        }
                        individual.leaveMyActivPattern();
                    }
                    r++;
                }
                r = 0;
                Household newHsh = new Household(myHouseH, false);
                newHsh.setMyVillage(myHouseH.getNextResMunicipality());
                myHouseH.getNextResMunicipality().getMyHouseholds().add(newHsh);
                newHsh.getNextResMunicipality().takeResidence(newHsh);
                newHsh.setNeedOfResidence(false);
                // secondly, we add him to the counter of the arrival municipality
                for (Individual individual : newHsh) {
                    if (individual.getStatus() == Status.UNEMPLOYED) {
                        individual.setJobLocation(newHsh.getMyVillage());
                        individual.takeAnActivPattern(actpat[r], individual.getJobLocation());
                    } else {
                        if (individual.getNextActPat() != null) {
                            individual.takeAnActivPattern(individual.getNextActPat(), individual.getJobLocation());
                        } else { // the individual is a worker and just change of municipality without changing its jobs, thus its activity pattern has to be counted in its new
                            // municipality
                            if (individual.getStatus() == Status.WORKER) {
                                individual.takeAnActivPattern(actpat[r], places[r]);
                            }
                        }
                    }
                    individual.setNextActPat(null);
                    r++;
                }
                //System.err.println(myHouseH.size()+" new "+newHsh.size()) ;
            }
            myHouseH.setJustSuppressed(true);
            //System.err.println("XXX "+myHouseH.size()) ;
        }
        myHouseH.resetLeader();
        myHouseH.setNeedOfResidence(false);
    }

    /**
     * Return the number of actives
     */
    public int getTotalActives() {
        int numActives = 0;
        for (Household hh : myHouseholds) {
            if (!hh.isJustSuppressed()) {
                for (Individual individual : hh) {
                    if (individual.getStatus() == Status.UNEMPLOYED || individual.getStatus() == Status.WORKER) {
                        numActives++;
                    }
                }
            }
        }
        return numActives;
    }

    /**
     * Method printing the vector available residence First version: S. Huet,
     * 28.07.2009
     */
    public String editAvailableRes() {
        StringBuilder buff = new StringBuilder("Residences Offer");
        buff.append(Arrays.toString(residencesOffer));
        buff.append(" - Free residences ");
        for (int i = 0; i < getResidencesTypeCount(); i++) {
            buff.append(" ").append(getFreeResidencesCount(i));
        }
        buff.append(" - Nb of households of the village without lodging: ");
        buff.append(counters.getNbOfDwellingLacking());
        buff.append(" nb of unsatisfied household by the residence: ");
        buff.append(getNbOfUnsatisfiedByRes());
        return buff.toString();
    }

    /**
     * Method to edit the demography S. Huet, 11.08.2009
     */
    public String editDemography(int time) {
        int numActives = getTotalActives();
        StringBuilder buff = new StringBuilder("Demography");
        buff.append(" size=").append(populationSize);
        buff.append(" actives=").append(numActives);
        buff.append(" death=").append(counters.getNumDeath());
        buff.append(" birth=").append(counters.getNumBirth());
        buff.append(" mov out=").append(counters.getNbIndividualMoveOut());
        buff.append(" mov in=").append(counters.getNbIndividualMoveIn());
        if (time != 0) {
            buff.append(" mov inside=").append(counters.getNbIndividualMoveInsideOtherMun());
        }
        buff.append(" HHout=").append(counters.getNbHouseholdMoveOut());
        buff.append(" HHin=").append(counters.getNbHouseholdMoveIn());
        if (time != 0) {
            buff.append(" HHinside=").append(counters.getNbHouseholdMoveInside());
            buff.append(" HHnew=").append(counters.getNewHouseholdNb());
        }
        buff.append(" HHsuppressed=").append(counters.getNbHouseholdSuppressed());
        buff.append(" HHexpulsed=").append(counters.getNbHouseholdExpulsed());
        buff.append(" HHtotal=").append(myHouseholds.size());
        return buff.toString();
    }

    /**
     * Method to edit pattern occupancy S. Huet, 11.08.2009
     */
    public String editPatternOccupancy() {
        int total = 0;
        StringBuilder buff = new StringBuilder("Pattern occupancy: ");
        for (int i = 0; i < patternOccupancy.length; i++) {
            buff.append(patternOccupancy[i]).append(" ");
            total = total + patternOccupancy[i];
        }
        buff.append("total: ").append(total);
        return buff.toString();
    }

    public int getTotalPatternOccupancy() {
        int total = 0;
        for (int i = 0; i < patternOccupancy.length; i++) {
            total = total + patternOccupancy[i];
        }
        return total;
    }

    /**
     * Method to print the results in files S. Huet, 11.08.2009
     */
    public void printVillageParameters(PrintStream ps, int time) {
        int i = 0;
        for (i = 0; i < getFicParameter().length; i++) {
            ps.print(getFicParameter()[i] + "\t");
        }
        ps.print(time + "\t");
    }

    /**
     * Method to edit various statistic on households: households details about
     * residence S. Huet, 12.08.2009
     */
    public void editHouseholdDetailResidence() {
        int i = 0;
        for (i = 0; i < getMyHouseholds().size(); i++) {
            System.err.println(" size flat " + getMyRegion().getCapacityOfAvailRes(getMyHouseholds().get(i).getResidence().getType()) + " size household "
                    + getMyHouseholds().get(i).size());
        }
    }

    /**
     * Method to edit various statistic on households: distribution of
     * households on household types Household types: correspond to the code
     * used by StartPopulation when it generates a population 0: single 1:
     * monoparenthal family 2: couple without children 3: couple with children
     * 4: complex household (other households) S. Huet, 13.08.2009
     */
    public String editHouseholdDetailTypes() {
        int i = 0;
        int j = 0;
        int nbOfTypes = 5;
        int[] hshTypes = new int[nbOfTypes];
        for (i = 0; i < getMyHouseholds().size(); i++) {
            for (j = 0; j < nbOfTypes; j++) {
                if (getMyHouseholds().get(i).getHshTypeCode() == j) {
                    hshTypes[j]++;
                    j = nbOfTypes;
                }
            }
        }
        StringBuilder buff = new StringBuilder("Distribution of hsh on hsh types\t");
        for (j = 0; j < nbOfTypes; j++) {
            buff.append(hshTypes[j]).append("\t");
        }
        return buff.toString();
    }

    /**
     * Method to edit various statistic on households: households size
     * distribution S. Huet, 12.08.2009
     */
    public String editHouseholdDetailSizes() {
        int i = 0;
        int j = 0;
        int maxBound = 30;
        int sizeMax = 0;
        int[] sizeTemp = new int[maxBound];
        Arrays.fill(sizeTemp, 0);
        for (i = 0; i < getMyHouseholds().size(); i++) {
            for (j = 0; j < maxBound; j++) {
                if (getMyHouseholds().get(i).size() == j + 1) {
                    sizeTemp[j]++;
                }
                if (getMyHouseholds().get(i).size() > sizeMax) {
                    sizeMax = getMyHouseholds().get(i).size();
                }
            }
        }
        int[] size = new int[sizeMax];
        StringBuilder buff = new StringBuilder("Distribution of household sizes:\t");
        for (i = 0; i < sizeMax; i++) {
            size[i] = sizeTemp[i];
            buff.append(size[i]).append("\t");
        }
        return buff.toString();
    }

    /**
     * Method to edit various statistic on individuals: individual age
     * distribution by step of 5 years from 0 to 90 and more S. Huet, 12.08.2009
     */
    public String editIndividualDetailAges(String shift) {
        int j = 0;
        int step = 5; // step of 5 years
        int max = 90; // age max considered for the last - 1 range; the last one
        // is higher than max
        int nbRange = (int) max / step + 1; // better if you are sure that the
        // result is already an int
        int[] ageDist = new int[nbRange];
        int[] indivStatus = new int[5];
        Arrays.fill(indivStatus, 0);
        int theOldest = 0;
        int bound = 0;
        for (Household hh : myHouseholds) {
            for (Individual ind : hh) {
                indivStatus[ind.getStatus().ordinal()]++;
                bound = 0;
                for (j = 0; j < nbRange; j++) {
                    bound = bound + step;
                    if (ind.getAge() < bound) {
                        ageDist[j]++;
                        j = nbRange;
                    } else {
                        if (j == nbRange - 1) {
                            ageDist[j]++;
                        }
                    }
                    if (ind.getAge() > theOldest) {
                        theOldest = ind.getAge();
                    }
                }
            }
        }
        StringBuilder buff = new StringBuilder(shift);
        buff.append("Distribution of individual ages by step: ");
        buff.append(step).append(" years\t");
        for (int i = 0; i < nbRange; i++) {
            buff.append(ageDist[i]).append("\t");
        }
        buff.append("The oldest is ").append(theOldest).append("\n");
        int totStat = 0;
        buff.append(shift).append("Distribution of individual status (in order student, unemployed, inactive, worker, retired:\t");
        for (int i = 0; i < indivStatus.length; i++) {
            buff.append(indivStatus[i]).append("\t");
            totStat = totStat + indivStatus[i];
        }
        buff.append("total ").append(totStat);
        return buff.toString();
    }

    /**
     * Method to edit the working state of individuals S. Huet, 12.08.2009
     */
    public String editIndividualWorkingDetail() {
        StringBuilder buff = new StringBuilder();
        for (Household hh : myHouseholds) {
            for (Individual ind : hh) {
                buff.append(ind.getStatus()).append(" ").append(ind.getJobLocation());
                buff.append(" ").append(ind.getMyActivityPattern()).append("\n");
            }
        }
        return buff.toString();
    }

    /**
     * Method to create by duplication the potentially migrant households
     * starting from a list of randomly chosen households in the population,
     * 3.11.2010 All the migrants are initialised with an unemployed status if
     * they are workers in the duplicate household
     */
    public void addTheMigrants(List<Household> households) {
        for (Household hh : households) {
            addMigrant(hh);
        }
    }

    public void addMigrant(Household hh) {
        //if (hh.getAdults().size()==0) System.err.println(hh) ;
        Household newHsh = new Household(hh, true);
        newHsh.setMyVillage(this);
        newHsh.createMigrants(hh);
        newHsh.setNeedOfResidence(true);
        getMyHouseholds().add(newHsh);
        //System.err.println(newHsh) ;
        for (Individual ind : newHsh) {
            if (ind.getAgeToDie() == ind.getAge()) {
                ind.initAgeToDie();
            }
            //System.err.println("id "+ind.getId()+" profession "+ind.getProfession()) ;
            if (ind.getAgeToEnterOnTheLabourMarket() == ind.getAge()) {
                ind.initAgeToEnterOnLabourMarket();
            }
            if (ind.getAgeToGoOnRetirement() == ind.getAge()) {
                ind.initAgeToGoOnRetirement();
            }
        }
    }

    @Override
    public int compareTo(Municipality mun) {
        return name.compareTo(mun.getName());
    }
}
