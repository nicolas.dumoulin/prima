# PRIMA Regional Model

You can play with the [demo software of the prima regional model](https://forgemia.inra.fr/nicolas.dumoulin/prima/-/raw/master/prima-regional-model-demo.zip?ref_type=heads&inline=false) developed for the European Project Prima, with the example of the Cantal case study (a French department).