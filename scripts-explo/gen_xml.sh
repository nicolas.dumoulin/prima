#!/bin/sh

NB_SIMUL_BY_JOB=30
INPUT=SMC.csv

CURRENT_DIR=$(basename $(pwd))
[ -e input ] && { echo "Supprimez d'abord le répertoire input"; exit 1; }
mkdir input
head -n 1 ${INPUT} > input/header.csv
tail -n +2 ${INPUT} > input/${CURRENT_DIR}.csv

split -d -l ${NB_SIMUL_BY_JOB} input/${CURRENT_DIR}.csv input/${CURRENT_DIR}_

for f in input/${CURRENT_DIR}_*; do
  cat input/header.csv > ${f}.csv
  cat ${f} >> ${f}.csv
  cat ../exploration.xml > ${f}.xml
  java -cp ~/Prima/prima-regional-model/target/prima-regional-model-1.0-SNAPSHOT-jar-with-dependencies.jar fr.cemagref.prima.regionalmodel.tools.GenerateExplorationFromSampledData ${f}.csv >> ${f}.xml
  echo "</Exploration>" >> ${f}.xml
  sed -i -e 's#<baseOutputDir>[^<]*#<baseOutputDir>../../dataOut/'$(basename ${f})'/#' ${f}.xml
done
