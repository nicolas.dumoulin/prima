#!/bin/sh

PREFIX=ExploPrima20121128
[ $# -eq 1 ] && PREFIX=$1 || read -p "Give the directories prefix: " PREFIX

[ -e ${PREFIX}_00 ] || {
  echo "The directory ${PREFIX}_00 hasn't been found"
  exit 1
}

echo "aggreg region.txt"
head -n 1 ${PREFIX}_00/1_region.txt > output_region.txt
for DIR in ${PREFIX}_*; do
  echo $DIR
  for f in ${DIR}/*_region.txt; do
    tail -n +2 $f >> output_region.txt
    echo -n "."
  done
  echo ""
done

zip -r output_region.txt.zip output_region.txt

exit

echo "aggreg munic.txt"

head -n 1 ${PREFIX}_00/1_munic.txt > output_munic.txt
for DIR in ${PREFIX}_*; do
  echo $DIR
  for f in ${DIR}/*_munic.txt; do
    tail -n +2 $f >> output_munic.txt
    echo -n "."
  done
  echo ""
done

zip -r output_munic.txt.zip output_munic.txt
