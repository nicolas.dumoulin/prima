#!/bin/sh

[ -e dataOut ] && { echo "Supprimez d'abord le répertoire dataOut"; exit 1; }
CURRENT_DIR=$(basename $(pwd))

for f in input/*.xml ; do
  PWD=$(pwd);
  bn=$(basename $f);
  [ -e ../../dataOut/${bn%%.xml} ] && { echo "Supprimez d'abord les répertoires ../../dataOut/${CURRENT_DIR}_*"; exit 1; }
  echo "cd ${PWD}; time /data1/dumoulin/jdk1.7.0_02/bin/java -cp ../../target/prima-regional-model-1.0-SNAPSHOT-jar-with-dependencies.jar fr.cemagref.prima.regionalmodel.Exploration ${f};" | \
    qsub -l nodes=1:ppn=1 -m n -N ${bn%%.xml} -j oe -o ./output_${bn%%.xml};
done

