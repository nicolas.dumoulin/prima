/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irstea.prima.mapobserver.styles;

import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import java.awt.Color;
import java.io.IOException;
import org.geotools.brewer.color.ColorBrewer;
import org.geotools.brewer.color.StyleGenerator;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.filter.function.Classifier;
import org.geotools.styling.FeatureTypeStyle;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.expression.Function;
import org.opengis.filter.expression.PropertyName;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class QuantileStyle implements MapStyle {

    private int nbGroups;
    private Classifier classifier;
    private Group[] groups;

    public QuantileStyle(int nbGroups) {
        this(nbGroups, null);
    }

    public QuantileStyle(int nbGroups, Classifier classifier) {
        this.nbGroups = nbGroups;
        this.classifier = classifier;
    }

    public Classifier getClassifier() {
        return classifier;
    }

    @Override
    public Group[] getGroups() {
        return groups;
    }

    @Override
    public Style createStyle(String fieldName, SimpleFeatureSource source) throws ProcessingException {
        try {
            StyleFactory styleFactory = CommonFactoryFinder.getStyleFactory();
            SimpleFeatureCollection featureCollection = source.getFeatures();
            // STEP 0 Set up Color Brewer
            ColorBrewer brewer = ColorBrewer.instance();
            // STEP 1 - call a classifier function to summarise your content
            FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
            PropertyName propertyExpression = ff.property(fieldName);
            //Function propteryExpression = ff.function("indicateurLive", ff.property("INSEE_COMM"), ff.literal(munData));
            // classify into five categories
            Function classify = ff.function("Quantile", propertyExpression, ff.literal(nbGroups));
            if (classifier == null) {
                classifier = (Classifier) classify.evaluate(featureCollection);
            }
            // STEP 2 - look up a predefined palette from color brewer
            String paletteName = "BuGn";
            Color[] colors = brewer.getPalette(paletteName).getColors(classifier.getSize());
            groups = Group.buildGroupsFromClassifier(classifier, colors);
            // STEP 3 - ask StyleGenerator to make a set of rules for the Classifier
            // assigning features the correct color based on height
            FeatureTypeStyle fts = StyleGenerator.createFeatureTypeStyle(
                    classifier,
                    propertyExpression,
                    colors,
                    "Generated FeatureTypeStyle",
                    featureCollection.getSchema().getGeometryDescriptor(),
                    StyleGenerator.ELSEMODE_IGNORE,
                    0.95,
                    null);
            Style style = styleFactory.createStyle();
            style.featureTypeStyles().add(fts);
            return style;
        } catch (IOException ex) {
            throw new ProcessingException(ex);
        }
    }
}
