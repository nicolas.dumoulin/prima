package fr.irstea.prima.mapobserver;

import fr.irstea.prima.mapobserver.styles.Group;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Rectangle2D.Double;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class LegendPanel extends JPanel {

    private double factor = 1;
    private TextLayout titleLayout;
    private String title;
    private Group[] groups;
    private TextLayout[] labelsLayouts;
    private int width = 100;
    private int rowHeight = 20;
    private int rowsMargin = 10;

    public LegendPanel(String title, Group[] groups) {
        this.title = title;
        this.groups = groups;
        setPreferredSize(new Dimension(150, 200));
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        if (titleLayout == null) {
            titleLayout = new TextLayout(title, g2d.getFont(), g2d.getFontRenderContext());
        }
        titleLayout.draw(g2d, (int) (width - titleLayout.getBounds().getWidth()) / 2, 15);
        // lazy load labels
        if (labelsLayouts == null) {
            labelsLayouts = new TextLayout[groups.length];
            for (int i = 0; i < groups.length; i++) {
                labelsLayouts[i] = new TextLayout(groups[i].getLabel(), g2d.getFont(), g2d.getFontRenderContext());
            }
        }
        // draw colours and labels
        for (int i = 0; i < groups.length; i++) {
            g2d.setColor(groups[i].getColor());
            Double colourRectangle = new Rectangle2D.Double(2, (i+1)*(rowHeight+rowsMargin), 30, rowHeight);
            g2d.fill(colourRectangle);
            g2d.setColor(Color.BLACK);
            g2d.draw(colourRectangle);
            labelsLayouts[i].draw(g2d,40,30 + i * 30+(int)(rowHeight+labelsLayouts[i].getBounds().getHeight())/2);
        }
    }

    public static void main(String[] args) {
        JFrame f = new JFrame("test");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //f.add(new LegendPanel("Test", new Color[]{Color.BLUE, Color.GREEN}, new String[]{"Bleu", "Vert"}), BorderLayout.CENTER);
        f.add(new LegendPanel("Test", new Group[]{new Group(Color.BLUE, "Bleu"), new Group(Color.GREEN, "Vert")}), BorderLayout.CENTER);
        f.pack();
        f.setVisible(true);
    }
}
