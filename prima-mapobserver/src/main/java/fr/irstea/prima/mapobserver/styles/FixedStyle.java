/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irstea.prima.mapobserver.styles;

import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import java.awt.Color;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.geotools.brewer.color.StyleGenerator;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.filter.function.Classifier;
import org.geotools.filter.function.ExplicitClassifier;
import org.geotools.styling.FeatureTypeStyle;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.expression.PropertyName;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class FixedStyle implements MapStyle {

    private Classifier classifier;
    private Group[] groups;

    @Override
    public Group[] getGroups() {
        return groups;
    }

    @Override
    public Style createStyle(String fieldName, SimpleFeatureSource source) throws ProcessingException {
        try {
            StyleFactory styleFactory = CommonFactoryFinder.getStyleFactory();
            SimpleFeatureCollection featureCollection = source.getFeatures();
            FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2();
            PropertyName propertyExpression = ff.property(fieldName);
            classifier = new ExplicitClassifier(new Set[]{
                        new HashSet(Arrays.asList(new Integer[]{-1})),
                        new HashSet(Arrays.asList(new Integer[]{1}))
                    });
            classifier.setTitle(0, "Decrease");
            classifier.setTitle(1, "Increase");
            Color[] colors = new Color[]{Color.RED, Color.BLUE};
            groups = Group.buildGroupsFromClassifier(classifier, colors);
            FeatureTypeStyle fts = StyleGenerator.createFeatureTypeStyle(
                    classifier,
                    propertyExpression, colors,
                    "Generated FeatureTypeStyle",
                    featureCollection.getSchema().getGeometryDescriptor(),
                    StyleGenerator.ELSEMODE_IGNORE,
                    0.95,
                    null);
            Style style = styleFactory.createStyle();
            style.featureTypeStyles().add(fts);
            return style;
        } catch (IOException ex) {
            throw new ProcessingException(ex);
        }
    }
}
