/*
 * Copyright (C) 2012 Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.prima.mapobserver;

import fr.cemagref.observation.gui.Drawable;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalityCounters;
import fr.cemagref.prima.regionalmodel.MunicipalitySet;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.irstea.prima.mapobserver.styles.QuantileStyle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.geotools.filter.function.Classifier;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class MunicipalitiesMap extends ObserverListener implements Drawable {

    private File shpFile;
    private Integer nbGroups;
    private String observable;
    private List<Integer> dates;
    private transient ObservablesHandler.ObservableFetcher fetcher;
    private transient MunicipalitiesMapPanel panel;
    private transient Classifier groups;
    private transient static final Logger LOGGER = Logger.getLogger(MunicipalitiesMap.class.getName());

    @Override
    public void addObservable(ObservablesHandler classObservable) {
    }

    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long date) {
        if (dates == null || dates.contains((int) date)) {
            if (!(instance instanceof MunicipalitySet)) {
                LOGGER.log(Level.SEVERE, "This observer can only receive MunicipalitySet instance");
            } else {
                if (fetcher == null) {
                    fetcher = ((MunicipalitySet) instance).getMyApplication().getObservableManager().addObservable(MunicipalityCounters.class).getObservableFetcherByName(observable);
                }
                Map<String, Integer> munData = new HashMap<String, Integer>();
                for (Municipality mun : ((MunicipalitySet) instance).getMyMunicipalities()) {
                    try {
                        munData.put(mun.getName(), (Integer) fetcher.fetchValue(mun.getCounters()));
                    } catch (Exception ex) {
                        LOGGER.log(Level.SEVERE, "", ex);
                    }
                }
                try {
                    IndicatorMap quantileMap = new IndicatorMap(shpFile, fetcher.getDescription(), munData);
                    QuantileStyle quantileStyle = new QuantileStyle(nbGroups, groups);
                    JComponent mapPane = quantileMap.getMapPane(quantileStyle,800, 800);
                    if (groups == null) {
                        groups = quantileStyle.getClassifier();
                    }
                    panel.addMap(mapPane, (int) date);
                } catch (ProcessingException ex) {
                    LOGGER.log(Level.SEVERE, "", ex);
                }
            }
        }
    }

    @Override
    public void init() {
        panel = new MunicipalitiesMapPanel();
        if (nbGroups == null) {
            nbGroups = 6;
        }
    }

    @Override
    public void close() {
    }

    public String getTitle() {
        return "Map for " + observable;
    }

    public JComponent getDisplay() {
        return panel;
    }
}
