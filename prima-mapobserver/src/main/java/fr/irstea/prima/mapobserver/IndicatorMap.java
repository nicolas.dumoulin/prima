/*
 *  Copyright (C) 2012 Irstea
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.prima.mapobserver;

import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.irstea.prima.mapobserver.styles.MapStyle;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.apache.commons.io.FileUtils;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.shapefile.dbf.DbaseFileReader;
import org.geotools.data.shapefile.dbf.DbaseFileWriter;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.renderer.lite.StreamingRenderer;
import org.geotools.swing.JMapPane;

/**
 * This class can be used for displaying a map of a set of municipalities with a an indicator for each municipality.
 * The class will need a shapefile for the boundaries of the municipalities.
 * The dbf file should be modified by appending a column "indicateur" (type integer) that will store the indicator value for each municipality.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class IndicatorMap {

    private Map<String, Integer> munData;
    private File shpFile;
    private String basename;
    private String dataLabel;

    public IndicatorMap(File shapefile, String dataLabel, Map<String, Integer> munData) throws ProcessingException {
        this.shpFile = shapefile;
        basename = shapefile.getAbsolutePath().substring(0, shapefile.getAbsolutePath().lastIndexOf(".shp"));
        this.dataLabel = dataLabel;
        this.munData = munData;
    }

    private void prepareData() throws ProcessingException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(basename + ".dbf");
            DbaseFileReader dbfReader = new DbaseFileReader(fis.getChannel(), false, Charset.forName("ISO-8859-1"));
            FileOutputStream fos = new FileOutputStream(basename + ".2.dbf");
            DbaseFileWriter dbfWriter = new DbaseFileWriter(dbfReader.getHeader(), fos.getChannel(), Charset.forName("ISO-8859-1"));
            while (dbfReader.hasNext()) {
                final Object[] fields = dbfReader.readEntry();
                fields[fields.length - 1] = munData.get(fields[2]);
                dbfWriter.write(fields);
            }
            dbfReader.close();
            fos.close();
            new File(basename + ".dbf.old").delete();
            FileUtils.moveFile(new File(basename + ".dbf"), new File(basename + ".dbf.old"));
            FileUtils.moveFile(new File(basename + ".2.dbf"), new File(basename + ".dbf"));
        } catch (IOException ex) {
            throw new ProcessingException(ex);
        }
    }

    public MapContent getMap(MapStyle style) throws ProcessingException {
        prepareData();
        try {
            FileDataStore store = FileDataStoreFinder.getDataStore(shpFile);
            SimpleFeatureSource source = store.getFeatureSource();
            //Style style = createStyle(source, "indicateur");
            Layer layer = new FeatureLayer(source, style.createStyle("indicateur", source));
            MapContent map = new MapContent();
            map.addLayer(layer);
            return map;
        } catch (IOException ex) {
            throw new ProcessingException(ex);
        }
    }

    public JComponent getMapPane(MapStyle style, int width, int height) throws ProcessingException {
        MapContent map = getMap(style);
        JPanel panel = new JPanel(new BorderLayout());
        JMapPane jMapPane = new JMapPane(map);
        jMapPane.setPreferredSize(new Dimension(width, height));
        panel.add(jMapPane, BorderLayout.CENTER);
        LegendPanel legendPanel = new LegendPanel(dataLabel, style.getGroups());
        panel.add(legendPanel, BorderLayout.WEST);
        return panel;
    }

    public static void toPNG(MapContent map, int width, int height, File pngOut) throws ProcessingException {
        Rectangle paintArea = new Rectangle(800, 800);
        ReferencedEnvelope mapArea = map.getViewport().getBounds();
        StreamingRenderer renderer = new StreamingRenderer();
        renderer.setMapContent(map);
        RenderingHints hints = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        renderer.setJava2DHints(hints);
        Map rendererParams = new HashMap();
        rendererParams.put("optimizedDataLoadingEnabled", true);
        renderer.setRendererHints(rendererParams);
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = bufferedImage.createGraphics();
        renderer.paint(g2d, paintArea, mapArea);
        g2d.dispose();
        try {
            ImageIO.write(bufferedImage, "png", pngOut);
        } catch (IOException ex) {
            throw new ProcessingException(ex);
        }
    }
}
