package fr.irstea.prima.mapobserver.styles;

import java.awt.Color;
import org.geotools.filter.function.Classifier;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class Group {

    private Color color;
    private String label;

    public Group(Color color, String label) {
        this.color = color;
        this.label = label;
    }
    
    public static Group[] buildGroupsFromClassifier(Classifier classifier, Color[] colours) {
        Group[] groups = new Group[classifier.getTitles().length];
        for (int i = 0; i < groups.length; i++) {
            groups[i] = new Group(colours[i], classifier.getTitles()[i]);
        }
        return groups;
    }

    public Color getColor() {
        return color;
    }

    public String getLabel() {
        return label;
    }
}
