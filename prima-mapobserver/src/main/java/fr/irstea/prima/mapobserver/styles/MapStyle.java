/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.irstea.prima.mapobserver.styles;

import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.styling.Style;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public interface MapStyle {

    public Group[] getGroups();
    
    public Style createStyle(String fieldName, SimpleFeatureSource source) throws ProcessingException;
}
