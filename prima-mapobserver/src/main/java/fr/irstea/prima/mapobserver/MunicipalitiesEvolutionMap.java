/*
 * Copyright (C) 2012 Irstea
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.irstea.prima.mapobserver;

import fr.cemagref.observation.gui.Drawable;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.prima.regionalmodel.Municipality;
import fr.cemagref.prima.regionalmodel.MunicipalityCounters;
import fr.cemagref.prima.regionalmodel.MunicipalitySetCounters;
import fr.cemagref.prima.regionalmodel.tools.ProcessingException;
import fr.irstea.prima.mapobserver.styles.FixedStyle;
import java.awt.BorderLayout;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JComponent;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@irstea.fr>
 */
public class MunicipalitiesEvolutionMap extends ObserverListener implements Drawable {

    private File shpFile;
    private Integer nbGroups;
    private String observable;
    private int start;
    private int end;
    private transient ObservablesHandler.ObservableFetcher fetcher;
    private transient JPanel panel;
    private transient Map<String, Integer> munData;
    private transient static final Logger LOGGER = Logger.getLogger(MunicipalitiesMap.class.getName());

    @Override
    public void addObservable(ObservablesHandler classObservable) {
    }

    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long date) {
        if (date == start || date == end) {
            if (!(instance instanceof MunicipalitySetCounters)) {
                LOGGER.log(Level.SEVERE, "This observer can only receive MunicipalitySetCounters instance");
            } else {
                if (fetcher == null) {
                    fetcher = ((MunicipalitySetCounters) instance).getMunicipalitySet().getMyApplication().getObservableManager().addObservable(MunicipalityCounters.class).getObservableFetcherByName(observable);
                }
                if (date == start) {
                    munData = new HashMap<String, Integer>();
                }
                for (Municipality mun : ((MunicipalitySetCounters) instance).getMunicipalitySet().getMyMunicipalities()) {
                    try {
                        if (date == start) {
                            munData.put(mun.getName(), (Integer) fetcher.fetchValue(mun.getCounters()));
                        } else {
                            int diff = (Integer) fetcher.fetchValue(mun.getCounters()) - munData.get(mun.getName());
                            munData.put(mun.getName(), diff > 0 ? 1 : -1);
                        }
                    } catch (Exception ex) {
                        LOGGER.log(Level.SEVERE, "", ex);
                    }
                }
                if (date == end) {
                    try {
                        IndicatorMap quantileMap = new IndicatorMap(shpFile, fetcher.getDescription(), munData);
                        JComponent mapPane = quantileMap.getMapPane(new FixedStyle(), 800, 800);
                        panel.removeAll();
                        panel.add(mapPane, BorderLayout.CENTER);
                        panel.revalidate();
                    } catch (ProcessingException ex) {
                        LOGGER.log(Level.SEVERE, "", ex);
                    }
                }
            }
        }
    }

    @Override
    public void init() {
        panel = new JPanel();
        panel.add(new JLabel("Waiting for data… (until " + end + ")"), BorderLayout.CENTER);
        if (nbGroups == null) {
            nbGroups = 6;
        }
    }

    @Override
    public void close() {
    }

    public String getTitle() {
        return "Map for " + observable + " " + start + "-" + end;
    }

    public JComponent getDisplay() {
        return panel;
    }
}
